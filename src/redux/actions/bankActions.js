import axios from '../../axiosConfig.js';
import { BANK } from '../constants/action-types';
import { getCookie } from './authActions'
import Swal from 'sweetalert2'


export const fetchBegin = () => ({
    type: BANK.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: BANK.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: BANK.FETCH_FAILURE,
    error
})



export function fetchBanks(data) {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        return axios.get(`/v1/tenant/ifsc/${data}`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data.data))
                console.log(res)
            })
            .catch(err => {
                dispatch(fetchFailure(err))
                console.log(err)
            })
    }
}

