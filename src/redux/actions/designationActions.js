import axios from '../../axiosConfig.js';
import { DESIGNATION } from '../constants/action-types';
import { getCookie } from './authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: DESIGNATION.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: DESIGNATION.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: DESIGNATION.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: DESIGNATION.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: DESIGNATION.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: DESIGNATION.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: DESIGNATION.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: DESIGNATION.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: DESIGNATION.DELETE_FAILURE,
    error
})


export function saveDesignation(data) {
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            designationName: data.name,
            shortCode: data.shortCode,
            department: {
                id: data.department
            }
        }
        return axios.post(`/v1/tenant/designation`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchDesignations())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchDesignations() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/tenant/designation`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function deleteDesignations(data) {
    return dispatch => {

        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                designation_ids: data
            }
        }
        return axios.delete(`/v1/tenant/designation`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchDesignations())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err)
                Swal.fire({
                    title: 'Error!',
                    text: 'test',
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

