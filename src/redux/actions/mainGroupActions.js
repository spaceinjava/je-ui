import axios from '../../axiosConfig.js';
import { MAIN_GROUP } from '../constants/action-types';
import { getCookie } from './authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: MAIN_GROUP.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: MAIN_GROUP.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: MAIN_GROUP.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: MAIN_GROUP.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: MAIN_GROUP.FETCH_SUCCESS,
    data
})

export const fetchProductsByMainGroupSuccess = (data) => ({
    type: MAIN_GROUP.FETCH_PRODUCT_BY_MAIN_GROUP_SUCCESS,
    data
})

export const fetchPuritiesByMainGroupSuccess = (data) => ({
    type: MAIN_GROUP.FETCH_PURITY_BY_MAIN_GROUP_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: MAIN_GROUP.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: MAIN_GROUP.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: MAIN_GROUP.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: MAIN_GROUP.DELETE_FAILURE,
    error
})

export function saveMainGroup(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            groupName: data.groupName,
            shortCode: data.shortCode
        }
        return axios.post(`/v1/products/main-group`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchMainGroups())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchMainGroups() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/main-group`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function fetchProductsByMainGroup(mainGroupId) {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return mainGroupId ? axios.get(`/v1/products/main-group/${mainGroupId}/products`, config)
            .then(res => {
                dispatch(fetchProductsByMainGroupSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
        : [];
    }
}

export function fetchPuritiesByMainGroup(mainGroupId) {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return mainGroupId ? axios.get(`/v1/products/main-group/${mainGroupId}/purity`, config)
            .then(res => {
                dispatch(fetchPuritiesByMainGroupSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
        : [];
    }
}

export function deleteMainGroup(data) {
    return dispatch => {

        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                main_group_ids: data
            }
        }
        return axios.delete(`/v1/products/main-group`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchMainGroups())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err)
                Swal.fire({
                    title: 'Error!',
                    text: 'test',
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

