import axios from '../../../axiosConfig.js';
import { SUPPLIER } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: SUPPLIER.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: SUPPLIER.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: SUPPLIER.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: SUPPLIER.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: SUPPLIER.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: SUPPLIER.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: SUPPLIER.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: SUPPLIER.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: SUPPLIER.DELETE_FAILURE,
    error
})

export function saveSupplier(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            partyTypeId: data.partyTypeId,
            partyType: data.partyType,
            shortCode: data.shortCode,
            partyName: data.partyName,
            email: data.email,
            contactNum: data.contactNum,
            gstNum: data.gstNum,
            hallmarkChargesPerPc: data.hallmarkChargesPerPc,
            isSmith: data.isSmith,
            isBranded: data.isBranded,
            isGemStone: data.isGemStone,
            isTestingDealer: data.isTestingDealer,
            logoUrl: data.logoUrl,
            address: {
                id: data.addressId,
                isDeleted: false,
                addressLine1: data.addressLine1,
                addressLine2: data.addressLine2,
                area: data.area,
                city: data.city,
                district: data.district,
                state: data.state,
                pincode: data.pincode,
            },
            bankDetails: data.bankDetails,
            supplierDetails: data.supplierDetails
        }
        let formData = new FormData();
        formData.append("bean", JSON.stringify(body));
        formData.append("image", data.logo)
        return axios.post(`/v1/tenant/supplier`, formData, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchSuppliers())
                Swal.fire({
                    position: "top-end",
                    title: 'Supplier saved successfully.!',
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 2000
                })
            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchSuppliers() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/tenant/supplier`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function deleteSuppliers(data) {
    return dispatch => {
        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                supplier_ids: data
            }
        }
        return axios.delete(`/v1/tenant/supplier`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchSuppliers())
                Swal.fire({
                    position: "top-end",
                    title: 'Success.!',
                    text: res.data.message,
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 2000
                })
            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err.response)
                if (err.response) {
                    Swal.fire({
                        title: 'Error!',
                        text: err.response.data.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    })
                }
            })
    }
}

