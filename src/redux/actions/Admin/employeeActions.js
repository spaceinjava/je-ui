import axios from '../../../axiosConfig.js';
import { EMPLOYEES } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: EMPLOYEES.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: EMPLOYEES.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: EMPLOYEES.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: EMPLOYEES.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: EMPLOYEES.FETCH_SUCCESS,
    data
})


export const fetchFailure = (error) => ({
    type: EMPLOYEES.FETCH_FAILURE,
    error
})

export const fetchListBegin = () => ({
    type: EMPLOYEES.LIST_FETCH_BEGIN
})

export const fetchListSuccess = (data) => ({
    type: EMPLOYEES.LIST_FETCH_SUCCESS,
    data
})

export const fetchListFailure = (error) => ({
    type: EMPLOYEES.LIST_FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: EMPLOYEES.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: EMPLOYEES.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: EMPLOYEES.DELETE_FAILURE,
    error
})

export function saveEmployee(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken"),
            }
        }
        let body = {
            isDeleted: false,
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            generateId: data.generateId,
            employeeId: data.employeeId,
            firstName: data.firstName,
            middleName: data.middleName,
            lastName: data.lastName,
            gender: data.gender,
            email: data.email,
            mobile: data.contactNumber,
            branch: {
                id: data.branchName,
            },
            designation: {
                id: data.designation,
            },
            dob: data.dob,
            emergencyContactNum: data.emgContactNumber,
            aadharNum: data.aadharNo,
            panNum: data.panNumber,
            address: {
                isDeleted: false,
                id: data.addressId,
                addressLine1: data.addressLine1,
                addressLine2: data.addressLine2,
                area: data.area,
                city: data.city,
                district: data.district,
                state: data.state,
                pincode: data.pincode,
                
            },
            salary: data.salary,
            photoUrl: data.photoUrl,
            bankDetails: {
                isDeleted: false,
                id: data.bankDetailsId,
                bankBranch: data.bankBranch,
                bankName: data.bankName,
                accountNum: data.accountNumber,
                accountHolderName: data.accountHolderName,
                ifscCode: data.ifscCode
            },
            passcode: "Spaceinje@123",
            loginEnabled: data.loginEnabled
        }
        const formdata = new FormData();
        formdata.append('bean', JSON.stringify(body))
        formdata.append('image', data.pic)
        return axios.post(`/v1/tenant/employee`, formdata, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchEmployees())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })
            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchEmployees() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/tenant/employee`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function fetchEmployeesList() {
    return dispatch => {
        dispatch(fetchListBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/tenant/employee`, config)
            .then(res => {
                dispatch(fetchListSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchListFailure(err))
            })
    }
}


export function deleteEmployees(data) {
    return dispatch => {

        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                employee_ids: data
            }
        }
        return axios.delete(`/v1/tenant/employee`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchEmployees())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err.response)
                if (err.response) {
                    Swal.fire({
                        title: 'Error!',
                        text: err.response.data.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    })
                }
            })
    }
}

