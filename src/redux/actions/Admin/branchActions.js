import axios from '../../../axiosConfig.js';
import { BRANCH } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: BRANCH.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: BRANCH.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: BRANCH.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: BRANCH.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: BRANCH.FETCH_SUCCESS,
    data
})


export const fetchFailure = (error) => ({
    type: BRANCH.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: BRANCH.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: BRANCH.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: BRANCH.DELETE_FAILURE,
    error
})

export function saveBranch(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken"),
            }
        }

        let bankDetails = data.BankList.map(bank => {
            return {
                  isDeleted: false,
                  id: bank.id.value,
                  bankBranch: bank.bankBranch.value,
                  bankName: bank.bankName.value,
                  accountNum: bank.accountNum.value,
                  accountHolderName: bank.accountHolderName.value,
                  ifscCode: bank.ifscCode.value
                }
        });

        let requestBody = {
            isDeleted: false,
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            branchName: data.branchName,
            shortCode: data.shortCode,
            email: data.email,
            contactNum: data.contactNum,
            alternateContactNum: data.alternateContactNum,
            gstNo: data.gstNo,
            address: {
                isDeleted: false,
                id: data.addressId,
                addressLine1: data.addressLine1,
                addressLine2: data.addressLine2,
                area: data.area,
                city: data.city,
                district: data.district,
                state: data.state,
                pincode: data.pincode
            },
            bankDetails: bankDetails,
            isHeadBranch: data.isHeadBranch
        }

        console.log(requestBody)

        return axios.post(`/v1/tenant/branch`, requestBody, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchBranches())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchBranches() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/tenant/branch`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}


export function deleteBranches(data) {
    return dispatch => {

        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                branch_ids: data
            }
        }
        return axios.delete(`/v1/tenant/branch`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchBranches())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err.response)
                if (err.response) {
                    Swal.fire({
                        title: 'Error!',
                        text: err.response.data.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    })
                }
            })
    }
}

