import axios from '../../axiosConfig.js';
import postalAxios from '../../customAxios'
import { ADDRESS } from '../constants/action-types';
import { getCookie } from './authActions'
import Swal from 'sweetalert2'


export const fetchBegin = () => ({
    type: ADDRESS.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: ADDRESS.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: ADDRESS.FETCH_FAILURE,
    error
})



export function fetchAddress(data) {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/tenant/postal/${data}`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data.data))
                console.log(res)
            })
            .catch(err => {
                dispatch(fetchFailure(err))
                console.log(err)
            })
    }
}

