import axios from '../../../axiosConfig.js';
import { STONEDETAILS } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: STONEDETAILS.SAVE_BEGIN
});

export const saveSuccess = (data, stoneDetailsData) => ({
    type: STONEDETAILS.SAVE_SUCCESS,
    data,
    stoneDetailsData
});

export const saveStoneDetails = (data) => ({
    type: STONEDETAILS.SAVE_STONE_SUCCESS,
    data
})

export const saveFailure = error => ({
    type: STONEDETAILS.SAVE_FAILURE,
    error
})

export const editBegin = () => ({
    type: STONEDETAILS.EDIT_BEGIN
});

export const editSuccess = (id, data) => ({
    type: STONEDETAILS.EDIT_SUCCESS,
    id,
    data,
});

export const editFailure = error => ({
    type: STONEDETAILS.EDIT_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: STONEDETAILS.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: STONEDETAILS.FETCH_SUCCESS,
    data
})

export const fetchByMainGroupSuccess = (data) => ({
    type: STONEDETAILS.FETCH_BY_MAIN_GROUP_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: STONEDETAILS.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: STONEDETAILS.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: STONEDETAILS.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: STONEDETAILS.DELETE_FAILURE,
    error
})

export const clearDetails = () => ({
    type: STONEDETAILS.ClEAR_DETAILS
})

export const saveStones = (data, weight) => ({
    type: STONEDETAILS.SAVE_STONE_DETAILS,
    data,
    weight
})

export function saveDetails(data, stoneDetailsData) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        dispatch(saveSuccess(data, stoneDetailsData))
    }
}

export function saveStoneDetailsForm(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveStoneDetails(data))
    }
}

export function saveStoneWeight(data) {

    let weight = data.filter(({isNoWeight}) => isNoWeight === false).reduce((prev, current) => {
            return parseInt(prev) +  parseInt(current.weight)
    }, 0);
    console.log(weight)

    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        dispatch(saveStones(data, weight))
    }
}

export function clearStoneDetails() {
    return dispatch => {
        dispatch(clearDetails())
    }
}

export function editDetails(data) {
    console.log(data.id)
    return dispatch => {
        dispatch(editBegin())
        dispatch(editSuccess(data.id, data))
    }
}


export function deleteDetails(data) {
    return dispatch => {
        dispatch(deleteBegin())
        dispatch(deleteSuccess(data))

    }
}

