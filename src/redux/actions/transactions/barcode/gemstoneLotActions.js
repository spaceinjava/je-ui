import axios from '../../../../axiosConfig.js';
import { GEMSTONE_LOT } from '../../../constants/action-types';
import { getCookie } from '../../authActions'
import Swal from 'sweetalert2'
import { formatDate } from "../../../../utils/util";

export const saveBegin = () => ({
    type: GEMSTONE_LOT.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: GEMSTONE_LOT.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: GEMSTONE_LOT.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: GEMSTONE_LOT.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: GEMSTONE_LOT.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: GEMSTONE_LOT.FETCH_FAILURE,
    error
})

export const fetchDetailsBegin = () => ({
    type: GEMSTONE_LOT.FETCH_DETAILS_BEGIN
})

export const fetchDetailsSuccess = (data) => ({
    type: GEMSTONE_LOT.FETCH_DETAILS_SUCCESS,
    data
})

export const fetchDetailsFailure = (error) => ({
    type: GEMSTONE_LOT.FETCH_DETAILS_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: GEMSTONE_LOT.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: GEMSTONE_LOT.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: GEMSTONE_LOT.DELETE_FAILURE,
    error
})

export function fetchGemstoneLotInvoices(supplierId, purchaseType, invoiceDate) {
    let url = ``;
    url = `/v1/products/journal/lot-creation/rc/GEMSTONE?supplier_Id=${supplierId}&from_date=${formatDate(invoiceDate[0])}&to_date=${formatDate(invoiceDate[1])}&purchaseType=${purchaseType}`;
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(url, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function fetchGemstoneLotDetails(invcNo) {
    let url = ``;
    url = `/v1/products/journal/lot-creation/GEMSTONE?rc_no=${invcNo}`;
    return dispatch => {
        dispatch(fetchDetailsBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(url, config)
            .then(res => {
                dispatch(fetchDetailsSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchDetailsFailure(err))
            })
    }
}

export function saveGemstoneLot(data) {
    let stoneDetails = data ? data.map(stone => {
        return {
            tenantCode: getCookie("tenantCode"),
            id: stone.id,
            isDeleted: false,
            purchaseType: stone.purchaseType,
            receiptNo: stone.receiptNo,
            purchaseDate: stone.purchaseDate,
            partyId: stone.partyId,
            lotNo: stone.partyId,
            stone: {
                id: stone.subCategoryId,
            },
            stoneSize: (stone.stoneSizeId ? {
                id: stone.stoneSizeId,
            } : null),
            stoneColor: (stone.stoneColorId ? {
                id: stone.stoneColorId,
            } : null),
            stoneClarity: (stone.stoneClarityId ? {
                id: stone.stoneClarityId,
            } : null),
            stonePolish: (stone.stonePolishId ? {
                id: stone.stonePolishId,
            } : null),
            uom: stone.uom,
            quantity: stone.quantity,
            weightPerGram: stone.weightPerGram,
            weightPerKarat: stone.weightPerKarat,
            remarks: stone.remarks,
            purchaseId: stone.purchaseId,
        }
    }) : [];
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        return axios.post(`/v1/products/journal/lot-creation/stone`, stoneDetails, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}