import axios from '../../../../axiosConfig';
import { METAL_BARCODE } from '../../../constants/action-types';
import { getCookie } from '../../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: METAL_BARCODE.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: METAL_BARCODE.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: METAL_BARCODE.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: METAL_BARCODE.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: METAL_BARCODE.FETCH_SUCCESS,
    data
})


export const fetchFailure = (error) => ({
    type: METAL_BARCODE.FETCH_FAILURE,
    error
})

export const fetchLotBegin = () => ({
    type: METAL_BARCODE.LOT_FETCH_BEGIN,
})

export const fetchLottSuccess = (data) => ({
    type: METAL_BARCODE.LOT_FETCH_SUCCESS,
    data
})

export const fetchLotFailure = (error) => ({
    type: METAL_BARCODE.LOT_FETCH_FAILURE,
    error
})


export function saveMetalBarcode(data) {
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken"),
            }
        }
        let body = {
            isDeleted: false,
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            generateId: data.generateId,
            employeeId: data.employeeId,
            firstName: data.firstName,
            middleName: data.middleName,
            lastName: data.lastName,
            gender: data.gender,
            email: data.email,
            mobile: data.contactNumber,
            branch: {
                id: data.branchName,
            },
            designation: {
                id: data.designation,
            },
            dob: data.dob,
            emergencyContactNum: data.emgContactNumber,
            aadharNum: data.aadharNo,
            panNum: data.panNumber,
            address: {
                isDeleted: false,
                id: data.addressId,
                addressLine1: data.addressLine1,
                addressLine2: data.addressLine2,
                area: data.area,
                city: data.city,
                district: data.district,
                state: data.state,
                pincode: data.pincode,
                
            },
            salary: data.salary,
            photoUrl: data.photoUrl,
            bankDetails: {
                isDeleted: false,
                id: data.bankDetailsId,
                bankBranch: data.bankBranch,
                bankName: data.bankName,
                accountNum: data.accountNumber,
                accountHolderName: data.accountHolderName,
                ifscCode: data.ifscCode
            },
            passcode: "Spaceinje@123",
            loginEnabled: data.loginEnabled
        }
        const formdata = new FormData();
        formdata.append('bean', JSON.stringify(body))
        formdata.append('image', data.pic)
        return axios.post(`/v1/tenant/employee`, formdata, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                // dispatch(fetchEmployees())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })
            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchLots(partyId, mainGroupId, purchaseDate) {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode"),
                party_id: partyId,
                main_group_id: mainGroupId,
                lot_date: purchaseDate
            }
        }
        return axios.get(`/v1/products/journal/barcode/Metal`, config)
            .then(res => {
                dispatch(fetchLottSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchLotFailure(err))
            })
    }
}


