import axios from '../../../../axiosConfig.js';
import { BRANDED_LOT } from '../../../constants/action-types';
import { getCookie } from '../../authActions'
import Swal from 'sweetalert2'
import { formatDate } from "../../../../utils/util";

export const saveBegin = () => ({
    type: BRANDED_LOT.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: BRANDED_LOT.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: BRANDED_LOT.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: BRANDED_LOT.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: BRANDED_LOT.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: BRANDED_LOT.FETCH_FAILURE,
    error
})

export const fetchDetailsBegin = () => ({
    type: BRANDED_LOT.FETCH_DETAILS_BEGIN
})

export const fetchDetailsSuccess = (data) => ({
    type: BRANDED_LOT.FETCH_DETAILS_SUCCESS,
    data
})

export const fetchDetailsFailure = (error) => ({
    type: BRANDED_LOT.FETCH_DETAILS_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: BRANDED_LOT.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: BRANDED_LOT.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: BRANDED_LOT.DELETE_FAILURE,
    error
})

export function fetchBrandedLotInvoices(supplierId, purchaseType, invoiceDate) {
    let url = ``;
    url = `/v1/products/journal/lot-creation/rc/BRANDED?supplier_Id=${supplierId}&from_date=${formatDate(invoiceDate[0])}&to_date=${formatDate(invoiceDate[1])}&purchaseType=${purchaseType}`;
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(url, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function fetchBrandedLotDetails(invcNo) {
    let url = ``;
    url = `/v1/products/journal/lot-creation/BRANDED?rc_no=${invcNo}`;
    return dispatch => {
        dispatch(fetchDetailsBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(url, config)
            .then(res => {
                dispatch(fetchDetailsSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchDetailsFailure(err))
            })
    }
}



export function saveBrandedLot(data) {
    let body = data ? data.map(brand => {
        return {
            tenantCode: getCookie("tenantCode"),
            id: brand.id,
            isDeleted: false,
            purchaseType: brand.purchaseType,
            receiptNo: brand.receiptNo,
            purchaseDate: brand.purchaseDate,
            partyId: brand.partyId,
            lotNo:"",
            brandSubProduct: {
                id: brand.subProductId
            },            
            quantity: brand.quantity,
            weight: brand.weight,
            employeeId: brand.employeeId,
            remarks: brand.remarks,
            purchaseId: brand.purchaseId,
            createdBarCodeQuantity: brand.createdBarCodeWeight,
            createdBarCodeWeight: brand.createdBarCodeWeight,
        }
    }) : [];
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        return axios.post(`/v1/products/journal/lot-creation/branded`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}