import axios from '../../../../axiosConfig.js';
import { BRANDED_BARCODE } from '../../../constants/action-types';
import { getCookie } from '../../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: BRANDED_BARCODE.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: BRANDED_BARCODE.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: BRANDED_BARCODE.SAVE_FAILURE,
    error
})

export const fetchLotBegin = () => ({
    type: BRANDED_BARCODE.FETCH_LOT_BEGIN
});

export const fetchLotSuccess = (data) => ({
    type: BRANDED_BARCODE.FETCH_LOT_SUCCESS,
    data
});

export const fetchLotFailure = error => ({
    type: BRANDED_BARCODE.FETCH_LOT_FAILURE,
    error
})

export function saveBrandedBarcode(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            lotDate: data.lotDate,
            lotNo: data.lotNo,
            partyId: data.party,
            subProduct: {
                id: data.subProduct,
            },
            purity: {
                id: data.purity,
            },
            quantity: data.quantity,
            weight: data.weight,
            rate: data.rate,
            amount: data.amount,
            remarks: data.remarks,
            labelType: data.labelType,
        }
        let formData = new FormData();
        formData.append("bean", JSON.stringify(body));
        formData.append("image", data.image)
        return axios.post(`/v1/products/journal/barcode/branded`, formData, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                Swal.fire({
                    position: "top-end",
                    title: 'Barcode generated successfully.!',
                    icon: 'success',
                    showConfirmButton: false,
                    timer: 2000
                })
            })
            .catch(err => {
                console.log(err);
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchBrandedLots(mainGroupId, partyId, fromDate) {
    return dispatch => {
        dispatch(fetchLotBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode"),
                party_id: partyId ? partyId : null,
                main_group_id: mainGroupId ? mainGroupId : null,
                from_date: fromDate ? fromDate : null,
                to_date: fromDate ? fromDate : null,
            }
        }
        return axios.get(`/v1/products/journal/barcode/branded`, config)
            .then(res => {
                dispatch(fetchLotSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchLotFailure(err))
            })
    }
}
