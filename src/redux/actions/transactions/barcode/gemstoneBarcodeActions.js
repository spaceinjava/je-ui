import axios from '../../../../axiosConfig.js';
import { GEMSTONE_BARCODE } from '../../../constants/action-types';
import { getCookie } from '../../authActions'
import Swal from 'sweetalert2'
import { formatDate } from "../../../../utils/util";

export const saveBegin = () => ({
    type: GEMSTONE_BARCODE.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: GEMSTONE_BARCODE.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: GEMSTONE_BARCODE.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: GEMSTONE_BARCODE.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: GEMSTONE_BARCODE.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: GEMSTONE_BARCODE.FETCH_FAILURE,
    error
})

export const fetchLotBegin = () => ({
    type: GEMSTONE_BARCODE.LOT_BEGIN
})

export const fetchLotSuccess = (data) => ({
    type: GEMSTONE_BARCODE.LOT_SUCCESS,
    data
})

export const fetchLotFailure = (error) => ({
    type: GEMSTONE_BARCODE.LOT_FAILURE,
    error
})

export function fetchGemstoneSaleRates(stoneId, stoneSizeId) {
    let url = ``;
    url = `/v1/products/stone/sale-rate?stone_id=${stoneId}&stone_size_id=${stoneSizeId}`;
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(url, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function fetchGemstoneBarcodeLots(supplier_Id,mainGroup,purchaseDate) {
    let url = ``;
    url = `/v1/products/journal/barcode/STONE?`;
    if(supplier_Id !== "")
    url += `&supplier_Id=${supplier_Id}`;
    if(mainGroup !== "")
    url += `&mainGroup=${mainGroup}`;
    if(purchaseDate !== null)
    url += `&from_date=${formatDate(purchaseDate[0])}&to_date=${formatDate(purchaseDate[1])}`;
    return dispatch => {
        dispatch(fetchLotBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(url, config)
            .then(res => {
                dispatch(fetchLotSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchLotFailure(err))
            })
    }
}

export function saveGemstoneBarcode(stone, logo) {    
    let body = {
        tenantCode: getCookie("tenantCode"),
            isDeleted: false,
            partyId: stone.partyId.value,
            lotNo: stone.lotNo.value,
            lotDate: stone.lotDate.value,
            stone: {
                id: stone.gemstoneSubCategoryId.value,
            },
            stoneSize: (stone.stoneSizeId.value ? {
                id: stone.stoneSizeId.value,
            } : null),
            stoneColor: (stone.stoneColorId.value ? {
                id: stone.stoneColorId.value,
            } : null),
            stoneClarity: (stone.stoneClarityId.value ? {
                id: stone.stoneClarityId.value,
            } : null),
            stonePolish: (stone.stonePolishId.value ? {
                id: stone.stonePolishId.value,
            } : null),
            uom: stone.uom.value,
            quantity: stone.quantity.value,
            weightPerGram: stone.weightPerGram.value,
            weightPerKarat: stone.weightPerKarat.value,
            rate: stone.rate.value,
            amount: stone.amount.value,
            remarks: stone.remarks.value,
            labelType: stone.labelType.value,
    };
    let formData = new FormData();
    formData.append("bean", JSON.stringify(body));
    formData.append("image", logo)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        return axios.post(`/v1/products/journal/barcode/stone`, formData, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}