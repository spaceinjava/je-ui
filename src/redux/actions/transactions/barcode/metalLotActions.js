import axios from '../../../../axiosConfig.js';
import { METAL_LOT } from '../../../constants/action-types';
import { getCookie } from '../../authActions'
import Swal from 'sweetalert2'
import { formatDate } from "../../../../utils/util";

export const saveBegin = () => ({
    type: METAL_LOT.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: METAL_LOT.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: METAL_LOT.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: METAL_LOT.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: METAL_LOT.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: METAL_LOT.FETCH_FAILURE,
    error
})

export const fetchDetailsBegin = () => ({
    type: METAL_LOT.FETCH_DETAILS_BEGIN
})

export const fetchDetailsSuccess = (data) => ({
    type: METAL_LOT.FETCH_DETAILS_SUCCESS,
    data
})

export const fetchDetailsFailure = (error) => ({
    type: METAL_LOT.FETCH_DETAILS_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: METAL_LOT.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: METAL_LOT.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: METAL_LOT.DELETE_FAILURE,
    error
})

export function fetchMetalLotInvoices(supplierId, purchaseType, invoiceDate) {
    let url = ``;
    url = `/v1/products/journal/lot-creation/rc/METAL?supplier_Id=${supplierId}&from_date=${formatDate(invoiceDate[0])}&to_date=${formatDate(invoiceDate[1])}&purchaseType=${purchaseType}`;
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(url, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}


export function fetchMetalLotDetails(invcNo) {
    let url = ``;
    url = `/v1/products/journal/lot-creation/METAL?rc_no=${invcNo}`;
    return dispatch => {
        dispatch(fetchDetailsBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(url, config)
            .then(res => {
                dispatch(fetchDetailsSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchDetailsFailure(err))
            })
    }
}

export function saveMetalLot(data) {
    let productDetails = data ? data.map(product => {
        let stoneDetails = product.stoneDetails ? product.stoneDetails.map(stone => {
            return {
                isDeleted: false,
                tenantCode: getCookie("tenantCode"),
                purchaseType: product.purchaseType,
                purchaseDate: product.purchaseDate,
                receiptNo: product.receiptNo,
                invoiceNo: product.invoiceNo,
                lotNo: "",
                partyId: product.partyId,
                stone: {
                    id: stone.stone.id,
                },
                stoneSize: (stone.stoneSize ? {
                    id: stone.stoneSize.id,
                } : null),
                stoneColor: (stone.stoneColor ? {
                    id: stone.stoneColor.id,
                } : null),
                stoneClarity: (stone.stoneClarity ? {
                    id: stone.stoneClarity.id,
                } : null),
                stonePolish: (stone.stonePolish ? {
                    id: stone.stonePolish.id,
                } : null),
                quantity: stone.quantity,
                uom: stone.uom,
                weightPerGram: parseFloat(stone.weightPerGram || 0),
                weightPerKarat: parseFloat(stone.weightPerKarat || 0),
                remarks: product.remarks,
                employeeId:product.employeeId,
                purchaseId: stone.id,
            }
        }) : [];
        return {
            tenantCode: getCookie("tenantCode"),
            purchaseType: product.purchaseType,
            purchaseDate: product.purchaseDate,
            receiptNo: product.receiptNo,
            invoiceNo: product.invoiceNo,
            lotNo: "",
            partyId: product.partyId,
            id: product.id,
            isDeleted: false,
            subProduct: {
                id: product.subProductId,
            },
            purity: {
                id: product.purityId,
            },
            productSize: {
                id: product.productSizeId,
            },
            quantity: parseInt(product.quantity),
            grossWeight: product.grossWeight,
            stoneWeight: product.stoneWeight,
            netWeight: product.netWeight,
            remarks: product.remarks,
            purchaseId: product.purchaseId,
            employeeId:product.employeeId,
            stoneLot: stoneDetails
        }
    }) : [];
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        return axios.post(`/v1/products/journal/lot-creation/metal`, productDetails, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}