import axios from '../../../../axiosConfig.js';
import { METAL_PURCHASE } from '../../../constants/action-types';
import { getCookie } from '../../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: METAL_PURCHASE.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: METAL_PURCHASE.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: METAL_PURCHASE.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: METAL_PURCHASE.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: METAL_PURCHASE.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: METAL_PURCHASE.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: METAL_PURCHASE.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: METAL_PURCHASE.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: METAL_PURCHASE.DELETE_FAILURE,
    error
})

export const saveProductBegin = () => ({
    type: METAL_PURCHASE.PRODUCT_SAVE_BEGIN
});

export const saveProductSuccess = (data) => ({
    type: METAL_PURCHASE.PRODUCT_SAVE_SUCCESS,
    data
});

export const saveProductFailure = error => ({
    type: METAL_PURCHASE.PRODUCT_SAVE_FAILURE,
    error
})

export const fetcProducthBegin = () => ({
    type: METAL_PURCHASE.PRODUCT_FETCH_BEGIN
})

export const fetcProducthSuccess = (data) => ({
    type: METAL_PURCHASE.PRODUCT_FETCH_SUCCESS,
    data
})

export const fetchProductFailure = (error) => ({
    type: METAL_PURCHASE.PRODUCT_FETCH_FAILURE,
    error
})

export const deleteProductBegin = () => ({
    type: METAL_PURCHASE.PRODUCT_DELETE_BEGIN
})

export const deleteProductSuccess = (data) => ({
    type: METAL_PURCHASE.PRODUCT_DELETE_SUCCESS,
    data
})

export const deleteProductFailure = (error) => ({
    type: METAL_PURCHASE.PRODUCT_DELETE_FAILURE,
    error
})

export function saveMetalPurchase(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }

        
        

        let productDetails = data.productDetails ? data.productDetails.map(product => {
            let stoneDetails = product.stoneDetails ? product.stoneDetails.map(stone => {
                return {
                    id: stone.id,
                    isDeleted: false,
                    stone: {
                        id: stone.stoneId,
                    },
                    stoneSize: (stone.stoneSizeId ? {
                        id: stone.stoneSizeId,
                    } : null),
                    stoneColor: (stone.stoneColorId ? {
                        id: stone.stoneColorId,
                    } : null),
                    stoneClarity: (stone.stoneClarityId ? {
                        id: stone.stoneClarityId,
                    } : null),
                    stonePolish: (stone.stonePolishId ? {
                        id: stone.stonePolishId,
                    } : null),
                    quantity: stone.quantity,
                    uom: stone.uom,
                    weightPerGram: stone.weightPerGram,
                    rate: stone.rate,
                    amount: stone.amount,
                    isNoWeight: stone.isNoWeight,
                    gstAmount: stone.gstAmount,
                    totalAmount: stone.totalAmount,
                    remarks: stone.remarks,
                }
            }) : [];
            return {
                tenantCode: getCookie("tenantCode"),
                id: product.id,
                isDeleted: false,
                subProduct: {
                    id: product.subProductId,
                },
                purity: {
                    id: product.purityId,
                },
                productSize: {
                    id: product.productSizeId,
                },
                quantity: product.quantity,
                grossWeight: product.grossWeight,
                stoneWeight: product.stoneWeight,
                netWeight: product.netWeight,
                touchCalc: product.touchCalc,
                touchPercent: product.touchPercent,
                touchValue: product.touchValue,
                wastageOn: product.wastageOn,
                wastageValue: product.wastageValue,
                pureWeight: product.pureWeight,
                mcOn: product.mcOn,
                mcValue: product.mcValue,
                rate: product.rate,
                amount: product.amount,
                stoneAmount: product.stoneAmount,
                mcAmount: product.mcAmount,
                gstAmount: product.gstAmount,
                totalAmount: product.totalAmount,
                remarks: product.remarks,
                stoneDetails: stoneDetails
            }
        }) : [];
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            purchaseType: data.purchaseType,
            invoiceDate: data.invoiceDate,
            invoiceNo: data.invoiceNo,
            partyId: data.partyId,
            productDetails: productDetails
        };
        console.log("==== == ========= ===== =========== ::::::: ", body)
        return axios.post(`/v1/products/journal/inventory/metal-purchase`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchMetalPurchases())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchMetalPurchases() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/journal/inventory/metal-purchase`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function deleteMetalPurchases(data) {
    return dispatch => {
        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                metal_purchase_ids: data
            }
        }
        return axios.delete(`/v1/products/journal/inventory/metal-purchase`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchMetalPurchases())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })
            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err.response)
                if (err.response) {
                    Swal.fire({
                        title: 'Error!',
                        text: err.response.data.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    })
                }
            })
    }
}

export function addProductDetails(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveProductBegin())
        dispatch(saveProductSuccess(data))
    }
}

export function fetchProductDetails() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/journal/inventory/branded-purchase`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function deleteProducts(data) {
    console.log(data)
    return dispatch => {
        dispatch(deleteProductBegin())
        dispatch(deleteProductSuccess(data))
    }
}

