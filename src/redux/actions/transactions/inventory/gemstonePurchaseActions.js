import axios from '../../../../axiosConfig.js';
import { GEMSTONE_PURCHASE } from '../../../constants/action-types';
import { getCookie } from '../../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: GEMSTONE_PURCHASE.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: GEMSTONE_PURCHASE.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: GEMSTONE_PURCHASE.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: GEMSTONE_PURCHASE.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: GEMSTONE_PURCHASE.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: GEMSTONE_PURCHASE.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: GEMSTONE_PURCHASE.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: GEMSTONE_PURCHASE.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: GEMSTONE_PURCHASE.DELETE_FAILURE,
    error
})

export function saveGemstonePurchase(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let stoneDetails = data.stoneDetails ? data.stoneDetails.map(stone => {
            return {
                tenantCode: getCookie("tenantCode"),
                id: stone.id,
                isDeleted: false,
                stone: {
                    id: stone.stoneId,
                },
                stoneSize: (stone.stoneSizeId ? {
                    id: stone.stoneSizeId,
                } : null),
                stoneColor: (stone.stoneColorId ? {
                    id: stone.stoneColorId,
                } : null),
                stoneClarity: (stone.stoneClarityId ? {
                    id: stone.stoneClarityId,
                } : null),
                stonePolish: (stone.stonePolishId ? {
                    id: stone.stonePolishId,
                } : null),
                uom: stone.uom,
                quantity: stone.quantity,
                weightPerGram: stone.weightPerGram,
                weightPerKarat: stone.weightPerKarat,
                rate: stone.rate,
                amount: stone.amount,
                gstAmount: stone.gstAmount,
                totalAmount: stone.totalAmount,
                remarks: stone.remarks,
            }
        }) : [];
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            purchaseType: data.purchaseType,
            invoiceDate: data.invoiceDate,
            invoiceNo: data.invoiceNo,
            partyId: data.partyId,
            stoneDetails: stoneDetails
        };
        console.log("==== == ========= ===== =========== ::::::: ", body)
        return axios.post(`/v1/products/journal/inventory/gemstone-purchase`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchGemstonePurchases())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchGemstonePurchases() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/journal/inventory/gemstone-purchase`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function deleteGemstonePurchases(data) {
    return dispatch => {
        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                purchase_ids: data
            }
        }
        return axios.delete(`/v1/products/journal/inventory/gemstone-purchase`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchGemstonePurchases())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })
            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err.response)
                if (err.response) {
                    Swal.fire({
                        title: 'Error!',
                        text: err.response.data.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    })
                }
            })
    }
}

