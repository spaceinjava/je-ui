import axios from '../../../../axiosConfig.js';
import { BRANDED_PURCHASE } from '../../../constants/action-types';
import { getCookie } from '../../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: BRANDED_PURCHASE.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: BRANDED_PURCHASE.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: BRANDED_PURCHASE.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: BRANDED_PURCHASE.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: BRANDED_PURCHASE.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: BRANDED_PURCHASE.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: BRANDED_PURCHASE.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: BRANDED_PURCHASE.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: BRANDED_PURCHASE.DELETE_FAILURE,
    error
})

export function saveBrandedPurchase(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let brandedDetails = data.brandedDetails ? data.brandedDetails.map(brand => {
            return {
                tenantCode: getCookie("tenantCode"),
                id: brand.id,
                isDeleted: false,
                subProduct: {
                    id: brand.subProductId,
                },
                quantity: brand.quantity,
                weight: brand.weight,
                rate: brand.rate,
                amount: brand.amount,
                gstAmount: brand.gstAmount,
                totalAmount: brand.totalAmount,
                remarks: brand.remarks,
            }
        }) : [];
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            purchaseType: data.purchaseType,
            invoiceDate: data.invoiceDate,
            invoiceNo: data.invoiceNo,
            partyId: data.partyId,
            brandedDetails: brandedDetails
        };
        console.log("==== == ========= ===== =========== ::::::: ", body)
        return axios.post(`/v1/products/journal/inventory/branded-purchase`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchBrandedPurchases())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchBrandedPurchases() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/journal/inventory/branded-purchase`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function deleteBrandedPurchases(data) {
    return dispatch => {
        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                branded_purchase_ids: data
            }
        }
        return axios.delete(`/v1/products/journal/inventory/branded-purchase`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchBrandedPurchases())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })
            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err.response)
                if (err.response) {
                    Swal.fire({
                        title: 'Error!',
                        text: err.response.data.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    })
                }
            })
    }
}

