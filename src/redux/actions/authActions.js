import axios from '../../axiosConfig.js';
import { USER, SET_CURRENT_USER, SET_VALID_USER, SET_AUTH_TOKEN } from '../constants/action-types';
import Swal from 'sweetalert2'

export const AuthorizationBegin = () => ({
    type: USER.AUTHORIZATION_BEGIN
});

export const AuthorizationSuccess = () => ({
    type: USER.AUTHORIZATION_SUCCESS
});

export const AuthorizationFailure = error => ({
    type: USER.AUTHORIZATION_FAILURE,
    error
})

export const validateUser = (info) => ({
    type: SET_VALID_USER,
    info
})

export const setCurrentUser = (info) => ({
    type: SET_CURRENT_USER,
    info
});

export const setAuthToken = (token) => ({
    type: SET_AUTH_TOKEN,
    token
})

export const newUser = () => ({
    type: USER.NEW_USER
})

export function validate(data) {
    return dispatch => {
        dispatch(AuthorizationBegin())
        let config = {
            headers: { hash: data.hash,
                secret: data.secret }
        }
        return axios.post(`/v1/gateway/authorize`, {}, config)
            .then(res => {
            let now = new Date();
            let time = now.getTime();
            let expireTime = time + 1000 * 60 * 60 * 24;
            now.setTime(expireTime);
            let future = new Date();
            let credentialsTime = future.getTime() + 1000 * 10 * 365 * 24 * 60 * 60;
            future.setTime(credentialsTime)
            document.cookie = "hash=" + res.data.data.userInfo.hash + ";" + "expires=" + future.toUTCString();
            document.cookie = "secret=" + res.data.data.userInfo.secret + ";" + "expires=" + future.toUTCString();
            document.cookie = "authCode=" + res.data.data.authCode + ";" + "expires=" + now.toUTCString();
                dispatch(validateUser(res.data))
                dispatch(AuthorizationSuccess())
                // document.cookie = "is_tenant=" + res.data.data.is_tenant;

            })
            .catch(err => {
                dispatch(AuthorizationFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function login(data) {
    return dispatch => {
        dispatch(AuthorizationBegin())
        let config = {
            headers: {
                authCode: data.code
            }
        }
        let body = {
            userName: data.username,
            password: data.password
        }
        return axios.post(`/v1/gateway/login`, body, config)
            .then(res => {
                document.cookie = "loginStatus=" + res.data.data.userInfo.loginStatus
                document.cookie = "authToken=" + res.data.data.authToken
                document.cookie = "loginId=" + res.data.data.userInfo.userName
                document.cookie = "tenantCode=" + res.data.data.userInfo.tenantCode
                if (res.data.data.userInfo.middleName) {
                    document.cookie = "username=" + res.data.data.userInfo.firstName + " " + res.data.data.userInfo.middleName + " " + res.data.data.userInfo.lastName;
                }
                else {
                    document.cookie = "username=" + res.data.data.userInfo.firstName + "  " + res.data.data.userInfo.lastName;
                }
                dispatch(setCurrentUser(res.data))
                dispatch(AuthorizationSuccess())
            })
            .catch(err => {
                dispatch(AuthorizationFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function resetPassword(data) {
    return dispatch => {
        dispatch(AuthorizationBegin())
        let config = {
            headers: {
                authToken: data.token
            }
        }
        let body = {
            userName: data.user_code,
            password: data.password,
            newPassword: data.new_password
        }
        return axios.post(`/v1/gateway/reset/password`, body, config)
            .then(res => {
                document.cookie = "loginStatus=" + "RESET"
                dispatch(setCurrentUser(res.data))
                dispatch(AuthorizationSuccess())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })
            })
            .catch(err => {
                dispatch(AuthorizationFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function logout() {
    return dispatch => {
        document.cookie = "authToken=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        dispatch(setCurrentUser({}));
    }
}

export function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}