import axios from '../../axiosConfig.js';
import { DEPARTMENT } from '../constants/action-types';
import { getCookie } from '../actions/authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: DEPARTMENT.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: DEPARTMENT.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: DEPARTMENT.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: DEPARTMENT.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: DEPARTMENT.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: DEPARTMENT.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: DEPARTMENT.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: DEPARTMENT.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: DEPARTMENT.DELETE_FAILURE,
    error
})

export function saveDepartment(data) {
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            departmentName: data.name,
            shortCode: data.shortCode
        }
        return axios.post(`/v1/tenant/department`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchDepartments())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchDepartments() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/tenant/department`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function deleteDepartments(data) {
    return dispatch => {

        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                department_ids: data
            }
        }
        return axios.delete(`/v1/tenant/department`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchDepartments())
                Swal.fire({
                    title: 'Deleted',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err)
                Swal.fire({
                    title: 'Error!',
                    text: 'test',
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

