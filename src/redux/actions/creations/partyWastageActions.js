import axios from '../../../axiosConfig.js';
import { PARTY_WASTAGE } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: PARTY_WASTAGE.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: PARTY_WASTAGE.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: PARTY_WASTAGE.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: PARTY_WASTAGE.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: PARTY_WASTAGE.FETCH_SUCCESS,
    data
})

export const fetchByCriteriaSuccess = (data) => ({
    type: PARTY_WASTAGE.FETCH_BY_CRITERIA_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: PARTY_WASTAGE.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: PARTY_WASTAGE.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: PARTY_WASTAGE.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: PARTY_WASTAGE.DELETE_FAILURE,
    error
})

export function savePartyWastage(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            partyId: data.partyId,
            subProduct: {
                id: data.subProduct
            },
            productSize: {
                id: data.productSize
            },
            purity: {
                id: data.purity
            },
            touchValue: data.touchValue,
            touchPercent: data.touchPercent,
            mcPerGram: data.mcPerGram,
            mcPerDir: data.mcPerDir,
            wastagePerGram: data.wastagePerGram,
            wastagePerDir: data.wastagePerDir,
            mcOn: data.mcOn,
            wastageOn: data.wastageOn,
        }
        return axios.post(`/v1/products/product/party-wastage`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchPartyWastages())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchPartyWastages() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/product/party-wastage`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

/**
 * retieve party wastage based on partyId, subProductId, purityId, productSizeId
 * @param {*} party 
 * @param {*} subProduct 
 * @param {*} purity 
 * @param {*} productSize 
 */
export function fetchPartyWastagesByCriteria(party, subProduct, purity, productSize) {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode"),
                party_id: party ? party : null,
                sub_product_id: subProduct ? subProduct : null,
                purity_id: purity ? purity : null,
                product_size_id: productSize ? productSize : null
            }
        }
        return axios.get(`/v1/products/product/party-wastage`, config)
            .then(res => {
                dispatch(fetchByCriteriaSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function deletePartyWastages(data) {
    return dispatch => {

        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                party_wastage_ids: data
            }
        }
        return axios.delete(`/v1/products/product/party-wastage`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchPartyWastages())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err.response)
                if (err.response) {
                    Swal.fire({
                        title: 'Error!',
                        text: err.response.data.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    })
                }
            })
    }
}

