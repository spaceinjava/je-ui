import axios from '../../../axiosConfig.js';
import { PURITY } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: PURITY.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: PURITY.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: PURITY.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: PURITY.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: PURITY.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: PURITY.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: PURITY.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: PURITY.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: PURITY.DELETE_FAILURE,
    error
})

export function savePurity(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            mainGroup: {
                id: data.mainGroup,
            },
            purity: data.purity,
        }
        return axios.post(`/v1/products/other/purity`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchPurities())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchPurities() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/other/purity`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function deletePurity(data) {
    return dispatch => {

        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                purity_ids: data
            }
        }
        return axios.delete(`/v1/products/other/purity`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchPurities())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err)
                Swal.fire({
                    title: 'Error!',
                    text: 'test',
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}


