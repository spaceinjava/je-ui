import axios from '../../../axiosConfig.js';
import { PARTY_STONE_RATE } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: PARTY_STONE_RATE.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: PARTY_STONE_RATE.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: PARTY_STONE_RATE.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: PARTY_STONE_RATE.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: PARTY_STONE_RATE.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: PARTY_STONE_RATE.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: PARTY_STONE_RATE.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: PARTY_STONE_RATE.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: PARTY_STONE_RATE.DELETE_FAILURE,
    error
})

export function savePartyStoneRate(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            partyId: data.partyId,
            stone: {
                id: data.stone
            },
            stoneSize: (data.stoneSize ? {
                id: data.stoneSize
            } : null),
            stoneColor: (data.stoneColor ? {
                id: data.stoneColor,
            } : null),
            stoneClarity: (data.stoneClarity ? {
                id: data.stoneClarity,
            } : null),
            stonePolish: (data.stonePolish ? {
                id: data.stonePolish,
            } : null),
            minRate: data.minRate,
            maxRate: data.maxRate,
            uom: data.uom,
            noWeight: data.noWeight,
            fixedRate: data.fixedRate
        }
        return axios.post(`/v1/products/stone/party-rate`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchPartyStoneRates())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchPartyStoneRates(partyId, stoneId, stoneSizeId) {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode"),
                party_id: partyId ? partyId : null,
                stone_id: stoneId ? stoneId : null,
                stone_size_id: stoneSizeId ? stoneSizeId : null,
            }
        }
        return axios.get(`/v1/products/stone/party-rate`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function deletePartyStoneRates(data) {
    return dispatch => {
        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                party_rate_ids: data
            }
        }
        return axios.delete(`/v1/products/stone/party-rate`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchPartyStoneRates())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })
            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err.response)
                if (err.response) {
                    Swal.fire({
                        title: 'Error!',
                        text: err.response.data.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    })
                }
            })
    }
}

