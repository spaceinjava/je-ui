import axios from '../../../axiosConfig.js';
import { BRAND } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: BRAND.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: BRAND.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: BRAND.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: BRAND.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: BRAND.FETCH_SUCCESS,
    data
})

export const fetchByMainGroupSuccess = (data) => ({
    type: BRAND.FETCH_BY_MAIN_GROUP_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: BRAND.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: BRAND.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: BRAND.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: BRAND.DELETE_FAILURE,
    error
})

export function saveBrand(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            brandName: data.brandName,
            shortCode: data.shortCode,
            mainGroup: {
                id: data.mainGroup
            }
        }
        return axios.post(`/v1/products/brand`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchBrands())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchBrands() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/brand`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function fetchBrandsByMainGroup(mainGroupId) {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode"),
                main_group_id: mainGroupId
            }
        }
        return mainGroupId ? axios.get(`/v1/products/brand/main-group`, config)
            .then(res => {
                dispatch(fetchByMainGroupSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
        : [];
    }
}

export function deleteBrands(data) {
    return dispatch => {

        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                brand_ids: data
            }
        }
        return axios.delete(`/v1/products/brand`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchBrands())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err.response)
                if(err.response) {
                    Swal.fire({
                        title: 'Error!',
                        text: err.response.data.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    })
                }
            })
    }
}

