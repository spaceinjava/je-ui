import axios from '../../../axiosConfig';
import { COUNTER } from '../../constants/action-types'
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: COUNTER.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: COUNTER.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: COUNTER.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: COUNTER.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: COUNTER.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: COUNTER.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: COUNTER.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: COUNTER.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: COUNTER.DELETE_FAILURE,
    error
})

export function saveCounter(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            floor: {
                id: data.floor,
            },
            counterName: data.counterName,
            shortCode: data.shortCode
        }
        return axios.post(`/v1/products/other/counter`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchCounterList())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchCounterList() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/other/counter`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}


export function deleteCounter(data) {
    return dispatch => {

        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                counter_ids: data
            }
        }
        return axios.delete(`/v1/products/other/counter`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchCounterList())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err)
                Swal.fire({
                    title: 'Error!',
                    text: 'test',
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

