import axios from '../../../axiosConfig.js';
import { HSN } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: HSN.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: HSN.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: HSN.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: HSN.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: HSN.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: HSN.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: HSN.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: HSN.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: HSN.DELETE_FAILURE,
    error
})

export function saveHsnCode(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            mainGroup: {
                id: data.mainGroup,
            },
            hsnCode: data.hsnCode,
            description: data.description,
        }
        return axios.post(`/v1/products/other/hsncode`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchHsnCodes())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchHsnCodes() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/other/hsncode`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function deleteHsnCode(data) {
    return dispatch => {

        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                hsn_ids: data
            }
        }
        return axios.delete(`/v1/products/other/hsncode`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchHsnCodes())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err)
                Swal.fire({
                    title: 'Error!',
                    text: 'test',
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}


