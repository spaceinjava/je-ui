import axios from '../../../axiosConfig.js';
import { RATE } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: RATE.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: RATE.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: RATE.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: RATE.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: RATE.FETCH_SUCCESS,
    data
})

export const fetchCalculatedRatesSuccess = (data) => ({
    type: RATE.FETCH_CALCULATED_RATES_SUCCESS,
    data
})

export const fetchRatesByMainGroupSuccess = (data) => ({
    type: RATE.FETCH_RATES_BY_MAIN_GROUP_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: RATE.FETCH_FAILURE,
    error
})

export function saveRate(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        return axios.post(`/v1/products/other/rate`, data, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchRates())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchRates() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/other/rate`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function fetchCalculatedRates(mainGroup, perGramRate) {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode"),
                groupId: mainGroup,
                perGramRate: perGramRate
            }
        }
        return axios.get(`/v1/products/other/rate`, config)
            .then(res => {
                dispatch(fetchCalculatedRatesSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function fetchRatesByMainGroup(mainGroup) {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode"),
                groupId: mainGroup
            }
        }
        return axios.get(`/v1/products/other/rate`, config)
            .then(res => {
                dispatch(fetchRatesByMainGroupSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}
