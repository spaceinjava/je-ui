import axios from '../../../axiosConfig.js';
import { PRODUCT } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: PRODUCT.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: PRODUCT.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: PRODUCT.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: PRODUCT.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: PRODUCT.FETCH_SUCCESS,
    data
})

export const fetchSubProductsByProductSuccess = (data) => ({
    type: PRODUCT.FETCH_SUB_PRODUCT_BY_PRODUCT_SUCCESS,
    data
})

export const fetchSizesByProductSuccess = (data) => ({
    type: PRODUCT.FETCH_SIZE_BY_PRODUCT_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: PRODUCT.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: PRODUCT.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: PRODUCT.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: PRODUCT.DELETE_FAILURE,
    error
})

export function saveProduct(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            productName: data.productName,
            shortCode: data.shortCode,
            mainGroup: {
                id: data.mainGroup
            }
        }
        return axios.post(`/v1/products/product`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchProducts())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchProducts() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/product`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function fetchSubProductsByProduct(productId) {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return productId ? axios.get(`/v1/products/product/${productId}/sub-products`, config)
            .then(res => {
                dispatch(fetchSubProductsByProductSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
        : [];
    }
}

export function fetchSizesByProduct(productId) {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return productId ? axios.get(`/v1/products/product/${productId}/sizes`, config)
            .then(res => {
                dispatch(fetchSizesByProductSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
        : [];
    }
}

export function deleteProducts(data) {
    return dispatch => {

        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                product_ids: data
            }
        }
        return axios.delete(`/v1/products/product`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchProducts())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err.response)
                if (err.response) {
                    Swal.fire({
                        title: 'Error!',
                        text: err.response.data.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    })
                }
            })
    }
}

