import axios from '../../../axiosConfig.js';
import { STONE_TYPE } from '../../constants/action-types';
import { getCookie } from '../authActions'

export const fetchBegin = () => ({
    type: STONE_TYPE.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: STONE_TYPE.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: STONE_TYPE.FETCH_FAILURE,
    error
})

export function fetchStoneTypes() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/stone/type`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}
