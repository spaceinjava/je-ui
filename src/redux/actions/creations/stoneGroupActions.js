import axios from '../../../axiosConfig.js';
import { STONE_GROUP } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: STONE_GROUP.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: STONE_GROUP.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: STONE_GROUP.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: STONE_GROUP.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: STONE_GROUP.FETCH_SUCCESS,
    data
})

export const fetchByTypeSuccess = (data) => ({
    type: STONE_GROUP.FETCH_BY_TYPE_SUCCESS,
    data
})

export const fetchStonesByStoneGroupSuccess = (data) => ({
    type: STONE_GROUP.FETCH_STONES_BY_GROUP_SUCCESS,
    data
})

export const fetchStoneSizesByStoneGroupSuccess = (data) => ({
    type: STONE_GROUP.FETCH_STONE_SIZES_BY_GROUP_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: STONE_GROUP.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: STONE_GROUP.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: STONE_GROUP.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: STONE_GROUP.DELETE_FAILURE,
    error
})

export function saveStoneGroup(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            groupName: data.groupName,
            shortCode: data.shortCode,
            diamond: data.diamond,
            stoneType: {
                id: data.stoneType
            }
        }
        return axios.post(`/v1/products/stone/group`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchStoneGroups())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchStoneGroups() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/stone/group`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function fetchStoneGroupsByType(stoneTypeId) {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode"),
                stone_type_id: stoneTypeId
            }
        }
        return stoneTypeId ? axios.get(`/v1/products/stone/group/type`, config)
            .then(res => {
                dispatch(fetchByTypeSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
        : [];
    }
}

export function fetchStonesByStoneGroup(stoneGroupId) {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return stoneGroupId ? axios.get(`/v1/products/stone/group/${stoneGroupId}/stones`, config)
            .then(res => {
                dispatch(fetchStonesByStoneGroupSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
        : [];
    }
}

export function fetchStoneSizesByStoneGroup(stoneGroupId) {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return stoneGroupId ? axios.get(`/v1/products/stone/group/${stoneGroupId}/sizes`, config)
            .then(res => {
                dispatch(fetchStoneSizesByStoneGroupSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
        : [];
    }
}

export function deleteStoneGroups(data) {
    return dispatch => {
        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                stone_group_ids: data
            }
        }
        return axios.delete(`/v1/products/stone/group`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchStoneGroups())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })
            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err.response)
                if (err.response) {
                    Swal.fire({
                        title: 'Error!',
                        text: err.response.data.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    })
                }
            })
    }
}

