import axios from '../../../axiosConfig';
import { FLOOR } from '../../constants/action-types'
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: FLOOR.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: FLOOR.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: FLOOR.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: FLOOR.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: FLOOR.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: FLOOR.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: FLOOR.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: FLOOR.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: FLOOR.DELETE_FAILURE,
    error
})

export function saveFloor(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            floorName: data.floorName,
            shortCode: data.shortCode
        }
        return axios.post(`/v1/products/other/floor`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchFloorList())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchFloorList() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/other/floor`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}


export function deleteFloor(data) {
    return dispatch => {

        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                floor_ids: data
            }
        }
        return axios.delete(`/v1/products/other/floor`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchFloorList())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err)
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

