import axios from '../../../axiosConfig.js';
import { PRODUCT_SIZE } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: PRODUCT_SIZE.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: PRODUCT_SIZE.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: PRODUCT_SIZE.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: PRODUCT_SIZE.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: PRODUCT_SIZE.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: PRODUCT_SIZE.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: PRODUCT_SIZE.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: PRODUCT_SIZE.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: PRODUCT_SIZE.DELETE_FAILURE,
    error
})

export function saveProductSize(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            productSize: data.productSize,
            product: {
                id: data.product
            }
        }
        return axios.post(`/v1/products/product/size`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchProductSizes())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchProductSizes() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/product/size`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function deleteProductSizes(data) {
    return dispatch => {

        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                size_ids: data
            }
        }
        return axios.delete(`/v1/products/product/size`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchProductSizes())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err.response)
                if (err.response) {
                    Swal.fire({
                        title: 'Error!',
                        text: err.response.data.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    })
                }
            })
    }
}

