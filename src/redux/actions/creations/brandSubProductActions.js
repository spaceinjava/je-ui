import axios from '../../../axiosConfig.js';
import { BRAND_SUB_PRODUCT } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: BRAND_SUB_PRODUCT.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: BRAND_SUB_PRODUCT.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: BRAND_SUB_PRODUCT.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: BRAND_SUB_PRODUCT.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: BRAND_SUB_PRODUCT.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: BRAND_SUB_PRODUCT.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: BRAND_SUB_PRODUCT.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: BRAND_SUB_PRODUCT.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: BRAND_SUB_PRODUCT.DELETE_FAILURE,
    error
})

export function saveBrandSubProduct(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            subProductName: data.subProductName,
            shortCode: data.shortCode,
            brandProduct: {
                id: data.brandProduct
            }
        }
        return axios.post(`/v1/products/brand/sub-product`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchBrandSubProducts())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchBrandSubProducts() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/brand/sub-product`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function deleteBrandSubProducts(data) {
    return dispatch => {

        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                sub_product_ids: data
            }
        }
        return axios.delete(`/v1/products/brand/sub-product`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchBrandSubProducts())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err.response)
                if(err.response) {
                    Swal.fire({
                        title: 'Error!',
                        text: err.response.data.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    })
                }
            })
    }
}

