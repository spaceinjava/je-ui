import axios from '../../../axiosConfig.js';
import { STONE_POLISH } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: STONE_POLISH.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: STONE_POLISH.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: STONE_POLISH.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: STONE_POLISH.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: STONE_POLISH.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: STONE_POLISH.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: STONE_POLISH.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: STONE_POLISH.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: STONE_POLISH.DELETE_FAILURE,
    error
})

export function saveStonePolish(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            shortCode: data.shortCode,
            polishName: data.name
        }
        return axios.post(`/v1/products/stone/polish`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchStonePolishes())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchStonePolishes() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/stone/polish`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function deleteStonePolishes(data) {
    return dispatch => {
        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                stone_polish_ids: data
            }
        }
        return axios.delete(`/v1/products/stone/polish`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchStonePolishes())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })
            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err.response)
                if (err.response) {
                    Swal.fire({
                        title: 'Error!',
                        text: err.response.data.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    })
                }
            })
    }
}

