import axios from '../../../axiosConfig.js';
import { SALE_WASTAGE } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: SALE_WASTAGE.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: SALE_WASTAGE.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: SALE_WASTAGE.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: SALE_WASTAGE.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: SALE_WASTAGE.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: SALE_WASTAGE.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: SALE_WASTAGE.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: SALE_WASTAGE.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: SALE_WASTAGE.DELETE_FAILURE,
    error
})

export const fetchSaleWastagesBySubProductBegin = () => ({
    type: SALE_WASTAGE.FETCH_BY_SUB_PRODUCT_BEGIN
})

export const fetchSaleWastagesBySubProductSuccess = (data) => ({
    type: SALE_WASTAGE.FETCH_BY_SUB_PRODUCT_SUCCESS,
    data
})

export const fetchSaleWastagesBySubProductFailure = (error) => ({
    type: SALE_WASTAGE.FETCH_BY_SUB_PRODUCT_FAILURE,
    error
})

export function saveSaleWastage(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            subProduct: {
                id: data.subProduct
            },
            productSize: {
                id: data.productSize
            },
            purity: {
                id: data.purity
            },
            fromWeight: data.fromWeight,
            toWeight: data.toWeight,
            maxWstPerGram: data.maxWstPerGram,
            minWstPerGram: data.minWstPerGram,
            maxDirWst: data.maxDirWst,
            minDirWst: data.minDirWst,
            maxMcPerGram: data.maxMcPerGram,
            minMcPerGram: data.minMcPerGram,
            minDirMc: data.minDirMc,
            maxDirMc: data.maxDirMc,
            mcOn: data.mcOn,
            wastageOn: data.wastageOn,
            incWstMc: data.incWstMc
        }
        return axios.post(`/v1/products/product/sale-wastage`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchSaleWastages())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchSaleWastages() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/product/sale-wastage`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function deleteSaleWastages(data) {
    return dispatch => {

        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                sale_wastage_ids: data
            }
        }
        return axios.delete(`/v1/products/product/sale-wastage`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchSaleWastages())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err.response)
                if (err.response) {
                    Swal.fire({
                        title: 'Error!',
                        text: err.response.data.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    })
                }
            })
    }
}

export function fetchSaleWastagesBySubProduct(sub_product_id, product_size_id, purity_id) {
    console.log(sub_product_id, product_size_id, purity_id)
    return dispatch => {
        dispatch(fetchSaleWastagesBySubProductBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode"),
                sub_product_id: sub_product_id,
                product_size_id: product_size_id,
                purity_id: purity_id
            }
        }
        return axios.get(`/v1/products/product/sale-wastage`, config)
            .then(res => {
                console.log(res.data)
                dispatch(fetchSaleWastagesBySubProductSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchSaleWastagesBySubProductFailure(err))
            })
    }
}

