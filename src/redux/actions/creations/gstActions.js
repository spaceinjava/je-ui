import axios from '../../../axiosConfig.js';
import { GST } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: GST.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: GST.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: GST.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: GST.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: GST.FETCH_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: GST.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: GST.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: GST.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: GST.DELETE_FAILURE,
    error
})

export const metalFetchBegin = () => ({
    type: GST.METAL_FETCH_BEGIN
})

export const metalFetchSuccess = (data) => ({
    type: GST.METAL_FETCH_SUCCESS,
    data
})

export const metalFetchFailure = (error) => ({
    type: GST.METAL_FETCH_FAILURE,
    error
})

export function saveGST(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            metalType: {
                id: data.metalType,
            },
            percent: data.percent,
        }
        return axios.post(`/v1/products/other/gst`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchGST())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchGST() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/other/gst`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function deleteGST(data) {
    return dispatch => {

        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                gst_ids: data
            }
        }
        return axios.delete(`/v1/products/other/gst`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchGST())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err)
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchMetalType() {
    return dispatch => {
        dispatch(metalFetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/other/metalType`, config)
            .then(res => {
                dispatch(metalFetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(metalFetchFailure(err))
            })
    }
}




