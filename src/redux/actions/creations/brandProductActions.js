import axios from '../../../axiosConfig.js';
import { BRAND_PRODUCT } from '../../constants/action-types';
import { getCookie } from '../authActions'
import Swal from 'sweetalert2'

export const saveBegin = () => ({
    type: BRAND_PRODUCT.SAVE_BEGIN
});

export const saveSuccess = (data) => ({
    type: BRAND_PRODUCT.SAVE_SUCCESS,
    data
});

export const saveFailure = error => ({
    type: BRAND_PRODUCT.SAVE_FAILURE,
    error
})

export const fetchBegin = () => ({
    type: BRAND_PRODUCT.FETCH_BEGIN
})

export const fetchSuccess = (data) => ({
    type: BRAND_PRODUCT.FETCH_SUCCESS,
    data
})

export const fetchByBrandSuccess = (data) => ({
    type: BRAND_PRODUCT.FETCH_BY_BRAND_SUCCESS,
    data
})

export const fetchSubProductsByProductSuccess = (data) => ({
    type: BRAND_PRODUCT.FETCH_SUB_PRODUCTS_BY_PRODUCT_SUCCESS,
    data
})

export const fetchFailure = (error) => ({
    type: BRAND_PRODUCT.FETCH_FAILURE,
    error
})

export const deleteBegin = () => ({
    type: BRAND_PRODUCT.DELETE_BEGIN
})

export const deleteSuccess = (data) => ({
    type: BRAND_PRODUCT.DELETE_SUCCESS,
    data
})

export const deleteFailure = (error) => ({
    type: BRAND_PRODUCT.DELETE_FAILURE,
    error
})

export function saveBrandProduct(data) {
    console.log(data)
    return dispatch => {
        dispatch(saveBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            }
        }
        let body = {
            tenantCode: getCookie("tenantCode"),
            id: data.id,
            isDeleted: false,
            shortCode: data.shortCode,
            productName: data.productName,
            gst: data.gst,
            tray: data.tray,
            brand: {
                id: data.brand
            }
        }
        return axios.post(`/v1/products/brand/product`, body, config)
            .then(res => {
                dispatch(saveSuccess(res.data))
                dispatch(fetchBrandProducts())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(saveFailure(err))
                Swal.fire({
                    title: 'Error!',
                    text: err.response.data.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            })
    }
}

export function fetchBrandProducts() {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return axios.get(`/v1/products/brand/product`, config)
            .then(res => {
                dispatch(fetchSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
    }
}

export function fetchBrandProductsByBrand(brandId) {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode"),
                brand_id: brandId
            }
        }
        return brandId ? axios.get(`/v1/products/brand/product/brand`, config)
            .then(res => {
                dispatch(fetchByBrandSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
        : [];
    }
}

export function fetchSubProductsByProduct(productId) {
    return dispatch => {
        dispatch(fetchBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                tenant_code: getCookie("tenantCode")
            }
        }
        return productId ? axios.get(`/v1/products/brand/product/${productId}/sub-product`, config)
            .then(res => {
                dispatch(fetchSubProductsByProductSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFailure(err))
            })
        : [];
    }
}

export function deleteBrandProducts(data) {
    return dispatch => {

        dispatch(deleteBegin())
        let config = {
            headers: {
                token: getCookie("authToken")
            },
            params: {
                product_ids: data
            }
        }
        return axios.delete(`/v1/products/brand/product`, config)
            .then(res => {
                dispatch(deleteSuccess(res.data))
                dispatch(fetchBrandProducts())
                Swal.fire({
                    title: 'Success',
                    text: res.data.message,
                    icon: 'success',
                    confirmButtonText: 'OK'
                })

            })
            .catch(err => {
                dispatch(deleteFailure(err))
                console.log(err.response)
                if(err.response) {
                    Swal.fire({
                        title: 'Error!',
                        text: err.response.data.message,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    })
                }
            })
    }
}

