export const SET_CURRENT_USER = 'SET_CURRENT_USER'
export const SET_VALID_USER = 'SET_VALID_USER'
export const SET_AUTH_TOKEN = "SET_AUTH_TOKEN"

export const USER = {
    AUTHORIZATION_BEGIN: 'AUTHORIZATION_BEGIN',
    AUTHORIZATION_SUCCESS: 'AUTHORIZATION_SUCCESS',
    AUTHORIZATION_FAILURE: 'AUTHORIZATION_FAILURE',
    NEW_USER: 'NEW_USER'
};

export const DEPARTMENT = {
    SAVE_BEGIN: 'department->Save->Begin',
    SAVE_SUCCESS: 'department->Save->Success',
    SAVE_FAILURE: 'department->Save->Failed',
    FETCH_BEGIN: 'department->Fetch->Begin',
    FETCH_SUCCESS: 'department->Fetch->Success',
    FETCH_FAILURE: 'department->Fetch->Failed',
    DELETE_BEGIN: 'department->Delete->Begin',
    DELETE_SUCCESS: 'department->Delete->Success',
    DELETE_FAILURE: 'department->Delete->Failed'
}

export const DESIGNATION = {
    SAVE_BEGIN: 'designation->Save->Begin',
    SAVE_SUCCESS: 'designation->Save->Success',
    SAVE_FAILURE: 'designation->Save->Failed',
    FETCH_BEGIN: 'designation->Fetch->Begin',
    FETCH_SUCCESS: 'designation->Fetch->Success',
    FETCH_FAILURE: 'designation->Fetch->Failed',
    DELETE_BEGIN: 'designation->Delete->Begin',
    DELETE_SUCCESS: 'designation->Delete->Success',
    DELETE_FAILURE: 'designation->Delete->Failed'
}

export const MAIN_GROUP = {
    SAVE_BEGIN: 'MAIN_GROUP->Save->Begin',
    SAVE_SUCCESS: 'MAIN_GROUP->Save->Success',
    SAVE_FAILURE: 'MAIN_GROUP->Save->Failed',
    FETCH_BEGIN: 'MAIN_GROUP->Fetch->Begin',
    FETCH_SUCCESS: 'MAIN_GROUP->Fetch->Success',
    FETCH_PRODUCT_BY_MAIN_GROUP_SUCCESS: 'MAIN_GROUP->FetchProductByMainGroup->Success',
    FETCH_PURITY_BY_MAIN_GROUP_SUCCESS: 'MAIN_GROUP->FetchPurityByMainGroup->Success',
    FETCH_FAILURE: 'MAIN_GROUP->Fetch->Failed',
    EDIT_BEGIN: 'MAIN_GROUP->Edit->Begin',
    EDIT_SUCCESS: 'MAIN_GROUP->Edit->Success',
    EDIT_FAILURE: 'MAIN_GROUP->Edit->Failed',
    DELETE_BEGIN: 'MAIN_GROUP->Delete->Begin',
    DELETE_SUCCESS: 'MAIN_GROUP->Delete->Success',
    DELETE_FAILURE: 'MAIN_GROUP->Delete->Failed'
}

export const BRAND = {
    SAVE_BEGIN: 'brand->Save->Begin',
    SAVE_SUCCESS: 'brand->Save->Success',
    SAVE_FAILURE: 'brand->Save->Failed',
    FETCH_BEGIN: 'brand->Fetch->Begin',
    FETCH_SUCCESS: 'brand->Fetch->Success',
    FETCH_BY_MAIN_GROUP_SUCCESS: 'brand->FetchByMainGroup->Success',
    FETCH_FAILURE: 'brand->Fetch->Failed',
    DELETE_BEGIN: 'brand->Delete->Begin',
    DELETE_SUCCESS: 'brand->Delete->Success',
    DELETE_FAILURE: 'brand->Delete->Failed'
}

export const BRAND_PRODUCT = {
    SAVE_BEGIN: 'brand_product->Save->Begin',
    SAVE_SUCCESS: 'brand_product->Save->Success',
    SAVE_FAILURE: 'brand_product->Save->Failed',
    FETCH_BEGIN: 'brand_product->Fetch->Begin',
    FETCH_SUCCESS: 'brand_product->Fetch->Success',
    FETCH_BY_BRAND_SUCCESS: 'brand_product->FetchByBrand->Success',
    FETCH_SUB_PRODUCTS_BY_PRODUCT_SUCCESS: 'brand_product->FetchSubProductsByProduct->Success',
    FETCH_FAILURE: 'brand_product->Fetch->Failed',
    DELETE_BEGIN: 'brand_product->Delete->Begin',
    DELETE_SUCCESS: 'brand_product->Delete->Success',
    DELETE_FAILURE: 'brand_product->Delete->Failed'
}

export const BRAND_SUB_PRODUCT = {
    SAVE_BEGIN: 'brand_sub_product->Save->Begin',
    SAVE_SUCCESS: 'brand_sub_product->Save->Success',
    SAVE_FAILURE: 'brand_sub_product->Save->Failed',
    FETCH_BEGIN: 'brand_sub_product->Fetch->Begin',
    FETCH_SUCCESS: 'brand_sub_product->Fetch->Success',
    FETCH_FAILURE: 'brand_sub_product->Fetch->Failed',
    DELETE_BEGIN: 'brand_sub_product->Delete->Begin',
    DELETE_SUCCESS: 'brand_sub_product->Delete->Success',
    DELETE_FAILURE: 'brand_sub_product->Delete->Failed'
}

export const ADDRESS = {
    FETCH_BEGIN: 'address->Fetch->Begin',
    FETCH_SUCCESS: 'address->Fetch->Success',
    FETCH_FAILURE: 'address->Fetch->Failed',
}

export const BANK = {
    FETCH_BEGIN: 'bank->Fetch->Begin',
    FETCH_SUCCESS: 'bank->Fetch->Success',
    FETCH_FAILURE: 'bank->Fetch->Failed',
}

export const STONE_TYPE = {
    FETCH_BEGIN: 'stone_type->Fetch->Begin',
    FETCH_SUCCESS: 'stone_type->Fetch->Success',
    FETCH_FAILURE: 'stone_type->Fetch->Failed',
}

export const STONE_GROUP = {
    SAVE_BEGIN: 'stone_group->Save->Begin',
    SAVE_SUCCESS: 'stone_group->Save->Success',
    SAVE_FAILURE: 'stone_group->Save->Failed',
    FETCH_BEGIN: 'stone_group->Fetch->Begin',
    FETCH_SUCCESS: 'stone_group->Fetch->Success',
    FETCH_BY_TYPE_SUCCESS: 'stone_group->FetchByStoneType->Success',
    FETCH_STONES_BY_GROUP_SUCCESS: 'stone_group->FetchStonesByStoneGroup->Success',
    FETCH_STONE_SIZES_BY_GROUP_SUCCESS: 'stone_group->FetchStoneSizesByStoneGroup->Success',
    FETCH_FAILURE: 'stone_group->Fetch->Failed',
    DELETE_BEGIN: 'stone_group->Delete->Begin',
    DELETE_SUCCESS: 'stone_group->Delete->Success',
    DELETE_FAILURE: 'stone_group->Delete->Failed'
}

export const STONE = {
    SAVE_BEGIN: 'stone->Save->Begin',
    SAVE_SUCCESS: 'stone->Save->Success',
    SAVE_FAILURE: 'stone->Save->Failed',
    FETCH_BEGIN: 'stone->Fetch->Begin',
    FETCH_SUCCESS: 'stone->Fetch->Success',
    FETCH_FAILURE: 'stone->Fetch->Failed',
    DELETE_BEGIN: 'stone->Delete->Begin',
    DELETE_SUCCESS: 'stone->Delete->Success',
    DELETE_FAILURE: 'stone->Delete->Failed'
}

export const STONE_SIZE = {
    SAVE_BEGIN: 'stone_size->Save->Begin',
    SAVE_SUCCESS: 'stone_size->Save->Success',
    SAVE_FAILURE: 'stone_size->Save->Failed',
    FETCH_BEGIN: 'stone_size->Fetch->Begin',
    FETCH_SUCCESS: 'stone_size->Fetch->Success',
    FETCH_FAILURE: 'stone_size->Fetch->Failed',
    DELETE_BEGIN: 'stone_size->Delete->Begin',
    DELETE_SUCCESS: 'stone_size->Delete->Success',
    DELETE_FAILURE: 'stone_size->Delete->Failed'
}

export const STONE_COLOR = {
    SAVE_BEGIN: 'stone_color->Save->Begin',
    SAVE_SUCCESS: 'stone_color->Save->Success',
    SAVE_FAILURE: 'stone_color->Save->Failed',
    FETCH_BEGIN: 'stone_color->Fetch->Begin',
    FETCH_SUCCESS: 'stone_color->Fetch->Success',
    FETCH_FAILURE: 'stone_color->Fetch->Failed',
    DELETE_BEGIN: 'stone_color->Delete->Begin',
    DELETE_SUCCESS: 'stone_color->Delete->Success',
    DELETE_FAILURE: 'stone_color->Delete->Failed'
}

export const STONE_CLARITY = {
    SAVE_BEGIN: 'stone_clarity->Save->Begin',
    SAVE_SUCCESS: 'stone_clarity->Save->Success',
    SAVE_FAILURE: 'stone_clarity->Save->Failed',
    FETCH_BEGIN: 'stone_clarity->Fetch->Begin',
    FETCH_SUCCESS: 'stone_clarity->Fetch->Success',
    FETCH_FAILURE: 'stone_clarity->Fetch->Failed',
    DELETE_BEGIN: 'stone_clarity->Delete->Begin',
    DELETE_SUCCESS: 'stone_clarity->Delete->Success',
    DELETE_FAILURE: 'stone_clarity->Delete->Failed'
}

export const STONE_POLISH = {
    SAVE_BEGIN: 'stone_polish->Save->Begin',
    SAVE_SUCCESS: 'stone_polish->Save->Success',
    SAVE_FAILURE: 'stone_polish->Save->Failed',
    FETCH_BEGIN: 'stone_polish->Fetch->Begin',
    FETCH_SUCCESS: 'stone_polish->Fetch->Success',
    FETCH_FAILURE: 'stone_polish->Fetch->Failed',
    DELETE_BEGIN: 'stone_polish->Delete->Begin',
    DELETE_SUCCESS: 'stone_polish->Delete->Success',
    DELETE_FAILURE: 'stone_polish->Delete->Failed'
}

export const PARTY_TYPE = {
    FETCH_BEGIN: 'party_type->Fetch->Begin',
    FETCH_SUCCESS: 'party_type->Fetch->Success',
    FETCH_FAILURE: 'party_type->Fetch->Failed',
}

export const PARTY_STONE_RATE = {
    SAVE_BEGIN: 'party_rate->Save->Begin',
    SAVE_SUCCESS: 'party_rate->Save->Success',
    SAVE_FAILURE: 'party_rate->Save->Failed',
    FETCH_BEGIN: 'party_rate->Fetch->Begin',
    FETCH_SUCCESS: 'party_rate->Fetch->Success',
    FETCH_FAILURE: 'party_rate->Fetch->Failed',
    DELETE_BEGIN: 'party_rate->Delete->Begin',
    DELETE_SUCCESS: 'party_rate->Delete->Success',
    DELETE_FAILURE: 'party_rate->Delete->Failed'
}

export const SALE_STONE_RATE = {
    SAVE_BEGIN: 'sale_stone_rate->Save->Begin',
    SAVE_SUCCESS: 'sale_stone_rate->Save->Success',
    SAVE_FAILURE: 'sale_stone_rate->Save->Failed',
    FETCH_BEGIN: 'sale_stone_rate->Fetch->Begin',
    FETCH_SUCCESS: 'sale_stone_rate->Fetch->Success',
    FETCH_FAILURE: 'sale_stone_rate->Fetch->Failed',
    DELETE_BEGIN: 'sale_stone_rate->Delete->Begin',
    DELETE_SUCCESS: 'sale_stone_rate->Delete->Success',
    DELETE_FAILURE: 'sale_stone_rate->Delete->Failed'
}

export const PRODUCT = {
    SAVE_BEGIN: 'product->Save->Begin',
    SAVE_SUCCESS: 'product->Save->Success',
    SAVE_FAILURE: 'product->Save->Failed',
    FETCH_BEGIN: 'product->Fetch->Begin',
    FETCH_SUCCESS: 'product->Fetch->Success',
    FETCH_SUB_PRODUCT_BY_PRODUCT_SUCCESS: 'product->FetchSubProductsByProduct->Success',
    FETCH_SIZE_BY_PRODUCT_SUCCESS: 'product->FetchSizesByProduct->Success',
    FETCH_FAILURE: 'product->Fetch->Failed',
    DELETE_BEGIN: 'product->Delete->Begin',
    DELETE_SUCCESS: 'product->Delete->Success',
    DELETE_FAILURE: 'product->Delete->Failed'
}

export const SUB_PRODUCT = {
    SAVE_BEGIN: 'sub_product->Save->Begin',
    SAVE_SUCCESS: 'sub_product->Save->Success',
    SAVE_FAILURE: 'sub_product->Save->Failed',
    FETCH_BEGIN: 'sub_product->Fetch->Begin',
    FETCH_SUCCESS: 'sub_product->Fetch->Success',
    FETCH_FAILURE: 'sub_product->Fetch->Failed',
    DELETE_BEGIN: 'sub_product->Delete->Begin',
    DELETE_SUCCESS: 'sub_product->Delete->Success',
    DELETE_FAILURE: 'sub_product->Delete->Failed'
}

export const PRODUCT_SIZE = {
    SAVE_BEGIN: 'product_size->Save->Begin',
    SAVE_SUCCESS: 'product_size->Save->Success',
    SAVE_FAILURE: 'product_size->Save->Failed',
    FETCH_BEGIN: 'product_size->Fetch->Begin',
    FETCH_SUCCESS: 'product_size->Fetch->Success',
    FETCH_FAILURE: 'product_size->Fetch->Failed',
    DELETE_BEGIN: 'product_size->Delete->Begin',
    DELETE_SUCCESS: 'product_size->Delete->Success',
    DELETE_FAILURE: 'product_size->Delete->Failed'
}

export const PURITY = {
    SAVE_BEGIN: 'purity->Save->Begin',
    SAVE_SUCCESS: 'purity->Save->Success',
    SAVE_FAILURE: 'purity->Save->Failed',
    FETCH_BEGIN: 'purity->Fetch->Begin',
    FETCH_SUCCESS: 'purity->Fetch->Success',
    FETCH_FAILURE: 'purity->Fetch->Failed',
    DELETE_BEGIN: 'purity->Delete->Begin',
    DELETE_SUCCESS: 'purity->Delete->Success',
    DELETE_FAILURE: 'purity->Delete->Failed'
}

export const PARTY_WASTAGE = {
    SAVE_BEGIN: 'party_wastage->Save->Begin',
    SAVE_SUCCESS: 'party_wastage->Save->Success',
    SAVE_FAILURE: 'party_wastage->Save->Failed',
    FETCH_BEGIN: 'party_wastage->Fetch->Begin',
    FETCH_SUCCESS: 'party_wastage->Fetch->Success',
    FETCH_BY_CRITERIA_SUCCESS: 'party_wastage->FetchByCriteria->Success',
    FETCH_FAILURE: 'party_wastage->Fetch->Failed',
    DELETE_BEGIN: 'party_wastage->Delete->Begin',
    DELETE_SUCCESS: 'party_wastage->Delete->Success',
    DELETE_FAILURE: 'party_wastage->Delete->Failed'
}

export const SALE_WASTAGE = {
    SAVE_BEGIN: 'sale_wastage->Save->Begin',
    SAVE_SUCCESS: 'sale_wastage->Save->Success',
    SAVE_FAILURE: 'sale_wastage->Save->Failed',
    FETCH_BEGIN: 'sale_wastage->Fetch->Begin',
    FETCH_SUCCESS: 'sale_wastage->Fetch->Success',
    FETCH_FAILURE: 'sale_wastage->Fetch->Failed',
    DELETE_BEGIN: 'sale_wastage->Delete->Begin',
    DELETE_SUCCESS: 'sale_wastage->Delete->Success',
    DELETE_FAILURE: 'sale_wastage->Delete->Failed',
    FETCH_BY_SUB_PRODUCT_BEGIN: 'sale_wastage_By_Sub_Product->Fetch->Begin',
    FETCH_BY_SUB_PRODUCT_SUCCESS: 'sale_wastage_By_Sub_Product->Fetch->Success',
    FETCH_BY_SUB_PRODUCT_FAILURE: 'sale_wastage_By_Sub_Product->Fetch->Failed',
}

export const FLOOR = {
    SAVE_BEGIN: 'floor->Save->Begin',
    SAVE_SUCCESS: 'floor->Save->Success',
    SAVE_FAILURE: 'floor->Save->Failed',
    FETCH_BEGIN: 'floor->Fetch->Begin',
    FETCH_SUCCESS: 'floor->Fetch->Success',
    FETCH_FAILURE: 'floor->Fetch->Failed',
    DELETE_BEGIN: 'floor->Delete->Begin',
    DELETE_SUCCESS: 'floor->Delete->Success',
    DELETE_FAILURE: 'floor->Delete->Failed'
}

export const COUNTER = {
    SAVE_BEGIN: 'counter->Save->Begin',
    SAVE_SUCCESS: 'counter->Save->Success',
    SAVE_FAILURE: 'counter->Save->Failed',
    FETCH_BEGIN: 'counter->Fetch->Begin',
    FETCH_SUCCESS: 'counter->Fetch->Success',
    FETCH_FAILURE: 'counter->Fetch->Failed',
    DELETE_BEGIN: 'counter->Delete->Begin',
    DELETE_SUCCESS: 'counter->Delete->Success',
    DELETE_FAILURE: 'counter->Delete->Failed'
}

export const HSN = {
    SAVE_BEGIN: 'hsn->Save->Begin',
    SAVE_SUCCESS: 'hsn->Save->Success',
    SAVE_FAILURE: 'hsn->Save->Failed',
    FETCH_BEGIN: 'hsn->Fetch->Begin',
    FETCH_SUCCESS: 'hsn->Fetch->Success',
    FETCH_FAILURE: 'hsn->Fetch->Failed',
    DELETE_BEGIN: 'hsn->Delete->Begin',
    DELETE_SUCCESS: 'hsn->Delete->Success',
    DELETE_FAILURE: 'hsn->Delete->Failed'
}

export const GST = {
    SAVE_BEGIN: 'gst->Save->Begin',
    SAVE_SUCCESS: 'gst->Save->Success',
    SAVE_FAILURE: 'gst->Save->Failed',
    FETCH_BEGIN: 'gst->Fetch->Begin',
    FETCH_SUCCESS: 'gst->Fetch->Success',
    FETCH_FAILURE: 'gst->Fetch->Failed',
    DELETE_BEGIN: 'gst->Delete->Begin',
    DELETE_SUCCESS: 'gst->Delete->Success',
    DELETE_FAILURE: 'gst->Delete->Failed',
    METAL_FETCH_BEGIN: 'metal->Fetch->Begin',
    METAL_FETCH_SUCCESS: 'metal->Fetch->Success',
    METAL_FETCH_FAILURE: 'metal->Fetch->Failed',
}

export const RATE = {
    SAVE_BEGIN: 'rate->Save->Begin',
    SAVE_SUCCESS: 'rate->Save->Success',
    SAVE_FAILURE: 'rate->Save->Failed',
    FETCH_BEGIN: 'rate->Fetch->Begin',
    FETCH_SUCCESS: 'rate->Fetch->Success',
    FETCH_CALCULATED_RATES_SUCCESS: 'rate->FetchCalculatedRates->Success',
    FETCH_RATES_BY_MAIN_GROUP_SUCCESS: 'rate->FetchRatesByMainGroup->Success',
    FETCH_FAILURE: 'rate->Fetch->Failed'
}

export const EMPLOYEES = {
    SAVE_BEGIN: 'employees->Save->Begin',
    SAVE_SUCCESS: 'employees->Save->Success',
    SAVE_FAILURE: 'employees->Save->Failed',
    FETCH_BEGIN: 'employees->Fetch->Begin',
    FETCH_SUCCESS: 'employees->Fetch->Success',
    FETCH_FAILURE: 'employees->Fetch->Failed',
    DELETE_BEGIN: 'employees->Delete->Begin',
    DELETE_SUCCESS: 'employees->Delete->Success',
    DELETE_FAILURE: 'employees->Delete->Failed',
    LIST_FETCH_BEGIN: 'employees->FetchList->Begin',
    LIST_FETCH_SUCCESS: 'employees->FetchList->Success',
    LIST_FETCH_FAILURE: 'employees->FetchList->Failed',
}

export const BRANCH = {
    SAVE_BEGIN: 'branch->Save->Begin',
    SAVE_SUCCESS: 'branch->Save->Success',
    SAVE_FAILURE: 'branch->Save->Failed',
    FETCH_BEGIN: 'branch->Fetch->Begin',
    FETCH_SUCCESS: 'branch->Fetch->Success',
    FETCH_FAILURE: 'branch->Fetch->Failed',
    DELETE_BEGIN: 'branch->Delete->Begin',
    DELETE_SUCCESS: 'branch->Delete->Success',
    DELETE_FAILURE: 'branch->Delete->Failed'
}

export const SUPPLIER = {
    SAVE_BEGIN: 'supplier->Save->Begin',
    SAVE_SUCCESS: 'supplier->Save->Success',
    SAVE_FAILURE: 'supplier->Save->Failed',
    FETCH_BEGIN: 'supplier->Fetch->Begin',
    FETCH_SUCCESS: 'supplier->Fetch->Success',
    FETCH_FAILURE: 'supplier->Fetch->Failed',
    DELETE_BEGIN: 'supplier->Delete->Begin',
    DELETE_SUCCESS: 'supplier->Delete->Success',
    DELETE_FAILURE: 'supplier->Delete->Failed'
}

export const STONEDETAILS = {
    SAVE_BEGIN: 'stone_details->Save->Begin',
    SAVE_SUCCESS: 'stone_details->Save->Success',
    SAVE_FAILURE: 'stone_details->Save->Failed',
    EDIT_BEGIN: 'stone_details->edit->Begin',
    EDIT_SUCCESS: 'stone_details->edit->Success',
    EDIT_FAILURE: 'stone_details->edit->Failed',
    FETCH_BEGIN: 'stone_details->Fetch->Begin',
    FETCH_SUCCESS: 'stone_details->Fetch->Success',
    FETCH_FAILURE: 'stone_details->Fetch->Failed',
    DELETE_BEGIN: 'stone_details->Delete->Begin',
    DELETE_SUCCESS: 'stone_details->Delete->Success',
    DELETE_FAILURE: 'stone_details->Delete->Failed',
    SAVE_STONE_SUCCESS: 'stone_details->Save_Stone',
    ClEAR_DETAILS: 'stone_details->Clear_Details',
    SAVE_STONE_DETAILS: 'save_stone_details->Save->Success',
}

export const GEMSTONE_PURCHASE = {
    SAVE_BEGIN: 'gemstone_purchase->Save->Begin',
    SAVE_SUCCESS: 'gemstone_purchase->Save->Success',
    SAVE_FAILURE: 'gemstone_purchase->Save->Failed',
    FETCH_BEGIN: 'gemstone_purchase->Fetch->Begin',
    FETCH_SUCCESS: 'gemstone_purchase->Fetch->Success',
    FETCH_FAILURE: 'gemstone_purchase->Fetch->Failed',
    DELETE_BEGIN: 'gemstone_purchase->Delete->Begin',
    DELETE_SUCCESS: 'gemstone_purchase->Delete->Success',
    DELETE_FAILURE: 'gemstone_purchase->Delete->Failed'
}

export const BRANDED_PURCHASE = {
    SAVE_BEGIN: 'branded_purchase->Save->Begin',
    SAVE_SUCCESS: 'branded_purchase->Save->Success',
    SAVE_FAILURE: 'branded_purchase->Save->Failed',
    FETCH_BEGIN: 'branded_purchase->Fetch->Begin',
    FETCH_SUCCESS: 'branded_purchase->Fetch->Success',
    FETCH_FAILURE: 'branded_purchase->Fetch->Failed',
    DELETE_BEGIN: 'branded_purchase->Delete->Begin',
    DELETE_SUCCESS: 'branded_purchase->Delete->Success',
    DELETE_FAILURE: 'branded_purchase->Delete->Failed'
}

export const METAL_LOT = {
    SAVE_BEGIN: 'metal_lot->Save->Begin',
    SAVE_SUCCESS: 'metal_lot->Save->Success',
    SAVE_FAILURE: 'metal_lot->Save->Failed',
    FETCH_BEGIN: 'metal_lot->Fetch->Begin',
    FETCH_SUCCESS: 'metal_lot->Fetch->Success',
    FETCH_FAILURE: 'metal_lot->Fetch->Failed',
    DELETE_BEGIN: 'metal_lot->Delete->Begin',
    DELETE_SUCCESS: 'metal_lot->Delete->Success',
    DELETE_FAILURE: 'metal_lot->Delete->Failed',
    FETCH_DETAILS_BEGIN: 'metal_lot->FetchDetails->Begin',
    FETCH_DETAILS_SUCCESS: 'metal_lot->FetchDetails->Success',
    FETCH_DETAILS_FAILURE: 'metal_lot->FetchDetails->Failed',
}

export const BRANDED_LOT = {
    SAVE_BEGIN: 'branded_lot->Save->Begin',
    SAVE_SUCCESS: 'branded_lot->Save->Success',
    SAVE_FAILURE: 'branded_lot->Save->Failed',
    FETCH_BEGIN: 'branded_lot->Fetch->Begin',
    FETCH_SUCCESS: 'branded_lot->Fetch->Success',
    FETCH_FAILURE: 'branded_lot->Fetch->Failed',
    DELETE_BEGIN: 'branded_lot->Delete->Begin',
    DELETE_SUCCESS: 'branded_lot->Delete->Success',
    DELETE_FAILURE: 'branded_lot->Delete->Failed',
    FETCH_DETAILS_BEGIN: 'branded_lot->FetchDetails->Begin',
    FETCH_DETAILS_SUCCESS: 'branded_lot->FetchDetails->Success',
    FETCH_DETAILS_FAILURE: 'branded_lot->FetchDetails->Failed',
}

export const GEMSTONE_LOT = {
    SAVE_BEGIN: 'gemstone_lot->Save->Begin',
    SAVE_SUCCESS: 'gemstone_lot->Save->Success',
    SAVE_FAILURE: 'gemstone_lot->Save->Failed',
    FETCH_BEGIN: 'gemstone_lot->Fetch->Begin',
    FETCH_SUCCESS: 'gemstone_lot->Fetch->Success',
    FETCH_FAILURE: 'gemstone_lot->Fetch->Failed',
    DELETE_BEGIN: 'gemstone_lot->Delete->Begin',
    DELETE_SUCCESS: 'gemstone_lot->Delete->Success',
    DELETE_FAILURE: 'gemstone_lot->Delete->Failed',
    FETCH_DETAILS_BEGIN: 'gemstone_lot->FetchDetails->Begin',
    FETCH_DETAILS_SUCCESS: 'gemstone_lot->FetchDetails->Success',
    FETCH_DETAILS_FAILURE: 'gemstone_lot->FetchDetails->Failed',
}

export const METAL_PURCHASE = {
    SAVE_BEGIN: 'metal_purchase->Save->Begin',
    SAVE_SUCCESS: 'metal_purchase->Save->Success',
    SAVE_FAILURE: 'metal_purchase->Save->Failed',
    FETCH_BEGIN: 'metal_purchase->Fetch->Begin',
    FETCH_SUCCESS: 'metal_purchase->Fetch->Success',
    FETCH_FAILURE: 'metal_purchase->Fetch->Failed',
    DELETE_BEGIN: 'metal_purchase->Delete->Begin',
    DELETE_SUCCESS: 'metal_purchase->Delete->Success',
    DELETE_FAILURE: 'metal_purchase->Delete->Failed',
    PRODUCT_SAVE_BEGIN: 'metal_purchase_product->Save->Begin',
    PRODUCT_SAVE_SUCCESS: 'metal_purchase_product->Save->Success',
    PRODUCT_SAVE_FAILURE: 'metal_purchase_product->Save->Failed',
    PRODUCT_FETCH_BEGIN: 'metal_purchase_product->Fetch->Begin',
    PRODUCT_FETCH_SUCCESS: 'metal_purchase_product->Fetch->Success',
    PRODUCT_FETCH_FAILURE: 'metal_purchase_product->Fetch->Failed',
    PRODUCT_DELETE_BEGIN: 'metal_purchase_product->Delete->Begin',
    PRODUCT_DELETE_SUCCESS: 'metal_purchase_product->Delete->Success',
    PRODUCT_DELETE_FAILURE: 'metal_purchase_product->Delete->Failed'
}

export const GEMSTONE_BARCODE = {
    SAVE_BEGIN: 'gemstone_barcode->Save->Begin',
    SAVE_SUCCESS: 'gemstone_barcode->Save->Success',
    SAVE_FAILURE: 'gemstone_barcode->Save->Failed',
    FETCH_BEGIN: 'gemstone_barcode->Fetch->Begin',
    FETCH_SUCCESS: 'gemstone_barcode->Fetch->Success',
    FETCH_FAILURE: 'gemstone_barcode->Fetch->Failed',
    LOT_BEGIN: 'gemstone_barcode_lot->Fetch->Begin',
    LOT_SUCCESS: 'gemstone_barcode_lot->Fetch->Success',
    LOT_FAILURE: 'gemstone_barcode_lot->Fetch->Failed',
}

export const BRANDED_BARCODE = {
    SAVE_BEGIN: 'branded_barcode->Save->Begin',
    SAVE_SUCCESS: 'branded_barcode->Save->Success',
    SAVE_FAILURE: 'branded_barcode->Save->Failed',
    FETCH_LOT_BEGIN: 'branded_barcode->FetchLotDetails->Begin',
    FETCH_LOT_SUCCESS: 'branded_barcode->FetchLotDetails->Success',
    FETCH_LOT_FAILURE: 'branded_barcode->FetchLotDetails->Failed'
}

export const METAL_BARCODE = {
    SAVE_BEGIN: 'metal_barcode->Save->Begin',
    SAVE_SUCCESS: 'metal_barcode->Save->Success',
    SAVE_FAILURE: 'metal_barcode->Save->Failed',
    FETCH_BEGIN: 'metal_barcode->Fetch->Begin',
    FETCH_SUCCESS: 'metal_barcode->Fetch->Success',
    FETCH_FAILURE: 'metal_barcode->Fetch->Failed',
    DELETE_BEGIN: 'metal_barcode->Delete->Begin',
    DELETE_SUCCESS: 'metal_barcode->Delete->Success',
    DELETE_FAILURE: 'metal_barcode->Delete->Failed',
    LOT_FETCH_BEGIN: 'lot->Fetch->Begin',
    LOT_FETCH_SUCCESS: 'lot->Fetch->Success',
    LOT_FETCH_FAILURE: 'lot->Fetch->Failed',
}
