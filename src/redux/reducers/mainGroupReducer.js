import { MAIN_GROUP } from '../constants/action-types'

const initialState = {
    allGroups: {},
    allProductsByMainGroup: {},
    allPuritiesByMainGroup: {},
    mainGroupInfo: {},
    loading: false,
    error: null
}

const MainGroupReducer = (state = initialState, action) => {
    switch (action.type) {
        case MAIN_GROUP.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case MAIN_GROUP.SAVE_SUCCESS:
            return {
                ...state,
                mainGroupInfo: action.data,
                loading: false
            }
        case MAIN_GROUP.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case MAIN_GROUP.EDIT_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case MAIN_GROUP.EDIT_SUCCESS:
            return {
                ...state,
                mainGroupInfo: action.data,
                loading: false
            }
        case MAIN_GROUP.EDIT_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case MAIN_GROUP.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case MAIN_GROUP.FETCH_SUCCESS:
            return {
                ...state,
                allGroups: action.data,
                loading: false
            }
        case MAIN_GROUP.FETCH_PRODUCT_BY_MAIN_GROUP_SUCCESS:
            return {
                ...state,
                allProductsByMainGroup: action.data,
                loading: false
            }
        case MAIN_GROUP.FETCH_PURITY_BY_MAIN_GROUP_SUCCESS:
            return {
                ...state,
                allPuritiesByMainGroup: action.data,
                loading: false
            }
        case MAIN_GROUP.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case MAIN_GROUP.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case MAIN_GROUP.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                mainGroupInfo: action.data
            }
        case MAIN_GROUP.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default MainGroupReducer