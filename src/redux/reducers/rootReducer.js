import { combineReducers } from 'redux'
import authReducer from './authReducer'
import departmentReducer from './departmentReducer'
import designationReducer from './designationReducer';
import brandReducer from './creations/brandReducer';
import brandProductReducer from './creations/brandProductReducer';
import brandSubProductReducer from './creations/brandSubProductReducer';
import MainGroupReducer from './mainGroupReducer';
import addressReducer from './addressReducer'
import stoneTypeReducer from './creations/stoneTypeReducer'
import stoneGroupReducer from './creations/stoneGroupReducer'
import stoneReducer from './creations/stoneReducer'
import stoneSizeReducer from './creations/stoneSizeReducer'
import stoneColorReducer from './creations/stoneColorReducer'
import stoneClarityReducer from './creations/stoneClarityReducer'
import stonePolishReducer from './creations/stonePolishReducer'
import partyTypeReducer from './creations/partyTypeReducer'
import partyStoneRateReducer from './creations/partyStoneRateReducer'
import saleStoneRateReducer from './creations/saleStoneRateReducer'
import productReducer from './creations/productReducer'
import subProductReducer from './creations/subProductReducer'
import productSizeReducer from './creations/productSizeReducer'
import purityReducer from './creations/purityReducer'
import partyWastageReducer from './creations/partyWastageReducer'
import saleWastageReducer from './creations/saleWastageReducer'
import floorReducer from './creations/floorReducer'
import counterReducer from './creations/counterReducer'
import hsnReducer from './creations/hsnReducer'
import gstReducer from './creations/gstReducer'
import rateReducer from './creations/rateReducer'
import bankReducer from './bankReducer';
import branchReducer from './Admin/branchReducer'
import employeeReducer from './Admin/employeeReducer'
import supplierReducer from './Admin/supplierReducer'
import stoneDetailsReducer from './transactions/stoneDetailsReducer'
import gemstonePurchaseReducer from './transactions/inventory/gemstonePurchaseReducer'
import brandedPurchaseReducer from './transactions/inventory/brandedPurchaseReducer'
import brandedLotReducer from './transactions/barcode/brandedLotReducer'
import metalLotReducer from './transactions/barcode/metalLotReducer'
import gemstoneLotReducer from './transactions/barcode/gemstoneLotReducer'
import gemstoneBarcodeReducer from './transactions/barcode/gemstoneBarcodeReducer'
import metalPurchaseReducer from './transactions/inventory/metalPurchaseReducer';
import brandedBarcodeReducer from './transactions/barcode/brandedBarcodeReducer';
import metalBarcodeReducer from './transactions/barcode/metalBarcodeReducer';

const rootReducer = combineReducers({
    authReducer,
    departmentReducer,
    designationReducer,
    brandReducer,
    brandProductReducer,
    brandSubProductReducer,
    MainGroupReducer,
    addressReducer,
    stoneTypeReducer,
    stoneGroupReducer,
    stoneReducer,
    stoneSizeReducer,
    stoneColorReducer,
    stoneClarityReducer,
    stonePolishReducer,
    partyTypeReducer,
    partyStoneRateReducer,
    saleStoneRateReducer,
    productReducer,
    subProductReducer,
    productSizeReducer,
    purityReducer,
    partyWastageReducer,
    saleWastageReducer,
    floorReducer,
    counterReducer,
    hsnReducer,
    gstReducer,
    rateReducer,
    bankReducer,
    branchReducer,
    employeeReducer,
    supplierReducer,
    stoneDetailsReducer,
    gemstonePurchaseReducer,
    brandedPurchaseReducer,
    brandedLotReducer,
    metalLotReducer,
    gemstoneLotReducer,
    gemstoneBarcodeReducer,
    metalPurchaseReducer,
    brandedBarcodeReducer,
    metalBarcodeReducer
});

export default rootReducer