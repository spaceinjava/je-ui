import { BRANCH } from '../../constants/action-types'

const initialState = {
    allBranches: {},
    branchInfo: {},
    loading: false,
    error: null
}

const branchReducer = (state = initialState, action) => {
    switch (action.type) {
        case BRANCH.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRANCH.SAVE_SUCCESS:
            return {
                ...state,
                branchInfo: action.data,
                loading: false
            }
        case BRANCH.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case BRANCH.EDIT_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRANCH.EDIT_SUCCESS:
            return {
                ...state,
                branchInfo: action.data,
                loading: false
            }
        case BRANCH.EDIT_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case BRANCH.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRANCH.FETCH_SUCCESS:
            return {
                ...state,
                allBranches: action.data,
                loading: false
            }
       
        case BRANCH.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case BRANCH.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRANCH.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                branchInfo: action.data
            }
        case BRANCH.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default branchReducer