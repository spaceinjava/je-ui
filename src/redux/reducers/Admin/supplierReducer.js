import { SUPPLIER } from '../../constants/action-types'

const initialState = {
    allSuppliers: {},
    supplierInfo: {},
    loading: false,
    error: null
}

const supplierReducer = (state = initialState, action) => {
    switch (action.type) {
        case SUPPLIER.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case SUPPLIER.SAVE_SUCCESS:
            return {
                ...state,
                supplierInfo: action.data,
                loading: false
            }
        case SUPPLIER.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case SUPPLIER.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case SUPPLIER.FETCH_SUCCESS:
            return {
                ...state,
                allSuppliers: action.data,
                loading: false
            }
        case SUPPLIER.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case SUPPLIER.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case SUPPLIER.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                supplierInfo: action.data
            }
        case SUPPLIER.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default supplierReducer