import { EMPLOYEES } from '../../constants/action-types'

const initialState = {
    allEmployees: {},
    allEmployeesList: {},
    EmployeeInfo: {},
    loading: false,
    error: null
}

const employeeReducer = (state = initialState, action) => {
    switch (action.type) {
        case EMPLOYEES.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case EMPLOYEES.SAVE_SUCCESS:
            return {
                ...state,
                EmployeeInfo: action.data,
                loading: false
            }
        case EMPLOYEES.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case EMPLOYEES.EDIT_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case EMPLOYEES.EDIT_SUCCESS:
            return {
                ...state,
                EmployeeInfo: action.data,
                loading: false
            }
        case EMPLOYEES.EDIT_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case EMPLOYEES.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case EMPLOYEES.FETCH_SUCCESS:
            return {
                ...state,
                allEmployees: action.data,
                loading: false
            }
       
        case EMPLOYEES.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
            case EMPLOYEES.LIST_FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case EMPLOYEES.LIST_FETCH_SUCCESS:
            return {
                ...state,
                allEmployeesList: action.data,
                loading: false
            }
       
        case EMPLOYEES.LIST_FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case EMPLOYEES.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case EMPLOYEES.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                EmployeeInfo: action.data
            }
        case EMPLOYEES.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default employeeReducer