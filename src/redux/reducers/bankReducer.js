import { BANK } from '../constants/action-types'

const initialState = {
    allBanks: {},
    selectedBank: {},
    loading: false,
    error: null
}

const bankReducer = (state = initialState, action) => {
    switch (action.type) {
        case BANK.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BANK.FETCH_SUCCESS:
            return {
                ...state,
                allBanks: action.data,
                loading: false
            }
        case BANK.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default bankReducer