import { STONE_COLOR } from '../../constants/action-types'

const initialState = {
    allStoneColors: {},
    stoneColorInfo: {},
    loading: false,
    error: null
}

const stoneColorReducer = (state = initialState, action) => {
    switch (action.type) {
        case STONE_COLOR.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE_COLOR.SAVE_SUCCESS:
            return {
                ...state,
                stoneColorInfo: action.data,
                loading: false
            }
        case STONE_COLOR.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case STONE_COLOR.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE_COLOR.FETCH_SUCCESS:
            return {
                ...state,
                allStoneColors: action.data,
                loading: false
            }
        case STONE_COLOR.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case STONE_COLOR.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE_COLOR.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                stoneColorInfo: action.data
            }
        case STONE_COLOR.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default stoneColorReducer