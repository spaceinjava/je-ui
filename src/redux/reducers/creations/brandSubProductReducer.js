import { BRAND_SUB_PRODUCT } from '../../constants/action-types'

const initialState = {
    allBrandSubProducts: {},
    brandSubProductInfo: {},
    loading: false,
    error: null
}

const brandSubProductReducer = (state = initialState, action) => {
    switch (action.type) {
        case BRAND_SUB_PRODUCT.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRAND_SUB_PRODUCT.SAVE_SUCCESS:
            return {
                ...state,
                brandSubProductInfo: action.data,
                loading: false
            }
        case BRAND_SUB_PRODUCT.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case BRAND_SUB_PRODUCT.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRAND_SUB_PRODUCT.FETCH_SUCCESS:
            return {
                ...state,
                allBrandSubProducts: action.data,
                loading: false
            }
        case BRAND_SUB_PRODUCT.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case BRAND_SUB_PRODUCT.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRAND_SUB_PRODUCT.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                brandSubProductInfo: action.data
            }
        case BRAND_SUB_PRODUCT.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default brandSubProductReducer