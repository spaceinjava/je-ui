import { STONE } from '../../constants/action-types'

const initialState = {
    allStones: {},
    stoneInfo: {},
    loading: false,
    error: null
}

const stoneReducer = (state = initialState, action) => {
    switch (action.type) {
        case STONE.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE.SAVE_SUCCESS:
            return {
                ...state,
                stoneInfo: action.data,
                loading: false
            }
        case STONE.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case STONE.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE.FETCH_SUCCESS:
            return {
                ...state,
                allStones: action.data,
                loading: false
            }
        case STONE.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case STONE.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                stoneInfo: action.data
            }
        case STONE.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default stoneReducer