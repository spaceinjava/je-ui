import { SUB_PRODUCT } from '../../constants/action-types'

const initialState = {
    allSubProducts: {},
    subProductInfo: {},
    loading: false,
    error: null
}

const subProductReducer = (state = initialState, action) => {
    switch (action.type) {
        case SUB_PRODUCT.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case SUB_PRODUCT.SAVE_SUCCESS:
            return {
                ...state,
                subProductInfo: action.data,
                loading: false
            }
        case SUB_PRODUCT.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case SUB_PRODUCT.EDIT_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case SUB_PRODUCT.EDIT_SUCCESS:
            return {
                ...state,
                subProductInfo: action.data,
                loading: false
            }
        case SUB_PRODUCT.EDIT_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case SUB_PRODUCT.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case SUB_PRODUCT.FETCH_SUCCESS:
            return {
                ...state,
                allSubProducts: action.data,
                loading: false
            }
        case SUB_PRODUCT.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case SUB_PRODUCT.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case SUB_PRODUCT.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                subProductInfo: action.data
            }
        case SUB_PRODUCT.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default subProductReducer