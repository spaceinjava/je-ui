import { BRAND_PRODUCT } from '../../constants/action-types'

const initialState = {
    allBrandProducts: {},
    allBrandProductsByBrand: {},
    allSubProductsByProduct: {},
    brandProductInfo: {},
    loading: false,
    error: null
}

const brandProductReducer = (state = initialState, action) => {
    switch (action.type) {
        case BRAND_PRODUCT.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRAND_PRODUCT.SAVE_SUCCESS:
            return {
                ...state,
                brandProductInfo: action.data,
                loading: false
            }
        case BRAND_PRODUCT.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case BRAND_PRODUCT.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRAND_PRODUCT.FETCH_SUCCESS:
            return {
                ...state,
                allBrandProducts: action.data,
                loading: false
            }
        case BRAND_PRODUCT.FETCH_BY_BRAND_SUCCESS:
            return {
                ...state,
                allBrandProductsByBrand: action.data,
                loading: false
            }
        case BRAND_PRODUCT.FETCH_SUB_PRODUCTS_BY_PRODUCT_SUCCESS:
            return {
                ...state,
                allSubProductsByProduct: action.data,
                loading: false
            }
        case BRAND_PRODUCT.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case BRAND_PRODUCT.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRAND_PRODUCT.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                brandProductInfo: action.data
            }
        case BRAND_PRODUCT.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default brandProductReducer