import { PURITY } from '../../constants/action-types'

const initialState = {
    allPurities: {},
    purityInfo: {},
    loading: false,
    error: null
}

const purityReducer = (state = initialState, action) => {
    switch (action.type) {
        case PURITY.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case PURITY.SAVE_SUCCESS:
            return {
                ...state,
                purityInfo: action.data,
                loading: false
            }
        case PURITY.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case PURITY.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case PURITY.FETCH_SUCCESS:
            return {
                ...state,
                allPurities: action.data,
                loading: false
            }
        case PURITY.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case PURITY.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case PURITY.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                purityInfo: action.data
            }
        case PURITY.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default purityReducer