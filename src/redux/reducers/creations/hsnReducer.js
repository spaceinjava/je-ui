import { HSN } from '../../constants/action-types'

const initialState = {
    allHsnCodes: {},
    hsnCodeInfo: {},
    loading: false,
    error: null
}

const hsnReducer = (state = initialState, action) => {
    switch (action.type) {
        case HSN.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case HSN.SAVE_SUCCESS:
            return {
                ...state,
                hsnCodeInfo: action.data,
                loading: false
            }
        case HSN.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case HSN.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case HSN.FETCH_SUCCESS:
            return {
                ...state,
                allHsnCodes: action.data,
                loading: false
            }
        case HSN.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case HSN.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case HSN.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                hsnCodeInfo: action.data
            }
        case HSN.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default hsnReducer