import { SALE_WASTAGE } from '../../constants/action-types'

const initialState = {
    allSaleWastages: {},
    saleWastageInfo: {},
    saleWastageBySubProduct: {},
    loading: false,
    error: null
}

const saleWastageReducer = (state = initialState, action) => {
    switch (action.type) {
        case SALE_WASTAGE.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case SALE_WASTAGE.SAVE_SUCCESS:
            return {
                ...state,
                saleWastageInfo: action.data,
                loading: false
            }
        case SALE_WASTAGE.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case SALE_WASTAGE.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case SALE_WASTAGE.FETCH_SUCCESS:
            return {
                ...state,
                allSaleWastages: action.data,
                loading: false
            }
        case SALE_WASTAGE.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case SALE_WASTAGE.FETCH_BY_SUB_PRODUCT_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case SALE_WASTAGE.FETCH_BY_SUB_PRODUCT_SUCCESS:
            return {
                ...state,
                saleWastageBySubProduct: action.data,
                loading: false
            }
        case SALE_WASTAGE.FETCH_BY_SUB_PRODUCT_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case SALE_WASTAGE.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case SALE_WASTAGE.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                saleWastageInfo: action.data
            }
        case SALE_WASTAGE.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default saleWastageReducer