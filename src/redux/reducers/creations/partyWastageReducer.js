import { PARTY_WASTAGE } from '../../constants/action-types'

const initialState = {
    allPartyWastages: {},
    partyWastagesByCriteria: {},
    partyWastageInfo: {},
    loading: false,
    error: null
}

const partyWastageReducer = (state = initialState, action) => {
    switch (action.type) {
        case PARTY_WASTAGE.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case PARTY_WASTAGE.SAVE_SUCCESS:
            return {
                ...state,
                partyWastageInfo: action.data,
                loading: false
            }
        case PARTY_WASTAGE.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case PARTY_WASTAGE.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case PARTY_WASTAGE.FETCH_SUCCESS:
            return {
                ...state,
                allPartyWastages: action.data,
                loading: false
            }
        case PARTY_WASTAGE.FETCH_BY_CRITERIA_SUCCESS:
            return {
                ...state,
                partyWastagesByCriteria: action.data,
                loading: false
            }
        case PARTY_WASTAGE.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case PARTY_WASTAGE.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case PARTY_WASTAGE.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                partyWastageInfo: action.data
            }
        case PARTY_WASTAGE.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default partyWastageReducer