import { PARTY_TYPE } from '../../constants/action-types'

const initialState = {
    allPartyTypes: {},
    partyTypeInfo: {},
    loading: false,
    error: null
}

const partyTypeReducer = (state = initialState, action) => {
    switch (action.type) {
        case PARTY_TYPE.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case PARTY_TYPE.FETCH_SUCCESS:
            return {
                ...state,
                allPartyTypes: action.data,
                loading: false
            }
        case PARTY_TYPE.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default partyTypeReducer