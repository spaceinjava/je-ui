import { PRODUCT } from '../../constants/action-types'

const initialState = {
    allProducts: {},
    allSubProductsByProduct: {},
    allSizesByProduct: {},
    productInfo: {},
    loading: false,
    error: null
}

const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case PRODUCT.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case PRODUCT.SAVE_SUCCESS:
            return {
                ...state,
                productInfo: action.data,
                loading: false
            }
        case PRODUCT.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case PRODUCT.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case PRODUCT.FETCH_SUCCESS:
            return {
                ...state,
                allProducts: action.data,
                loading: false
            }
        case PRODUCT.FETCH_SUB_PRODUCT_BY_PRODUCT_SUCCESS:
            return {
                ...state,
                allSubProductsByProduct: action.data,
                loading: false
            }
        case PRODUCT.FETCH_SIZE_BY_PRODUCT_SUCCESS:
            return {
                ...state,
                allSizesByProduct: action.data,
                loading: false
            }
        case PRODUCT.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case PRODUCT.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case PRODUCT.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                productInfo: action.data
            }
        case PRODUCT.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default productReducer