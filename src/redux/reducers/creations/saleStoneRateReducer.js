import { SALE_STONE_RATE } from '../../constants/action-types'

const initialState = {
    allSaleStoneRates: {},
    saleStoneRateInfo: {},
    loading: false,
    error: null
}

const saleStoneRateReducer = (state = initialState, action) => {
    switch (action.type) {
        case SALE_STONE_RATE.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case SALE_STONE_RATE.SAVE_SUCCESS:
            return {
                ...state,
                saleStoneRateInfo: action.data,
                loading: false
            }
        case SALE_STONE_RATE.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case SALE_STONE_RATE.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case SALE_STONE_RATE.FETCH_SUCCESS:
            return {
                ...state,
                allSaleStoneRates: action.data,
                loading: false
            }
        case SALE_STONE_RATE.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case SALE_STONE_RATE.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case SALE_STONE_RATE.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                saleStoneRateInfo: action.data
            }
        case SALE_STONE_RATE.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default saleStoneRateReducer