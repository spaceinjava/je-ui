import { STONE_CLARITY } from '../../constants/action-types'

const initialState = {
    allStoneClarities: {},
    stoneClarityInfo: {},
    loading: false,
    error: null
}

const stoneClarityReducer = (state = initialState, action) => {
    switch (action.type) {
        case STONE_CLARITY.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE_CLARITY.SAVE_SUCCESS:
            return {
                ...state,
                stoneClarityInfo: action.data,
                loading: false
            }
        case STONE_CLARITY.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case STONE_CLARITY.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE_CLARITY.FETCH_SUCCESS:
            return {
                ...state,
                allStoneClarities: action.data,
                loading: false
            }
        case STONE_CLARITY.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case STONE_CLARITY.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE_CLARITY.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                stoneClarityInfo: action.data
            }
        case STONE_CLARITY.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default stoneClarityReducer