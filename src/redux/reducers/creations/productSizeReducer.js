import { PRODUCT_SIZE } from '../../constants/action-types'

const initialState = {
    allProductSizes: {},
    productSizeInfo: {},
    loading: false,
    error: null
}

const productSizeReducer = (state = initialState, action) => {
    switch (action.type) {
        case PRODUCT_SIZE.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case PRODUCT_SIZE.SAVE_SUCCESS:
            return {
                ...state,
                productSizeInfo: action.data,
                loading: false
            }
        case PRODUCT_SIZE.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case PRODUCT_SIZE.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case PRODUCT_SIZE.FETCH_SUCCESS:
            return {
                ...state,
                allProductSizes: action.data,
                loading: false
            }
        case PRODUCT_SIZE.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case PRODUCT_SIZE.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case PRODUCT_SIZE.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                productSizeInfo: action.data
            }
        case PRODUCT_SIZE.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default productSizeReducer