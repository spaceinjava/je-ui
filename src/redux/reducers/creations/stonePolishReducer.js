import { STONE_POLISH } from '../../constants/action-types'

const initialState = {
    allStonePolishes: {},
    stonePolishInfo: {},
    loading: false,
    error: null
}

const stonePolishReducer = (state = initialState, action) => {
    switch (action.type) {
        case STONE_POLISH.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE_POLISH.SAVE_SUCCESS:
            return {
                ...state,
                stonePolishInfo: action.data,
                loading: false
            }
        case STONE_POLISH.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case STONE_POLISH.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE_POLISH.FETCH_SUCCESS:
            return {
                ...state,
                allStonePolishes: action.data,
                loading: false
            }
        case STONE_POLISH.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case STONE_POLISH.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE_POLISH.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                stonePolishInfo: action.data
            }
        case STONE_POLISH.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default stonePolishReducer