import { FLOOR } from '../../constants/action-types'

const initialState = {
    allFloors: {},
    floorInfo: {},
    loading: false,
    error: null
}

const floorReducer = (state = initialState, action) => {
    switch (action.type) {
        case FLOOR.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case FLOOR.SAVE_SUCCESS:
            return {
                ...state,
                floorInfo: action.data,
                loading: false
            }
        case FLOOR.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case FLOOR.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case FLOOR.FETCH_SUCCESS:
            return {
                ...state,
                allFloors: action.data,
                loading: false
            }
        case FLOOR.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case FLOOR.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case FLOOR.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                floorInfo: action.data
            }
        case FLOOR.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default floorReducer