import { GST } from '../../constants/action-types'

const initialState = {
    allgst: {},
    gstInfo: {},
    metalTypes: {},
    loading: false,
    error: null
}

const gstReducer = (state = initialState, action) => {
    switch (action.type) {
        case GST.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case GST.SAVE_SUCCESS:
            return {
                ...state,
                gstInfo: action.data,
                loading: false
            }
        case GST.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case GST.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case GST.FETCH_SUCCESS:
            return {
                ...state,
                allgst: action.data,
                loading: false
            }
        case GST.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case GST.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case GST.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                gstInfo: action.data
            }
        case GST.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case GST.METAL_FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case GST.METAL_FETCH_SUCCESS:
            return {
                ...state,
                metalTypes: action.data,
                loading: false
            }
        case GST.METAL_FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default gstReducer