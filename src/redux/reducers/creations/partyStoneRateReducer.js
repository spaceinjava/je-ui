import { PARTY_STONE_RATE } from '../../constants/action-types'

const initialState = {
    allPartyStoneRates: {},
    partyStoneRateInfo: {},
    loading: false,
    error: null
}

const partyStoneRateReducer = (state = initialState, action) => {
    switch (action.type) {
        case PARTY_STONE_RATE.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case PARTY_STONE_RATE.SAVE_SUCCESS:
            return {
                ...state,
                partyStoneRateInfo: action.data,
                loading: false
            }
        case PARTY_STONE_RATE.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case PARTY_STONE_RATE.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case PARTY_STONE_RATE.FETCH_SUCCESS:
            return {
                ...state,
                allPartyStoneRates: action.data,
                loading: false
            }
        case PARTY_STONE_RATE.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case PARTY_STONE_RATE.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case PARTY_STONE_RATE.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                partyStoneRateInfo: action.data
            }
        case PARTY_STONE_RATE.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default partyStoneRateReducer