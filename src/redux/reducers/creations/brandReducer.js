import { BRAND } from '../../constants/action-types'

const initialState = {
    allBrands: {},
    allMainGroupBrands: {},
    BrandInfo: {},
    loading: false,
    error: null
}

const brandReducer = (state = initialState, action) => {
    switch (action.type) {
        case BRAND.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRAND.SAVE_SUCCESS:
            return {
                ...state,
                BrandInfo: action.data,
                loading: false
            }
        case BRAND.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case BRAND.EDIT_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRAND.EDIT_SUCCESS:
            return {
                ...state,
                BrandInfo: action.data,
                loading: false
            }
        case BRAND.EDIT_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case BRAND.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRAND.FETCH_SUCCESS:
            return {
                ...state,
                allBrands: action.data,
                loading: false
            }
        case BRAND.FETCH_BY_MAIN_GROUP_SUCCESS:
            return {
                ...state,
                allMainGroupBrands: action.data,
                loading: false
            }
        case BRAND.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case BRAND.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRAND.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                BrandInfo: action.data
            }
        case BRAND.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default brandReducer