import { COUNTER } from '../../constants/action-types'

const initialState = {
    allCounters: {},
    counterInfo: {},
    loading: false,
    error: null
}

const counterReducer = (state = initialState, action) => {
    switch (action.type) {
        case COUNTER.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case COUNTER.SAVE_SUCCESS:
            return {
                ...state,
                counterInfo: action.data,
                loading: false
            }
        case COUNTER.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case COUNTER.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case COUNTER.FETCH_SUCCESS:
            return {
                ...state,
                allCounters: action.data,
                loading: false
            }
        case COUNTER.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case COUNTER.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case COUNTER.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                counterInfo: action.data
            }
        case COUNTER.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default counterReducer