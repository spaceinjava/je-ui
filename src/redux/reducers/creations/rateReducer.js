import { RATE } from '../../constants/action-types'

const initialState = {
    allRates: {},
    calculatedRates: {},
    ratesByMainGroup: {},
    rateInfo: {},
    loading: false,
    error: null
}

const rateReducer = (state = initialState, action) => {
    switch (action.type) {
        case RATE.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case RATE.SAVE_SUCCESS:
            return {
                ...state,
                rateInfo: action.data,
                loading: false
            }
        case RATE.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case RATE.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case RATE.FETCH_SUCCESS:
            return {
                ...state,
                allRates: action.data,
                loading: false
            }
        case RATE.FETCH_CALCULATED_RATES_SUCCESS:
            return {
                ...state,
                calculatedRates: action.data,
                loading: false
            }
        case RATE.FETCH_RATES_BY_MAIN_GROUP_SUCCESS:
            return {
                ...state,
                ratesByMainGroup: action.data,
                loading: false
            }
        case RATE.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default rateReducer