import { STONE_SIZE } from '../../constants/action-types'

const initialState = {
    allStoneSizes: {},
    stoneSizeInfo: {},
    loading: false,
    error: null
}

const stoneSizeReducer = (state = initialState, action) => {
    switch (action.type) {
        case STONE_SIZE.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE_SIZE.SAVE_SUCCESS:
            return {
                ...state,
                stoneSizeInfo: action.data,
                loading: false
            }
        case STONE_SIZE.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case STONE_SIZE.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE_SIZE.FETCH_SUCCESS:
            return {
                ...state,
                allStoneSizes: action.data,
                loading: false
            }
        case STONE_SIZE.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case STONE_SIZE.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE_SIZE.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                stoneSizeInfo: action.data
            }
        case STONE_SIZE.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default stoneSizeReducer