import { STONE_GROUP } from '../../constants/action-types'

const initialState = {
    allStoneGroups: {},
    allStoneGroupsByType: {},
    allStonesByStoneGroup: {},
    allStoneSizesByStoneGroup: {},
    stoneGroupInfo: {},
    loading: false,
    error: null
}

const stoneGroupReducer = (state = initialState, action) => {
    switch (action.type) {
        case STONE_GROUP.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE_GROUP.SAVE_SUCCESS:
            return {
                ...state,
                stoneGroupInfo: action.data,
                loading: false
            }
        case STONE_GROUP.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case STONE_GROUP.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE_GROUP.FETCH_SUCCESS:
            return {
                ...state,
                allStoneGroups: action.data,
                loading: false
            }
        case STONE_GROUP.FETCH_BY_TYPE_SUCCESS:
            return {
                ...state,
                allStoneGroupsByType: action.data,
                loading: false
            }
        case STONE_GROUP.FETCH_STONES_BY_GROUP_SUCCESS:
            return {
                ...state,
                allStonesByStoneGroup: action.data,
                loading: false
            }
        case STONE_GROUP.FETCH_STONE_SIZES_BY_GROUP_SUCCESS:
            return {
                ...state,
                allStoneSizesByStoneGroup: action.data,
                loading: false
            }
        case STONE_GROUP.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case STONE_GROUP.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE_GROUP.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                stoneGroupInfo: action.data
            }
        case STONE_GROUP.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default stoneGroupReducer