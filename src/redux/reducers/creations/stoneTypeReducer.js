import { STONE_TYPE } from '../../constants/action-types'

const initialState = {
    allStoneTypes: {},
    stoneTypeInfo: {},
    loading: false,
    error: null
}

const stoneTypeReducer = (state = initialState, action) => {
    switch (action.type) {
        case STONE_TYPE.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONE_TYPE.FETCH_SUCCESS:
            return {
                ...state,
                allStoneTypes: action.data,
                loading: false
            }
        case STONE_TYPE.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default stoneTypeReducer