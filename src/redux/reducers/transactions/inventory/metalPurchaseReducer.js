import { METAL_PURCHASE } from '../../../constants/action-types'

const initialState = {
    allMetalPurchases: {},
    metalPurchaseInfo: {},
    allProducts: [],
    loading: false,
    error: null
}

const metalPurchaseReducer = (state = initialState, action) => {
    switch (action.type) {
        case METAL_PURCHASE.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case METAL_PURCHASE.SAVE_SUCCESS:
            return {
                ...state,
                metalPurchaseInfo: action.data,
                loading: false
            }
        case METAL_PURCHASE.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case METAL_PURCHASE.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case METAL_PURCHASE.FETCH_SUCCESS:
            return {
                ...state,
                allMetalPurchases: action.data,
                loading: false
            }
        case METAL_PURCHASE.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case METAL_PURCHASE.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case METAL_PURCHASE.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                metalPurchaseInfo: action.data
            }
        case METAL_PURCHASE.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case METAL_PURCHASE.PRODUCT_SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case METAL_PURCHASE.PRODUCT_SAVE_SUCCESS:
            return {
                ...state,
                allProducts: action.data,
                loading: false
            }
        case METAL_PURCHASE.PRODUCT_SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case METAL_PURCHASE.PRODUCT_DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case METAL_PURCHASE.PRODUCT_DELETE_SUCCESS:
            console.log(state.allProducts)
            console.log(action.data)
            return {
                
                ...state,
                allProducts: state.allProducts.filter(item => !action.data.includes(item.identifier)),
                loading: false
            }
        case METAL_PURCHASE.PRODUCT_DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default metalPurchaseReducer