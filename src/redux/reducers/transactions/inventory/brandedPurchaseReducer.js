import { BRANDED_PURCHASE } from '../../../constants/action-types'

const initialState = {
    allBrandedPurchases: {},
    brandedPurchaseInfo: {},
    loading: false,
    error: null
}

const brandedPurchaseReducer = (state = initialState, action) => {
    switch (action.type) {
        case BRANDED_PURCHASE.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRANDED_PURCHASE.SAVE_SUCCESS:
            return {
                ...state,
                brandedPurchaseInfo: action.data,
                loading: false
            }
        case BRANDED_PURCHASE.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case BRANDED_PURCHASE.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRANDED_PURCHASE.FETCH_SUCCESS:
            return {
                ...state,
                allBrandedPurchases: action.data,
                loading: false
            }
        case BRANDED_PURCHASE.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case BRANDED_PURCHASE.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRANDED_PURCHASE.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                brandedPurchaseInfo: action.data
            }
        case BRANDED_PURCHASE.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default brandedPurchaseReducer