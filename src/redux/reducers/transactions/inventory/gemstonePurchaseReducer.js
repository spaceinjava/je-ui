import { GEMSTONE_PURCHASE } from '../../../constants/action-types'

const initialState = {
    allGemstonePurchases: {},
    gemstonePurchaseInfo: {},
    loading: false,
    error: null
}

const gemstonePurchaseReducer = (state = initialState, action) => {
    switch (action.type) {
        case GEMSTONE_PURCHASE.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case GEMSTONE_PURCHASE.SAVE_SUCCESS:
            return {
                ...state,
                gemstonePurchaseInfo: action.data,
                loading: false
            }
        case GEMSTONE_PURCHASE.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case GEMSTONE_PURCHASE.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case GEMSTONE_PURCHASE.FETCH_SUCCESS:
            return {
                ...state,
                allGemstonePurchases: action.data,
                loading: false
            }
        case GEMSTONE_PURCHASE.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case GEMSTONE_PURCHASE.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case GEMSTONE_PURCHASE.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                gemstonePurchaseInfo: action.data
            }
        case GEMSTONE_PURCHASE.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default gemstonePurchaseReducer