import { STONEDETAILS } from '../../constants/action-types'

const initialState = {
    stoneDetails: [],
    stoneDetailsData: [],
    stoneWeight: 0,
    stoneAmount: 0,
    formSaved: false,
    loading: false,
    error: null
}

const stoneDetailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case STONEDETAILS.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONEDETAILS.SAVE_SUCCESS:
            return {
                ...state,
                stoneDetails: [...state.stoneDetails, action.data],
                stoneDetailsData: [...state.stoneDetailsData, action.stoneDetailsData],
                stoneWeight: action.data.isNoWeight ? state.stoneWeight : state.stoneWeight + parseInt(action.data.weightPerGram),
                stoneAmount: action.data.amount ? state.stoneAmount + parseInt(action.data.amount) : state.stoneAmount,
                loading: false
            }
        case STONEDETAILS.SAVE_STONE_DETAILS: 
            return {
                ...state,
                stoneDetails:  action.data,
                formSaved: action.data,
                stoneWeight: action.weightPerGram
            }
        case STONEDETAILS.SAVE_STONE_SUCCESS:
            return {
                ...state,
                formSaved: action.data
            }
        case STONEDETAILS.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case STONEDETAILS.EDIT_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONEDETAILS.EDIT_SUCCESS:
            let updatedData = state.stoneDetails.map((item, index) => {
                console.log(item.id)
                console.log(action.id)
                if (item.id === action.id) {
                    return state.stoneDetails[index] = action.data
                }
            })
            let stWeight = 0;
            state.stoneDetails.forEach(val => {
                if(!val.isNoWeight)
                stWeight += parseInt(val.weightPerGram);
            });
            return {
                ...state,
                stoneDetails: [...state.stoneDetails],
                stoneWeight: stWeight,
                loading: false
            }
        case STONEDETAILS.EDIT_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case STONEDETAILS.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONEDETAILS.FETCH_SUCCESS:
            return {
                ...state,
                allBrands: action.data,
                loading: false
            }
        case STONEDETAILS.FETCH_BY_MAIN_GROUP_SUCCESS:
            return {
                ...state,
                allMainGroupBrands: action.data,
                loading: false
            }
        case STONEDETAILS.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case STONEDETAILS.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case STONEDETAILS.DELETE_SUCCESS:
            let data = action.data.length ? action.data : [action.data];
            return {
                ...state,
                loading: false,
                stoneDetails: [...state.stoneDetails.filter(item => !data.includes(item.id))],
            }
        case STONEDETAILS.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case STONEDETAILS.ClEAR_DETAILS:
            return {
                stoneDetails: [],
                stoneDetailsData: [],
                stoneWeight: 0,
            }
        default: return state
    }
}

export default stoneDetailsReducer