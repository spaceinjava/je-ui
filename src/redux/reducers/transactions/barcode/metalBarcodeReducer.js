import { METAL_BARCODE } from '../../../constants/action-types'

const initialState = {
    allMetalBarcodes: {},
    metalBarcodeInfo: {},
    lotInfo: {},
    loading: false,
    error: null
}

const metalBarcodeReducer = (state = initialState, action) => {
    switch (action.type) {
        case METAL_BARCODE.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case METAL_BARCODE.SAVE_SUCCESS:
            return {
                ...state,
                EmployeeInfo: action.data,
                loading: false
            }
        case METAL_BARCODE.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case METAL_BARCODE.EDIT_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case METAL_BARCODE.EDIT_SUCCESS:
            return {
                ...state,
                EmployeeInfo: action.data,
                loading: false
            }
        case METAL_BARCODE.EDIT_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case METAL_BARCODE.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case METAL_BARCODE.FETCH_SUCCESS:
            return {
                ...state,
                allEmployees: action.data,
                loading: false
            }

        case METAL_BARCODE.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case METAL_BARCODE.LOT_FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case METAL_BARCODE.LOT_FETCH_SUCCESS:
            return {
                ...state,
                lotInfo: action.data,
                loading: false
            }

        case METAL_BARCODE.LOT_FETCH_FAILUREE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
       
        default: return state
    }
}

export default metalBarcodeReducer