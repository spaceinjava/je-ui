import { GEMSTONE_BARCODE } from '../../../constants/action-types'

const initialState = {
    allGemstoneBarcodeLots: {},
    allGemstoneBarcodeRates:{},
    gemstoneBarcodeInfo: {},
    loading: false,
    error: null
}

const gemstoneBarcodeReducer = (state = initialState, action) => {
    switch (action.type) {
        case GEMSTONE_BARCODE.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case GEMSTONE_BARCODE.SAVE_SUCCESS:
            return {
                ...state,
                gemstoneBarcodeInfo: action.data,
                loading: false
            }
        case GEMSTONE_BARCODE.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case GEMSTONE_BARCODE.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case GEMSTONE_BARCODE.FETCH_SUCCESS:
            return {
                ...state,
                allGemstoneBarcodeRates: action.data,
                loading: false
            }
        case GEMSTONE_BARCODE.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }            
            case GEMSTONE_BARCODE.LOT_BEGIN:
                return {
                    ...state,
                    loading: true,
                    error: null
                }
            case GEMSTONE_BARCODE.LOT_SUCCESS:
                return {
                    ...state,
                    allGemstoneBarcodeLots: action.data,
                    loading: false
                }
            case GEMSTONE_BARCODE.LOT_FAILURE:
                return {
                    ...state,
                    loading: false,
                    error: action.error
                }
        default: return state
    }
}

export default gemstoneBarcodeReducer;