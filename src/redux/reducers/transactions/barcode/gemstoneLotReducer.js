import { GEMSTONE_LOT } from '../../../constants/action-types'

const initialState = {
    allGemstoneLotsInvoices: {},
    allGemstoneLotDetails:{},
    gemstoneLotInfo: {},
    loading: false,
    error: null
}

const gemstoneLotReducer = (state = initialState, action) => {
    switch (action.type) {
        case GEMSTONE_LOT.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case GEMSTONE_LOT.SAVE_SUCCESS:
            return {
                ...state,
                gemstoneLotInfo: action.data,
                loading: false
            }
        case GEMSTONE_LOT.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case GEMSTONE_LOT.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case GEMSTONE_LOT.FETCH_SUCCESS:
            return {
                ...state,
                allGemstoneLotsInvoices: action.data,
                loading: false
            }
        case GEMSTONE_LOT.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }            
            case GEMSTONE_LOT.FETCH_DETAILS_BEGIN:
                return {
                    ...state,
                    loading: true,
                    error: null
                }
            case GEMSTONE_LOT.FETCH_DETAILS_SUCCESS:
                return {
                    ...state,
                    allGemstoneLotDetails: action.data,
                    loading: false
                }
            case GEMSTONE_LOT.FETCH_DETAILS_FAILURE:
                return {
                    ...state,
                    loading: false,
                    error: action.error
                }
        case GEMSTONE_LOT.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case GEMSTONE_LOT.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                gemstoneLotInfo: action.data
            }
        case GEMSTONE_LOT.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default gemstoneLotReducer;