import { BRANDED_BARCODE } from '../../../constants/action-types'

const initialState = {
    brandedBarcodeInfo: {},
    brandedLots: {},
    loading: false,
    error: null
}

const brandedBarcodeReducer = (state = initialState, action) => {
    switch (action.type) {
        case BRANDED_BARCODE.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRANDED_BARCODE.SAVE_SUCCESS:
            return {
                ...state,
                brandedBarcodeInfo: action.data,
                loading: false
            }
        case BRANDED_BARCODE.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case BRANDED_BARCODE.FETCH_LOT_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRANDED_BARCODE.FETCH_LOT_SUCCESS:
            return {
                ...state,
                brandedLots: action.data,
                loading: false
            }
        case BRANDED_BARCODE.FETCH_LOT_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default brandedBarcodeReducer;