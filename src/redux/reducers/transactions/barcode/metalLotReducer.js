import { METAL_LOT } from '../../../constants/action-types'

const initialState = {
    allMetalLotsInvoices: {},
    allMetalLotDetails:{},
    metalLotInfo: {},
    loading: false,
    error: null
}

const metalLotReducer = (state = initialState, action) => {
    switch (action.type) {
        case METAL_LOT.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case METAL_LOT.SAVE_SUCCESS:
            return {
                ...state,
                metalLotInfo: action.data,
                loading: false
            }
        case METAL_LOT.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case METAL_LOT.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case METAL_LOT.FETCH_SUCCESS:
            return {
                ...state,
                allMetalLotsInvoices: action.data,
                loading: false
            }
        case METAL_LOT.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }            
            case METAL_LOT.FETCH_DETAILS_BEGIN:
                return {
                    ...state,
                    loading: true,
                    error: null
                }
            case METAL_LOT.FETCH_DETAILS_SUCCESS:
                return {
                    ...state,
                    allMetalLotDetails: action.data,
                    loading: false
                }
            case METAL_LOT.FETCH_DETAILS_FAILURE:
                return {
                    ...state,
                    loading: false,
                    error: action.error
                }
        case METAL_LOT.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case METAL_LOT.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                metalLotInfo: action.data
            }
        case METAL_LOT.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default metalLotReducer;