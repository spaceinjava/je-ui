import { BRANDED_LOT } from '../../../constants/action-types'

const initialState = {
    allBrandedLotsInvoices: {},
    brandedLotInfo: {},
    allBrandedLotDetails:{},
    loading: false,
    error: null
}

const brandedLotReducer = (state = initialState, action) => {
    switch (action.type) {
        case BRANDED_LOT.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRANDED_LOT.SAVE_SUCCESS:
            return {
                ...state,
                brandedLotInfo: action.data,
                loading: false
            }
        case BRANDED_LOT.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case BRANDED_LOT.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRANDED_LOT.FETCH_SUCCESS:
            return {
                ...state,
                allBrandedLotsInvoices: action.data,
                loading: false
            }
        case BRANDED_LOT.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
            case BRANDED_LOT.FETCH_DETAILS_BEGIN:
                return {
                    ...state,
                    loading: true,
                    error: null
                }
            case BRANDED_LOT.FETCH_DETAILS_SUCCESS:
                return {
                    ...state,
                    allBrandedLotDetails: action.data,
                    loading: false
                }
            case BRANDED_LOT.FETCH_DETAILS_FAILURE:
                return {
                    ...state,
                    loading: false,
                    error: action.error
                }
        case BRANDED_LOT.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case BRANDED_LOT.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                brandedLotInfo: action.data
            }
        case BRANDED_LOT.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default brandedLotReducer;