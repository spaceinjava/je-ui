import { DESIGNATION } from '../constants/action-types'

const initialState = {
    allDesignations: {},
    designationInfo: {},
    loading: false,
    error: null
}

const designationReducer = (state = initialState, action) => {
    switch (action.type) {
        case DESIGNATION.SAVE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case DESIGNATION.SAVE_SUCCESS:
            return {
                ...state,
                designationInfo: action.data,
                loading: false
            }
        case DESIGNATION.SAVE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case DESIGNATION.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case DESIGNATION.FETCH_SUCCESS:
            return {
                ...state,
                allDesignations: action.data,
                loading: false
            }
        case DESIGNATION.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case DESIGNATION.DELETE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case DESIGNATION.DELETE_SUCCESS:
            return {
                ...state,
                loading: false,
                designationInfo: action.data
            }
        case DESIGNATION.DELETE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default designationReducer