import { ADDRESS } from '../constants/action-types'

const initialState = {
    allAddresses: {},
    selectedAddress: {},
    loading: false,
    error: null
}

const addressReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADDRESS.FETCH_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case ADDRESS.FETCH_SUCCESS:
            return {
                ...state,
                allAddresses: action.data,
                loading: false
            }
        case ADDRESS.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default addressReducer