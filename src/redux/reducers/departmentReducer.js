import { DEPARTMENT } from '../constants/action-types'

const initialState = {
    allDepartments: {},
    departmentInfo: {},
    loading: false,
    error: null
}

const departmentReducer = (state = initialState, action) => {
    switch(action.type) {
        case DEPARTMENT.SAVE_BEGIN: 
            return {
                ...state,
                loading: true,
                error: null
            }
        case DEPARTMENT.SAVE_SUCCESS: 
            return {
                ...state,
                departmentInfo: action.data,
                loading: false 
            }
        case DEPARTMENT.SAVE_FAILURE: 
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case DEPARTMENT.FETCH_BEGIN: 
            return {
                ...state,
                loading: true,
                error: null
            }
        case DEPARTMENT.FETCH_SUCCESS:
            return {
                ...state,
                allDepartments: action.data,
                loading: false
            }
        case DEPARTMENT.FETCH_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case DEPARTMENT.DELETE_BEGIN: 
            return {
                ...state,
                loading: true,
                error: null
            }
        case DEPARTMENT.DELETE_SUCCESS: 
            return {
                ...state,
                loading: false,
                departmentInfo: action.data
            }
        case DEPARTMENT.DELETE_FAILURE: 
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default departmentReducer