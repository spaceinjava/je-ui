import { USER, SET_CURRENT_USER, SET_VALID_USER, SET_AUTH_TOKEN } from '../constants/action-types'
import isEmpty from 'lodash/isEmpty'

const initialState = {
    isValidated: false,
    isAuthenticated: false,
    authInfo: {},
    loginInfo: {},
    authToken: '',
    newUser: false,
    loading: false,
    error: null
}

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_VALID_USER:
            return {
                isValidated: !isEmpty(action.info),
                authInfo: action.info
            }
        case SET_CURRENT_USER:
            return {
                isAuthenticated: !isEmpty(action.info),
                loginInfo: action.info
            }
        case USER.NEW_USER: 
            return {
                newUser: true
            }
        case SET_AUTH_TOKEN:
            return {
                isAuthenticated: !isEmpty(action.token),
                authToken: action.token
            }
        case USER.AUTHORIZATION_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case USER.AUTHORIZATION_SUCCESS:
            return {
                ...state,
                loading: false
            }
        case USER.AUTHORIZATION_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default: return state
    }
}

export default authReducer