import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from '../reducers/rootReducer'
import { setAuthToken, getCookie } from '../actions/authActions'

const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    rootReducer,
    storeEnhancers(applyMiddleware(thunk))
)

let auth_token = getCookie("authToken")
let login_status = getCookie("loginStatus")
if(auth_token && login_status === "ACTIVE") {
    store.dispatch(setAuthToken(auth_token))
}

export default store;