import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { brandedPurchaseForm, brandedDetailsForm, brandedDetailsColumns } from '../Constants';
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../../components/Grid";
import Select from "../../../components/Form/Select";
import CollapseGrid from "../../../components/CollapseGrid";
import { fetchSuppliers } from '../../../redux/actions/Admin/supplierActions'
import { fetchMainGroups } from '../../../redux/actions/mainGroupActions'
import { fetchBrandsByMainGroup } from '../../../redux/actions/creations/brandActions'
import { fetchBrandProductsByBrand, fetchSubProductsByProduct } from '../../../redux/actions/creations/brandProductActions'
import { saveBrandedPurchase, fetchBrandedPurchases, deleteBrandedPurchases } from '../../../redux/actions/transactions/inventory/brandedPurchaseActions'
import Swal from 'sweetalert2'
import LoadingOverlay from 'react-loading-overlay';
import { GST_PERCENT } from "../../../utils/commonConstants";

export const BrandedPurchase = (props) => {
  const { t } = props;
  const [formData, setFromData] = useState(cloneDeep(brandedPurchaseForm));
  const [brandFormData, setBrandFormData] = useState(cloneDeep(brandedDetailsForm));
  const [viewForm, setToggleView] = useState(true);
  let [selectedBrands, setSelectedBrands] = useState([]);
  let [selected, setSelected] = useState([]);
  const [brandDetails, setBrandDetails] = useState([])
  const [purchaseData, setPurchaseData] = useState()
  const dispatch = useDispatch();

  const suppliers = useSelector(state => state.supplierReducer.allSuppliers)
  const mainGroups = useSelector(state => state.MainGroupReducer.allGroups)
  const brands = useSelector(state => state.brandReducer.allMainGroupBrands)
  const brandProducts = useSelector(state => state.brandProductReducer.allBrandProductsByBrand)
  const brandSubProducts = useSelector(state => state.brandProductReducer.allSubProductsByProduct)
  const brandedPurchases = useSelector(state => state.brandedPurchaseReducer.allBrandedPurchases)
  const loader = useSelector(state => state.supplierReducer.loading);

  const suppliersData = suppliers.data ? suppliers.data.map((supplier) => {
    return {
      id: supplier.id,
      name: supplier.partyName
    }
  }) : [];
  const mainGroupData = mainGroups.data ? mainGroups.data.map((group) => {
    return {
      id: group.id,
      name: group.groupName,
      shortCode: group.shortCode,
    }
  }) : [];
  const brandData = brands.data ? brands.data.map((brand) => {
    return {
      id: brand.id,
      name: brand.brandName,
      shortCode: brand.shortCode,
    }
  }) : [];
  const brandProductsData = brandProducts.data ? brandProducts.data.map((product) => {
    return {
      id: product.id,
      name: product.productName,
      shortCode: product.shortCode,
    }
  }) : [];
  const brandSubProductsData = brandSubProducts.data ? brandSubProducts.data.map((subProduct) => {
    return {
      id: subProduct.id,
      name: subProduct.subProductName,
      shortCode: subProduct.shortCode,
    }
  }) : [];

  const fetchPartyNameById = (partyId) => {
    let selectedParty = suppliersData.find(ele => ele.id === partyId);
    return selectedParty ? selectedParty.name : "";
  }

  const brandedPurchaseData = brandedPurchases.data ? brandedPurchases.data.map((purchase) => {
    let brandedDetails = purchase.brandedDetails ? purchase.brandedDetails.map(brand => {
      return {
        id: brand.id,
        mainGroupId: brand.subProduct.brandProduct.brand.mainGroup.id,
        mainGroup: brand.subProduct.brandProduct.brand.mainGroup.groupName,
        brandId: brand.subProduct.brandProduct.brand.id,
        brand: brand.subProduct.brandProduct.brand.brandName,
        productId: brand.subProduct.brandProduct.id,
        product: brand.subProduct.brandProduct.productName,
        subProductId: brand.subProduct.id,
        subProduct: brand.subProduct.subProductName,
        quantity: brand.quantity,
        weight: brand.weight,
        rate: brand.rate,
        amount: brand.amount,
        gstAmount: brand.gstAmount,
        totalAmount: brand.totalAmount,
        remarks: brand.remarks,
        identifier: new Date().getTime() + brand.id
      }
    }) : [];
    return {
      id: purchase.id,
      purchaseType: purchase.purchaseType,
      invoiceDate: purchase.invoiceDate,
      invoiceNo: purchase.invoiceNo,
      partyId: purchase.partyId,
      party: fetchPartyNameById(purchase.partyId),
      brandedDetails: brandedDetails
    }
  }) : [];

  const setFormValue = (field, value) => {
    formData[field]['value'] = value;
    setFromData({ ...formData });
  }
  const setBrandFormValue = (field, value) => {
    brandFormData[field]['value'] = value;
    setBrandFormData({ ...brandFormData });
  }
  const resetForm = () => {
    map(formData, function (value, key, object) {
      formData[key]['value'] = ''
    });
    setFromData({ ...formData });
    resetBrandForm();
    setBrandDetails([]);
  }
  const saveForm = () => {
    let isFormValid = true;
    map(formData, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      formData[key] = { ...value, ...isValid };
    });
    if (isFormValid) {
      if (brandDetails.length < 1) {
        Swal.fire({
          position: "top-end",
          title: 'Please enter brand details.!',
          text: 'And try saving purchase info',
          icon: 'info',
          showConfirmButton: false,
          timer: 2000
        })
      } else {
        let data = {};
        map(formData, function (value, key, object) {
          data = {
            ...data,
            [key]: value.value,
          };
        });
        data["brandedDetails"] = brandDetails;
        console.log("saving branded purchase with data : ", data);
        Swal.fire({
          title: 'Confirm Saving Purchase Info.!',
          text: 'Continue to save / Cancel to stop',
          icon: 'info',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Continue'
        }).then((result) => {
          if (result.isConfirmed) {
            dispatch(saveBrandedPurchase(data))
              .then(() => {
                resetForm();
                resetBrandForm();
                setBrandDetails([]);
                setPurchaseData();
              })
          }
        })
      }
    }
    
  }

  const addBrands = () => {
    let isBrandFormValid = true;
    map(brandFormData, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isBrandFormValid = false;
      }
      brandFormData[key] = { ...value, ...isValid }
    });
    if (isBrandFormValid) {
      let brandDetailsData = {};
      map(brandFormData, function (value, key, object) {
        brandDetailsData = {
          ...brandDetailsData,
          [key]: value.value,
          isDeleted: false
        }
      });
      let list = [...brandDetails];
      if (brandDetailsData.identifier) {
        let eleIndex = list.findIndex(ele => ele.identifier === brandDetailsData.identifier)
        list[eleIndex] = brandDetailsData;
      } else {
        brandDetailsData['identifier'] = new Date().getTime() + brandDetailsData.id;
        list.push(cloneDeep(brandDetailsData));
      }
      setBrandDetails(list);
      resetBrandForm();
    }
  }

  const resetBrandForm = () => {
    map(brandFormData, function (value, key, object) {
      if(brandFormData[key]["type"] === 'number') {
        brandFormData[key]["value"] = 0;
      } else {
        brandFormData[key]["value"] = '';
      }
    });
    setBrandFormData({ ...brandFormData });
  }

  const validateField = (fieldObj) => {
    let errorObj = { isValid: true, error: null }
    if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
    else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
    else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }
    return errorObj;
  }

  const editAction = (ele) => {
    console.log("Editing Branded Purchase :: ", ele)
    resetForm();
    resetBrandForm();
    setBrandDetails([]);
    setFormValue('id', ele.id)
    setFormValue('purchaseType', ele.purchaseType)
    setFormValue('invoiceNo', ele.invoiceNo)
    setFormValue('invoiceDate', ele.invoiceDate)
    setFormValue('partyId', ele.partyId)
    setFormValue('party', ele.party)
    setFormValue('brandedDetails', ele.brandedDetails)
    setBrandDetails(ele.brandedDetails);
  }

  const editBrandDetails = (ele) => {
    console.log("editing brand details : ", ele)
    setBrandFormValue('id', ele.id)
    setBrandFormValue('mainGroupId', ele.mainGroupId)
    setBrandFormValue('mainGroup', ele.mainGroup)
    setBrandFormValue('brandId', ele.brandId)
    setBrandFormValue('brand', ele.brand)
    setBrandFormValue('productId', ele.productId)
    setBrandFormValue('product', ele.product)
    setBrandFormValue('subProductId', ele.subProductId)
    setBrandFormValue('subProduct', ele.subProduct)
    setBrandFormValue('quantity', ele.quantity)
    setBrandFormValue('weight', ele.weight)
    setBrandFormValue('rate', ele.rate)
    setBrandFormValue('amount', ele.amount)
    setBrandFormValue('gstAmount', ele.gstAmount)
    setBrandFormValue('totalAmount', ele.totalAmount)
    setBrandFormValue('remarks', ele.remarks)
    setBrandFormValue('identifier', ele.identifier)
  }

  const deleteAction = (data) => {
    let selectedIds;
    if (data instanceof Array && data.length > 0) {
      selectedIds = data.map((ele) => {
        return ele.id
      })
    } else {
      selectedIds = data.id;
    }
    Swal.fire({
      title: 'Confirm Deleting Branded Purchase.!',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deleteBrandedPurchases(selectedIds.toString()))
          .then(() => {
            selected = []
            setSelected(selected);
          })
      }
    })
  }

  const deleteBrandDetails = (data) => {
    console.log(" delete brand details ====== ", data)
  }

  useEffect(() => {
    dispatch(fetchSuppliers());
    dispatch(fetchMainGroups());
    dispatch(fetchBrandedPurchases());
  }, [])

  useEffect(() => {
    dispatch(fetchBrandsByMainGroup(get(brandFormData, 'mainGroupId.value')));
  }, [get(brandFormData, 'mainGroupId.value')])

  useEffect(() => {
    dispatch(fetchBrandProductsByBrand(get(brandFormData, 'brandId.value')));
  }, [get(brandFormData, 'brandId.value')])

  useEffect(() => {
    dispatch(fetchSubProductsByProduct(get(brandFormData, 'productId.value')));
  }, [get(brandFormData, 'productId.value')])

  useEffect(() => {
    let quantity = get(brandFormData, 'quantity.value');
    let weight = get(brandFormData, 'weight.value');
    let rate = get(brandFormData, 'rate.value');
    let amount = 0;
    let gstAmount = 0;
    let totalAmount = amount + gstAmount;
    amount = Math.round(quantity * rate);
    // amount = Math.round(weight * rate);
    gstAmount = Math.round(amount * GST_PERCENT / 100);
    totalAmount = amount + gstAmount;
    setBrandFormValue("amount", amount);
    setBrandFormValue("gstAmount", gstAmount);
    setBrandFormValue("totalAmount", totalAmount);
  }, [get(brandFormData, 'quantity.value'), get(brandFormData, 'weight.value'), get(brandFormData, 'rate.value')])

  const toggleView = () => {
    const formNode = document.getElementById('form');
    formNode.classList.toggle('collapse-panel');
    setToggleView(!viewForm);
  }

  const partyNameChange = (e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    setFormValue('partyId', e.target.value)
    setFormValue('party', label)
  }

  const selectOnChange = (idKey, labelKey, e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    if (e.target.value) {
      setBrandFormValue(idKey, e.target.value)
      setBrandFormValue(labelKey, label)
    }
  }

  return (
    <div className="panel panel-default hero-panel">
      {console.log('brandedPurchaseData', brandedPurchaseData)}
      <LoadingOverlay
        active={loader}
        spinner
        text='Loading...'
      >
        <div className="panel-heading">
          <h1 className="panel-title">{t("transactions.brandedPurchase")}</h1>
          <div className="panel-options">
            <a
              href="#"
              className="text-info collapseBtn"
              title="Show Add Form"
            >
              {viewForm ?
                <i className="zmdi zmdi-minus-circle  zmdi-hc-2x" onClick={() => { toggleView() }}>
                </i> :
                <i className="zmdi zmdi-plus-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                </i>}
            </a>
          </div>
        </div>
        <div className="panel-body">
          <div id="form">
            <div className="panel update-panel form-pnel-container">
              {/* <div className="text-danger">* {t("common.requiredFieldsMsg")} </div> */}
              <div className="panel-body p-t-0">
                <div className="row">
                  <div className="col-md-3">
                    <div className="panel-heading p-l-0">
                      <h4 className="panel-title">{t("transactions.purchase")} {t("common.type")}</h4>
                    </div>
                    <div className="form-group no-min-height">
                      <div className="radio  radio-primary m-b-0">
                        <input
                          type="radio"
                          name="purchaseType"
                          value="Approval"
                          checked={get(formData, 'purchaseType.value') === "Approval"}
                          onChange={(e) => setFormValue('purchaseType', "Approval")}
                        />
                        <label htmlFor="">{t("transactions.approval")}</label>
                      </div>
                      <div className="radio  radio-primary m-b-0">
                        <input
                          type="radio"
                          name="purchaseType"
                          value="Invoice"
                          checked={get(formData, 'purchaseType.value') === "Invoice"}
                          onChange={(e) => setFormValue('purchaseType', "Invoice")}
                        />
                        <label htmlFor="">{t("transactions.invoice")}</label>
                      </div>
                      <div className="invalid-feedback">{get(formData, 'purchaseType.error')}</div>
                    </div>
                  </div>
                  <div className="col-md-9">
                    <div className="panel-heading">
                      <h4 className="panel-title">{t("transactions.invoice")} &amp; {t("common.Party")} {t("common.details")}</h4>
                    </div>
                    <div className="form-group col-md-4">
                      <label htmlFor="">
                        {t("transactions.invoice")} {t("common.number")}<span className="asterisk">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="invoiceNo"
                        name="invoiceNo"
                        value={get(formData, 'invoiceNo.value')}
                        onChange={(e) => setFormValue('invoiceNo', e.target.value)}
                      />
                      <div className="invalid-feedback">{get(formData, 'invoiceNo.error')}</div>
                    </div>
                    <div className="form-group col-md-4">
                      <label htmlFor="">
                        {t("transactions.invoice")} {t("common.date")}<span className="asterisk">*</span>
                      </label>
                      <input type="date"
                        className="form-control"
                        placeholder=""
                        value={get(formData, 'invoiceDate.value')}
                        onChange={(e) => setFormValue('invoiceDate', e.target.value)} />
                      <div className="invalid-feedback">{get(formData, 'invoiceDate.error')}</div>
                    </div>
                    <div className="form-group col-md-4">
                      <label htmlFor="">
                        {t("common.Party")} {t("common.name")}<span className="asterisk">*</span>
                      </label>
                      <Select name="partyId" value={get(formData, 'partyId.value')} onChange={(e) => partyNameChange(e)} data={suppliersData} />
                      <div className="invalid-feedback">{get(formData, 'partyId.error')}</div>
                    </div>
                  </div>
                </div>
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h4 className="panel-title">{t("transactions.brandDetailsandRate")}</h4>
                  </div>
                  <div className="panel-body">
                    <div className="row">
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("creations.mainGroup")}<span className="asterisk">*</span>
                          </label>
                          <Select name="mainGroupId" value={get(brandFormData, 'mainGroupId.value')} onChange={e => selectOnChange('mainGroupId', 'mainGroup', e)} data={mainGroupData} />
                          <div className="invalid-feedback">{get(brandFormData, 'mainGroupId.error')}</div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.brand")}<span className="asterisk">*</span>
                            <a href="" className="" data-toggle="modal" data-target="#addProduct" data-backdrop="static" data-keyboard="false"><i className="zmdi zmdi-plus-circle f-s-16"></i></a>
                          </label>
                          <Select name="brandId" value={get(brandFormData, 'brandId.value')} onChange={(e) => selectOnChange('brandId', 'brand', e)} data={brandData} />
                          <div className="invalid-feedback">{get(brandFormData, 'brandId.error')}</div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("transactions.brandProduct")}<span className="asterisk">*</span>
                            <a href="" className="" data-toggle="modal" data-target="#addProduct" data-backdrop="static" data-keyboard="false"><i className="zmdi zmdi-plus-circle f-s-16"></i></a>
                          </label>
                          <Select name="productId" value={get(brandFormData, 'productId.value')} onChange={(e) => selectOnChange('productId', 'product', e)} data={brandProductsData} />
                          <div className="invalid-feedback">{get(brandFormData, 'productId.error')}</div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("creations.subProduct")}<span className="asterisk">*</span>
                            <a href="" className="" data-toggle="modal" data-target="#addProduct" data-backdrop="static" data-keyboard="false"><i className="zmdi zmdi-plus-circle f-s-16"></i></a>
                          </label>
                          <Select name="subProductId" value={get(brandFormData, 'subProductId.value')} onChange={(e) => selectOnChange('subProductId', 'subProduct', e)} data={brandSubProductsData} />
                          <div className="invalid-feedback">{get(brandFormData, 'subProductId.error')}</div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.quantity")}<span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="quantity"
                            name="quantity"
                            value={get(brandFormData, 'quantity.value')}
                            onChange={(e) => setBrandFormValue('quantity', e.target.value)}
                          />
                          <div className="invalid-feedback">{get(brandFormData, 'quantity.error')}</div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("transactions.weight")}<span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="weight"
                            name="weight"
                            value={get(brandFormData, 'weight.value')}
                            onChange={(e) => setBrandFormValue('weight', e.target.value)}
                          />
                          <div className="invalid-feedback">{get(brandFormData, 'weight.error')}</div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.rate")}<span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="rate"
                            name="rate"
                            value={get(brandFormData, 'rate.value')}
                            onChange={(e) => setBrandFormValue('rate', e.target.value)}
                          />
                          <div className="invalid-feedback">{get(brandFormData, 'rate.error')}</div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.amount")}<span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="amount"
                            name="amount"
                            value={get(brandFormData, 'amount.value')}
                            onChange={(e) => setBrandFormValue('amount', e.target.value)}
                            disabled={get(brandFormData, 'amount.disabled')}
                          />
                          <div className="invalid-feedback">{get(brandFormData, 'amount.error')}</div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("transactions.gstAmount")}<span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="gst Amount"
                            name="gstAmount"
                            value={get(brandFormData, 'gstAmount.value')}
                            onChange={(e) => setBrandFormValue('gstAmount', e.target.value)}
                            disabled={get(brandFormData, 'gstAmount.disabled')}
                          />
                          <div className="invalid-feedback">{get(brandFormData, 'gstAmount.error')}</div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("transactions.totalAmount")}<span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="total Amount"
                            name="totalAmount"
                            value={get(brandFormData, 'totalAmount.value')}
                            onChange={(e) => setBrandFormValue('totalAmount', e.target.value)}
                            disabled={get(brandFormData, 'totalAmount.disabled')}
                          />
                          <div className="invalid-feedback">{get(brandFormData, 'totalAmount.error')}</div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-6 col-lg-6">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.remarks")}
                          </label>
                          <textarea
                            type="text"
                            className="form-control"
                            name="remarks"
                            style={{ height: '30px' }}
                            value={get(brandFormData, 'remarks.value')}
                            onChange={(e) => setBrandFormValue('remarks', e.target.value)}
                          />
                          <div className="invalid-feedback">{get(brandFormData, 'remarks.error')}</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="panel-footer text-center">
                <a href="#" className="btn btn-primary" onClick={addBrands}>
                  <i className="zmdi zmdi-playlist-plus"></i> {t("common.addToList")}
                </a>
                <a href="#" className="btn btn-default" onClick={resetBrandForm}>
                  <i className="zmdi zmdi-close"></i> {t("common.clear")} {t("common.brand")}
                </a>
              </div>
            </div>
            <div className={`${brandDetails && brandDetails.length > 0 ? '' : 'hide'}`}>
            <div className="panel panel-default">
              <div className="panel-heading">
                <h3 className="panel-title">Selected List</h3>
                <div className="panel-options">
                  <p>
                    {selectedBrands.length > 0 ?
                      <a href="#" className="btn btn-info btn-sm" onClick={() => deleteBrandDetails(selectedBrands)} >
                        <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                      </a> :
                      null}
                  </p>
                </div>
              </div>
              <div className="panel-body">
                <div style={{ height: '100%', boxSizing: 'border-box' }}>
                  <div id="myGrid" style={{ height: '300px' }} className="ag-theme-alpine">
                    <Grid colDefs={brandedDetailsColumns} rowData={brandDetails} selectedRows={setSelectedBrands} actions={{ isRequired: true, editAction: editBrandDetails, deleteAction: deleteBrandDetails }} flex />
                  </div>
                </div>
              </div>
            </div>
              <div className="panel-footer text-center">
                <a href="#" className="btn btn-primary" onClick={saveForm}>
                  <i className="zmdi zmdi-check"></i> {t("common.save")}
                </a>
                <a href="#" className="btn btn-default" onClick={resetForm}>
                  <i className="zmdi zmdi-close"></i> {t("common.clear")}
                </a>
              </div>
            </div>
          </div>
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">Branded Purchase List</h3>
              <div className="panel-options">
                <p>
                  {selected.length > 0 ?
                    <a href="#" className="btn btn-info btn-sm" onClick={() => { deleteAction(selected) }}>
                      <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                    </a> :
                    null}
                </p>
              </div>
            </div>
            <div className="panel-body">
            <CollapseGrid
                data={brandedPurchaseData}
                columns={[
                  {
                    id: "mainGroup",
                    label: "Main Group",
                    minWidth: 100,
                  },
                  {
                    id: "brand",
                    label: "Brand",
                    minWidth: 100,
                  },
                  {
                    id: "product",
                    label: "Product",
                    minWidth: 100,
                  },
                  {
                    id: "subProduct",
                    label: "Product",
                    minWidth: 100,
                  },
                  {
                    id: "rate",
                    label: "Rate",
                    minWidth: 100,
                  },
                  {
                    id: "weight",
                    label: "Weight",
                    minWidth: 100,
                  },
                ]}
                expandColumns={[
                  {
                    id: "invoiceNo",
                    children: "brandedDetails",
                    isEdit: true,
                    isDelete: true,
                  },
                ]}
                uniqueId="id"
                sortColumn="invoiceNo"
                sortOrder="asc"
                isCheckBox={false}
                editData={editAction}
                deleteData={deleteAction}
              />
            </div>
          </div>
        </div>
      </LoadingOverlay>
    </div>
  );
};

export default withTranslation()(BrandedPurchase);
