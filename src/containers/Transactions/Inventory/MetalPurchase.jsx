import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import {
  metalPurchaseForm,
  productDetailsForm,
  stoneDetailsForm,
  brandColumns,
} from "../Constants";
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../../components/Grid";
import CollapseGrid from "../../../components/CollapseGrid";
import Select from "../../../components/Form/Select";
import {
  saveStoneDetailsForm,
  clearStoneDetails,
  saveStoneWeight
} from "../../../redux/actions/transactions/stoneDetailsActions";
import {
  fetchMainGroups,
  fetchProductsByMainGroup,
  fetchPuritiesByMainGroup,
} from "../../../redux/actions/mainGroupActions";
import {
  fetchSubProductsByProduct,
  fetchSizesByProduct,
} from "../../../redux/actions/creations/productActions";
import { addProductDetails, deleteProducts, saveMetalPurchase, fetchMetalPurchases, deleteMetalPurchases } from "../../../redux/actions/transactions/inventory/metalPurchaseActions"
import { fetchSuppliers } from "../../../redux/actions/Admin/supplierActions";
import { fetchPartyWastagesByCriteria } from "../../../redux/actions/creations/partyWastageActions";
import { fetchRatesByMainGroup } from '../../../redux/actions/creations/rateActions';

import StoneDetails from "../StoneDetails/StoneDetails";
import Swal from "sweetalert2";
import LoadingOverlay from "react-loading-overlay";
import { validateNum, roundToThree } from "../../../utils/util";
import { GST_PERCENT } from "../../../utils/commonConstants";

export const MetalPurchase = (props) => {
  const { t } = props;
  const [formData, setFromData] = useState(cloneDeep(metalPurchaseForm));
  const [productsFormData, setProductsFormData] = useState(
    cloneDeep(productDetailsForm)
  );
  const [viewForm, setToggleView] = useState(true);
  let [selectedProducts, setSelectedProducts] = useState([]);
  let [selected, setSelected] = useState([]);
  const [productDetails, setProductDetails] = useState([]);
  const [purchaseData, setPurchaseData] = useState([]);
  const [wastageDataObject, setWastageDataObject] = useState(null);
  const [wastagesRetrieved, setWastagesRetrieved] = useState(false);
  const [isTouchEnabled, setIsTouchEnabled] = useState(false);
  const dispatch = useDispatch();

  const suppliers = useSelector((state) => state.supplierReducer.allSuppliers);
  const mainGroups = useSelector((state) => state.MainGroupReducer.allGroups);
  const products = useSelector(
    (state) => state.MainGroupReducer.allProductsByMainGroup
  );
  const subProducts = useSelector(
    (state) => state.productReducer.allSubProductsByProduct
  );
  const productSizes = useSelector(
    (state) => state.productReducer.allSizesByProduct
  );
  const purities = useSelector(
    (state) => state.MainGroupReducer.allPuritiesByMainGroup
  );

  const brands = useSelector((state) => state.brandReducer.allBrands);
  const loader = useSelector((state) => state.metalPurchaseReducer.loading);
  const stoneWeight = useSelector(
    (state) => state.stoneDetailsReducer.stoneWeight
  );
  const stoneAmount = useSelector(
    (state) => state.stoneDetailsReducer.stoneAmount
  );
  const formSaved = useSelector((state) => state.stoneDetailsReducer.formSaved);
  const stoneDetailsList = useSelector(({ stoneDetailsReducer }) => stoneDetailsReducer.stoneDetails)
  const stoneDetailsData = useSelector(
    (state) => state.stoneDetailsReducer.stoneDetailsData
  );
  const metalPurchases = useSelector(state => state.metalPurchaseReducer.allMetalPurchases)
  const partyWastages = useSelector((state) => state.partyWastageReducer.partyWastagesByCriteria)
  const rates = useSelector((state) => state.rateReducer.ratesByMainGroup)

  const mainGroupData = mainGroups.data
    ? mainGroups.data.map((group) => {
      return {
        id: group.id,
        name: group.groupName,
        shortCode: group.shortCode,
      };
    })
    : [];
  const productsData = products.data
    ? products.data.map((product) => {
      return {
        id: product.id,
        name: product.productName,
        shortCode: product.shortCode,
      };
    })
    : [];
  const subProductsData = subProducts.data
    ? subProducts.data.map((subProduct) => {
      return {
        id: subProduct.id,
        name: subProduct.subProductName,
        shortCode: subProduct.shortCode,
      };
    })
    : [];
  const puritiesData = purities.data
    ? purities.data.map((purity) => {
      return {
        id: purity.id,
        name: purity.purity,
      };
    })
    : [];
  const productSizesData = productSizes.data
    ? productSizes.data.map((productSize) => {
      return {
        id: productSize.id,
        name: productSize.productSize,
      };
    })
    : [];
  const suppliersData = suppliers.data
    ? suppliers.data.map((supplier) => {
      return {
        id: supplier.id,
        name: supplier.partyName,
      };
    })
    : [];

  const brandsData = brands.data
    ? brands.data.map((brand) => {
      return {
        id: brand.id,
        groupId: brand.mainGroup.id,
        groupName: brand.mainGroup.groupName,
        brandName: brand.brandName,
        shortCode: brand.shortCode,
      };
    })
    : [];

  let partyWastagesData = partyWastages.data ? partyWastages.data.map((partyWastage) => {
    return {
      id: partyWastage.id,
      partyId: partyWastage.partyId,
      subProductId: partyWastage.subProduct.id,
      productSizeId: partyWastage.productSize.id,
      purityId: partyWastage.purity.id,
      touchValue: partyWastage.touchValue,
      touchPercent: partyWastage.touchPercent,
      mcPerGram: partyWastage.mcPerGram,
      mcPerDir: partyWastage.mcPerDir,
      wastagePerGram: partyWastage.wastagePerGram,
      wastagePerDir: partyWastage.wastagePerDir
    }
  }) : [];

  const allMainGroupRatesData = rates.data ? rates.data.map((rate) => {
    return {
      id: rate.id,
      mainGroupId: rate.mainGroup.id,
      perGramRate: rate.perGramRate,
      purityId: rate.purity.id,
      purityRate: rate.purityRate,
    }
  }) : [];

  const fetchPartyNameById = (partyId) => {
    let selectedParty = suppliersData.find(ele => ele.id === partyId);
    return selectedParty ? selectedParty.name : "";
  }

  const metalPurchaseData = metalPurchases.data
    ? metalPurchases.data.map((purchase) => {
      let productDetails = purchase.productDetails
        ? purchase.productDetails.map((product) => {
          // let stoneDetails = product.stoneDetails
          // identifier: new Date().getTime() + stone.id
          let stoneDetails = product.stoneDetails ? product.stoneDetails.map(stone => {
            return {
              id: stone.id,
              isDeleted: false,
              stoneGroupId: stone.stone.stoneGroup.id,
              stoneGroup: stone.stone.stoneGroup.groupName,
              stoneId: stone.stone.id,
              stone: stone.stone.stoneName,
              // isDiamond: isStoneADiamond(stone.stone.stoneGroup.id),
              stoneSizeId: stone.stoneSize ? stone.stoneSize.id : "",
              stoneSize: stone.stoneSize ? stone.stoneSize.stoneSize : "",
              stoneColorId: stone.stoneColor ? stone.stoneColor.id : "",
              stoneColor: stone.stoneColor ? stone.stoneColor.colorName : "",
              stoneClarityId: stone.stoneClarity ? stone.stoneClarity.id : "",
              stoneClarity: stone.stoneClarity
                ? stone.stoneClarity.clarityName
                : "",
              stonePolishId: stone.stonePolish ? stone.stonePolish.id : "",
              stonePolish: stone.stonePolish
                ? stone.stonePolish.polishName
                : "",
              quantity: stone.quantity,
              uom: stone.uom,
              weightPerGram: stone.weightPerGram,
              rate: stone.rate,
              amount: stone.amount,
              isNoWeight: stone.isNoWeight,
              gstAmount: stone.gstAmount,
              totalAmount: stone.totalAmount,
              remarks: stone.remarks,
              identifier: new Date().getTime() + stone.id
            }
          }) : [];
          return {
            id: product.id,
            mainGroupId: product.subProduct.product.mainGroup.id,
            mainGroup: product.subProduct.product.mainGroup.groupName,
            productId: product.subProduct.product.id,
            product: product.subProduct.product.productName,
            subProductId: product.subProduct.id,
            subProduct: product.subProduct.subProductName,
            purityId: product.purity.id,
            purity: product.purity.purity,
            productSizeId: product.productSize.id,
            productSize: product.productSize.productSize,
            quantity: product.quantity,
            grossWeight: product.grossWeight,
            stoneWeight: product.stoneWeight,
            netWeight: product.netWeight,
            touchCalc: product.touchCalc,
            touchPercent: product.touchPercent,
            touchValue: product.touchValue,
            wastageOn: product.wastageOn,
            wastageValue: product.wastageValue,
            pureWeight: product.pureWeight,
            mcOn: product.mcOn,
            mcValue: product.mcValue,
            rate: product.rate,
            amount: product.amount,
            stoneAmount: product.stoneAmount,
            mcAmount: product.mcAmount,
            gstAmount: product.gstAmount,
            totalAmount: product.totalAmount,
            remarks: product.remarks,
            stoneDetails: stoneDetails,
            identifier: new Date().getTime() + product.id,
          };
        })
        : [];
      return {
        id: purchase.id,
        purchaseType: purchase.purchaseType,
        invoiceDate: purchase.invoiceDate,
        invoiceNo: purchase.invoiceNo,
        partyId: purchase.partyId,
        party: fetchPartyNameById(purchase.partyId),
        productDetails: productDetails,
      };
    })
    : [];

  const setFormValue = (field, value) => {
    formData[field]["value"] = value;
    setFromData({ ...formData });
  };
  const setProductFormValue = (field, value) => {
    productsFormData[field]["value"] = value;
    setProductsFormData({ ...productsFormData });
  };
  const resetForm = () => {
    map(formData, function (value, key, object) {
      formData[key]["value"] = "";
    });
    setFromData({ ...formData });
    setProductDetails([]);
    resetProductDetailsForm();
  };

  const saveForm = () => {
    let isFormValid = true;
    map(formData, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      formData[key] = { ...value, ...isValid }
    });
    if (isFormValid) {
      if (productDetails.length < 1) {
        Swal.fire({
          position: "top-end",
          title: 'Please enter product details.!',
          text: 'And try saving purchase info',
          icon: 'info',
          showConfirmButton: false,
          timer: 2000
        })
      } else {
        let data = {};
        map(formData, function (value, key, object) {
          data = {
            ...data,
            [key]: value.value
          }
        });
        data["productDetails"] = productDetails;
        Swal.fire({
          title: 'Confirm Saving Purchase Info.!',
          text: 'Continue to save / Cancel to stop',
          icon: 'info',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Continue'
        }).then((result) => {
          if (result.isConfirmed) {
            dispatch(saveMetalPurchase(data)).then(() => {
              resetForm();
              resetProductDetailsForm();
              setProductDetails([]);
              setPurchaseData();
            });
          }
        })
      }
    }
  };

  const addProducts = () => {
    let isProductFormValid = true;
    map(productsFormData, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isProductFormValid = false;
      }
      productsFormData[key] = { ...value, ...isValid };
    });
    if (!productsFormData.touchCalc.value && productsFormData.wastageOn.value == "") {
      productsFormData.wastageOn.error = "Please select any one wastage type";
      isProductFormValid = false;
    }
    if (productsFormData.rate.value <= 0) {
      productsFormData.rate.error = "Rate must be greater than 0";
      isProductFormValid = false;
    }
    if (productsFormData.amount.value <= 0) {
      productsFormData.amount.error = "Amount must be greater than 0";
      isProductFormValid = false;
    }
    if (isProductFormValid) {
      let productDetailsData = {};
      map(productsFormData, function (value, key, object) {
        productDetailsData = {
          ...productDetailsData,
          [key]: value.value,
          isDeleted: false,
        };
      });
      let list = [...productDetails];
      productDetailsData.stoneDetails = formSaved ? stoneDetailsList : [];

      if (productDetailsData.identifier) {
        let eleIndex = list.findIndex(
          (ele) => ele.identifier === productDetailsData.identifier
        );
        list[eleIndex] = productDetailsData;
      } else {
        productDetailsData["identifier"] = new Date().getTime();
        list.push(cloneDeep(productDetailsData));
      }

      // list.push(cloneDeep(productDetailsData));
      setProductDetails(list);
      // data["productDetails"] = list;
      // setPurchaseData(data);
      dispatch(addProductDetails(list))
      // dispatch(clearStoneDetails());
      resetProductDetailsForm();
      dispatch(saveStoneDetailsForm(true));
    }
  };

  const resetProductDetailsForm = () => {
    map(productsFormData, function (value, key, object) {
      if (key === "touchCalc") {
        document.getElementById("touchCalc").checked = false;
        setProductFormValue("touchCalc", false);
      } else if (productsFormData[key]["type"] === 'number') {
        productsFormData[key]["value"] = 0;
      } else {
        productsFormData[key]["value"] = "";
      }
    });
    setProductsFormData({ ...productsFormData });
    dispatch(clearStoneDetails());
  };

  const validateField = (fieldObj) => {
    let errorObj = { isValid: true, error: null };
    if (fieldObj.required && !fieldObj.value) {
      errorObj.isValid = false;
      errorObj.error = "Please fill this field";
    } else if (fieldObj.value && fieldObj.value.length < fieldObj.min) {
      errorObj.isValid = false;
      errorObj.error = `Please enter min length ${fieldObj.min}`;
    } else if (fieldObj.value && fieldObj.value.length > fieldObj.max) {
      errorObj.isValid = false;
      errorObj.error = `Please enter max length ${fieldObj.max}`;
    }
    return errorObj;
  };

  const editAction = (ele) => {
    resetForm();
    resetProductDetailsForm();
    setProductDetails([]);
    setFormValue('id', ele.id)
    setFormValue('purchaseType', ele.purchaseType)
    setFormValue('invoiceNo', ele.invoiceNo)
    setFormValue('invoiceDate', ele.invoiceDate)
    setFormValue('partyId', ele.partyId)
    setFormValue('party', ele.party)
    setFormValue('productDetails', ele.productDetails)
    setProductDetails(ele.productDetails);
  };

  const editProductDetails = (ele) => {
    setProductFormValue('mainGroupId', ele.mainGroupId);
    setProductFormValue('mainGroup', ele.mainGroup);
    setProductFormValue('productId', ele.productId);
    setProductFormValue('product', ele.product);
    setProductFormValue('subProductId', ele.subProductId);
    setProductFormValue('subProduct', ele.subProduct);
    setProductFormValue('purityId', ele.purityId);
    setProductFormValue('purity', ele.purity);
    setProductFormValue('productSizeId', ele.productSizeId);
    setProductFormValue('productSize', ele.productSize);
    setProductFormValue('quantity', ele.quantity);
    setProductFormValue('grossWeight', ele.grossWeight);
    setProductFormValue('stoneWeight', ele.stoneWeight);
    setProductFormValue('netWeight', ele.netWeight);
    document.getElementById("touchCalc").checked = ele.touchCalc;
    setProductFormValue('touchCalc', ele.touchCalc);
    setProductFormValue('touchPercent', ele.touchPercent);
    setProductFormValue('touchValue', ele.touchValue);
    setProductFormValue('wastageOn', ele.wastageOn);
    setProductFormValue('wastageValue', ele.wastageValue);
    setProductFormValue('pureWeight', ele.pureWeight);
    setProductFormValue('mcOn', ele.mcOn);
    setProductFormValue('mcValue', ele.mcValue);
    setProductFormValue('rate', ele.rate);
    setProductFormValue('amount', ele.amount);
    setProductFormValue('stoneAmount', ele.stoneAmount);
    setProductFormValue('mcAmount', ele.mcAmount);
    setProductFormValue('gstAmount', ele.gstAmount);
    setProductFormValue('totalAmount', ele.totalAmount);
    setProductFormValue('remarks', ele.remarks);
    setProductFormValue("identifier", ele.identifier);
    dispatch(clearStoneDetails());
    dispatch(saveStoneWeight(ele.stoneDetails))
  }

  const deleteAction = (data) => {
    let selectedIds;
    if (data instanceof Array && data.length > 0) {
      selectedIds = data.map((ele) => {
        return ele.id;
      });
    } else {
      selectedIds = data.id;
    }
    Swal.fire({
      title: "Confirm Deleting Metal Purchase.!",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deleteMetalPurchases(selectedIds.toString())).then(() => {
          selected = [];
          setSelected(selected);
        });
      }
    });
  };

  const deleteProductDetails = (data) => {
    let selectedIds;
    if (data instanceof Array && data.length > 0) {
      selectedIds = data.map((ele) => {
        return ele.identifier;
      });
    } else {
      selectedIds = data.identifier;
    }
    Swal.fire({
      title: "Confirm Deleting Stone Details.!",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        productDetails.splice(productDetails.findIndex(product => product.identifier === selectedIds[0]), 1)
        let filtered = productDetails.filter(function (el) {
          return el != null;
        });
        setProductDetails(filtered)
      }
    });

  };

  useEffect(() => {
    dispatch(fetchSuppliers());
    dispatch(fetchMainGroups());
    dispatch(fetchMetalPurchases());
  }, []);

  useEffect(() => {
    // setProductFormValue("productId", "");
    // setProductFormValue("product", "");
    // setProductFormValue("purityId", "");
    // setProductFormValue("purity", "");
    dispatch(
      fetchProductsByMainGroup(get(productsFormData, "mainGroupId.value"))
    );
    dispatch(
      fetchPuritiesByMainGroup(get(productsFormData, "mainGroupId.value"))
    );
    dispatch(fetchRatesByMainGroup(get(productsFormData, "mainGroupId.value")));
  }, [get(productsFormData, "mainGroupId.value")]);

  useEffect(() => {
    // setProductFormValue("subProductId", "");
    // setProductFormValue("subProduct", "");
    // setProductFormValue("productSizeId", "");
    // setProductFormValue("productSize", "");
    dispatch(
      fetchSubProductsByProduct(get(productsFormData, "productId.value"))
    );
    dispatch(fetchSizesByProduct(get(productsFormData, "productId.value")));
  }, [get(productsFormData, "productId.value")]);

  const toggleView = () => {
    const formNode = document.getElementById("form");
    formNode.classList.toggle("collapse-panel");
    setToggleView(!viewForm);
  };

  const partyChange = (e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    setFormValue("partyId", e.target.value);
    setFormValue("party", label);
  };

  const selectOnChange = (idKey, labelKey, e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    setProductFormValue(idKey, e.target.value);
    setProductFormValue(labelKey, label);
  };

  const stoneForm = () => {
    dispatch(saveStoneDetailsForm(false));
    setProductFormValue('stoneWeight', stoneWeight);
  };

  useEffect(() => {
    if (formSaved) {
      setProductFormValue('stoneWeight', stoneWeight);
      setProductFormValue('stoneAmount', stoneAmount);
    }
  }, [formSaved])

  useEffect(() => {
    // resetCalculatedFields();
    let partyId = get(formData, "partyId.value");
    let subProductId = get(productsFormData, "subProductId.value");
    let purityId = get(productsFormData, "purityId.value");
    let productSizeId = get(productsFormData, "productSizeId.value");
    if (partyId && subProductId && purityId && productSizeId) {
      dispatch(fetchPartyWastagesByCriteria(partyId, subProductId, purityId, productSizeId))
        .then(() => setWastagesRetrieved(true));
    } else {
      partyWastagesData = [];
      setWastageDataObject(null);
    }
  }, [get(formData, "partyId.value"), get(productsFormData, "subProductId.value"), get(productsFormData, "purityId.value"), get(productsFormData, "productSizeId.value")]);

  useEffect(() => {
    // resetCalculatedFields();
    setWastageDataObject(partyWastagesData[0]);
    setWastagesRetrieved(false);
  }, [wastagesRetrieved]);

  const resetCalculatedFields = () => {
    document.getElementById("touchCalc").checked = false;
    setProductFormValue("touchCalc", false);
    setIsTouchEnabled(false);
    setProductFormValue("touchPercent", 0);
    setProductFormValue("touchValue", 0);
    setProductFormValue("wastageOn", "");
    setProductFormValue("wastageValue", 0);
    setProductFormValue("pureWeight", 0);
    setProductFormValue("mcOn", "");
    setProductFormValue("mcValue", 0);
  }

  // useEffect(() => {
  //   // resetCalculatedFields();
  // }, [wastageDataObject])

  useEffect(() => {
    let grossWeight = get(productsFormData, "grossWeight.value");
    let stoneWeight = get(productsFormData, "stoneWeight.value");
    let netWeight = 0;
    grossWeight = validateNum(grossWeight) ? grossWeight : 0;
    stoneWeight = validateNum(stoneWeight) ? stoneWeight : 0;
    if (grossWeight >= stoneWeight) {
      netWeight = grossWeight - stoneWeight;
      setProductFormValue("netWeight", roundToThree(netWeight));
    }
  }, [get(productsFormData, "grossWeight.value"), get(productsFormData, "stoneWeight.value")])

  useEffect(() => {
    let netWeight = get(productsFormData, "netWeight.value");
    let pureWeight = 0;
    if (isTouchEnabled) {
      let touchPercent = get(productsFormData, "touchPercent.value");
      let touchValue = get(productsFormData, "touchValue.value");
      touchPercent = validateNum(touchPercent) ? touchPercent : 0;
      touchValue = validateNum(touchValue) ? touchValue : 0;
      let calculateTouch = 0;
      calculateTouch = roundToThree(netWeight * (touchPercent / 100));
      calculateTouch = roundToThree(calculateTouch + (touchValue / 100));
      pureWeight = netWeight + calculateTouch;
    } else {
      let wastageOn = get(productsFormData, "wastageOn.value");
      let wastageValue = get(productsFormData, "wastageValue.value");
      wastageValue = validateNum(wastageValue) ? wastageValue : 0;
      if (wastageOn && wastageOn == "Percent") {
        pureWeight = netWeight + roundToThree(netWeight * (wastageValue / 100));
      } else {
        pureWeight = netWeight + wastageValue;
      }
    }
    setProductFormValue("pureWeight", roundToThree(pureWeight));
  }, [isTouchEnabled, get(productsFormData, "wastageOn.value"), get(productsFormData, "netWeight.value")]);

  useEffect(() => {
    let pureWeight = get(productsFormData, "pureWeight.value");
    let mcOn = get(productsFormData, "mcOn.value");
    let mcValue = get(productsFormData, "mcValue.value");
    let mcAmount = 0;
    if (mcOn && mcOn == "Gram") {
      mcAmount = roundToThree(pureWeight * mcValue);
    } else {
      mcAmount = mcValue;
    }
    setProductFormValue("mcAmount", roundToThree(mcAmount));
  }, [get(productsFormData, "pureWeight.value"), get(productsFormData, "mcOn.value"), get(productsFormData, "mcValue.value")])

  useEffect(() => {
    let selectedPurity = get(productsFormData, 'purityId.value');
    let selectedMainGroup = get(productsFormData, 'mainGroupId.value');
    if (selectedMainGroup != "" && selectedPurity != "") {
      if (allMainGroupRatesData.length > 0) {
        let filteredRateObject = allMainGroupRatesData.filter((ele) => (ele.mainGroupId === selectedMainGroup) && (ele.purityId === selectedPurity))[0];
        if (filteredRateObject) {
          setProductFormValue('rate', filteredRateObject.purityRate);
        } else {
          setProductFormValue('rate', 0);
          Swal.fire({
            position: "top-end",
            title: 'No Rates for the selected purity.!',
            text: 'Please define rates before going forward',
            icon: 'info',
            showConfirmButton: false,
            timer: 2000
          })
        }
      } else {
        setProductFormValue('rate', 0);
        Swal.fire({
          position: "top-end",
          title: 'No Rates defined for selected mainGroup.!',
          text: 'Please define rates before generating barcode',
          icon: 'info',
          showConfirmButton: false,
          timer: 2000
        })
      }
    }
  }, [get(productsFormData, 'purityId.value')])

  useEffect(() => {
    let pureWeight = get(productsFormData, "pureWeight.value");
    let rate = get(productsFormData, "rate.value");
    setProductFormValue('amount', roundToThree(pureWeight * rate));
  }, [get(productsFormData, "pureWeight.value"), get(productsFormData, "rate.value")])

  useEffect(() => {
    let amount = get(productsFormData, "amount.value");
    let stoneAmount = get(productsFormData, "stoneAmount.value");
    let mcAmount = get(productsFormData, "mcAmount.value");
    let totalAmount = amount + stoneAmount + mcAmount;
    let gstAmount = roundToThree(totalAmount * GST_PERCENT / 100);
    setProductFormValue('gstAmount', gstAmount);
    setProductFormValue('totalAmount', roundToThree(totalAmount + gstAmount));
  }, [get(productsFormData, "amount.value"), get(productsFormData, "stoneAmount.value"), get(productsFormData, "mcAmount.value")])

  const touchCalcOnChange = (e) => {
    let isTouch = !productsFormData.touchCalc.value;
    setProductFormValue("touchCalc", isTouch);
    setIsTouchEnabled(isTouch);
    if (wastageDataObject && isTouch) {
      // if (isTouch) {
      setProductFormValue("touchPercent", wastageDataObject.touchPercent);
      setProductFormValue("touchValue", wastageDataObject.touchValue);
      setProductFormValue("wastageOn", "");
      setProductFormValue("wastageValue", 0);
    } else {
      setProductFormValue("touchPercent", 0);
      setProductFormValue("touchValue", 0);
      setProductFormValue("wastageOn", "");
      setProductFormValue("wastageValue", 0);
      // }
    }
  }

  const wastageTypeOnChange = (e, value) => {
    setProductFormValue("wastageOn", value)
    let wastageValue = 0;
    if (wastageDataObject) {
      if (value === "Percent") {
        wastageValue = wastageDataObject.wastagePerGram;
      } else {
        wastageValue = wastageDataObject.wastagePerDir;
      }
    }
    setProductFormValue("wastageValue", wastageValue);
  }

  const mcOnChange = (e, value) => {
    setProductFormValue("mcOn", value);
    let mcValue = 0;
    if (wastageDataObject) {
      if (value === 'Gram') {
        mcValue = wastageDataObject.mcPerGram;
      } else {
        mcValue = wastageDataObject.mcPerDir;
      }
    }
    setProductFormValue("mcValue", mcValue);
  }

  return (
    <div className="panel panel-default hero-panel">
      <LoadingOverlay active={loader} spinner text="Loading...">
        <div className="panel-heading">
          <h1 className="panel-title">
            {t("transactions.metal")} {t("transactions.purchase")}
          </h1>
          <div className="panel-options">
            <a href="#" className="text-info collapseBtn" title="Show Add Form">
              {viewForm ? (
                <i
                  className="zmdi zmdi-minus-circle  zmdi-hc-2x"
                  onClick={() => {
                    toggleView();
                  }}
                ></i>
              ) : (
                  <i
                    className="zmdi zmdi-plus-circle zmdi-hc-2x"
                    onClick={() => {
                      toggleView();
                    }}
                  ></i>
                )}
            </a>
          </div>
        </div>
        <div className="panel-body">
          <div className="panel update-panel form-pnel-container" id="form">
            {/* <div className="text-danger">* {t("common.requiredFieldsMsg")} </div> */}
            <div className="panel-body p-t-0">
              <div className="row">
                <div className="col-md-3">
                  <div className="panel-heading p-l-0">
                    <h4 className="panel-title">
                      {t("transactions.purchase")} {t("common.type")}
                    </h4>
                  </div>
                  <div className="form-group no-min-height">
                    <div className="radio  radio-primary m-b-0">
                      <input
                        type="radio"
                        name="purchaseType"
                        value="Approval"
                        checked={
                          get(formData, "purchaseType.value") === "Approval"
                        }
                        onChange={(e) =>
                          setFormValue("purchaseType", "Approval")
                        }
                      />
                      <label htmlFor="">{t("transactions.approval")}</label>
                    </div>
                    <div className="radio  radio-primary m-b-0">
                      <input
                        type="radio"
                        name="purchaseType"
                        value="Invoice"
                        checked={
                          get(formData, "purchaseType.value") === "Invoice"
                        }
                        onChange={(e) =>
                          setFormValue("purchaseType", "Invoice")
                        }
                      />
                      <label htmlFor="">{t("transactions.invoice")}</label>
                    </div>
                    <div className="invalid-feedback">
                      {get(formData, "purchaseType.error")}
                    </div>
                  </div>
                </div>
                <div className="col-md-9">
                  <div className="panel-heading">
                    <h4 className="panel-title">
                      {t("transactions.invoice")} &amp; {t("common.Party")}{" "}
                      {t("common.details")}
                    </h4>
                  </div>
                  <div className="form-group col-md-4">
                    <label htmlFor="">
                      {t("transactions.invoice")} {t("common.number")}
                      <span className="asterisk">*</span>
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="invoiceNo"
                      name="invoiceNo"
                      value={get(formData, "invoiceNo.value")}
                      onChange={(e) =>
                        setFormValue("invoiceNo", e.target.value)
                      }
                    />
                    <div className="invalid-feedback">
                      {get(formData, "invoiceNo.error")}
                    </div>
                  </div>
                  <div className="form-group col-md-4">
                    <label htmlFor="">
                      {t("transactions.invoice")} {t("common.date")}
                      <span className="asterisk">*</span>
                    </label>
                    <input
                      type="date"
                      className="form-control"
                      placeholder=""
                      value={get(formData, "invoiceDate.value")}
                      onChange={(e) =>
                        setFormValue("invoiceDate", e.target.value)
                      }
                    />
                    <div className="invalid-feedback">
                      {get(formData, "invoiceDate.error")}
                    </div>
                  </div>
                  <div className="form-group col-md-4">
                    <label htmlFor="">
                      {t("common.Party")} {t("common.name")}
                      <span className="asterisk">*</span>
                    </label>
                    <Select
                      name="partyId"
                      value={get(formData, "partyId.value")}
                      onChange={(e) => partyChange(e)}
                      data={suppliersData}
                    />
                    <div className="invalid-feedback">
                      {get(formData, "partyId.error")}
                    </div>
                  </div>
                </div>
              </div>
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h4 className="panel-title">
                    {t("creations.product")} {t("common.details")}
                  </h4>
                </div>
                <div className="panel-body">
                  <div className="row">
                    <div className="col-sm-6 col-md-3 col-lg-3">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("creations.mainGroup")}
                          <span className="asterisk">*</span>
                        </label>
                        <Select
                          name="mainGroupId"
                          value={get(productsFormData, "mainGroupId.value")}
                          onChange={(e) =>
                            selectOnChange("mainGroupId", "mainGroup", e)
                          }
                          data={mainGroupData}
                        />
                        <div className="invalid-feedback">
                          {get(productsFormData, "mainGroupId.error")}
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-3 col-lg-3">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("creations.productName")}
                          <span className="asterisk">*</span>
                          <a
                            href=""
                            className=""
                            data-toggle="modal"
                            data-target="#addProduct"
                            data-backdrop="static"
                            data-keyboard="false"
                          >
                            <i className="zmdi zmdi-plus-circle f-s-16"></i>
                          </a>
                        </label>
                        <Select
                          name="productId"
                          value={get(productsFormData, "productId.value")}
                          onChange={(e) =>
                            selectOnChange("productId", "product", e)
                          }
                          data={productsData}
                        />
                        <div className="invalid-feedback">
                          {get(productsFormData, "productId.error")}
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-2 col-lg-2">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("creations.subProduct")}
                          <span className="asterisk">*</span>
                          <a
                            href=""
                            className=""
                            data-toggle="modal"
                            data-target="#addProduct"
                            data-backdrop="static"
                            data-keyboard="false"
                          >
                            <i className="zmdi zmdi-plus-circle f-s-16"></i>
                          </a>
                        </label>
                        <Select
                          name="subProductId"
                          value={get(productsFormData, "subProductId.value")}
                          onChange={(e) =>
                            selectOnChange("subProductId", "subProduct", e)
                          }
                          data={subProductsData}
                        />
                        <div className="invalid-feedback">
                          {get(productsFormData, "subProductId.error")}
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-2 col-lg-2">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.Purity")}
                          <span className="asterisk">*</span>
                          <a
                            href=""
                            className=""
                            data-toggle="modal"
                            data-target="#addProduct"
                            data-backdrop="static"
                            data-keyboard="false"
                          >
                            <i className="zmdi zmdi-plus-circle f-s-16"></i>
                          </a>
                        </label>
                        <Select
                          name="purityId"
                          value={get(productsFormData, "purityId.value")}
                          onChange={(e) =>
                            selectOnChange("purityId", "purity", e)
                          }
                          data={puritiesData}
                        />
                        <div className="invalid-feedback">
                          {get(productsFormData, "purityId.error")}
                        </div>
                      </div>
                    </div>
                    <div className="col-md-2">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("creations.productSize")}
                          <span className="asterisk">*</span>
                        </label>
                        <Select
                          name="productSizeId"
                          value={get(productsFormData, "productSizeId.value")}
                          onChange={(e) =>
                            selectOnChange("productSizeId", "productSize", e)
                          }
                          data={productSizesData}
                        />
                        <div className="invalid-feedback">
                          {get(productsFormData, "productSizeId.error")}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h4 className="panel-title">
                    {t("common.Weight")} &amp; {t("common.Purity")}
                  </h4>
                </div>
                <div className="panel-body">
                  <div className="row">
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.quantity")}
                          <span className="asterisk">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="quantity"
                          name="quantity"
                          value={get(productsFormData, "quantity.value")}
                          onChange={(e) =>
                            setProductFormValue("quantity", e.target.value)
                          }
                        />
                        <div className="invalid-feedback">
                          {get(productsFormData, "quantity.error")}
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.gross")} {t("common.Weight")}
                          <span className="asterisk">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="grossWeight"
                          name="grossWeight"
                          value={get(productsFormData, "grossWeight.value")}
                          onChange={(e) =>
                            setProductFormValue("grossWeight", e.target.value)
                          }
                        />
                        <div className="invalid-feedback">
                          {get(productsFormData, "grossWeight.error")}
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.stone")} {t("common.Weight")}
                          <span className="asterisk">*</span>
                          <a
                            href="#"
                            id="stoneDetails"
                            data-toggle="modal"
                            data-target="#addNew"
                            onClick={stoneForm}
                          >
                            <i className="zmdi zmdi-plus-circle f-s-16"></i>
                          </a>
                        </label>
                        {/* <input type="text" className="form-control" placeholder="" disabled /> */}
                        <input
                          type="text"
                          className="form-control"
                          placeholder="stoneWeight"
                          name="stoneWeight"
                          value={get(productsFormData, "stoneWeight.value")}
                          disabled
                        />
                        <div className="invalid-feedback">
                          {get(productsFormData, "stoneWeight.error")}
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        {t("common.net")} {t("common.Weight")}
                        <span className="asterisk">*</span>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="netWeight"
                          name="netWeight"
                          value={get(productsFormData, "netWeight.value")}
                          onChange={(e) =>
                            setProductFormValue("netWeight", e.target.value)
                          }
                          disabled={get(productsFormData, "netWeight.disabled")}
                        />
                        <div className="invalid-feedback">
                          {get(productsFormData, "netWeight.error")}
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="bg-light m-t-5 m-b-5 p-10">
                    <div className="m-b-5">
                      <h4 className="panel-title">
                        {t("transactions.supplierWastageOrMc")}
                      </h4>
                    </div>
                    <div className="row">
                      <div className="col-md-2">
                        <div className="form-group">
                          <div className="checkbox checkbox-primary">
                            <input
                              type="checkbox"
                              name="touchCalc"
                              id="touchCalc"
                              defaultChecked={get(
                                productsFormData,
                                "touchCalc.value"
                              )}
                              onChange={(e) => touchCalcOnChange(e)}
                            />
                            <label htmlFor="">
                              {t("transactions.touchCalc")}
                            </label>
                          </div>
                        </div>
                      </div>
                      {isTouchEnabled ?
                        <div>
                          <div className="col-md-2 wst1">
                            <div className="form-group">
                              <label htmlFor="">
                                {t("creations.touchPercent")}
                                <span className="asterisk">*</span>
                              </label>
                              <input
                                type="text"
                                className="form-control"
                                placeholder="touchPercent"
                                name="touchPercent"
                                value={get(
                                  productsFormData,
                                  "touchPercent.value"
                                )}
                                onChange={(e) =>
                                  setProductFormValue(
                                    "touchPercent",
                                    e.target.value
                                  )
                                }
                                disabled={get(productsFormData, "touchPercent.disabled")}
                              />
                              <div className="invalid-feedback">
                                {get(productsFormData, "touchPercent.error")}
                              </div>
                            </div>
                          </div>
                          <div className="col-md-2 wst1">
                            <div className="form-group">
                              <label htmlFor="">
                                {t("creations.touchValue")}
                                <span className="asterisk">*</span>
                              </label>
                              <input
                                type="text"
                                className="form-control"
                                placeholder="touchValue"
                                name="touchValue"
                                value={get(productsFormData, "touchValue.value")}
                                onChange={(e) =>
                                  setProductFormValue("touchValue", e.target.value)
                                }
                                disabled={get(productsFormData, "touchValue.disabled")}
                              />
                              <div className="invalid-feedback">
                                {get(productsFormData, "touchValue.error")}
                              </div>
                            </div>
                          </div>
                        </div>
                        :
                        <div>
                          <div className="col-md-2 wst1">
                            <div className="form-group">
                              <label htmlFor="">{t("common.Wastage")} {t("common.per")}<span className="asterisk">*</span></label>
                              <div className="radio radio-inline radio-primary">
                                <input
                                  type="radio"
                                  name="wastageOn"
                                  value="Percent"
                                  checked={get(productsFormData, 'wastageOn.value') === "Percent"}
                                  onChange={(e) => wastageTypeOnChange(e, "Percent")} />
                                <label htmlFor="">{t("common.percent")}</label>
                              </div>
                              <div className="radio radio-inline radio-primary">
                                <input
                                  type="radio"
                                  name="wastageOn"
                                  value="Direct"
                                  checked={get(productsFormData, 'wastageOn.value') === "Direct"}
                                  onChange={(e) => wastageTypeOnChange(e, "Direct")} />
                                <label htmlFor="">{t("common.direct")}</label>
                              </div>
                              <div className="invalid-feedback">{get(productsFormData, 'wastageOn.error')}</div>
                            </div>
                          </div>
                          <div className="col-md-2 wst1">
                            <div className="form-group">
                              <label htmlFor="">
                                {t("common.Wastage")} {t("common.value")}
                                <span className="asterisk">*</span>
                              </label>
                              <input
                                type="text"
                                className="form-control"
                                placeholder="wastageValue"
                                name="wastageValue"
                                value={get(productsFormData, "wastageValue.value")}
                                onChange={(e) =>
                                  setProductFormValue("wastageValue", e.target.value)
                                }
                                disabled={get(productsFormData, "wastageValue.disabled")}
                              />
                              <div className="invalid-feedback">
                                {get(productsFormData, "wastageValue.error")}
                              </div>
                            </div>
                          </div>
                        </div>}

                      <div className="col-md-2">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("transactions.pureWeight")}
                            <span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="pureWeight"
                            name="pureWeight"
                            value={get(productsFormData, "pureWeight.value")}
                            onChange={(e) =>
                              setProductFormValue("pureWeight", e.target.value)
                            }
                            disabled={get(productsFormData, "pureWeight.disabled")}
                          />
                          <div className="invalid-feedback">
                            {get(productsFormData, "pureWeight.error")}
                          </div>
                        </div>
                      </div>
                      <div className="col-md-2">
                        <div className="form-group">
                          <label htmlFor="">{t("common.mc")} {t("common.on")}<span className="asterisk">*</span></label>
                          <div className="radio radio-inline radio-primary">
                            <input
                              type="radio"
                              name="mcOn"
                              value="Gram"
                              checked={get(productsFormData, 'mcOn.value') === "Gram"}
                              onChange={(e) => mcOnChange(e, "Gram")} />
                            <label htmlFor="">{t("common.gram")}</label>
                          </div>
                          <div className="radio radio-inline radio-primary">
                            <input
                              type="radio"
                              name="mcOn"
                              value="Direct"
                              checked={get(productsFormData, 'mcOn.value') === "Direct"}
                              onChange={(e) => mcOnChange(e, "Direct")} />
                            <label htmlFor="">{t("common.direct")}</label>
                          </div>
                          <div className="invalid-feedback">{get(productsFormData, 'mcOn.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-2">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.mc")} {t("common.value")}
                            <span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="mcValue"
                            name="mcValue"
                            value={get(productsFormData, "mcValue.value")}
                            onChange={(e) =>
                              setProductFormValue("mcValue", e.target.value)
                            }
                            disabled={get(productsFormData, "mcValue.disabled")}
                          />
                          <div className="invalid-feedback">
                            {get(productsFormData, "mcValue.error")}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* Add Stone model */}
              <div
                className="modal fade"
                id="addNew"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="myModalLabel"
              >
                <StoneDetails fetchPartyNameById={fetchPartyNameById} partyId={get(formData, "partyId.value")} />
              </div>

              <div className="panel panel-default m-t-10">
                <div className="panel-heading">
                  <h4 className="panel-title">
                    {t("common.rate")} &amp; {t("common.amount")}
                  </h4>
                </div>
                <div className="panel-body">
                  <div className="row">
                    <div className="col-sm-6 col-md-2 col-lg-2">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.rate")}
                          <span className="asterisk">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="rate"
                          name="rate"
                          value={get(productsFormData, "rate.value")}
                          onChange={(e) =>
                            setProductFormValue("rate", e.target.value)
                          }
                          disabled={get(productsFormData, "rate.disabled")}
                        />
                        <div className="invalid-feedback">
                          {get(productsFormData, "rate.error")}
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-2 col-lg-2">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.amount")}
                          <span className="asterisk">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="amount"
                          name="amount"
                          value={get(productsFormData, "amount.value")}
                          onChange={(e) =>
                            setProductFormValue("amount", e.target.value)
                          }
                          disabled={get(productsFormData, "amount.disabled")}
                        />
                        <div className="invalid-feedback">
                          {get(productsFormData, "amount.error")}
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-2 col-lg-2">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.stone")} {t("common.amount")}
                          <span className="asterisk">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="stoneAmount"
                          name="stoneAmount"
                          disabled
                          value={get(productsFormData, "stoneAmount.value")}
                          onChange={(e) =>
                            setProductFormValue("stoneAmount", e.target.value)
                          }
                        />
                        <div className="invalid-feedback">
                          {get(productsFormData, "stoneAmount.error")}
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-2 col-lg-2">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("transactions.mcAmount")}
                          <span className="asterisk">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="mcAmount"
                          name="mcAmount"
                          value={get(productsFormData, "mcAmount.value")}
                          onChange={(e) =>
                            setProductFormValue("mcAmount", e.target.value)
                          }
                          disabled={get(productsFormData, "mcAmount.disabled")}
                        />
                        <div className="invalid-feedback">
                          {get(productsFormData, "mcAmount.error")}
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-2 col-lg-2">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("creations.gst")} {t("common.amount")}
                          <span className="asterisk">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="gstAmount"
                          name="gstAmount"
                          value={get(productsFormData, "gstAmount.value")}
                          onChange={(e) =>
                            setProductFormValue("gstAmount", e.target.value)
                          }
                          disabled={get(productsFormData, "gstAmount.disabled")}
                        />
                        <div className="invalid-feedback">
                          {get(productsFormData, "gstAmount.error")}
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-2 col-lg-2">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.total")} {t("common.amount")}
                          <span className="asterisk">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="totalAmount"
                          name="totalAmount"
                          value={get(productsFormData, "totalAmount.value")}
                          onChange={(e) =>
                            setProductFormValue("totalAmount", e.target.value)
                          }
                          disabled={get(productsFormData, "totalAmount.disabled")}
                        />
                        <div className="invalid-feedback">
                          {get(productsFormData, "totalAmount.error")}
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 col-lg-4">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.remarks")}
                          <span className="asterisk">*</span>
                        </label>
                        <textarea
                          type="text"
                          className="form-control"
                          placeholder="remarks"
                          name="remarks"
                          value={get(productsFormData, "remarks.value")}
                          onChange={(e) =>
                            setProductFormValue("remarks", e.target.value)
                          }
                        />
                        <div className="invalid-feedback">
                          {get(productsFormData, "remarks.error")}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="panel-footer text-center">
              <a href="#" className="btn btn-primary" onClick={addProducts}>
                <i className="zmdi zmdi-playlist-plus"></i> {t("common.addToList")}
              </a>
              <a href="#" className="btn btn-default" onClick={resetProductDetailsForm}>
                <i className="zmdi zmdi-close"></i> {t("common.clear")}
              </a>
            </div>
            <div className={`${productDetails && productDetails.length > 0 ? '' : 'hide'}`}>
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h3 className="panel-title">Selected List</h3>
                  <div className="panel-options">
                    <p>
                      {selectedProducts.length > 0 ? (
                        <a
                          href="#"
                          className="btn btn-info btn-sm"
                          onClick={() => deleteProductDetails(selectedProducts)}
                        >
                          <i className="zmdi zmdi-delete"></i>{" "}
                          {t("common.delete")}
                        </a>
                      ) : null}
                    </p>
                  </div>
                </div>
                <div className="panel-body">
                  <div style={{ height: "100%", boxSizing: "border-box" }}>
                    <CollapseGrid
                      data={productDetails}
                      columns={[
                        {
                          id: "amount",
                          label: "Amount",
                          minWidth: 100,
                        },
                        {
                          id: "quantity",
                          label: "Quantity",
                          minWidth: 100,
                        },
                        {
                          id: "rate",
                          label: "Rate",
                          minWidth: 100,
                        },
                        {
                          id: "uom",
                          label: "Uom",
                          minWidth: 100,
                        },
                        {
                          id: "weightPerGram",
                          label: "Weight(Gram)",
                          minWidth: 100,
                        },
                      ]}
                      expandColumns={[
                        {
                          id: "product",
                          children: "stoneDetails",
                          isEdit: true,
                          isDelete: true,
                        },
                      ]}
                      uniqueId="identifier"
                      sortColumn="product"
                      sortOrder="asc"
                      isCheckBox={false}
                      editData={editProductDetails}
                      deleteData={deleteProductDetails}
                    />
                  </div>
                </div>
              </div>

              <div>
                <div className="panel-footer text-center">
                  <a href="#" className="btn btn-primary" onClick={saveForm}>
                    <i className="zmdi zmdi-check"></i> {t("common.save")}
                  </a>
                  <a href="#" className="btn btn-default" onClick={resetForm}>
                    <i className="zmdi zmdi-close"></i> {t("common.clear")}
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">Metal Purchase List</h3>
              <div className="panel-options">
                <p>
                  {selected.length > 0 ? (
                    <a
                      href="#"
                      className="btn btn-info btn-sm"
                      onClick={() => {
                        deleteAction(selected);
                      }}
                    >
                      <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                    </a>
                  ) : null}
                </p>
              </div>
            </div>
            <div className="panel-body">
              <CollapseGrid
                data={metalPurchaseData}
                columns={[
                  {
                    id: "product",
                    label: "Product",
                    minWidth: 100,
                  },
                  {
                    id: "purity",
                    label: "Purity",
                    minWidth: 100,
                  },
                  {
                    id: "productSize",
                    label: "Size",
                    minWidth: 100,
                  },
                  {
                    id: "quantity",
                    label: "Quantity",
                    minWidth: 100,
                  },
                  {
                    id: "netWeight",
                    label: "Net Weight",
                    minWidth: 100,
                  },
                  {
                    id: "rate",
                    label: "Rate",
                    minWidth: 100,
                  },
                  {
                    id: "totalAmount",
                    label: "Total Amount",
                    minWidth: 100,
                  },
                ]}
                expandColumns={[
                  {
                    id: "invoiceNo",
                    children: "productDetails",
                    isEdit: true,
                    isDelete: true,
                  },
                ]}
                uniqueId="id"
                sortColumn="invoiceNo"
                sortOrder="asc"
                isCheckBox={false}
                editData={editAction}
                deleteData={deleteAction}
              />
            </div>
          </div>
        </div>
      </LoadingOverlay>
    </div>
  );
};

export default withTranslation()(MetalPurchase);
