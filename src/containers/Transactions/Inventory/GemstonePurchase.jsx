import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import {
  gemstonePurchaseForm,
  stoneDetailsForm,
  gemstoneDetailsColumns,
} from "../Constants";
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../../components/Grid";
import Select from "../../../components/Form/Select";
import CollapseGrid from "../../../components/CollapseGrid";
import { fetchSuppliers } from "../../../redux/actions/Admin/supplierActions";
import {
  fetchStoneGroups,
  fetchStonesByStoneGroup,
  fetchStoneSizesByStoneGroup,
} from "../../../redux/actions/creations/stoneGroupActions";
import { fetchStoneColors } from "../../../redux/actions/creations/stoneColorActions";
import { fetchStoneClarities } from "../../../redux/actions/creations/stoneClarityActions";
import { fetchStonePolishes } from "../../../redux/actions/creations/stonePolishActions";
import { fetchPartyStoneRates } from "../../../redux/actions/creations/partyStoneRateActions";
import {
  saveGemstonePurchase,
  fetchGemstonePurchases,
  deleteGemstonePurchases,
} from "../../../redux/actions/transactions/inventory/gemstonePurchaseActions";

import Swal from "sweetalert2";
import LoadingOverlay from "react-loading-overlay";
import UOMElement from "../../../components/UOMElement";
import { UOM_TYPE, GRAM_TO_KARAT, KARAT_TO_GRAM, GST_PERCENT } from "../../../utils/commonConstants";

export const GemstonePurchase = (props) => {
  const { t } = props;
  const [formData, setFromData] = useState(cloneDeep(gemstonePurchaseForm));
  const [stoneFormData, setStoneFormData] = useState(
    cloneDeep(stoneDetailsForm)
  );
  const [viewForm, setToggleView] = useState(true);
  let [selectedStones, setSelectedStones] = useState([]);
  let [selected, setSelected] = useState([]);
  const [stoneDetails, setStoneDetails] = useState([]);
  const [purchaseData, setPurchaseData] = useState();
  const dispatch = useDispatch();

  const suppliers = useSelector((state) => state.supplierReducer.allSuppliers);
  const stoneGroups = useSelector(
    (state) => state.stoneGroupReducer.allStoneGroups
  );
  const stones = useSelector(
    (state) => state.stoneGroupReducer.allStonesByStoneGroup
  );
  const stoneSizes = useSelector(
    (state) => state.stoneGroupReducer.allStoneSizesByStoneGroup
  );
  const stoneColors = useSelector(
    (state) => state.stoneColorReducer.allStoneColors
  );
  const stoneClarities = useSelector(
    (state) => state.stoneClarityReducer.allStoneClarities
  );
  const stonePolishes = useSelector(
    (state) => state.stonePolishReducer.allStonePolishes
  );
  const gemstonePurchases = useSelector(
    (state) => state.gemstonePurchaseReducer.allGemstonePurchases
  );
  const partyRates = useSelector(state => state.partyStoneRateReducer.allPartyStoneRates)
  const loader = useSelector((state) => state.supplierReducer.loading);

  const suppliersData = suppliers.data
    ? suppliers.data.map((supplier) => {
      return {
        id: supplier.id,
        name: supplier.partyName,
      };
    })
    : [];

  const stoneGroupData = stoneGroups.data
    ? stoneGroups.data.map((group) => {
      return {
        id: group.id,
        name: group.groupName,
        shortCode: group.shortCode,
        diamond: group.diamond,
      };
    })
    : [];
  const stoneData = stones.data
    ? stones.data.map((stone) => {
      return {
        id: stone.id,
        name: stone.stoneName,
        shortCode: stone.shortCode,
      };
    })
    : [];
  const stoneSizeData = stoneSizes.data
    ? stoneSizes.data.map((size) => {
      return {
        id: size.id,
        name: size.stoneSize,
        shortCode: size.shortCode,
      };
    })
    : [];
  const stoneColorData = stoneColors.data
    ? stoneColors.data.map((color) => {
      return {
        id: color.id,
        name: color.colorName,
        shortCode: color.shortCode,
      };
    })
    : [];
  const stoneClarityData = stoneClarities.data
    ? stoneClarities.data.map((clarity) => {
      return {
        id: clarity.id,
        name: clarity.clarityName,
        shortCode: clarity.shortCode,
      };
    })
    : [];
  const stonePolishData = stonePolishes.data
    ? stonePolishes.data.map((polish) => {
      return {
        id: polish.id,
        name: polish.polishName,
        shortCode: polish.shortCode,
      };
    })
    : [];

  const fetchPartyNameById = (partyId) => {
    let selectedParty = suppliersData.find(ele => ele.id === partyId);
    return selectedParty ? selectedParty.name : "";
  }

  const isStoneADiamond = (selectedStoneGroupId) => {
    let selectedStoneGroup = stoneGroupData.find(ele => ele.id === selectedStoneGroupId);
    return selectedStoneGroup ? selectedStoneGroup.diamond : false;
  }

  const gemstonePurchaseData = gemstonePurchases.data
    ? gemstonePurchases.data.map((purchase) => {
      let stoneDetails = purchase.stoneDetails
        ? purchase.stoneDetails.map((stone) => {
          return {
            id: stone.id,
            stoneGroupId: stone.stone.stoneGroup.id,
            stoneGroup: stone.stone.stoneGroup.groupName,
            stoneId: stone.stone.id,
            stone: stone.stone.stoneName,
            isDiamond: isStoneADiamond(stone.stone.stoneGroup.id),
            stoneSizeId: stone.stoneSize ? stone.stoneSize.id : "",
            stoneSize: stone.stoneSize ? stone.stoneSize.stoneSize : "",
            stoneColorId: stone.stoneColor ? stone.stoneColor.id : "",
            stoneColor: stone.stoneColor ? stone.stoneColor.colorName : "",
            stoneClarityId: stone.stoneClarity ? stone.stoneClarity.id : "",
            stoneClarity: stone.stoneClarity
              ? stone.stoneClarity.clarityName
              : "",
            stonePolishId: stone.stonePolish ? stone.stonePolish.id : "",
            stonePolish: stone.stonePolish
              ? stone.stonePolish.polishName
              : "",
            uom: stone.uom,
            quantity: stone.quantity,
            weightPerGram: stone.weightPerGram,
            weightPerKarat: stone.weightPerKarat,
            rate: stone.rate,
            amount: stone.amount,
            gstAmount: stone.gstAmount,
            totalAmount: stone.totalAmount,
            remarks: stone.remarks,
            identifier: new Date().getTime() + stone.id,
          };
        })
        : [];
      return {
        id: purchase.id,
        purchaseType: purchase.purchaseType,
        invoiceDate: purchase.invoiceDate,
        invoiceNo: purchase.invoiceNo,
        partyId: purchase.partyId,
        party: fetchPartyNameById(purchase.partyId),
        stoneDetails: stoneDetails,
      };
    })
    : [];

  const partyRatesData = partyRates.data ? partyRates.data.map((partyRate) => {
    let stoneGroup = partyRate.stone.stoneGroup;
    return {
      id: partyRate.id,
      partyId: partyRate.partyId,
      party: fetchPartyNameById(partyRate.partyId),
      stoneTypeId: stoneGroup.stoneType.id,
      stoneType: stoneGroup.stoneType.typeName,
      groupId: stoneGroup.id,
      stoneGroup: stoneGroup.groupName,
      isDiamond: stoneGroup.diamond,
      stoneId: partyRate.stone.id,
      stone: partyRate.stone.stoneName,
      stoneSizeId: partyRate.stoneSize ? partyRate.stoneSize.id : "",
      stoneSize: partyRate.stoneSize ? partyRate.stoneSize.stoneSize : "",
      stoneColorId: partyRate.stoneColor ? partyRate.stoneColor.id : "",
      stoneColor: partyRate.stoneColor ? partyRate.stoneColor.colorName : "",
      stoneClarityId: partyRate.stoneClarity ? partyRate.stoneClarity.id : "",
      stoneClarity: partyRate.stoneClarity ? partyRate.stoneClarity.clarityName : "",
      stonePolishId: partyRate.stonePolish ? partyRate.stonePolish.id : "",
      stonePolish: partyRate.stonePolish ? partyRate.stonePolish.polishName : "",
      minRate: partyRate.minRate,
      maxRate: partyRate.maxRate,
      uom: partyRate.uom,
      noWeight: partyRate.noWeight,
      fixedRate: partyRate.fixedRate
    }
  }) : [];

  const setFormValue = (field, value) => {
    formData[field]["value"] = value;
    setFromData({ ...formData });
  };
  const setStoneFormValue = (field, value) => {
    stoneFormData[field]["value"] = value;
    setStoneFormData({ ...stoneFormData });
  };
  const resetForm = () => {
    map(formData, function (value, key, object) {
      formData[key]["value"] = "";
    });
    setFromData({ ...formData });
    resetStoneForm();
    setStoneDetails([]);
  };

  const saveForm = () => {
    let isFormValid = true;
    map(formData, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      formData[key] = { ...value, ...isValid };
    });
    if (isFormValid) {
      if (stoneDetails.length < 1) {
        Swal.fire({
          position: "top-end",
          title: 'Please enter stone details.!',
          text: 'And try saving purchase info',
          icon: 'info',
          showConfirmButton: false,
          timer: 2000
        })
      } else {
        let data = {};
        map(formData, function (value, key, object) {
          data = {
            ...data,
            [key]: value.value,
          };
        });
        data["stoneDetails"] = stoneDetails;
        console.log("saving gemstone purchase with data : ", data);
        Swal.fire({
          title: "Confirm Saving Purchase Info.!",
          text: "Continue to save / Cancel to stop",
          icon: "info",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Continue",
        }).then((result) => {
          if (result.isConfirmed) {
            dispatch(saveGemstonePurchase(data)).then(() => {
              resetForm();
              resetStoneForm();
              setStoneDetails([]);
              setPurchaseData();
            });
          }
        });
      }
    }
  };

  const addStones = () => {
    let isStoneFormValid = true;
    map(stoneFormData, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isStoneFormValid = false;
      }
      stoneFormData[key] = { ...value, ...isValid };
    });
    if (isStoneFormValid) {
      let stoneDetailsData = {};
      map(stoneFormData, function (value, key, object) {
        stoneDetailsData = {
          ...stoneDetailsData,
          [key]: value.value,
          isDeleted: false,
        };
      });
      let list = [...stoneDetails];
      if (stoneDetailsData.identifier) {
        let eleIndex = list.findIndex(
          (ele) => ele.identifier === stoneDetailsData.identifier
        );
        list[eleIndex] = stoneDetailsData;
      } else {
        stoneDetailsData["identifier"] = new Date().getTime() + stoneDetailsData.id;
        list.push(cloneDeep(stoneDetailsData));
      }
      setStoneDetails(list);
      resetStoneForm();
    }
  };

  const resetStoneForm = () => {
    map(stoneFormData, function (value, key, object) {
      if(stoneFormData[key]["type"] === 'number') {
        stoneFormData[key]["value"] = 0;
      } else {
        stoneFormData[key]["value"] = "";
      }
    });
    setStoneFormData({ ...stoneFormData });
    setIsStoneGroupDiamond(false);
  };

  const validateField = (fieldObj) => {
    let errorObj = { isValid: true, error: null };
    if (fieldObj.required && !fieldObj.value) {
      errorObj.isValid = false;
      errorObj.error = "Please fill this field";
    } else if (fieldObj.value && fieldObj.value.length < fieldObj.min) {
      errorObj.isValid = false;
      errorObj.error = `Please enter min length ${fieldObj.min}`;
    } else if (fieldObj.value && fieldObj.value.length > fieldObj.max) {
      errorObj.isValid = false;
      errorObj.error = `Please enter max length ${fieldObj.max}`;
    } else if (fieldObj.value && (fieldObj.value < fieldObj.minValue || fieldObj.value > fieldObj.maxValue)) {
      errorObj.isValid = false;
      errorObj.error = `Please enter value between ${fieldObj.minValue} and ${fieldObj.maxValue}`;
    }
    return errorObj;
  };

  const editAction = (ele) => {
    console.log("Editing Gemstone Purchase :: ", ele);
    resetForm();
    resetStoneForm();
    setStoneDetails([]);
    setFormValue('id', ele.id)
    setFormValue('purchaseType', ele.purchaseType)
    setFormValue('invoiceNo', ele.invoiceNo)
    setFormValue('invoiceDate', ele.invoiceDate)
    setFormValue('partyId', ele.partyId)
    setFormValue('party', ele.party)
    setFormValue('stoneDetails', ele.stoneDetails)
    setStoneDetails(ele.stoneDetails);
  };

  const editStoneDetails = (ele) => {
    console.log("editing stone details : ", ele);
    setStoneFormValue("id", ele.id);
    setStoneFormValue("stoneGroupId", ele.stoneGroupId);
    setStoneFormValue("stoneGroup", ele.stoneGroup);
    setIsStoneGroupDiamond(ele.isDiamond);
    setStoneFormValue("stoneId", ele.stoneId);
    setStoneFormValue("stone", ele.stone);
    setStoneFormValue("stoneSizeId", ele.stoneSizeId);
    setStoneFormValue("stoneSize", ele.stoneSize);
    setStoneFormValue("stoneColorId", ele.stoneColorId);
    setStoneFormValue("stoneColor", ele.stoneColor);
    setStoneFormValue("stoneClarityId", ele.stoneClarityId);
    setStoneFormValue("stoneClarity", ele.stoneClarity);
    setStoneFormValue("stonePolishId", ele.stonePolishId);
    setStoneFormValue("stonePolish", ele.stonePolish);
    setStoneFormValue("uom", ele.uom);
    setStoneFormValue("quantity", ele.quantity);
    setStoneFormValue("weightPerGram", ele.weightPerGram);
    setStoneFormValue("weightPerKarat", ele.weightPerKarat);
    setStoneFormValue("rate", ele.rate);
    setStoneFormValue("amount", ele.amount);
    setStoneFormValue("gstAmount", ele.gstAmount);
    setStoneFormValue("totalAmount", ele.totalAmount);
    setStoneFormValue("remarks", ele.remarks);
    setStoneFormValue("identifier", ele.identifier);
  };

  const deleteAction = (data) => {
    let selectedIds;
    if (data instanceof Array && data.length > 0) {
      selectedIds = data.map((ele) => {
        return ele.id;
      });
    } else {
      selectedIds = data.id;
    }
    Swal.fire({
      title: "Confirm Deleting Gemstone Purchase.!",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deleteGemstonePurchases(selectedIds.toString())).then(() => {
          selected = [];
          setSelected(selected);
        });
      }
    });
  };

  const deleteStoneDetails = (data) => {
    console.log(" delete stone details ====== ", data);
  };

  useEffect(() => {
    dispatch(fetchSuppliers());
    dispatch(fetchStoneGroups());
    dispatch(fetchGemstonePurchases());
  }, []);

  useEffect(() => {
    setStoneFormValue("uom", "")
    setStoneFormValue("rate", 0)
    dispatch(fetchStonesByStoneGroup(get(stoneFormData, "stoneGroupId.value")));
    dispatch(
      fetchStoneSizesByStoneGroup(get(stoneFormData, "stoneGroupId.value"))
    );
    if (get(stoneFormData, "stoneGroupId.value") && isStoneADiamond(get(stoneFormData, "stoneGroupId.value"))) {
      dispatch(fetchStoneColors());
      dispatch(fetchStoneClarities());
      dispatch(fetchStonePolishes());
    }
  }, [get(stoneFormData, "stoneGroupId.value")]);

  useEffect(() => {
    setStoneFormValue("uom", "")
    setStoneFormValue("rate", 0)
    dispatch(fetchPartyStoneRates(get(formData, "partyId.value"), get(stoneFormData, "stoneId.value"), get(stoneFormData, "stoneSizeId.value")));
  }, [get(formData, "partyId.value"), get(stoneFormData, "stoneId.value"), get(stoneFormData, "stoneSizeId.value")])

  useEffect(() => {
    stoneFormData.weightPerGram['disabled'] = false;
    stoneFormData.weightPerKarat['disabled'] = false;
    let uomValue = get(stoneFormData, 'uom.value');
    if(uomValue) {
      if(uomValue === UOM_TYPE.GRAM || uomValue === UOM_TYPE.PIECE) {
        stoneFormData.weightPerGram['disabled'] = false;
        stoneFormData.weightPerKarat['disabled'] = true;
      } else if(uomValue === UOM_TYPE.KARAT) {
        stoneFormData.weightPerKarat['disabled'] = false;
        stoneFormData.weightPerGram['disabled'] = true;
      }
    }
    let uom = get(formData, "partyId.value") && get(stoneFormData, "stoneGroupId.value") && get(stoneFormData, "stoneId.value") && partyRatesData.filter((ele) => ele.uom === uomValue && ele.stoneSizeId === get(stoneFormData, "stoneSizeId.value"))[0];
    if(uom) {
      setStoneFormValue("rate", uom.maxRate);
      stoneFormData.rate['minValue'] = uom.minRate;
      stoneFormData.rate['maxValue'] = uom.maxRate;
      stoneFormData.rate['disabled'] = uom.fixedRate ? true : false;
      // setStoneFormData({ ...stoneFormData });
    } else {
      setStoneFormValue("rate", 0)
      stoneFormData.rate['disabled'] = false;
    }
  }, [get(stoneFormData, 'uom.value')])

  useEffect(() => {
    let quantity = get(stoneFormData, 'quantity.value');
    let weightPerGram = get(stoneFormData, 'weightPerGram.value');
    let weightPerKarat = get(stoneFormData, 'weightPerKarat.value');
    let rate = get(stoneFormData, 'rate.value');
    let uom = get(stoneFormData, 'uom.value');
    let amount = 0;
    let gstAmount = 0;
    let totalAmount = amount + gstAmount;
    if(uom) {
      if(uom === UOM_TYPE.PIECE) {
        setStoneFormValue("weightPerKarat", Math.round(weightPerGram * GRAM_TO_KARAT));
        amount = Math.round(quantity * rate);
      } else if(uom === UOM_TYPE.GRAM) {
        setStoneFormValue("weightPerKarat", Math.round(weightPerGram * GRAM_TO_KARAT));
        amount = Math.round(weightPerGram * rate);
      } else if(uom === UOM_TYPE.KARAT) {
        setStoneFormValue("weightPerGram", Math.round(weightPerKarat * KARAT_TO_GRAM));
        amount = Math.round(weightPerKarat * rate);
      }
    }
    gstAmount = Math.round(amount * GST_PERCENT / 100);
    totalAmount = amount + gstAmount;
    setStoneFormValue("amount", amount);
    setStoneFormValue("gstAmount", gstAmount);
    setStoneFormValue("totalAmount", totalAmount);
  }, [get(stoneFormData, 'uom.value'), get(stoneFormData, 'quantity.value'), get(stoneFormData, 'weightPerGram.value'), get(stoneFormData, 'weightPerKarat.value')])

  const toggleView = () => {
    const formNode = document.getElementById("form");
    formNode.classList.toggle("collapse-panel");
    setToggleView(!viewForm);
  };

  const partyNameChange = (e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    setFormValue("partyId", e.target.value);
    setFormValue("party", label);
  };

  const selectOnChange = (idKey, labelKey, e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    setStoneFormValue(idKey, e.target.value);
    setStoneFormValue(labelKey, label);
  };

  let [isStoneGroupDiamond, setIsStoneGroupDiamond] = useState(false);

  const stoneGroupOnChange = (e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    resetElementsOnStoneGroupChange();
    setStoneFormValue('stoneGroupId', e.target.value);
    setStoneFormValue('stoneGroup', label);
    setIsStoneGroupDiamond(isStoneADiamond(e.target.value));
  }

  const resetElementsOnStoneGroupChange = () => {
    setStoneFormValue('stoneColorId', '')
    setStoneFormValue('stoneColor', '')
    setStoneFormValue('stoneClarityId', '')
    setStoneFormValue('stoneClarity', '')
    setStoneFormValue('stonePolishId', '')
    setStoneFormValue('stonePolish', '')
  }

  return (
    <div className="panel panel-default hero-panel">
      <LoadingOverlay active={loader} spinner text="Loading...">
        <div className="panel-heading">
          <h1 className="panel-title">{t("transactions.gemstonePurchase")}</h1>
          <div className="panel-options">
            <a href="#" className="text-info collapseBtn" title="Show Add Form">
              {viewForm ? (
                <i
                  className="zmdi zmdi-minus-circle  zmdi-hc-2x"
                  onClick={() => {
                    toggleView();
                  }}
                ></i>
              ) : (
                  <i
                    className="zmdi zmdi-plus-circle zmdi-hc-2x"
                    onClick={() => {
                      toggleView();
                    }}
                  ></i>
                )}
            </a>
          </div>
        </div>
        <div className="panel-body">
          <div id="form">
            <div className="panel update-panel form-pnel-container">
              {/* <div className="text-danger">* {t("common.requiredFieldsMsg")} </div> */}
              <div className="panel-body p-t-0">
                <div className="row">
                  <div className="col-md-3">
                    <div className="panel-heading p-l-0">
                      <h4 className="panel-title">
                        {t("transactions.purchase")} {t("common.type")}
                      </h4>
                    </div>
                    <div className="form-group no-min-height">
                      <div className="radio  radio-primary m-b-0">
                        <input
                          type="radio"
                          name="purchaseType"
                          value="Approval"
                          checked={
                            get(formData, "purchaseType.value") === "Approval"
                          }
                          onChange={(e) =>
                            setFormValue("purchaseType", "Approval")
                          }
                        />
                        <label htmlFor="">{t("transactions.approval")}</label>
                      </div>
                      <div className="radio  radio-primary m-b-0">
                        <input
                          type="radio"
                          name="purchaseType"
                          value="Invoice"
                          checked={
                            get(formData, "purchaseType.value") === "Invoice"
                          }
                          onChange={(e) =>
                            setFormValue("purchaseType", "Invoice")
                          }
                        />
                        <label htmlFor="">{t("transactions.invoice")}</label>
                      </div>
                      <div className="invalid-feedback">
                        {get(formData, "purchaseType.error")}
                      </div>
                    </div>
                  </div>
                  <div className="col-md-9">
                    <div className="panel-heading">
                      <h4 className="panel-title">
                        {t("transactions.invoice")} &amp; {t("common.Party")}{" "}
                        {t("common.details")}
                      </h4>
                    </div>
                    <div className="form-group col-md-4">
                      <label htmlFor="">
                        {t("transactions.invoice")} {t("common.number")}
                        <span className="asterisk">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="invoiceNo"
                        name="invoiceNo"
                        value={get(formData, "invoiceNo.value")}
                        onChange={(e) =>
                          setFormValue("invoiceNo", e.target.value)
                        }
                      />
                      <div className="invalid-feedback">
                        {get(formData, "invoiceNo.error")}
                      </div>
                    </div>
                    <div className="form-group col-md-4">
                      <label htmlFor="">
                        {t("transactions.invoice")} {t("common.date")}
                        <span className="asterisk">*</span>
                      </label>
                      <input
                        type="date"
                        className="form-control"
                        placeholder=""
                        value={get(formData, "invoiceDate.value")}
                        onChange={(e) =>
                          setFormValue("invoiceDate", e.target.value)
                        }
                      />
                      <div className="invalid-feedback">
                        {get(formData, "invoiceDate.error")}
                      </div>
                    </div>
                    <div className="form-group col-md-4">
                      <label htmlFor="">
                        {t("common.Party")} {t("common.name")}
                        <span className="asterisk">*</span>
                      </label>
                      <Select
                        name="partyId"
                        value={get(formData, "partyId.value")}
                        onChange={(e) => partyNameChange(e)}
                        data={suppliersData}
                      />
                      <div className="invalid-feedback">
                        {get(formData, "partyId.error")}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h4 className="panel-title">
                      {t("transactions.gemStoneDetailsandRate")}
                    </h4>
                  </div>
                  <div className="panel-body">
                    <div className="row">
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("transactions.category")}
                            <span className="asterisk">*</span>
                            <a
                              href=""
                              className=""
                              data-toggle="modal"
                              data-target="#addProduct"
                              data-backdrop="static"
                              data-keyboard="false"
                            >
                              <i className="zmdi zmdi-plus-circle f-s-16"></i>
                            </a>
                          </label>
                          <Select
                            name="stoneGroupId"
                            value={get(stoneFormData, "stoneGroupId.value")}
                            onChange={(e) => stoneGroupOnChange(e)}
                            data={stoneGroupData}
                          />
                          <div className="invalid-feedback">
                            {get(stoneFormData, "stoneGroupId.error")}
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("transactions.subCategory")}
                            <span className="asterisk">*</span>
                            <a
                              href=""
                              className=""
                              data-toggle="modal"
                              data-target="#addProduct"
                              data-backdrop="static"
                              data-keyboard="false"
                            >
                              <i className="zmdi zmdi-plus-circle f-s-16"></i>
                            </a>
                          </label>
                          <Select
                            name="stoneId"
                            value={get(stoneFormData, "stoneId.value")}
                            onChange={(e) =>
                              selectOnChange("stoneId", "stone", e)
                            }
                            data={stoneData}
                          />
                          <div className="invalid-feedback">
                            {get(stoneFormData, "stoneId.error")}
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.stone")} {t("common.size")}
                          </label>
                          <Select
                            name="stoneSizeId"
                            value={get(stoneFormData, "stoneSizeId.value")}
                            onChange={(e) =>
                              selectOnChange("stoneSizeId", "stoneSize", e)
                            }
                            data={stoneSizeData}
                          />
                          <div className="invalid-feedback">
                            {get(stoneFormData, "stoneSizeId.error")}
                          </div>
                        </div>
                      </div>
                      <div className={`${get(stoneFormData, "stoneGroupId.value") && isStoneGroupDiamond ? '' : 'hide'}`}>
                        <div className="col-sm-6 col-md-3 col-lg-3">
                          <div className="form-group">
                            <label htmlFor="">
                              {t("common.stone")} {t("common.color")}
                            </label>
                            <Select
                              name="stoneColorId"
                              value={get(stoneFormData, "stoneColorId.value")}
                              onChange={(e) =>
                                selectOnChange("stoneColorId", "stoneColor", e)
                              }
                              data={stoneColorData}
                            />
                            <div className="invalid-feedback">
                              {get(stoneFormData, "stoneColorId.error")}
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-6 col-md-3 col-lg-3">
                          <div className="form-group">
                            <label htmlFor="">
                              {t("common.stone")} {t("common.clarity")}
                            </label>
                            <Select
                              name="stoneClarityId"
                              value={get(stoneFormData, "stoneClarityId.value")}
                              onChange={(e) =>
                                selectOnChange(
                                  "stoneClarityId",
                                  "stoneClarity",
                                  e
                                )
                              }
                              data={stoneClarityData}
                            />
                            <div className="invalid-feedback">
                              {get(stoneFormData, "stoneClarityId.error")}
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-6 col-md-3 col-lg-3">
                          <div className="form-group">
                            <label htmlFor="">
                              {t("common.stone")} {t("common.polish")}
                            </label>
                            <Select
                              name="stonePolishId"
                              value={get(stoneFormData, "stonePolishId.value")}
                              onChange={(e) =>
                                selectOnChange("stonePolishId", "stonePolish", e)
                              }
                              data={stonePolishData}
                            />
                            <div className="invalid-feedback">
                              {get(stoneFormData, "stonePolishId.error")}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.uom")}
                            <span className="asterisk">*</span>
                          </label>
                          <UOMElement name="uom" value={get(stoneFormData, 'uom.value')} onChange={(e) => setStoneFormValue('uom', e.target.value)} />
                          <div className="invalid-feedback">
                            {get(stoneFormData, "uom.error")}
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.quantity")}
                            <span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="quantity"
                            name="quantity"
                            value={get(stoneFormData, "quantity.value")}
                            onChange={(e) =>
                              setStoneFormValue("quantity", e.target.value)
                            }
                          />
                          <div className="invalid-feedback">
                            {get(stoneFormData, "quantity.error")}
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("transactions.weight")} ({t("common.gram")})
                            <span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="weightPerGram"
                            name="weightPerGram"
                            value={get(stoneFormData, "weightPerGram.value")}
                            onChange={(e) =>
                              setStoneFormValue("weightPerGram", e.target.value)
                            }
                            disabled={get(stoneFormData, 'weightPerGram.disabled')}
                          />
                          <div className="invalid-feedback">
                            {get(stoneFormData, "weightPerGram.error")}
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("transactions.weight")} ({t("common.karat")})
                            <span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="weightPerKarat"
                            name="weightPerKarat"
                            value={get(stoneFormData, "weightPerKarat.value")}
                            onChange={(e) =>
                              setStoneFormValue("weightPerKarat", e.target.value)
                            }
                            disabled={get(stoneFormData, 'weightPerKarat.disabled')}
                          />
                          <div className="invalid-feedback">
                            {get(stoneFormData, "weightPerKarat.error")}
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.rate")}
                            <span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="rate"
                            name="rate"
                            value={get(stoneFormData, "rate.value")}
                            onChange={(e) =>
                              setStoneFormValue("rate", e.target.value)
                            }
                            disabled={get(stoneFormData, 'rate.disabled')}
                          />
                          <div className="invalid-feedback">
                            {get(stoneFormData, "rate.error")}
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.amount")}
                            <span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="amount"
                            name="amount"
                            value={get(stoneFormData, "amount.value")}
                            onChange={(e) =>
                              setStoneFormValue("amount", e.target.value)
                            }
                            disabled={get(stoneFormData, 'amount.disabled')}
                          />
                          <div className="invalid-feedback">
                            {get(stoneFormData, "amount.error")}
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("transactions.gstAmount")}
                            <span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="gst Amount"
                            name="gstAmount"
                            value={get(stoneFormData, "gstAmount.value")}
                            onChange={(e) =>
                              setStoneFormValue("gstAmount", e.target.value)
                            }
                            disabled={get(stoneFormData, 'gstAmount.disabled')}
                          />
                          <div className="invalid-feedback">
                            {get(stoneFormData, "gstAmount.error")}
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("transactions.totalAmount")}
                            <span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="total Amount"
                            name="totalAmount"
                            value={get(stoneFormData, "totalAmount.value")}
                            onChange={(e) =>
                              setStoneFormValue("totalAmount", e.target.value)
                            }
                            disabled={get(stoneFormData, 'totalAmount.disabled')}
                          />
                          <div className="invalid-feedback">
                            {get(stoneFormData, "totalAmount.error")}
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-6 col-lg-6">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.remarks")}
                            <span className="asterisk">*</span>
                          </label>
                          <textarea
                            type="text"
                            className="form-control"
                            name="remarks"
                            value={get(stoneFormData, "remarks.value")}
                            style={{ height: "30px" }}
                            onChange={(e) =>
                              setStoneFormValue("remarks", e.target.value)
                            }
                          />
                          <div className="invalid-feedback">
                            {get(stoneFormData, "remarks.error")}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="panel-footer text-center">
                <a href="#" className="btn btn-primary" onClick={addStones}>
                  <i className="zmdi zmdi-playlist-plus"></i>{" "}
                  {t("common.addToList")}
                </a>
                <a href="#" className="btn btn-default" onClick={resetStoneForm}>
                  <i className="zmdi zmdi-close"></i> {t("common.clear")} {t("common.stone")}
                </a>
              </div>
            </div>
            <div className={`${stoneDetails && stoneDetails.length > 0 ? '' : 'hide'}`}>
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h3 className="panel-title">Selected List</h3>
                  <div className="panel-options">
                    <p>
                      {selectedStones.length > 0 ? (
                        <a
                          href="#"
                          className="btn btn-info btn-sm"
                          onClick={() => deleteStoneDetails(selectedStones)}
                        >
                          <i className="zmdi zmdi-delete"></i>{" "}
                          {t("common.delete")}
                        </a>
                      ) : null}
                    </p>
                  </div>
                </div>
                <div className="panel-body">
                  <div style={{ height: "100%", boxSizing: "border-box" }}>
                    <div
                      id="myGrid"
                      style={{ height: "300px" }}
                      className="ag-theme-alpine"
                    >
                      <Grid
                        colDefs={gemstoneDetailsColumns}
                        rowData={stoneDetails}
                        selectedRows={setSelectedStones}
                        actions={{
                          isRequired: true,
                          editAction: editStoneDetails,
                          deleteAction: deleteStoneDetails,
                        }}
                        flex
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="panel-footer text-center">
                <a href="#" className="btn btn-primary" onClick={saveForm}>
                  <i className="zmdi zmdi-check"></i> {t("common.save")}
                </a>
                <a href="#" className="btn btn-default" onClick={resetForm}>
                  <i className="zmdi zmdi-close"></i> {t("common.clear")}
                </a>
              </div>
            </div>
          </div>
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">Gemstone Purchase List</h3>
              <div className="panel-options">
                <p>
                  {selected.length > 0 ? (
                    <a
                      href="#"
                      className="btn btn-info btn-sm"
                      onClick={() => {
                        deleteAction(selected);
                      }}
                    >
                      <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                    </a>
                  ) : null}
                </p>
              </div>
            </div>
            <div className="panel-body">
              <CollapseGrid
                data={gemstonePurchaseData}
                columns={[
                  {
                    id: "stone",
                    label: "Stone",
                    minWidth: 100,
                  },
                  {
                    id: "stoneGroup",
                    label: "Group",
                    minWidth: 100,
                  },
                  {
                    id: "stoneColor",
                    label: "Color",
                    minWidth: 100,
                  },
                  {
                    id: "stoneClarity",
                    label: "Clarity",
                    minWidth: 100,
                  },
                  {
                    id: "stonePolish",
                    label: "Polish",
                    minWidth: 100,
                  },
                  {
                    id: "rate",
                    label: "Rate",
                    minWidth: 100,
                  },
                  {
                    id: "weightPerGram",
                    label: "Weight (Gram)",
                    minWidth: 100,
                  },
                ]}
                expandColumns={[
                  {
                    id: "invoiceNo",
                    children: "stoneDetails",
                    isEdit: true,
                    isDelete: true,
                  },
                ]}
                uniqueId="id"
                sortColumn="invoiceNo"
                sortOrder="asc"
                isCheckBox={false}
                editData={editAction}
                deleteData={deleteAction}
              />
            </div>
          </div>
        </div>
      </LoadingOverlay>
    </div>
  );
};

export default withTranslation()(GemstonePurchase);
