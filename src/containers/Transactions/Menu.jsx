import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import SecondMenu from "./../../components/SecondMenu";

export const Menu = (props) => {
  const { t } = props;
  const [viewMenu, setMenuView] = React.useState(true);
  const inventoryMenuOptions = [
    {
      name: "metalPurchase",
      active: false,
      href: "/transactions/inventory/metal-purchase",
      title: t("transactions.metalPurchase"),
    },
    {
      name: "brandedPurchase",
      active: false,
      href: "/transactions/inventory/branded-purchase",
      title: t("transactions.brandedPurchase"),
    },
    {
      name: "gemstonePurchase",
      active: false,
      href: "/transactions/inventory/gemstone-purchase",
      title: t("transactions.gemstonePurchase"),
    }
  ];

  const lotCreationsMenuOptions = [
    {
      name: "metalLot",
      active: false,
      href: "/transactions/barcode/metal-lot",
      title: t("transactions.metalLot"),
    },
    {
      name: "brandedLot",
      active: false,
      href: "/transactions/barcode/branded-lot",
      title: t("transactions.brandedLot"),
    },
    {
      name: "gemstoneLot",
      active: false,
      href: "/transactions/barcode/gemstone-lot",
      title: t("transactions.gemstoneLot"),
    }
  ];

  const barcodeGenerationMenuOptions = [
    {
      name: "metalBarcode",
      active: false,
      href: "/transactions/barcode/metal-barcode",
      title: t("transactions.metalBarcode"),
    },
    {
      name: "brandedBarcode",
      active: false,
      href: "/transactions/barcode/branded-barcode",
      title: t("transactions.brandedBarcode"),
    },
    {
      name: "gemstoneBarcode",
      active: false,
      href: "/transactions/barcode/gemstone-barcode",
      title: t("transactions.gemstoneBarcode"),
    }
  ];

  const barcodeMenuOptions = [
    {
      name: "barcodeDelete",
      active: false,
      href: "/transactions/barcode/delete",
      title: t("transactions.barcode") + " " + t("common.delete"),
    },
    {
      name: "barcodeReprint",
      active: false,
      href: "/transactions/barcode/reprint",
      title: t("transactions.barcode") + " " + t("common.reprint"),
    }
  ]

  const billingMenuOptions = [
    {
      name: "mainGroup",
      active: false,
      href: "/transactions/mainGroup",
      title: t("creations.mainGroup"),
    },
    {
      name: "product",
      active: false,
      href: "/transactions/product/name",
      title: t("creations.product"),
    },
    {
      name: "subProduct",
      active: false,
      href: "/transactions/product/sub",
      title: t("creations.subProduct"),
    },
    {
      name: "productSize",
      active: false,
      href: "/transactions/product/size",
      title: t("creations.productSize"),
    },
    {
      name: "saleWastage",
      active: false,
      href: "/transactions/product/sale-wastage",
      title: t("creations.saleWastage"),
    },
    {
      name: "partyWastage",
      active: false,
      href: "/transactions/product/party-wastage",
      title: t("creations.partyWastage"),
    }
  ];

  const inventoryMenu = () => {
    let active = false;
    return {
      id: "inventoryManagement",
      name: t("transactions.inventory") + " " + t("transactions.management"),
      menuItems: inventoryMenuOptions.map(eachMenu => {
        if (eachMenu.href.includes(props.pathname)) {
          active = true;
        }
        return { ...eachMenu, active: eachMenu.href.includes(props.pathname) }
      }),
      active
    };
  }

  const lotCreationsMenu = () => {
    let active = false;
    return {
      id: "lotCreations",
      name: t("transactions.lotCreation"),
      menuItems: lotCreationsMenuOptions.map(eachMenu => {
        if (eachMenu.href.includes(props.pathname)) {
          active = true;
        }
        return { ...eachMenu, active: eachMenu.href.includes(props.pathname) }
      }),
      active
    };
  }

  const barcodeGenerationsMenu = () => {
    let active = false;
    return {
      id: "barcodeGenerations",
      name: t("transactions.barcodeGeneration"),
      menuItems: barcodeGenerationMenuOptions.map(eachMenu => {
        if (eachMenu.href.includes(props.pathname)) {
          active = true;
        }
        return { ...eachMenu, active: eachMenu.href.includes(props.pathname) }
      }),
      active
    };
  }

  const barcodeMenu = () => {
    let active = false;
    let menuItems = barcodeMenuOptions.map(eachMenu => {
      if (eachMenu.href.includes(props.pathname)) {
        active = true;
      }
      return { ...eachMenu, active: eachMenu.href.includes(props.pathname) }
    });
    let barcodeGenerationItems = barcodeGenerationsMenu();
    active = barcodeGenerationItems.active || active;
    menuItems = [barcodeGenerationItems, ...menuItems]
    let lotCreationItems = lotCreationsMenu();
    active = lotCreationItems.active || active;
    menuItems = [lotCreationItems, ...menuItems];
    return {
      id: "barcodeManagement",
      name: t("transactions.barcode") + " " + t("transactions.management"),
      menuItems: menuItems,
      active
    };
  }

  const billingMenu = () => {
    let active = false;
    let menuItems = billingMenuOptions.map(eachMenu => {
      if (eachMenu.href.includes(props.pathname)) {
        active = true;
      }
      return { ...eachMenu, active: eachMenu.href.includes(props.pathname) }
    });
    return {
      id: "billingManagement",
      name: t("transactions.billing") + " " + t("transactions.management"),
      menuItems: menuItems,
      active
    };
  }

  const menuOptions = [
    inventoryMenu(),
    barcodeMenu(),
    billingMenu()
  ];

  return (
    <div className="left-nav-holder">
      <ul className="left-subnav" id="sidebar">
        <SecondMenu menuOptions={menuOptions} />
      </ul>
    </div>
  );
};

export default withTranslation()(Menu);
