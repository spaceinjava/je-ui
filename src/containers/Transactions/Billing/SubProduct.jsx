import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { subProductForm, subProductColumns } from '../Constants';
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../../components/Grid";
import Select from "../../../components/Form/Select";
import { fetchMainGroups, fetchProductsByMainGroup } from '../../../redux/actions/mainGroupActions'
import { saveSubProduct, fetchSubProducts, deleteSubProducts } from "../../../redux/actions/creations/subProductActions";
import Swal from 'sweetalert2'

export const SubProduct = (props) => {
    const { t } = props;
    const [formData, setFromData] = useState(cloneDeep(subProductForm));
    const [viewForm, setToggleView] = useState(true);
    let [selected, setSelected] = useState([]);
    const dispatch = useDispatch();
    const mainGroups = useSelector(state => state.MainGroupReducer.allGroups)
    const products = useSelector(state => state.MainGroupReducer.allProductsByMainGroup)
    const subProducts = useSelector(state => state.subProductReducer.allSubProducts)
    const mainGroupData = mainGroups.data ? mainGroups.data.map((group) => {
        return {
            id: group.id,
            name: group.groupName,
            shortCode: group.shortCode,
        }
    }) : [];
    const productsData = products.data ? products.data.map((product) => {
        return {
            id: product.id,
            name: product.productName,
            shortCode: product.shortCode,
        }
    }) : [];
    const subProductsData = subProducts.data ? subProducts.data.map((subProduct) => {
        return {
            id: subProduct.id,
            groupId: subProduct.product.mainGroup.id,
            groupName: subProduct.product.mainGroup.groupName,
            productId: subProduct.product.id,
            productName: subProduct.product.productName,
            subProductName: subProduct.subProductName,
            shortCode: subProduct.shortCode,
        }
    }) : [];
    const setFormValue = (field, value) => {
        formData[field]['value'] = value;
        setFromData({ ...formData });
    }
    const resetForm = () => {
        map(formData, function (value, key, object) {
            formData[key]['value'] = ''
        });
        setFromData({ ...formData });
    }
    const saveForm = () => {
        let isFormValid = true;
        map(formData, function (value, key, object) {
            const isValid = validateField(value);
            if (!isValid.isValid) {
                isFormValid = false;
            }
            formData[key] = { ...value, ...isValid }
        });
        if (isFormValid) {
            let data = {};
            map(formData, function (value, key, object) {
                data = {
                    ...data,
                    [key]: value.value
                }
            });
            Swal.fire({
                title: 'Confirm Saving Sub Product Info.!',
                text: 'Continue to save / Cancel to stop',
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Continue'
            }).then((result) => {
                if (result.isConfirmed) {
                    dispatch(saveSubProduct(data))
                        .then(() => {
                            resetForm()
                        })
                }
            })
        }
    }

    const validateField = (fieldObj) => {
        let errorObj = { isValid: true, error: null }
        if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
        else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
        else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }
        return errorObj;
    }

    const editAction = (ele) => {
        setFormValue('id', ele.id)
        setFormValue('mainGroup', ele.groupId)
        setFormValue('product', ele.productId)
        setFormValue('subProductName', ele.subProductName)
        setFormValue('shortCode', ele.shortCode)
    }

    const deleteAction = (data) => {
        let selectedIds;
        if (data instanceof Array && data.length > 0) {
            selectedIds = data.map((ele) => {
                return ele.id
            })
        } else {
            selectedIds = data.id;
        }
        Swal.fire({
            title: 'Confirm Deleting Sub Product.!',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(deleteSubProducts(selectedIds.toString()))
                    .then(() => {
                        selected = []
                        setSelected(selected);
                    })
            }
        })
    }

    useEffect(() => {
        dispatch(fetchMainGroups());
        dispatch(fetchSubProducts());
    }, [])

    useEffect(() => {
        dispatch(fetchProductsByMainGroup(get(formData, 'mainGroup.value')));
    }, [get(formData, 'mainGroup.value')])

    const toggleView = () => {
        const formNode = document.getElementById('form');
        formNode.classList.toggle('collapse-panel');
        setToggleView(!viewForm);
    }

    return (
        <div className="panel panel-default hero-panel">
            <div className="panel-heading">
                <h1 className="panel-title">{t("creations.product")} {t("common.creation")}</h1>
                <div className="panel-options">
                    <a
                        href="#"
                        className="text-info collapseBtn"
                        title="Show Add Form"
                    >
                        {viewForm ?
                            <i className="zmdi zmdi-minus-circle  zmdi-hc-2x" onClick={() => { toggleView() }}>
                            </i> :
                            <i className="zmdi zmdi-plus-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                            </i>}
                    </a>
                </div>
            </div>
            <div className="panel-body">
                <div className="panel update-panel form-pnel-container" id="form">
                    <div className="text-danger">* {t("common.requiredFieldsMsg")} </div>
                    <div className="panel update-panel">
                        <div className="panel-body ">
                            <div className="row">
                                <div className="col-sm-6 col-md-3 col-lg-3">
                                    <div className="form-group">
                                        <label htmlFor="">
                                            {t("creations.mainGroup")}<span className="asterisk">*</span>
                                        </label>
                                        <Select name="mainGroup" value={get(formData, 'mainGroup.value')} onChange={(e) => setFormValue('mainGroup', e.target.value)} data={mainGroupData} />
                                        <div className="invalid-feedback">{get(formData, 'mainGroup.error')}</div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-md-3 col-lg-3">
                                    <div className="form-group">
                                        <label htmlFor="">
                                            {t("creations.product")} {t("common.name")}<span className="asterisk">*</span>
                                        </label>
                                        <Select name="product" value={get(formData, 'product.value')} onChange={(e) => setFormValue('product', e.target.value)} data={productsData} />
                                        <div className="invalid-feedback">{get(formData, 'product.error')}</div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-md-3 col-lg-3">
                                    <div className="form-group">
                                        <label htmlFor="">
                                            {t("creations.subProduct")} {t("common.name")}<span className="asterisk">*</span>
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            placeholder="name"
                                            name="subProductName"
                                            value={get(formData, 'subProductName.value')}
                                            onChange={(e) => setFormValue('subProductName', e.target.value)}
                                        />
                                        <div className="invalid-feedback">{get(formData, 'subProductName.error')}</div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-md-3 col-lg-3">
                                    <div className="form-group">
                                        <label htmlFor="">
                                            {t("common.shortCode")}<span className="asterisk">*</span>
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            placeholder="code"
                                            name="shortCode"
                                            value={get(formData, 'shortCode.value')}
                                            onChange={(e) => setFormValue('shortCode', e.target.value)}
                                        />
                                        <div className="invalid-feedback">{get(formData, 'shortCode.error')}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="panel-footer text-center">
                        <a href="#" className="btn btn-primary" onClick={saveForm}>
                            <i className="zmdi zmdi-check"></i> {t("common.save")}
                        </a>
                        <a href="#" className="btn btn-default" onClick={resetForm}>
                            <i className="zmdi zmdi-close"></i> {t("common.clear")}
                        </a>
                    </div>
                </div>
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">Sub Products List</h3>
                        <div className="panel-options">
                            <p>
                                {selected.length > 0 ?
                                    <a href="#" className="btn btn-info btn-sm" onClick={() => { deleteAction(selected) }}>
                                        <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                                    </a> :
                                    null}
                            </p>
                        </div>
                    </div>
                    <div className="panel-body">
                        <div className="ag-grid-wrapper">
                            <Grid colDefs={subProductColumns} rowData={subProductsData} selectedRows={setSelected} actions={{ isRequired: true, editAction: editAction, deleteAction: deleteAction }} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default withTranslation()(SubProduct);
