import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { saleWastageForm, saleWastageColumns } from '../Constants';
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../../components/Grid";
import Select from "../../../components/Form/Select";
import { fetchMainGroups, fetchProductsByMainGroup, fetchPuritiesByMainGroup } from '../../../redux/actions/mainGroupActions'
import { fetchSubProductsByProduct, fetchSizesByProduct } from "../../../redux/actions/creations/productActions";
import { saveSaleWastage, fetchSaleWastages, deleteSaleWastages } from "../../../redux/actions/creations/saleWastageActions";
import Swal from 'sweetalert2'

export const SaleWastage = (props) => {
    const { t } = props;
    const [formData, setFromData] = useState(cloneDeep(saleWastageForm));
    const [viewForm, setToggleView] = useState(true);
    let [selected, setSelected] = useState([]);
    const dispatch = useDispatch();
    const mainGroups = useSelector(state => state.MainGroupReducer.allGroups)
    const products = useSelector(state => state.MainGroupReducer.allProductsByMainGroup)
    const subProducts = useSelector(state => state.productReducer.allSubProductsByProduct)
    const productSizes = useSelector(state => state.productReducer.allSizesByProduct)
    const purities = useSelector(state => state.MainGroupReducer.allPuritiesByMainGroup)
    const saleWastages = useSelector(state => state.saleWastageReducer.allSaleWastages)
    const mainGroupData = mainGroups.data ? mainGroups.data.map((group) => {
        return {
            id: group.id,
            name: group.groupName,
            shortCode: group.shortCode,
        }
    }) : [];
    const productsData = products.data ? products.data.map((product) => {
        return {
            id: product.id,
            name: product.productName,
            shortCode: product.shortCode,
        }
    }) : [];
    const subProductsData = subProducts.data ? subProducts.data.map((subProduct) => {
        return {
            id: subProduct.id,
            name: subProduct.subProductName,
            shortCode: subProduct.shortCode,
        }
    }) : [];
    const puritiesData = purities.data ? purities.data.map((purity) => {
        return {
            id: purity.id,
            name: purity.purity,
        }
    }) : [];
    const productSizesData = productSizes.data ? productSizes.data.map((productSize) => {
        return {
            id: productSize.id,
            name: productSize.productSize
        }
    }) : [];
    const saleWastagesData = saleWastages.data ? saleWastages.data.map((saleWastage) => {
        const product = saleWastage.subProduct.product;
        return {
            id: saleWastage.id,
            mainGroupId: product.mainGroup.id,
            mainGroup: product.mainGroup.groupName,
            productId: product.id,
            product: product.productName,
            subProductId: saleWastage.subProduct.id,
            subProduct: saleWastage.subProduct.subProductName,
            productSizeId: saleWastage.productSize.id,
            productSize: saleWastage.productSize.productSize,
            purityId: saleWastage.purity.id,
            purity: saleWastage.purity.purity,
            fromWeight: saleWastage.fromWeight,
            toWeight: saleWastage.toWeight,
            maxWstPerGram: saleWastage.maxWstPerGram,
            minWstPerGram: saleWastage.minWstPerGram,
            maxDirWst: saleWastage.maxDirWst,
            minDirWst: saleWastage.minDirWst,
            maxMcPerGram: saleWastage.maxMcPerGram,
            minMcPerGram: saleWastage.minMcPerGram,
            maxDirMc: saleWastage.maxDirMc,
            minDirMc: saleWastage.minDirMc,
            mcOn: saleWastage.mcOn,
            wastageOn: saleWastage.wastageOn,
            incWstMc: saleWastage.incWstMc
        }
    }) : [];
    const setFormValue = (field, value) => {
        formData[field]['value'] = value;
        setFromData({ ...formData });
    }
    const resetForm = () => {
        map(formData, function (value, key, object) {
            if (key === 'mcOnGross') {
                document.getElementById("mcOnGross").checked = false;
                setFormValue('mcOnGross', false)
            } else if (key === 'mcOnNet') {
                document.getElementById("mcOnNet").checked = false;
                setFormValue('mcOnNet', false)
            } else if (key === 'wastageOnGross') {
                document.getElementById("wastageOnGross").checked = false;
                setFormValue('wastageOnGross', false)
            } else if (key === 'wastageOnNet') {
                document.getElementById("wastageOnNet").checked = false;
                setFormValue('wastageOnNet', false)
            } else if (key === 'incWstMc') {
                document.getElementById("incWstMc").checked = false;
                setFormValue('incWstMc', false)
            } else {
                formData[key]['value'] = '';
            }
        });
        setFromData({ ...formData });
    }
    const saveForm = () => {
        let isFormValid = true;
        map(formData, function (value, key, object) {
            const isValid = validateField(value);
            if (!isValid.isValid) {
                isFormValid = false;
            }
            formData[key] = { ...value, ...isValid }
        });
        if (isFormValid) {
            let data = {};
            map(formData, function (value, key, object) {
                data = {
                    ...data,
                    [key]: value.value
                }
            });
            Swal.fire({
                title: 'Confirm Saving Sale Wastage Info.!',
                text: 'Continue to save / Cancel to stop',
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Continue'
            }).then((result) => {
                if (result.isConfirmed) {
                    dispatch(saveSaleWastage(data))
                        .then(() => {
                            resetForm()
                        })
                }
            })
        }
    }

    const validateField = (fieldObj) => {
        let errorObj = { isValid: true, error: null }
        if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
        else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
        else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }
        return errorObj;
    }

    const editAction = (ele) => {
        setFormValue('id', ele.id)
        setFormValue('mainGroup', ele.mainGroupId)
        setFormValue('product', ele.productId)
        setFormValue('subProduct', ele.subProductId)
        setFormValue('productSize', ele.productSizeId)
        setFormValue('purity', ele.purityId)
        setFormValue('fromWeight', ele.fromWeight)
        setFormValue('toWeight', ele.toWeight)
        setFormValue('maxWstPerGram', ele.maxWstPerGram)
        setFormValue('minWstPerGram', ele.minWstPerGram)
        setFormValue('maxDirWst', ele.maxDirWst)
        setFormValue('minDirWst', ele.minDirWst)
        setFormValue('maxMcPerGram', ele.maxMcPerGram)
        setFormValue('minMcPerGram', ele.minMcPerGram)
        setFormValue('maxDirMc', ele.maxDirMc)
        setFormValue('minDirMc', ele.minDirMc)
        // document.getElementById("mcOnGross").checked = ele.mcOnGross;
        setFormValue('mcOn', ele.mcOn)
        // document.getElementById("wastageOnGross").checked = ele.wastageOnGross;
        setFormValue('wastageOn', ele.wastageOn)
        document.getElementById("incWstMc").checked = ele.incWstMc;
        setFormValue('incWstMc', ele.incWstMc)
    }

    const deleteAction = (data) => {
        let selectedIds;
        if (data instanceof Array && data.length > 0) {
            selectedIds = data.map((ele) => {
                return ele.id
            })
        } else {
            selectedIds = data.id;
        }
        Swal.fire({
            title: 'Confirm Deleting Sale Wastage.!',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(deleteSaleWastages(selectedIds.toString()))
                    .then(() => {
                        selected = []
                        setSelected(selected);
                    })
            }
        })
    }

    useEffect(() => {
        dispatch(fetchMainGroups());
        dispatch(fetchSaleWastages());
    }, [])

    useEffect(() => {
        dispatch(fetchProductsByMainGroup(get(formData, 'mainGroup.value')));
        dispatch(fetchPuritiesByMainGroup(get(formData, 'mainGroup.value')));
    }, [get(formData, 'mainGroup.value')])

    useEffect(() => {
        dispatch(fetchSubProductsByProduct(get(formData, 'product.value')));
        dispatch(fetchSizesByProduct(get(formData, 'product.value')));
    }, [get(formData, 'product.value')])

    const toggleView = () => {
        const formNode = document.getElementById('form');
        formNode.classList.toggle('collapse-panel');
        setToggleView(!viewForm);
    }

    return (
        <div className="panel panel-default hero-panel">
            <div className="panel-heading">
                <h1 className="panel-title">{t("creations.saleWastage")} {t("common.creation")}</h1>
                <div className="panel-options">
                    <a
                        href="#"
                        className="text-info collapseBtn"
                        title="Show Add Form"
                    >
                        {viewForm ?
                            <i className="zmdi zmdi-minus-circle  zmdi-hc-2x" onClick={() => { toggleView() }}>
                            </i> :
                            <i className="zmdi zmdi-plus-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                            </i>}
                    </a>
                </div>
            </div>
            <div className="panel-body">
                <div className="panel update-panel form-pnel-container" id="form">
                    <div className="text-danger">* {t("common.requiredFieldsMsg")} </div>
                    <div className="panel update-panel">
                        <div className="panel-body ">
                            <div className="row">
                                <div className="col-sm-6 col-md-4 col-lg-4">
                                    <div className="form-group">
                                        <label htmlFor="">
                                            {t("creations.mainGroup")}<span className="asterisk">*</span>
                                        </label>
                                        <Select name="mainGroup" value={get(formData, 'mainGroup.value')} onChange={(e) => setFormValue('mainGroup', e.target.value)} data={mainGroupData} />
                                        <div className="invalid-feedback">{get(formData, 'mainGroup.error')}</div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-md-4 col-lg-4">
                                    <div className="form-group">
                                        <label htmlFor="">
                                            {t("creations.productName")}<span className="asterisk">*</span>
                                        </label>
                                        <Select name="product" value={get(formData, 'product.value')} onChange={(e) => setFormValue('product', e.target.value)} data={productsData} />
                                        <div className="invalid-feedback">{get(formData, 'product.error')}</div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-md-4 col-lg-4">
                                    <div className="form-group">
                                        <label htmlFor="">
                                            {t("creations.subProduct")}<span className="asterisk">*</span>
                                        </label>
                                        <Select name="subProduct" value={get(formData, 'subProduct.value')} onChange={(e) => setFormValue('subProduct', e.target.value)} data={subProductsData} />
                                        <div className="invalid-feedback">{get(formData, 'subProduct.error')}</div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-md-4 col-lg-4">
                                    <div className="form-group">
                                        <label htmlFor="">
                                            {t("common.Purity")}<span className="asterisk">*</span>
                                        </label>
                                        <Select name="purity" value={get(formData, 'purity.value')} onChange={(e) => setFormValue('purity', e.target.value)} data={puritiesData} />
                                        <div className="invalid-feedback">{get(formData, 'purity.error')}</div>
                                    </div>
                                </div>
                                <div className="col-sm-6 col-md-4 col-lg-4">
                                    <div className="form-group">
                                        <label htmlFor="">
                                            {t("creations.productSize")}<span className="asterisk">*</span>
                                        </label>
                                        <Select name="productSize" value={get(formData, 'productSize.value')} onChange={(e) => setFormValue('productSize', e.target.value)} data={productSizesData} />
                                        <div className="invalid-feedback">{get(formData, 'productSize.error')}</div>
                                    </div>
                                </div>
                            </div>
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    <h3 className="panel-title">{t("common.Wastage")} &amp; {t("common.Making")} {t("common.details")}</h3>
                                </div>
                                <div className="row">
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("common.From")} {t("common.Weight")}<span className="asterisk">*</span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="fromWeight"
                                                name="fromWeight"
                                                value={get(formData, 'fromWeight.value')}
                                                onChange={(e) => setFormValue('fromWeight', e.target.value)}
                                            />
                                            <div className="invalid-feedback">{get(formData, 'fromWeight.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("common.To")} {t("common.Weight")}<span className="asterisk">*</span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="toWeight"
                                                name="toWeight"
                                                value={get(formData, 'toWeight.value')}
                                                onChange={(e) => setFormValue('toWeight', e.target.value)}
                                            />
                                            <div className="invalid-feedback">{get(formData, 'toWeight.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("creations.minWstPerGram")}<span className="asterisk">*</span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="minWstPerGram"
                                                name="minWstPerGram"
                                                value={get(formData, 'minWstPerGram.value')}
                                                onChange={(e) => setFormValue('minWstPerGram', e.target.value)}
                                            />
                                            <div className="invalid-feedback">{get(formData, 'minWstPerGram.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("creations.maxWstPerGram")}<span className="asterisk">*</span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="maxWstPerGram"
                                                name="maxWstPerGram"
                                                value={get(formData, 'maxWstPerGram.value')}
                                                onChange={(e) => setFormValue('maxWstPerGram', e.target.value)}
                                            />
                                            <div className="invalid-feedback">{get(formData, 'maxWstPerGram.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("creations.minDirWst")}<span className="asterisk">*</span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="minDirWst"
                                                name="minDirWst"
                                                value={get(formData, 'minDirWst.value')}
                                                onChange={(e) => setFormValue('minDirWst', e.target.value)}
                                            />
                                            <div className="invalid-feedback">{get(formData, 'minDirWst.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("creations.maxDirWst")}<span className="asterisk">*</span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="maxDirWst"
                                                name="maxDirWst"
                                                value={get(formData, 'maxDirWst.value')}
                                                onChange={(e) => setFormValue('maxDirWst', e.target.value)}
                                            />
                                            <div className="invalid-feedback">{get(formData, 'maxDirWst.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("creations.minMcPerGram")}<span className="asterisk">*</span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="minMcPerGram"
                                                name="minMcPerGram"
                                                value={get(formData, 'minMcPerGram.value')}
                                                onChange={(e) => setFormValue('minMcPerGram', e.target.value)}
                                            />
                                            <div className="invalid-feedback">{get(formData, 'minMcPerGram.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("creations.maxMcPerGram")}<span className="asterisk">*</span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="maxMcPerGram"
                                                name="maxMcPerGram"
                                                value={get(formData, 'maxMcPerGram.value')}
                                                onChange={(e) => setFormValue('maxMcPerGram', e.target.value)}
                                            />
                                            <div className="invalid-feedback">{get(formData, 'maxMcPerGram.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("creations.minDirMc")}<span className="asterisk">*</span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="minDirMc"
                                                name="minDirMc"
                                                value={get(formData, 'minDirMc.value')}
                                                onChange={(e) => setFormValue('minDirMc', e.target.value)}
                                            />
                                            <div className="invalid-feedback">{get(formData, 'minDirMc.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("creations.maxDirMc")}<span className="asterisk">*</span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="maxDirMc"
                                                name="maxDirMc"
                                                value={get(formData, 'maxDirMc.value')}
                                                onChange={(e) => setFormValue('maxDirMc', e.target.value)}
                                            />
                                            <div className="invalid-feedback">{get(formData, 'maxDirMc.error')}</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">{t("creations.mcCalcOn")}</label>
                                            <div className="radio radio-inline radio-primary">
                                                <input
                                                    type="radio"
                                                    value="Gross"
                                                    name="mcOn"
                                                    checked={get(formData, 'mcOn.value') === "Gross"}
                                                    onChange={(e) => setFormValue('mcOn', "Gross")} />
                                                <label htmlFor="">{t("common.gross")}</label>
                                            </div>
                                            <div className="radio radio-inline radio-primary">
                                                <input
                                                    type="radio"
                                                    value="Net"
                                                    name="mcOn"
                                                    checked={get(formData, 'mcOn.value') === "Net"}
                                                    onChange={(e) => setFormValue('mcOn', "Net")} />

                                                <label htmlFor="">{t("common.net")}</label>
                                            </div>
                                            <div className="invalid-feedback">{get(formData, 'mcOn.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">{t("creations.wastageCalcOn")}</label>
                                            <div className="radio radio-inline radio-primary">
                                                <input
                                                    type="radio"
                                                    name="wastageOn"
                                                    value="Gross"
                                                    checked={get(formData, 'wastageOn.value') === "Gross"}
                                                    onChange={(e) => setFormValue('wastageOn', "Gross")} />
                                                <label htmlFor="">{t("common.gross")}</label>
                                            </div>
                                            <div className="radio radio-inline radio-primary">
                                                <input
                                                    type="radio"
                                                    name="wastageOn"
                                                    value="Net"
                                                    checked={get(formData, 'wastageOn.value') === "Net"}
                                                    onChange={(e) => setFormValue('wastageOn', "Net")} />
                                                <label htmlFor="">{t("common.net")}</label>
                                            </div>
                                            <div className="invalid-feedback">{get(formData, 'wastageOn.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">&nbsp;</label>
                                            <div className="checkbox checkbox-primary">
                                                <input
                                                    type="checkbox"
                                                    name="incWstMc"
                                                    id="incWstMc"
                                                    defaultChecked={get(formData, 'incWstMc.value')}
                                                    onChange={(e) => setFormValue('incWstMc', !formData.incWstMc.value)} />
                                                <label htmlFor="">{t("creations.incWstMc")}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="panel-footer text-center">
                        <a href="#" className="btn btn-primary fire" onClick={saveForm}>
                            <i className="zmdi zmdi-check"></i> {t("common.save")}
                        </a>
                        <a href="#" className="btn btn-default" onClick={resetForm}>
                            <i className="zmdi zmdi-close"></i> {t("common.clear")}
                        </a>
                    </div>
                </div>
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">Sale Wastage List</h3>
                        <div className="panel-options">
                            <p>
                                {selected.length > 0 ?
                                    <a href="#" className="btn btn-info btn-sm" onClick={() => deleteAction(selected)} >
                                        <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                                    </a> :
                                    null}
                            </p>
                        </div>
                    </div>
                    <div className="panel-body">
                        <div className="ag-grid-wrapper">
                            <Grid colDefs={saleWastageColumns} rowData={saleWastagesData} selectedRows={setSelected} actions={{ isRequired: true, editAction: editAction, deleteAction: deleteAction }} flex={true} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default withTranslation()(SaleWastage);
