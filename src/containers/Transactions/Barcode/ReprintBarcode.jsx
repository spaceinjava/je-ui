import React, { Component, useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { hsnForm, hsnColumns } from '../Constants';
import Menu from "../Menu";
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../../components/Grid";
import Select from "../../../components/Form/Select";
import { fetchMainGroups } from '../../../redux/actions/mainGroupActions'
import { saveHsnCode, fetchHsnCodes, deleteHsnCode } from "../../../redux/actions/creations/hsnActions";
import Swal from 'sweetalert2'

export const ReprintBarcode = (props) => {
    const { t } = props;
    const [formData, setFromData] = useState(cloneDeep(hsnForm));
    const [viewForm, setToggleView] = useState(true);
    let [selected, setSelected] = useState([]);
    const dispatch = useDispatch();
    const mainGroups = useSelector(state => state.MainGroupReducer.allGroups)
    const hsnCodes = useSelector(state => state.hsnReducer.allHsnCodes)
    const groupData = mainGroups.data ? mainGroups.data.map((group) => {
        return {
            id: group.id,
            name: group.groupName,
            shortCode: group.shortCode,
        }
    }) : [];
    const hsnCodesData = hsnCodes.data ? hsnCodes.data.map((hsn) => {
        return {
            id: hsn.id,
            groupId: hsn.mainGroup.id,
            groupName: hsn.mainGroup.groupName,
            hsnCode: hsn.hsnCode,
            description: hsn.description
        }
    }) : [];
    const setFormValue = (field, value) => {
        formData[field]['value'] = value;
        setFromData({ ...formData });
    }
    const resetForm = () => {
        map(formData, function (value, key, object) {
            formData[key]['value'] = ''
        });
        setFromData({ ...formData });
    }

    const saveForm = () => {
        let isFormValid = true;
        map(formData, function (value, key, object) {
            const isValid = validateField(value);
            if (!isValid.isValid) {
                isFormValid = false;
            }
            formData[key] = { ...value, ...isValid }
        });
        if (isFormValid) {
            let data = {};
            map(formData, function (value, key, object) {
                data = {
                    ...data,
                    [key]: value.value
                }
            });
            Swal.fire({
                title: 'Confirm Saving HSNCode Info.!',
                text: 'Continue to save / Cancel to stop',
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Continue'
            }).then((result) => {
                if (result.isConfirmed) {
                    dispatch(saveHsnCode(data))
                        .then(() => {
                            resetForm()
                        })
                }
            })
        }
    }

    const validateField = (fieldObj) => {
        let errorObj = { isValid: true, error: null }
        if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
        else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
        else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }
        return errorObj;
    }

    const editAction = (ele) => {
        setFormValue('id', ele.id)
        setFormValue('mainGroup', ele.groupId)
        setFormValue('hsnCode', ele.hsnCode)
        setFormValue('description', ele.description)
    }

    const deleteAction = (data) => {
        let selectedIds;
        if (data instanceof Array && data.length > 0) {
            selectedIds = data.map((ele) => {
                return ele.id
            })
        } else {
            selectedIds = data.id;
        }
        Swal.fire({
            title: 'Confirm Deleting HSNCode.!',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(deleteHsnCode(selectedIds.toString()))
                    .then(() => {
                        selected = []
                        setSelected(selected);
                    })
            }
        })
    }

    useEffect(() => {
        dispatch(fetchMainGroups());
        dispatch(fetchHsnCodes());
    }, [])

    const toggleView = () => {
        const formNode = document.getElementById('form');
        formNode.classList.toggle('collapse-panel');
        setToggleView(!viewForm);
    }

    return (
        <div className="panel panel-default hero-panel">
            <div className="panel-heading">
                <h1 className="panel-title">{t("common.hsn")} {t("common.code")} {t("common.creation")}</h1>
                <div className="panel-options">
                    <a
                        href="#"
                        className="text-info collapseBtn"
                        title="Show Add Form"
                    >
                        {viewForm ?
                            <i className="zmdi zmdi-minus-circle  zmdi-hc-2x" onClick={() => { toggleView() }}>
                            </i> :
                            <i className="zmdi zmdi-plus-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                            </i>}
                    </a>
                </div>
            </div>
            <div className="panel-body">
                <div className="panel update-panel form-pnel-container" id="form">
                    <div className="text-danger">* {t("common.requiredFieldsMsg")} </div>
                    <div className="panel update-panel">
                        <div className="panel-body ">
                            <div className="row">
                                <div className="col-sm-6 col-md-3 col-lg-3">
                                    <div className="form-group">
                                        <label htmlFor="">
                                            {t("creations.mainGroup")}<span className="asterisk">*</span>
                                        </label>
                                        <Select name="mainGroup" value={get(formData, 'mainGroup.value')} onChange={(e) => setFormValue('mainGroup', e.target.value)} data={groupData} />
                                        <div className="invalid-feedback">{get(formData, 'mainGroup.error')}</div>
                                    </div>
                                </div>

                                <div className="col-sm-6 col-md-3 col-lg-3">
                                    <div className="form-group">
                                        <label htmlFor="">
                                            {t("common.hsn")} {t("common.code")}<span className="asterisk">*</span>
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            placeholder=""
                                            name="hsnCode"
                                            value={get(formData, 'hsnCode.value')}
                                            onChange={(e) => setFormValue('hsnCode', e.target.value)}
                                        />
                                        <div className="invalid-feedback">{get(formData, 'hsnCode.error')}</div>
                                    </div>
                                </div>

                                <div className="col-sm-6 col-md-3 col-lg-3">
                                    <div className="form-group">
                                        <label htmlFor="">
                                            {t("common.description")}<span className="asterisk">*</span>
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            placeholder=""
                                            name="description"
                                            value={get(formData, 'description.value')}
                                            onChange={(e) => setFormValue('description', e.target.value)}
                                        />
                                        <div className="invalid-feedback">{get(formData, 'description.error')}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="panel-footer text-center">
                        <a href="#" className="btn btn-primary fire" onClick={saveForm}>
                            <i className="zmdi zmdi-check"></i> {t("common.save")}
                        </a>
                        <a href="#" className="btn btn-default" onClick={resetForm}>
                            <i className="zmdi zmdi-close"></i> {t("common.clear")}
                        </a>
                    </div>
                </div>
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">HSN Codes List</h3>
                        <div className="panel-options">
                            <p>
                                {selected.length > 0 ?
                                    <a href="#" className="btn btn-info btn-sm" onClick={() => deleteAction(selected)} >
                                        <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                                    </a> :
                                    null}
                            </p>
                        </div>
                    </div>
                    <div className="panel-body">
                        <div className="ag-grid-wrapper">
                            <Grid colDefs={hsnColumns} rowData={hsnCodesData} selectedRows={setSelected} actions={{ isRequired: true, editAction: editAction, deleteAction: deleteAction }} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default withTranslation()(ReprintBarcode);
