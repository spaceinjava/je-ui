import React, { useState, useEffect } from "react"
import { withTranslation } from "react-i18next"
import { metalBarcodeForm } from '../Constants'
import { get, map, cloneDeep } from "lodash"
import { useDispatch, useSelector } from "react-redux"
import Select from "../../../components/Form/Select"
import StoneDetails from "../StoneDetails/StoneDetails"
import { saveStoneDetailsForm } from "../../../redux/actions/transactions/stoneDetailsActions";
import { fetchSuppliers } from '../../../redux/actions/Admin/supplierActions'
import { fetchMainGroups, fetchProductsByMainGroup, fetchPuritiesByMainGroup } from '../../../redux/actions/mainGroupActions'
import { fetchSubProductsByProduct, fetchSizesByProduct } from "../../../redux/actions/creations/productActions";
import { fetchBrandProductsByBrand } from '../../../redux/actions/creations/brandProductActions'
import { saveBrandedBarcode } from '../../../redux/actions/transactions/barcode/brandedBarcodeActions'
import { fetchLots } from '../../../redux/actions/transactions/barcode/metalBarcodeActions'
import { fetchRatesByMainGroup } from '../../../redux/actions/creations/rateActions'
import { fetchSaleWastagesBySubProduct } from '../../../redux/actions/creations/saleWastageActions'
import Swal from 'sweetalert2'
import LoadingOverlay from 'react-loading-overlay';
import { roundToThree } from "../../../utils/util";

export const BrandedBarcode = (props) => {
  const { t } = props;
  const [formData, setFromData] = useState(cloneDeep(metalBarcodeForm));
  const [viewForm, setToggleView] = useState(true);
  const [isChecked, setIsChecked] = useState(false);
  const [actualQuantity, setActualQuantity] = useState(0)
  const [actualGrossWeight, setActualGrossWeight] = useState(0)
  const [actualNetWeight, setActualNetWeight] = useState(0)
  const [pendingQuantity, setPendingQuantity] = useState(0)
  const [pendingGrossWeight, setPendingGrossWeight] = useState(0)
  const [pendingNetWeight, setPendingNetWeight] = useState(0)
  const dispatch = useDispatch();

  const lotDetails = useSelector((state) => state.metalBarcodeReducer.lotInfo)
  const suppliers = useSelector((state) => state.supplierReducer.allSuppliers)
  const mainGroups = useSelector((state) => state.MainGroupReducer.allGroups)
  const products = useSelector(state => state.MainGroupReducer.allProductsByMainGroup)
  const subProducts = useSelector(state => state.productReducer.allSubProductsByProduct)
  const productSizes = useSelector(state => state.productReducer.allSizesByProduct)
  const rates = useSelector((state) => state.rateReducer.ratesByMainGroup)
  const saleWastages = useSelector((state) => state.saleWastageReducer.saleWastageBySubProduct)
  const loader = useSelector((state) => state.supplierReducer.loading);
  const purities = useSelector(
    (state) => state.MainGroupReducer.allPuritiesByMainGroup
  );

  const [image, setImage] = useState({
    preview: "",
    raw: ""
  })

  const lotData = lotDetails.data ? lotDetails.data.map((lot) => {
    return {
      id: lot.id,
      name: lot.lotNo
    }
  }) : [];


  const actualDetails = lotDetails.data ? lotDetails.data.filter((lot) => lot.id === get(formData, 'lotNo.value')) : [];

  useEffect(() => {
    if (actualDetails.length > 0) {
      setActualQuantity(actualDetails[0].quantity)
      setActualGrossWeight(actualDetails[0].grossWeight)
      setActualNetWeight(actualDetails[0].netWeight)
      setPendingQuantity(actualDetails[0].quantity - actualDetails[0].createdBarCodeQuantity)
      setPendingGrossWeight(actualDetails[0].grossWeight - actualDetails[0].createdBarCodeWeight)
      setPendingNetWeight(actualDetails[0].netWeight) //further calculation
    }
  }, [get(formData, 'lotNo.value')])



  console.log(actualDetails)
  console.log(actualDetails.length > 0 && actualDetails[0].quantity)

  const suppliersData = suppliers.data ? suppliers.data.map((supplier) => {
    return {
      id: supplier.id,
      name: supplier.partyName
    }
  }) : [];
  const mainGroupData = mainGroups.data ? mainGroups.data.map((group) => {
    return {
      id: group.id,
      name: group.groupName,
      shortCode: group.shortCode,
    }
  }) : [];
  const productsData = products.data ? products.data.map((product) => {
    return {
      id: product.id,
      name: product.productName,
      shortCode: product.shortCode,
    }
  }) : [];
  const subProductsData = subProducts.data ? subProducts.data.map((subProduct) => {
    return {
      id: subProduct.id,
      name: subProduct.subProductName,
      shortCode: subProduct.shortCode,
    }
  }) : [];
  const puritiesData = purities.data ? purities.data.map((purity) => {
    return {
      id: purity.id,
      name: purity.purity,
    }
  }) : [];

  const allMainGroupRatesData = rates.data ? rates.data.map((rate) => {
    return {
      id: rate.id,
      mainGroupId: rate.mainGroup.id,
      perGramRate: rate.perGramRate,
      purityId: rate.purity.id,
      purityRate: rate.purityRate,
    }
  }) : [];

  const productSizesData = productSizes.data ? productSizes.data.map((productSize) => {
    return {
      id: productSize.id,
      name: productSize.productSize
    }
  }) : [];

  const setFormValue = (field, value) => {
    formData[field]['value'] = value;
    setFromData({ ...formData });
  }

  const resetForm = () => {
    map(formData, function (value, key, object) {
      if (formData[key]["type"] === 'number') {
        formData[key]["value"] = 0;
      } else {
        formData[key]["value"] = '';
      }
    });
    setImage({
      preview: "",
      raw: ""
    });
    setFromData({ ...formData });
  }
  const saveForm = () => {
    let isFormValid = true;
    map(formData, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      formData[key] = { ...value, ...isValid };
    });
    if (isFormValid) {
      let data = {};
      map(formData, function (value, key, object) {
        data = {
          ...data,
          [key]: value.value,
        };
      });
      data.image = image.raw;
      console.log("saving branded barcode with data : ", data);
      Swal.fire({
        title: 'Confirm Saving Barcode Info.!',
        text: 'Continue to save / Cancel to stop',
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Continue'
      }).then((result) => {
        if (result.isConfirmed) {
          dispatch(saveBrandedBarcode(data))
            .then(() => {
              // resetForm();
            })
        }
      })
    }
  }

  const validateField = (fieldObj) => {
    let errorObj = { isValid: true, error: null }
    if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
    else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
    else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }
    return errorObj;
  }

  useEffect(() => {
    dispatch(fetchSuppliers());
    dispatch(fetchMainGroups());
  }, [])

  useEffect(() => {
    console.log(get(formData, "partyNameId.value"))
    console.log(get(formData, "mainGroupId.value"))
    if (get(formData, "partyNameId.value") !== "" && get(formData, "mainGroupId.value") !== "")
      dispatch(
        fetchLots(get(formData, "partyNameId.value"), get(formData, "mainGroupId.value"), get(formData, "date.value"))
      );
  }, [get(formData, "partyNameId.value"), get(formData, "mainGroupId.value"), get(formData, "date.value")]);

  useEffect(() => {
    if (get(formData, "subProduct.value") !== "" && get(formData, "productSize.value") !== "" && get(formData, "purity.value") !== "")
      dispatch(
        fetchSaleWastagesBySubProduct(get(formData, "subProduct.value"), get(formData, "productSize.value"), get(formData, "purity.value"))
      );
    setFormValue('grossWeight', '');
  }, [get(formData, "subProduct.value"), get(formData, "productSize.value"), get(formData, "purity.value")]);

  useEffect(() => {
    dispatch(fetchProductsByMainGroup(get(formData, 'mainGroupId.value')));
    dispatch(fetchPuritiesByMainGroup(get(formData, "mainGroupId.value")));
    dispatch(fetchRatesByMainGroup(get(formData, "mainGroupId.value")));
  }, [get(formData, 'mainGroupId.value')])

  useEffect(() => {
    dispatch(fetchBrandProductsByBrand(get(formData, 'brandId.value')));
  }, [get(formData, 'brandId.value')])

  useEffect(() => {
    dispatch(fetchSubProductsByProduct(get(formData, 'product.value')));
    dispatch(fetchSizesByProduct(get(formData, 'product.value')));
  }, [get(formData, 'product.value')])

  useEffect(() => {
    let selectedPurity = get(formData, 'purity.value');
    let selectedMainGroup = get(formData, 'mainGroupId.value');
    if (selectedMainGroup != "" && selectedPurity != "") {
      if (allMainGroupRatesData.length > 0) {
        let filteredRateObject = allMainGroupRatesData.filter((ele) => (ele.mainGroupId === selectedMainGroup) && (ele.purityId === selectedPurity))[0];
        if (filteredRateObject) {
          setFormValue('rate', filteredRateObject.purityRate);
        } else {
          setFormValue('rate', 0);
          Swal.fire({
            position: "top-end",
            title: 'No Rates for the selected purity.!',
            text: 'Please define rates before going forward',
            icon: 'info',
            showConfirmButton: false,
            timer: 2000
          })

        }
      } else {
        setFormValue('rate', 0);
        Swal.fire({
          position: "top-end",
          title: 'No Rates defined for selected mainGroup.!',
          text: 'Please define rates before generating barcode',
          icon: 'info',
          showConfirmButton: false,
          timer: 2000
        })
      }
    }
  }, [get(formData, 'purity.value')])

  let grossWeight = get(formData, 'grossWeight.value')
  let fromWeight = get(formData, 'fromWeight.value')
  let toWeight = get(formData, 'toWeight.value')

  const onSelectedGrossWeight = () => {
    if (grossWeight < fromWeight || grossWeight > toWeight) {
      setFormValue('grossWeight', '');
      setFormValue('fromWeight', '');
      setFormValue('toWeight', '');
      Swal.fire({
        position: "top-end",
        title: 'No data found!',
        text: 'Please define sale wastage for the Gross Weight',
        icon: 'info',
        showConfirmButton: false,
        timer: 2000
      })
    }
  }

  const emptyMcWst = () => {
    setFormValue('fromWeight', '');
    setFormValue('toWeight', '');
    setFormValue('mcPerGram', '');
    setFormValue('mcDir', '');
    setFormValue('wastagePercent', '');
    setFormValue('dirWastage', '');
  }

  useEffect(() => {
    if (get(formData, 'grossWeight.value') !== null) {
      emptyMcWst()
      if (saleWastages.data && saleWastages.data.length > 0) {
        let saleWastagesData = saleWastages.data

        saleWastagesData.filter(data => (data.fromWeight <= grossWeight && data.toWeight >= grossWeight))
          .map(filteredData => {
            setFormValue('fromWeight', filteredData.fromWeight);
            setFormValue('toWeight', filteredData.toWeight);
            setFormValue('mcPerGram', filteredData.maxMcPerGram);
            setFormValue('mcDir', filteredData.maxDirMc);
            setFormValue('wastagePercent', filteredData.maxWstPerGram);
            setFormValue('dirWastage', filteredData.maxDirWst);
          })
      } else {
        emptyMcWst()
      }
    }
  }, [get(formData, 'grossWeight.value')])

  useEffect(() => {
    let quantity = get(formData, 'quantity.value');
    let weight = get(formData, 'weight.value');
    let rate = get(formData, 'rate.value');
    let amount = 0;
    amount = roundToThree(quantity * rate);
    setFormValue("amount", amount);
  }, [get(formData, 'quantity.value'), get(formData, 'weight.value'), get(formData, 'rate.value')])

  const toggleView = () => {
    const formNode = document.getElementById('form');
    formNode.classList.toggle('collapse-panel');
    setToggleView(!viewForm);
  }

  const partyNameChange = (e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    setFormValue('partyId', e.target.value)
    setFormValue('party', label)
  }

  const selectOnChange = (idKey, labelKey, e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    if (e.target.value) {
      setFormValue(idKey, e.target.value)
      setFormValue(labelKey, label)
    }
  }

  const handleChange = (e) => {
    if (e.target.files.length) {
      setImage({
        preview: URL.createObjectURL(e.target.files[0]),
        raw: e.target.files[0]
      })
    }
  }

  const editMcWst = () => {
    setIsChecked(!isChecked);
  }

  const stoneForm = () => {
    dispatch(saveStoneDetailsForm(false));
  };

  const fetchPartyNameById = (partyId) => {
    let selectedParty = suppliersData.find(ele => ele.id === partyId);
    return selectedParty ? selectedParty.name : "";
  }

  return (
    <div className="panel panel-default hero-panel">
      <LoadingOverlay
        active={loader}
        spinner
        text='Loading...'
      >
        <div className="panel-heading">
          <h1 className="panel-title">{t("transactions.metalPurchase")}</h1>
          <div className="panel-options">
            <a
              href="#"
              className="text-info collapseBtn"
              title="Show Add Form"
            >
              {viewForm ?
                <i className="zmdi zmdi-minus-circle  zmdi-hc-2x" onClick={() => { toggleView() }}>
                </i> :
                <i className="zmdi zmdi-plus-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                </i>}
            </a>
          </div>
        </div>
        <div className="panel-body">
          <div id="form">
            <div className="panel update-panel form-pnel-container">
              {/* <div className="text-danger">* {t("common.requiredFieldsMsg")} </div> */}
              <div className="panel-body">
                <div className="row">
                  <div className="col-md-3">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("creations.mainGroup")}<span className="asterisk">*</span>
                      </label>
                      <Select name="mainGroupId" value={get(formData, 'mainGroupId.value')} onChange={e => selectOnChange('mainGroupId', 'mainGroup', e)} data={mainGroupData} />
                      <div className="invalid-feedback">{get(formData, 'mainGroupId.error')}</div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label htmlFor="">
                        {t("common.supplier")} / {t("common.Party")}<span className="asterisk">*</span>
                      </label>
                      <Select name="partyId" value={get(formData, 'partyId.value')} onChange={(e) => partyNameChange(e)} data={suppliersData} />
                      <div className="invalid-feedback">{get(formData, 'partyId.error')}</div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label htmlFor="">
                        {t("transactions.lotDate")}<span className="asterisk">*</span>
                      </label>
                      <input type="date"
                        className="form-control"
                        placeholder=""
                        name="lotDate"
                        value={get(formData, 'lotDate.value')}
                        onChange={(e) => setFormValue('lotDate', e.target.value)} />
                      <div className="invalid-feedback">{get(formData, 'lotDate.error')}</div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label htmlFor="">
                        {t("transactions.lotNo")}<span className="asterisk">*</span>
                      </label>
                      <Select name="lotNo" value={get(formData, 'lotNo.value')} onChange={e => setFormValue('lotNo', e.target.value)} data={lotData} />
                      <div className="invalid-feedback">{get(formData, 'lotNo.error')}</div>
                    </div>
                  </div>
                </div>
                <div class="m-t-10 m-b-30 text-center">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="bg-light">
                        <div class="panel-heading p-l-0">
                          <h4 class="panel-title">{t("common.actual")} {t("common.details")}</h4>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-4 m-b-0">
                            <label class="">Qty</label>
                            <p class="m-b-0 text-info">{actualQuantity}</p>
                          </div>
                          <div class="form-group col-md-4 m-b-0">
                            <label for="">Gr. Weight</label>
                            <p class="m-b-0 text-info">{actualGrossWeight} gms</p>
                          </div>
                          <div class="form-group col-md-4 m-b-0">
                            <label for="">Net Weight</label>
                            <p class="m-b-0 text-info">{actualNetWeight} gms</p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="bg-light">
                        <div class="panel-heading p-l-0">
                          <h4 class="panel-title">{t("common.pending")} {t("common.details")}</h4>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-4 m-b-0">
                            <label class="">Qty</label>
                            <p class="m-b-0 text-info">{pendingQuantity}</p>
                          </div>
                          <div class="form-group col-md-4 m-b-0">
                            <label for="">Gr. Weight</label>
                            <p class="m-b-0 text-info">{pendingGrossWeight} gms</p>
                          </div>
                          <div class="form-group col-md-4 m-b-0">
                            <label for="">Net Weight</label>
                            <p class="m-b-0 text-info">{pendingNetWeight} gms</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-8">
                    <div className="row">
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">Product<span className="asterisk">*</span>
                            <a href="" className="" data-toggle="modal" data-target="#addProduct" data-backdrop="static" data-keyboard="false"><i className="zmdi zmdi-plus-circle f-s-16"></i></a>
                          </label>
                          <Select name="product" value={get(formData, 'product.value')} onChange={(e) => setFormValue('product', e.target.value)} data={productsData} />
                          <div className="invalid-feedback">{get(formData, 'product.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">Sub Product<span className="asterisk">*</span>
                            <a href="" className="" data-toggle="modal" data-target="#addProduct" data-backdrop="static" data-keyboard="false"><i className="zmdi zmdi-plus-circle f-s-16"></i></a>
                          </label>
                          <Select name="subProduct" value={get(formData, 'subProduct.value')} onChange={(e) => setFormValue('subProduct', e.target.value)} data={subProductsData} />
                          <div className="invalid-feedback">{get(formData, 'subProduct.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">Purity<span className="asterisk">*</span>
                            <a href="" className="" data-toggle="modal" data-target="#addProduct" data-backdrop="static" data-keyboard="false"><i className="zmdi zmdi-plus-circle f-s-16"></i></a>
                          </label>
                          <Select name="purity" value={get(formData, 'purity.value')} onChange={(e) => setFormValue('purity', e.target.value)} data={puritiesData} />
                          <div className="invalid-feedback">{get(formData, 'purity.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">Size<span className="asterisk">*</span>
                            <a href="" className="" data-toggle="modal" data-target="#addProduct" data-backdrop="static" data-keyboard="false"><i className="zmdi zmdi-plus-circle f-s-16"></i></a>
                          </label>
                          <Select name="productSize" value={get(formData, 'productSize.value')} onChange={(e) => setFormValue('productSize', e.target.value)} data={productSizesData} />
                          <div className="invalid-feedback">{get(formData, 'productSize.error')}</div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">Qty<span className="asterisk">*</span></label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Qty"
                            name="quantity"
                            value={get(formData, 'quantity.value')}
                            onChange={(e) => setFormValue('quantity', e.target.value)}
                          />
                          <div className="invalid-feedback">{get(formData, 'quantity.error')}</div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">Gr. Weight<span className="asterisk">*</span></label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Gr. Weight"
                            name="grossWeight"
                            value={get(formData, 'grossWeight.value')}
                            onChange={(e) => setFormValue('grossWeight', e.target.value)}
                            onBlur={onSelectedGrossWeight}
                          />
                          <div className="invalid-feedback">{get(formData, 'grossWeight.error')}</div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">St. Weight<span className="asterisk">*</span>
                            <a
                              href="#"
                              id="stoneDetails"
                              data-toggle="modal"
                              data-target="#addNew"
                              onClick={stoneForm}
                            >
                              <i className="zmdi zmdi-plus-circle f-s-16"></i>
                            </a>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Stone Weight"
                            name="stoneWeight"
                            value={get(formData, 'stoneWeight.value')}
                            onChange={(e) => setFormValue('stoneWeight', e.target.value)}
                          />
                          <div className="invalid-feedback">{get(formData, 'stoneWeight.error')}</div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">Net Weight<span className="asterisk">*</span></label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Net Weight"
                            name="netWeight"
                            value={get(formData, 'netWeight.value')}
                            onChange={(e) => setFormValue('netWeight', e.target.value)}
                          />
                          <div className="invalid-feedback">{get(formData, 'netWeight.error')}</div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">Rate<span className="asterisk">*</span></label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Rate"
                            name="rate"
                            value={get(formData, 'rate.value')}
                            onChange={(e) => setFormValue('rate', e.target.value)}
                            disabled
                          />
                          <div className="invalid-feedback">{get(formData, 'rate.error')}</div>
                        </div>
                      </div>

                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">Design<span className="asterisk">*</span></label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Design"
                            name="design"
                            value={get(formData, 'design.value')}
                            onChange={(e) => setFormValue('design', e.target.value)}
                          />
                          <div className="invalid-feedback">{get(formData, 'design.error')}</div>
                        </div>
                      </div>
                      <div className="col-sm-6 col-md-3 col-lg-3">
                        <div className="form-group">
                          <label htmlFor="">Remarks<span className="asterisk">*</span></label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Remarks"
                            name="remarks"
                            value={get(formData, 'remarks.value')}
                            onChange={(e) => setFormValue('remarks', e.target.value)}
                          />
                          <div className="invalid-feedback">{get(formData, 'remarks.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("transactions.labelType")}
                            <span className="asterisk">*</span>
                          </label>
                          <select
                            className="form-control select"
                            name="labelType"
                            value={get(formData, "labelType.value")}
                            onChange={(e) =>
                              setFormValue('labelType', e.target.value)
                            }
                          >
                            <option value={""}>Select</option>
                            <option value={"barcode"}>Bar Code</option>
                            <option value={"qrcode"}>QR Code</option>
                          </select>
                          <div className="invalid-feedback">
                            {get(formData, "labelType.error")}
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <div class="checkbox checkbox-primary checkbox-inline">
                            <input type="checkbox" id="mc-wst" checked={isChecked} onChange={editMcWst} />
                            <label for="mc-wst">Edit MC/Wst</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row showPanel">
                      <div class="col-md-2">
                        <div class="form-group">
                          <label for="">From Wt</label>
                          <input
                            type="text"
                            class="form-control"
                            placeholder=""
                            value={get(formData, 'fromWeight.value')}
                            onChange={(e) => setFormValue('fromWeight', e.target.value)}
                            disabled />
                          <div className="invalid-feedback">{get(formData, 'fromWeight.error')}</div>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label for="">To Weight</label>
                          <input
                            type="text"
                            class="form-control"
                            placeholder=""
                            value={get(formData, 'toWeight.value')}
                            onChange={(e) => setFormValue('toWeight', e.target.value)}
                            disabled />
                          <div className="invalid-feedback">{get(formData, 'toWeight.error')}</div>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label for="">Mc/g</label>
                          <input
                            type="text"
                            class="form-control"
                            placeholder=""
                            value={get(formData, 'mcPerGram.value')}
                            onChange={(e) => setFormValue('mcPerGram', e.target.value)}
                            disabled={!isChecked} />
                          <span class="small text-danger">0-5</span>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label for="">Mc Dir</label>
                          <input
                            type="text"
                            class="form-control"
                            placeholder=""
                            value={get(formData, 'mcDir.value')}
                            onChange={(e) => setFormValue('mcDir', e.target.value)}
                            disabled={!isChecked} />
                          <span class="small text-danger">6-9</span>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label for="">Wst %</label>
                          <input
                            type="text"
                            class="form-control"
                            placeholder=""
                            value={get(formData, 'wastagePercent.value')}
                            onChange={(e) => setFormValue('wastagePercent', e.target.value)}
                            disabled={!isChecked} />
                          <span class="small text-danger">0-5</span>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label for="">Dir Wst</label>
                          <input
                            type="text"
                            class="form-control"
                            placeholder=""
                            value={get(formData, 'dirWastage.value')}
                            onChange={(e) => setFormValue('dirWastage', e.target.value)}
                            disabled={!isChecked} />
                          <span class="small text-danger">0-5</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div id="" class="dropzone">
                      <label htmlFor="upload-button">
                        {image.preview ?
                          <img src={image.preview} alt="image" style={{ width: '128px', height: '128px', margin: 'auto', display: 'block' }} /> :
                          <div className="circle m-t-20" style={{ margin: 'auto' }}>
                            <i className="zmdi zmdi-camera zmdi-hc-2x"></i>
                          </div>
                        }
                      </label>
                      <input
                        type="file"
                        id="upload-button"
                        style={{ display: 'none' }}
                        onChange={handleChange} />
                    </div>
                  </div>
                </div>
                {/* Add Stone model */}
                <div
                  className="modal fade"
                  id="addNew"
                  tabIndex="-1"
                  role="dialog"
                  aria-labelledby="myModalLabel"
                >
                  <StoneDetails fetchPartyNameById={fetchPartyNameById} partyId={get(formData, "partyId.value")} />
                </div>
              </div>
              <div className="panel-footer text-center">
                <a href="#" className="btn btn-primary" onClick={saveForm}>
                  <i className="zmdi zmdi-check"></i> {t("common.save")}
                </a>
                <a href="#" className="btn btn-default" onClick={resetForm}>
                  <i className="zmdi zmdi-close"></i> {t("common.clear")}
                </a>
              </div>
            </div>
          </div>
        </div>
      </LoadingOverlay>
    </div>
  );
};

export default withTranslation()(BrandedBarcode);
