import React, { Component, useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { makeStyles } from "@material-ui/core";
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";

import { brandedLotSearchForm, brandedLotCreationForm } from "../Constants";
import StoneDetailsLot from "../StoneDetails/StoneDetailsLot";
import "../../../assets/css/search-select.css";
import CustomGrid from "../../../components/CustomGrid";
import Select from "../../../components/Form/Select";
import SearchSelect from "../../../components/Form/SearchSelect";
import { fetchSuppliers } from '../../../redux/actions/Admin/supplierActions'
import { fetchMainGroups } from '../../../redux/actions/mainGroupActions'
import { fetchBrandsByMainGroup } from '../../../redux/actions/creations/brandActions'
import { fetchBrandProductsByBrand, fetchSubProductsByProduct } from '../../../redux/actions/creations/brandProductActions'
import { fetchBrandedLotInvoices, fetchBrandedLotDetails, saveBrandedLot } from "../../../redux/actions/transactions/barcode/brandedLotActions";
import { fetchEmployeesList } from '../../../redux/actions/Admin/employeeActions';
import DatePicker from "../../../components/Form/DatePicker";

const useStyles = makeStyles((theme) => ({}));

export const BrandedLot = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const suppliers = useSelector(state => state.supplierReducer.allSuppliers)
  const mainGroups = useSelector(state => state.MainGroupReducer.allGroups)
  const brands = useSelector(state => state.brandReducer.allMainGroupBrands)
  const brandProducts = useSelector(state => state.brandProductReducer.allBrandProductsByBrand)
  const brandSubProducts = useSelector(state => state.brandProductReducer.allSubProductsByProduct)
  const employees = useSelector(state => state.employeeReducer.allEmployeesList);
  const allBrandedLotsInvoices = useSelector(state => state.brandedLotReducer.allBrandedLotsInvoices);
  const allBrandedLotDetails = useSelector(state => state.brandedLotReducer.allBrandedLotDetails);
  const brandedLotInfo = useSelector(state => state.brandedLotReducer.brandedLotInfo);

  const suppliersData = suppliers.data ? suppliers.data.map((supplier) => {
    return {
      id: supplier.id,
      name: supplier.partyName
    }
  }) : [];
  const mainGroupData = mainGroups.data ? mainGroups.data.map((group) => {
    return {
      id: group.id,
      name: group.groupName,
      shortCode: group.shortCode,
    }
  }) : [];
  const brandData = brands.data ? brands.data.map((brand) => {
    return {
      id: brand.id,
      name: brand.brandName,
      shortCode: brand.shortCode,
    }
  }) : [];
  const brandProductsData = brandProducts.data ? brandProducts.data.map((product) => {
    return {
      id: product.id,
      name: product.productName,
      shortCode: product.shortCode,
    }
  }) : [];
  const brandSubProductsData = brandSubProducts.data ? brandSubProducts.data.map((subProduct) => {
    return {
      id: subProduct.id,
      name: subProduct.subProductName,
      shortCode: subProduct.shortCode,
    }
  }) : [];
  const employeesData = employees.data ? employees.data.map((employee) => {
    return {
      id: employee.id,
      name: employee.firstName + " " + employee.middleName + " " + employee.lastName,
    }
  }) : [];

  const validateField = (fieldObj) => {
    let errorObj = { isValid: true, error: null }
    if (fieldObj.required && !fieldObj.value && fieldObj.value !== 0) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
    else if (fieldObj.value && parseInt(fieldObj.value) > parseInt(fieldObj.maxValue)) { errorObj.isValid = false; errorObj.error = `Value should be less than ${fieldObj.maxValue}` }
    else if (fieldObj.value && parseInt(fieldObj.value) < parseInt(fieldObj.minValue)) { errorObj.isValid = false; errorObj.error = `Value should be greater than ${fieldObj.minValue}` }
    else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
    else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }
    return errorObj;
  }


  const purchaseDetailsColumns = [
    {
      id: "purchaseDate",
      label: "Date",
      isEdit: false,
      isSort: true,
    },
    {
      id: "invoiceNo",
      label: "Invoice No",
      isEdit: false,
      isSort: true,
    },
    {
      id: "receiptNo",
      label: "RC No",
      isEdit: false,
      isSort: true,
    },
    {
      id: "supplier",
      label: "Supplier",
      isEdit: false,
      isSort: true,
    },
    {
      id: "groupName",
      label: "Main Category",
      isEdit: false,
      isSort: true,
    },
    {
      id: "brandName",
      label: "Brand",
      isEdit: false,
      isSort: true,
    },
    {
      id: "brandProductName",
      label: "Brand Product",
      isEdit: false,
      isSort: true,
    },
    {
      id: "quantity",
      label: "QTY",
      isEdit: false,
      isSort: true,
    },
    {
      id: "weight",
      label: "Weight",
      isEdit: false,
      isSort: true,
    },
  ];


  const selectedListColumns = [

    {
      id: "employee",
      label: "Employee",
      isEdit: false,
      isSort: true,
    },
    {
      id: "receiptNo",
      label: "RC No",
      isEdit: false,
      isSort: true,
    },
    {
      id: "groupName",
      label: "Main Category",
      isEdit: false,
      isSort: true,
    },
    {
      id: "brandName",
      label: "Brand",
      isEdit: false,
      isSort: true,
    },
    {
      id: "brandProductName",
      label: "Brand Product",
      isEdit: false,
      isSort: true,
    },
    {
      id: "quantity",
      label: "Pieces",
      isEdit: false,
      isSort: true,
    },
    {
      id: "weight",
      label: "Weight",
      isEdit: false,
      isSort: true,
    },
  ];

  const { t } = props;
  const [formData, setFromData] = useState(cloneDeep(brandedLotSearchForm));
  const [formDataLotCreation, setFormDataLotCreation] = useState(
    cloneDeep(brandedLotCreationForm)
  );
  const [selectedList, setSelectedList] = useState([]);
  const [selectedItem, setSelectedItem] = useState("");
  const [brandedLotPurchaseDetails, setBrandedLotPurchaseDetails] = useState([]);
  const [selectedItems, setSelectedItems] = useState([]);
  const [selectedItemIndexValue, setSelectedItemIndexValue] = useState(0);
  const [selectedItemToEdit, setSelectedItemToEdit] = useState("");
  const [allBrandedLotsInvoicesData, setAllBrandedLotsInvoicesData] = useState([]);

  useEffect(() => {
    let brandedLotPurchaseDetailsData = [];
    if (allBrandedLotDetails.data) {
      let i = 0;
      const dataToBind = { ...allBrandedLotDetails.data };
      dataToBind.brandedDetails.forEach(val1 => {
        i += 1;
        const item = {};
        item.invoiceNo = dataToBind.invoiceNo;
        item.isDeleted = dataToBind.isDeleted;
        item.purchaseType = dataToBind.purchaseType;
        item.partyId = dataToBind.partyId;
        item.purchaseDate = dataToBind.invoiceDate;
        item.mainGroup = val1.subProduct.brandProduct.brand.mainGroup.id;
        item.groupName = val1.subProduct.brandProduct.brand.mainGroup.groupName;
        item.brandId = val1.subProduct.brandProduct.brand.id;
        item.brandName = val1.subProduct.brandProduct.brand.brandName;
        item.brandProductId = val1.subProduct.brandProduct.id;
        item.brandProductName = val1.subProduct.brandProduct.productName;
        item.subProductId = val1.subProduct.id;
        item.subProductName = val1.subProduct.subProductName;
        item.quantity = val1.quantity - val1.createdLotQuantity;
        item.weight = `${val1.weight - val1.createdLotWeight}g`;
        item.createdBarCodeQuantity = val1.createdLotQuantity;
        item.createdBarCodeWeight = val1.createdLotWeight;
        item.id = dataToBind.id;
        item.indexVal = i;
        item.receiptNo = dataToBind.receiptNo;
        item.purchaseId = val1.id;
        item.supplier = suppliersData.filter(val => val.id === get(formData, "partyNameId.value"))[0].name;
        brandedLotPurchaseDetailsData.push(item);
      });
      setBrandedLotPurchaseDetails(brandedLotPurchaseDetailsData);
    }
  }, [allBrandedLotDetails]);

  const setFormValue = (field, value) => {
    formData[field]["value"] = value;
    setFromData({ ...formData });
  };

  const setFormValueLotCreation = (field, value) => {
    formDataLotCreation[field]["value"] = value;
    setFormDataLotCreation({ ...formDataLotCreation });
  };

  const onFilterClick = () => {
    let isFormValid = true;
    map(formData, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      formData[key] = { ...value, ...isValid };
    });
    setFromData({ ...formData });
    if (!isFormValid)
      return;
    clearForm();
    setSelectedList([]);
    setSelectedItem("");
    dispatch(fetchBrandedLotDetails(get(formData, "invoiceId.value")));
  }

  useEffect(() => {
    dispatch(fetchSuppliers());
    dispatch(fetchMainGroups());
    dispatch(fetchEmployeesList());
  }, [dispatch]);

  useEffect(() => {
    if (get(formData, 'partyNameId.value') !== "" && get(formData, "date.value") !== null && get(formData, "purchaseType.value") !== "") {
      dispatch(fetchBrandedLotInvoices(get(formData, 'partyNameId.value'), get(formData, "purchaseType.value"), get(formData, "date.value")));
    }
  }, [get(formData, 'partyNameId.value'), get(formData, "date.value"), get(formData, "purchaseType.value")]);

  useEffect(() => {
    dispatch(fetchBrandsByMainGroup(get(formDataLotCreation, 'mainGroupId.value')));
  }, [get(formDataLotCreation, 'mainGroupId.value')])

  useEffect(() => {
    dispatch(fetchBrandProductsByBrand(get(formDataLotCreation, 'brandId.value')));
  }, [get(formDataLotCreation, 'brandId.value')])

  useEffect(() => {
    dispatch(fetchSubProductsByProduct(get(formDataLotCreation, 'productId.value')));
  }, [get(formDataLotCreation, 'productId.value')])

  const onSelectChangeSearch = (e) => {
    setFormValue(e.nativeEvent.target.name, e.target.value);
  };

  const onSelectChange = (e) => {
    setFormValueLotCreation(e.nativeEvent.target.name, e.target.value);
  };

  const onInvoiceChange = (e) => {
    setFormValue("invoiceId", e.value);
  }

  const onChangePurchaseItem = (item) => {
    setSelectedItemToEdit("");
    const selectedItem = brandedLotPurchaseDetails.filter(val => val.indexVal === item)[0];
    setSelectedItem(selectedItem);
    formDataLotCreation["mainGroupId"]["value"] = selectedItem.mainGroup;
    formDataLotCreation["brandId"]["value"] = selectedItem.brandId;
    formDataLotCreation["productId"]["value"] = selectedItem.brandProductId;
    formDataLotCreation["subProductId"]["value"] = selectedItem.subProductId;
    formDataLotCreation["qty"]["value"] = selectedItem.quantity;
    formDataLotCreation["qty"]["maxValue"] = selectedItem.quantity;
    formDataLotCreation["qty"]["minValue"] = "0";
    formDataLotCreation["weight"]["value"] = selectedItem.weight.replace('g', '');
    formDataLotCreation["weight"]["maxValue"] = selectedItem.weight.replace('g', '');
    formDataLotCreation["weight"]["minValue"] = "0";
    setFormDataLotCreation({ ...formDataLotCreation });
  };

  const clearForm = () => {
    formDataLotCreation["mainGroupId"]["value"] = "";
    formDataLotCreation["brandId"]["value"] = "";
    formDataLotCreation["productId"]["value"] = "";
    formDataLotCreation["subProductId"]["value"] = "";
    formDataLotCreation["qty"]["value"] = "";
    formDataLotCreation["weight"]["value"] = "";
    formDataLotCreation["remarks"]["value"] = "";
    formDataLotCreation["employeeId"]["value"] = "";
    setSelectedItem("");
    setSelectedItemToEdit("");
    setFormDataLotCreation({ ...formDataLotCreation });
  }

  const addToList = () => {
    let isFormValid = true;
    map(formDataLotCreation, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      formDataLotCreation[key] = { ...value, ...isValid };
    });
    setFormDataLotCreation({ ...formDataLotCreation });
    if (!isFormValid)
      return;
    const dataToChange = [...brandedLotPurchaseDetails];
    if (selectedItemToEdit === "") {
      const itemSelected = JSON.parse(JSON.stringify(selectedItem));
      const originalQty = parseInt(itemSelected.quantity) ? parseInt(itemSelected.quantity) : 0;
      const originalWeight = parseInt(itemSelected.weight) ? parseInt(itemSelected.weight.replace("g", "")) : 0;
      if (itemSelected.rowColorStyle)
        delete itemSelected.rowColorStyle;
      if (itemSelected.isRadioDisable)
        delete itemSelected.isRadioDisable;
      if (get(formDataLotCreation, "qty.value") !== "" && get(formDataLotCreation, "weight.value") !== "" && parseInt(get(formDataLotCreation, "qty.value")) <= originalQty && parseInt(get(formDataLotCreation, "weight.value")) <= originalWeight) {
        itemSelected.quantity = get(formDataLotCreation, "qty.value");
        itemSelected.weight = `${get(formDataLotCreation, "weight.value")}g`;
        itemSelected.employee = employeesData.filter(val => val.id === get(formDataLotCreation, "employeeId.value")).length > 0 ? employeesData.filter(val => val.id === get(formDataLotCreation, "employeeId.value"))[0].name : "";
        itemSelected.employeeId = get(formDataLotCreation, "employeeId.value");
        itemSelected.remarks = get(formDataLotCreation, "remarks.value");
        itemSelected.selectedItemIndex = selectedItemIndexValue + 1;
        setSelectedItemIndexValue(selectedItemIndexValue + 1);
        dataToChange.forEach(item => {
          if (item.indexVal === itemSelected.indexVal) {
            item.quantity = originalQty - parseInt(get(formDataLotCreation, "qty.value"));
            item.weight = `${originalWeight - parseInt(get(formDataLotCreation, "weight.value"))}g`;
            formDataLotCreation["weight"]["maxValue"] = originalWeight - parseInt(get(formDataLotCreation, "weight.value"));
            formDataLotCreation["qty"]["maxValue"] = originalQty - parseInt(get(formDataLotCreation, "qty.value"));
            if (originalQty - parseInt(get(formDataLotCreation, "qty.value")) === 0 || originalWeight - parseInt(get(formDataLotCreation, "weight.value")) === 0) {
              item.rowColorStyle = "redColor";
              item.isRadioDisable = true;
            }
            else {
              item.rowColorStyle = "orangeColor";
              item.isRadioDisable = true;
            }
          }
        });
        setSelectedList([...selectedList, itemSelected]);
        setBrandedLotPurchaseDetails(dataToChange);
        formDataLotCreation["qty"]["value"] = "";
        formDataLotCreation["weight"]["value"] = "";
        formDataLotCreation["remarks"]["value"] = "";
        formDataLotCreation["employeeId"]["value"] = "";
        setSelectedItemToEdit("");
      }
    }
    else {
      const itemSelected = selectedList.filter(val => val.selectedItemIndex === selectedItemToEdit)[0];
      const initialQty = parseInt(dataToChange.filter(val => val.indexVal === itemSelected.indexVal)[0].quantity);
      const initialWeight = parseInt(dataToChange.filter(val => val.indexVal === itemSelected.indexVal)[0].weight.replace("g", ""));
      const originalQty = parseInt(itemSelected.quantity) ? parseInt(itemSelected.quantity) : 0;
      const originalWeight = parseInt(itemSelected.weight) ? parseInt(itemSelected.weight.replace("g", "")) : 0;
      const currentQty = get(formDataLotCreation, "qty.value") !== "" ? parseInt(get(formDataLotCreation, "qty.value")) : 0;
      const currentWeight = get(formDataLotCreation, "weight.value") !== "" ? parseInt(get(formDataLotCreation, "weight.value")) : 0;
      const weightDeviation = currentWeight - originalWeight;
      const qtyDeviation = currentQty - originalQty;
      const itemsList = [...selectedList];
      if (currentQty !== 0 && currentWeight !== 0 && initialQty - qtyDeviation >= 0 && initialWeight - weightDeviation >= 0) {
        itemsList.forEach(item => {
          if (item.selectedItemIndex === itemSelected.selectedItemIndex) {
            item.quantity = get(formDataLotCreation, "qty.value");
            item.weight = `${get(formDataLotCreation, "weight.value")}g`;
            item.employee = employeesData.filter(val => val.id === get(formDataLotCreation, "employeeId.value")).length > 0 ? employeesData.filter(val => val.id === get(formDataLotCreation, "employeeId.value"))[0].name : "";
            item.employeeId = get(formDataLotCreation, "employeeId.value");
            item.remarks = get(formDataLotCreation, "remarks.value");
          }
        });
        dataToChange.forEach(item => {
          if (item.indexVal === itemSelected.indexVal) {
            item.quantity = initialQty - qtyDeviation;
            item.weight = `${initialWeight - weightDeviation}g`;
            formDataLotCreation["weight"]["maxValue"] = initialWeight - weightDeviation;
            formDataLotCreation["qty"]["maxValue"] = initialQty - qtyDeviation;
            if (initialQty - qtyDeviation === 0 || initialWeight - weightDeviation === 0) {
              item.rowColorStyle = "redColor";
              item.isRadioDisable = true;
            }
            else {
              item.rowColorStyle = "orangeColor";
              item.isRadioDisable = true;
            }
          }
        });
        setSelectedList(itemsList);
        setBrandedLotPurchaseDetails(dataToChange);
        formDataLotCreation["qty"]["value"] = "";
        formDataLotCreation["weight"]["value"] = "";
        formDataLotCreation["remarks"]["value"] = "";
        formDataLotCreation["employeeId"]["value"] = "";
        setSelectedItemToEdit("");
      }
    }
  };

  const onItemsChange = (items) => {
    setSelectedItems(items);
  };

  const getUpdatedQty = (val, id, items) => {
    let changedQty = parseInt(val);
    items.filter(value => value.indexVal === id).forEach(item => {
      changedQty += parseInt(item.quantity);
    })
    return changedQty;
  };

  const getUpdatedWeight = (val, id, items) => {
    let changedWeight = parseInt(val.replace('g', ''));
    items.filter(value => value.indexVal === id).forEach(item => {
      changedWeight += parseInt(item.weight.replace('g', ''));
    })
    return `${changedWeight}g`;
  };

  const deleteSelectedItems = () => {
    const selectedDataToRemove = selectedList.filter(val => selectedItems.indexOf(val.selectedItemIndex) >= 0);
    const dataToChange = [...brandedLotPurchaseDetails];
    const filteredData = selectedList.filter(val => selectedItems.indexOf(val.selectedItemIndex) < 0);
    dataToChange.forEach(item => {
      item.quantity = getUpdatedQty(item.quantity, item.indexVal, selectedDataToRemove);
      item.weight = getUpdatedWeight(item.weight, item.indexVal, selectedDataToRemove);
      formDataLotCreation["weight"]["maxValue"] = getUpdatedWeight(item.weight, item.indexVal, selectedDataToRemove).replace('g', '');
      formDataLotCreation["qty"]["maxValue"] = getUpdatedQty(item.quantity, item.indexVal, selectedDataToRemove);
      setFormDataLotCreation({ ...formDataLotCreation });
      if (item.quantity === 0 || parseInt(item.weight.replace('g', '')) === 0) {
        item.rowColorStyle = "redColor";
        item.isRadioDisable = true;
      }
      else if (item.quantity !== 0 && parseInt(item.weight.replace('g', '')) !== 0) {
        if (filteredData.filter(val => val.indexVal === item.indexVal).length > 0) {
          item.rowColorStyle = "orangeColor";
          if (item.isRadioDisable)
            delete item.isRadioDisable;
        }
        else {
          if (item.rowColorStyle)
            delete item.rowColorStyle;
          if (item.isRadioDisable)
            delete item.isRadioDisable;
        }
      }
    });
    setBrandedLotPurchaseDetails(dataToChange);
    setSelectedList(filteredData);
    setSelectedItems([]);
    clearForm();
  };

  const onEditClickList = (val) => {
    setSelectedItemToEdit(val);
    const selectedItem = selectedList.filter(item => item.selectedItemIndex === val)[0];
    formDataLotCreation["mainGroupId"]["value"] = selectedItem.mainGroup;
    formDataLotCreation["brandId"]["value"] = selectedItem.brandId;
    formDataLotCreation["productId"]["value"] = selectedItem.brandProductId;
    formDataLotCreation["subProductId"]["value"] = selectedItem.subProductId;
    formDataLotCreation["qty"]["value"] = selectedItem.quantity;
    formDataLotCreation["weight"]["value"] = selectedItem.weight.replace('g', '');
    formDataLotCreation["employeeId"]["value"] = selectedItem.employeeId;
    formDataLotCreation["remarks"]["value"] = selectedItem.remarks;
    setFormDataLotCreation({ ...formDataLotCreation });
  }

  const onSaveClick = () => {
    const dataToInsert = [...selectedList];
    dataToInsert.forEach(val => {
      val.weight = parseFloat(val.weight.replace("g", ""));
    })
    dispatch(saveBrandedLot(dataToInsert));
  };

  const onClearClick = () => {
    clearForm();
    setSelectedList([]);
    setBrandedLotPurchaseDetails([]);
    formData["purchaseType"]["value"] = "";
    formData["partyNameId"]["value"] = "";
    formData["date"]["value"] = null;
    formData["invoiceId"]["value"] = "";
    setAllBrandedLotsInvoicesData([]);
  }

  useEffect(() => {
    if (brandedLotInfo && brandedLotInfo.status && brandedLotInfo.status.toLowerCase() === "success") {
      onClearClick();
    }
  }, [brandedLotInfo]);

  useEffect(() => {
    if (allBrandedLotsInvoices.data) {
      setAllBrandedLotsInvoicesData(allBrandedLotsInvoices.data.map((invc) => {
        return {
          value: invc.recieptNo,
          label: invc.invoiceNo === null ? invc.recieptNo : invc.recieptNo + " - " + invc.invoiceNo,
        }
      }));
    }
  }, [allBrandedLotsInvoices]);

  return (
    <div className="panel panel-default hero-panel">
      <div className="panel-heading">
        <h1 className="panel-title">
          {t("transactions.brandedLot")} {t("common.creation")}
        </h1>
      </div>
      <div className="panel-body">
        <div className="panel-body p-t-0">
          <div class="bg-light p-15 p-b-0">
            <div className="row">
              <div className="col-md-3">
                <label for="">Purchase Type</label>
                <div className="form-group no-min-height">
                  <div className="radio radio-inline radio-primary m-b-0">
                    <input
                      type="radio"
                      name="purchaseType"
                      value="Approval"
                      checked={
                        get(formData, "purchaseType.value") === "Approval"
                      }
                      onChange={(e) => setFormValue("purchaseType", "Approval")}
                    />
                    <label htmlFor="">{t("transactions.approval")}</label>
                  </div>
                  <div className="radio radio-inline radio-primary m-b-0">
                    <input
                      type="radio"
                      name="purchaseType"
                      value="Invoice"
                      checked={
                        get(formData, "purchaseType.value") === "Invoice"
                      }
                      onChange={(e) => setFormValue("purchaseType", "Invoice")}
                    />
                    <label htmlFor="">{t("transactions.invoice")}</label>
                  </div>
                  <div className="invalid-feedback">
                    {get(formData, "purchaseType.error")}
                  </div>
                </div>
              </div>
              <div className="form-group col-md-2">
                <label htmlFor="">{t("common.supplier")}</label>
                <Select
                  name="partyNameId"
                  value={get(formData, "partyNameId.value")}
                  onChange={(e) => onSelectChangeSearch(e)}
                  data={suppliersData}
                  placeholder={t("common.supplier")}
                />
                <div className="invalid-feedback">
                  {get(formData, "partyNameId.error")}
                </div>
              </div>
              <div className="form-group col-md-4">
                <label htmlFor="">{t("common.date")}</label>
                <DatePicker
                  value={get(formData, "date.value")}
                  onChange={(value) => setFormValue("date", value)}
                />
                <div className="invalid-feedback">
                  {get(formData, "date.error")}
                </div>
              </div>
              <div className="form-group col-md-2">
                <label htmlFor="">
                  {t("transactions.invoice")} / {t("transactions.rcNo")}
                </label>
                <SearchSelect options={allBrandedLotsInvoicesData} placeholder="Option"
                  name="invoiceId"
                  value={allBrandedLotsInvoicesData.filter(val => val.value === get(formData, "invoiceId.value"))}
                  onChange={(e) => onInvoiceChange(e)}
                  name="purchaseType" />
                <div className="invalid-feedback">
                  {get(formData, "invoiceId.error")}
                </div>
              </div>
              <div className="form-group col-md-1">
                <button onClick={onFilterClick} class="btn btn-primary btn-sm m-t-22">Filter</button>
              </div>
            </div>
          </div>
          <div class="border p-15 m-b-20">
            <h3 class="panel-title">
              {t("transactions.purchase")} {t("common.details")}
            </h3>
            <CustomGrid
              gridData={brandedLotPurchaseDetails}
              columns={purchaseDetailsColumns}
              isRadio={true}
              isCheckbox={false}
              uniqueId="indexVal"
              sortColumn="receiptNo"
              isEdit={false}
              isDelete={false}
              defaultRowsPerPage={5}
              fnOnChangeRadioItem={onChangePurchaseItem}
            />
          </div>
          <div class="">
            <div class="">
              <h4 class="panel-title">{t("common.details")}</h4>
            </div>
            <div className="row">
              <div className="col-md-3">
                <label htmlFor="">{t("transactions.mainGroup")}</label>
                <Select
                  name="mainGroupId"
                  value={get(formDataLotCreation, "mainGroupId.value")}
                  onChange={(e) => onSelectChange(e)}
                  data={mainGroupData}
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "mainGroupId.error")}
                </div>
              </div>
              <div className="col-md-3">
                <label htmlFor="">{t("transactions.brand")}
                  <a
                    href="#"
                    id="brand"
                    data-toggle="modal"
                  >
                    <i className="zmdi zmdi-plus-circle f-s-16"></i>
                  </a>
                </label>
                <Select
                  name="brandId"
                  value={get(formDataLotCreation, "brandId.value")}
                  onChange={(e) => onSelectChange(e)}
                  data={brandData}
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "brandId.error")}
                </div>
              </div>
              <div className="col-md-3">
                <label htmlFor="">{t("transactions.brandProduct")}
                  <a
                    href="#"
                    id="brand"
                    data-toggle="modal"
                  >
                    <i className="zmdi zmdi-plus-circle f-s-16"></i>
                  </a>
                </label>
                <Select
                  name="productId"
                  value={get(formDataLotCreation, "productId.value")}
                  onChange={(e) => onSelectChange(e)}
                  data={brandProductsData}
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "productId.error")}
                </div>
              </div>
              <div className="form-group col-md-3">
                <label htmlFor="">{t("transactions.subProduct")}
                  <a
                    href="#"
                    id="brand"
                    data-toggle="modal"
                  >
                    <i className="zmdi zmdi-plus-circle f-s-16"></i>
                  </a>
                </label>
                <Select
                  name="subProductId"
                  value={get(formDataLotCreation, "subProductId.value")}
                  onChange={(e) => onSelectChange(e)}
                  data={brandSubProductsData}
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "subProductId.error")}
                </div>
              </div>
              <div className="form-group col-md-2">
                <label htmlFor="">{t("transactions.qty")}</label>
                <input
                  type="number"
                  className="form-control"
                  placeholder="Qty"
                  name="qty"
                  value={get(formDataLotCreation, "qty.value")}
                  onChange={(e) =>
                    setFormValueLotCreation("qty", e.target.value)
                  }
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "qty.error")}
                </div>
              </div>
              <div className="form-group col-md-3">
                <label htmlFor="">
                  {t("transactions.weight")}
                </label>
                <input
                  type="number"
                  className="form-control"
                  placeholder="Weight"
                  name="weight"
                  value={get(formDataLotCreation, "weight.value")}
                  onChange={(e) =>
                    setFormValueLotCreation("weight", e.target.value)
                  }
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "weight.error")}
                </div>
              </div>

              <div className="col-md-3">
                <label htmlFor="">{t("common.employee")}</label>
                <Select
                  name="employeeId"
                  value={get(formDataLotCreation, "employeeId.value")}
                  onChange={(e) => onSelectChange(e)}
                  data={employeesData}
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "employeeId.error")}
                </div>
              </div>
              <div className="col-md-2">
                <div className="form-group">
                  <label htmlFor="">{t("common.remarks")}</label>
                  <textarea
                    type="text"
                    className="form-control"
                    placeholder="Remarks"
                    style={{ height: "30px" }}
                    name="remarks"
                    value={get(formDataLotCreation, "remarks.value")}
                    onChange={(e) =>
                      setFormValueLotCreation("remarks", e.target.value)
                    }
                  />
                  <div className="invalid-feedback">
                    {get(formDataLotCreation, "remarks.error")}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="panel-footer text-center">
            <a href="#" class="btn btn-primary" disabled={selectedItem === "" && selectedItemToEdit === ""} href="javascript:void(0)" onClick={addToList}>
              <i class="zmdi zmdi-playlist-plus"></i> {selectedItemToEdit === "" ? `Add to list` : `Update`}
            </a>{" "}
            <a href="javascript:void(0)" class="btn btn-default" onClick={clearForm}>
              Clear
            </a>{" "}
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Selected List</h3>
              <div class="panel-options ">
                <a href="javascript:void(0)" class="btn btn-default btn-sm" href="javascript:void(0)" onClick={deleteSelectedItems} disabled={selectedItems.length === 0}>
                  Remove
                </a>
              </div>
            </div>
            <CustomGrid
              gridData={selectedList}
              columns={selectedListColumns}
              isRadio={false}
              isCheckbox={true}
              uniqueId="selectedItemIndex"
              sortColumn="employee"
              isEdit={true}
              isDelete={false}
              defaultRowsPerPage={5}
              onCheckboxChange={onItemsChange}
              setEditId={onEditClickList}
            />
          </div>
        </div>
      </div>
      <div class="panel-footer text-center">
        {" "}
        <a href="javascript:void(0)" disabled={selectedList.length === 0} class="btn btn-primary " onClick={onSaveClick} >
          Save
        </a>{" "}
        <a href="javascript:void(0)" disabled={selectedList.length === 0} class="btn btn-default " onClick={onClearClick} >
          Clear
        </a>{" "}
      </div>
      <div
        className="modal fade"
        id="addNew"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="myModalLabel"
      >
        <StoneDetailsLot />
      </div>
    </div>
  );
};

export default withTranslation()(BrandedLot);
