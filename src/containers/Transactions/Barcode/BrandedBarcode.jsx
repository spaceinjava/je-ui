import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { brandedBarcodeForm } from '../Constants';
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Select from "../../../components/Form/Select";
import { fetchSuppliers } from '../../../redux/actions/Admin/supplierActions'
import { fetchMainGroups, fetchPuritiesByMainGroup } from '../../../redux/actions/mainGroupActions'
import { fetchBrandsByMainGroup } from '../../../redux/actions/creations/brandActions'
import { fetchBrandProductsByBrand, fetchSubProductsByProduct } from '../../../redux/actions/creations/brandProductActions'
import { saveBrandedBarcode, fetchBrandedLots } from '../../../redux/actions/transactions/barcode/brandedBarcodeActions'
import { fetchRatesByMainGroup } from '../../../redux/actions/creations/rateActions';
import Swal from 'sweetalert2'
import LoadingOverlay from 'react-loading-overlay';
import { roundToThree } from "../../../utils/util";
import SearchSelect from "../../../components/Form/SearchSelect";

export const BrandedBarcode = (props) => {
  const { t } = props;
  const [formData, setFromData] = useState(cloneDeep(brandedBarcodeForm));
  const [viewForm, setToggleView] = useState(true);
  const [lotData, setLotData] = useState([]);
  const [lotDataDdl, setLotDataDdl] = useState([]);
  const [selectedLot, setSelectedLot] = useState("");
  const dispatch = useDispatch();

  const suppliers = useSelector((state) => state.supplierReducer.allSuppliers)
  const mainGroups = useSelector((state) => state.MainGroupReducer.allGroups)
  const brands = useSelector((state) => state.brandReducer.allMainGroupBrands)
  const brandProducts = useSelector((state) => state.brandProductReducer.allBrandProductsByBrand)
  const brandSubProducts = useSelector((state) => state.brandProductReducer.allSubProductsByProduct)
  const allBrandedLots = useSelector(state => state.brandedBarcodeReducer.brandedLots);
  const rates = useSelector((state) => state.rateReducer.ratesByMainGroup)
  const loader = useSelector((state) => state.supplierReducer.loading);
  const purities = useSelector(
    (state) => state.MainGroupReducer.allPuritiesByMainGroup
  );

  const [image, setImage] = useState({
    preview: "",
    raw: ""
  })

  const suppliersData = suppliers.data ? suppliers.data.map((supplier) => {
    return {
      id: supplier.id,
      name: supplier.partyName
    }
  }) : [];
  const mainGroupData = mainGroups.data ? mainGroups.data.map((group) => {
    return {
      id: group.id,
      name: group.groupName,
      shortCode: group.shortCode,
    }
  }) : [];
  const brandData = brands.data ? brands.data.map((brand) => {
    return {
      id: brand.id,
      name: brand.brandName,
      shortCode: brand.shortCode,
    }
  }) : [];
  const brandProductsData = brandProducts.data ? brandProducts.data.map((product) => {
    return {
      id: product.id,
      name: product.productName,
      shortCode: product.shortCode,
    }
  }) : [];
  const brandSubProductsData = brandSubProducts.data ? brandSubProducts.data.map((subProduct) => {
    return {
      id: subProduct.id,
      name: subProduct.subProductName,
      shortCode: subProduct.shortCode,
    }
  }) : [];

  const puritiesData = purities.data
    ? purities.data.map((purity) => {
      return {
        id: purity.id,
        name: purity.purity,
      };
    })
    : [];

  const allMainGroupRatesData = rates.data ? rates.data.map((rate) => {
    return {
      id: rate.id,
      mainGroupId: rate.mainGroup.id,
      perGramRate: rate.perGramRate,
      purityId: rate.purity.id,
      purityRate: rate.purityRate,
    }
  }) : [];

  const setFormValue = (field, value) => {
    formData[field]['value'] = value;
    setFromData({ ...formData });
  }

  const resetForm = () => {
    map(formData, function (value, key, object) {
      if (formData[key]["type"] === 'number') {
        formData[key]["value"] = 0;
      } else {
        formData[key]["value"] = '';
      }
    });
    setImage({
      preview: "",
      raw: ""
    });
    setFromData({ ...formData });
    setLotData([]);
    setLotDataDdl([]);
    setSelectedLot("");
  }
  const saveForm = () => {
    let isFormValid = true;
    map(formData, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      formData[key] = { ...value, ...isValid };
    });
    let pendingQuantity = selectedLot.quantity - selectedLot.createdBarCodeQuantity;
    let pendingWeight = selectedLot.weight - selectedLot.createdBarCodeWeight;
    if (formData.quantity.value == pendingQuantity && formData.weight.value != pendingWeight) {
      isFormValid = false;
      formData.weight.error = "Weight must be \"" + pendingWeight + "\", as full quantity \"" + pendingQuantity + "\" is given";
    }
    if (formData.weight.value == pendingWeight && formData.quantity.value != pendingQuantity) {
      isFormValid = false;
      formData.quantity.error = "Quantity must be \"" + pendingQuantity + "\", as full weight \"" + pendingWeight + "\" is given";
    }
    if (formData.rate.value <= 0) {
      isFormValid = false;
      formData.rate.error = "Rate must greater than 0";
    }
    if (isFormValid) {
      let data = {};
      map(formData, function (value, key, object) {
        data = {
          ...data,
          [key]: value.value,
        };
      });
      data.image = image.raw;
      console.log("saving branded barcode with data : ", data);
      Swal.fire({
        title: 'Confirm Saving Barcode Info.!',
        text: 'Continue to save / Cancel to stop',
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Continue'
      }).then((result) => {
        if (result.isConfirmed) {
          dispatch(saveBrandedBarcode(data))
            .then(() => {
              // resetForm();
            })
        }
      })
    }
  }

  const validateField = (fieldObj) => {
    let errorObj = { isValid: true, error: null }
    if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
    else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
    else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }
    else if (fieldObj.value && fieldObj.maxValue && (fieldObj.value > fieldObj.maxValue)) {
      errorObj.isValid = false;
      errorObj.error = `Should be less than or equal to ${fieldObj.maxValue}`;
    }
    return errorObj;
  }

  useEffect(() => {
    dispatch(fetchSuppliers());
    dispatch(fetchMainGroups());
  }, [])

  useEffect(() => {
    dispatch(fetchBrandsByMainGroup(get(formData, 'mainGroup.value')));
    dispatch(fetchPuritiesByMainGroup(get(formData, "mainGroup.value")));
    dispatch(fetchRatesByMainGroup(get(formData, "mainGroup.value")));
  }, [get(formData, 'mainGroup.value')])

  useEffect(() => {
    dispatch(fetchBrandProductsByBrand(get(formData, 'brand.value')));
  }, [get(formData, 'brand.value')])

  useEffect(() => {
    dispatch(fetchSubProductsByProduct(get(formData, 'product.value')));
  }, [get(formData, 'product.value')])

  useEffect(() => {
    let selectedPurity = get(formData, 'purity.value');
    let selectedMainGroup = get(formData, 'mainGroup.value');
    if (selectedMainGroup != "" && selectedPurity != "") {
      if (allMainGroupRatesData.length > 0) {
        let filteredRateObject = allMainGroupRatesData.filter((ele) => (ele.mainGroupId === selectedMainGroup) && (ele.purityId === selectedPurity))[0];
        if (filteredRateObject) {
          setFormValue('rate', filteredRateObject.purityRate);
        } else {
          Swal.fire({
            position: "top-end",
            title: 'No Rates for the selected purity.!',
            text: 'Please define rates before going forward',
            icon: 'info',
            showConfirmButton: false,
            timer: 2000
          })
        }
      } else {
        Swal.fire({
          position: "top-end",
          title: 'No Rates defined for selected mainGroup.!',
          text: 'Please define rates before generating barcode',
          icon: 'info',
          showConfirmButton: false,
          timer: 2000
        })
      }
    }
  }, [get(formData, 'purity.value')])

  useEffect(() => {
    dispatch(fetchBrandedLots(get(formData, "mainGroup.value"), get(formData, "party.value"), get(formData, "lotDate.value")));
  }, [get(formData, "mainGroup.value"), get(formData, "party.value"), get(formData, "lotDate.value")])

  useEffect(() => {
    if (allBrandedLots && allBrandedLots.status && allBrandedLots.status.toLowerCase() === "success") {
      const data = [...allBrandedLots.data];
      setLotData(data);
      const ddlData = [];
      data.forEach(item => {
        ddlData.push({ label: item.lotNo, value: item.lotNo });
      });
      setLotDataDdl(ddlData);
      // setFormValue("lotDate", "");
    }
  }, [allBrandedLots]);

  useEffect(() => {
    if (lotData.length > 0) {
      const dataSelected = lotData.filter(val => val.lotNo === get(formData, "lotNo.value"))[0];
      setSelectedLot(dataSelected);
      setFormValue("lotDate", dataSelected.purchaseDate);
      setFormValue("party", dataSelected.partyId);
      let mainGroup = dataSelected.brandSubProduct.brandProduct.brand.mainGroup;
      setFormValue("mainGroup", mainGroup.id);
      setFormValue("quantity", 0);
      setFormValue("weight", 0);
    }
  }, [get(formData, "lotNo.value")]);

  useEffect(() => {
    if (selectedLot) {
      let quantity = get(formData, 'quantity.value');
      let weight = get(formData, 'weight.value');
      let pendingQuantity = selectedLot.quantity - selectedLot.createdBarCodeQuantity;
      let pendingWeight = selectedLot.weight - selectedLot.createdBarCodeWeight;
      formData.quantity['maxValue'] = selectedLot.quantity - selectedLot.createdBarCodeQuantity;
      if (quantity > pendingQuantity) {
        formData.quantity.isValid = false;
        formData.quantity.error = 'Quantity can\'t exceed: ' + pendingQuantity;
      } else if (quantity == pendingQuantity) {
        formData.quantity.isValid = true;
        formData.quantity.error = null;
        formData.weight.value = pendingWeight;
      }
      setFromData({ ...formData })
    }
  }, [get(formData, 'quantity.value')])

  useEffect(() => {
    if (selectedLot) {
      let weight = get(formData, 'weight.value');
      let pendingQuantity = selectedLot.quantity - selectedLot.createdBarCodeQuantity;
      let pendingWeight = selectedLot.weight - selectedLot.createdBarCodeWeight;
      formData.weight['maxValue'] = selectedLot.weight - selectedLot.createdBarCodeWeight;
      if (weight > pendingWeight) {
        formData.weight.isValid = false;
        formData.weight.error = 'Weight can\'t exceed: ' + pendingWeight;
      } else if (weight == pendingWeight) {
        formData.weight.isValid = false;
        formData.weight.error = null;
        formData.quantity.value = pendingQuantity;
      }
      setFromData({ ...formData })
    }
  }, [get(formData, 'weight.value')])

  useEffect(() => {
    let quantity = get(formData, 'quantity.value');
    let weight = get(formData, 'weight.value');
    let rate = get(formData, 'rate.value');
    let amount = 0;
    amount = roundToThree(quantity * rate);
    setFormValue("amount", amount);
  }, [get(formData, 'quantity.value'), get(formData, 'weight.value'), get(formData, 'rate.value')])

  const toggleView = () => {
    const formNode = document.getElementById('form');
    formNode.classList.toggle('collapse-panel');
    setToggleView(!viewForm);
  }

  const partyNameChange = (e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    setFormValue('partyId', e.target.value)
    setFormValue('party', label)
  }

  const selectOnChange = (idKey, labelKey, e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    setFormValue(idKey, e.target.value)
    setFormValue(labelKey, label)
  }

  const handleChange = (e) => {
    if (e.target.files.length) {
      setImage({
        preview: URL.createObjectURL(e.target.files[0]),
        raw: e.target.files[0]
      })
    }
  }

  return (
    <div className="panel panel-default hero-panel">
      <LoadingOverlay
        active={loader}
        spinner
        text='Loading...'
      >
        <div className="panel-heading">
          <h1 className="panel-title">{t("transactions.brandedPurchase")}</h1>
          <div className="panel-options">
            <a
              href="#"
              className="text-info collapseBtn"
              title="Show Add Form"
            >
              {viewForm ?
                <i className="zmdi zmdi-minus-circle  zmdi-hc-2x" onClick={() => { toggleView() }}>
                </i> :
                <i className="zmdi zmdi-plus-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                </i>}
            </a>
          </div>
        </div>
        <div className="panel-body">
          <div id="form">
            <div className="panel update-panel form-pnel-container">
              {/* <div className="text-danger">* {t("common.requiredFieldsMsg")} </div> */}
              <div className="panel-body">
                <div className="row">
                  <div className="col-md-3">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("creations.mainGroup")}<span className="asterisk">*</span>
                      </label>
                      <Select name="mainGroup" value={get(formData, 'mainGroup.value')} onChange={e => setFormValue('mainGroup', e.target.value)} data={mainGroupData} />
                      <div className="invalid-feedback">{get(formData, 'mainGroup.error')}</div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label htmlFor="">
                        {t("common.supplier")} / {t("common.Party")}<span className="asterisk">*</span>
                      </label>
                      <Select name="party" value={get(formData, 'party.value')} onChange={(e) => setFormValue('party', e.target.value)} data={suppliersData} />
                      <div className="invalid-feedback">{get(formData, 'party.error')}</div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label htmlFor="">
                        {t("transactions.lotDate")}<span className="asterisk">*</span>
                      </label>
                      <input type="date"
                        className="form-control"
                        placeholder=""
                        name="lotDate"
                        value={get(formData, 'lotDate.value')}
                        onChange={(e) => setFormValue('lotDate', e.target.value)} />
                      <div className="invalid-feedback">{get(formData, 'lotDate.error')}</div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label htmlFor="">
                        {t("transactions.lotNo")}<span className="asterisk">*</span>
                      </label>
                      <SearchSelect options={lotDataDdl} placeholder="Option"
                        name="lotNo"
                        value={lotDataDdl.filter(val => val.value === get(formData, "lotNo.value"))}
                        onChange={(e) => setFormValue('lotNo', e.value)}
                        name="lotNo" />
                      {/* <Select name="lotNo" value={get(formData, 'lotNo.value')} onChange={e => setFormValue('lotNo', e.target.value)} data={mainGroupData} /> */}
                      <div className="invalid-feedback">{get(formData, 'lotNo.error')}</div>
                    </div>
                  </div>
                </div>
                <div class="m-t-10 m-b-30 text-center">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="bg-light">
                        <div class="panel-heading p-l-0">
                          <h4 class="panel-title">{t("common.actual")} {t("common.details")}</h4>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6 m-b-0">
                            <label class="">{t("common.quantity")}</label>
                            <p class="m-b-0 text-info">{selectedLot !== "" ? `${selectedLot.quantity}` : `--`}</p>
                          </div>
                          <div class="form-group col-md-6 m-b-0">
                            <label for="">{t("common.Weight")}</label>
                            <p class="m-b-0 text-info">{selectedLot !== "" ? `${selectedLot.weight} gms` : `--`}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="bg-light">
                        <div class="panel-heading p-l-0">
                          <h4 class="panel-title">{t("common.pending")} {t("common.details")}</h4>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6 m-b-0">
                            <label class="">{t("common.quantity")}</label>
                            <p class="m-b-0 text-info">{selectedLot !== "" ? selectedLot.quantity - selectedLot.createdBarCodeQuantity : `--`}</p>
                          </div>
                          <div class="form-group col-md-6 m-b-0">
                            <label for="">{t("common.Weight")}</label>
                            <p class="m-b-0 text-info">{selectedLot !== "" ? `${selectedLot.weight - selectedLot.createdBarCodeWeight} gms` : `--`}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-8">
                    <div className="row">
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.brand")}<span className="asterisk">*</span>
                            <a href="" className="" data-toggle="modal" data-target="#addProduct" data-backdrop="static" data-keyboard="false"><i className="zmdi zmdi-plus-circle f-s-16"></i></a>
                          </label>
                          <Select name="brand" value={get(formData, 'brand.value')} onChange={(e) => setFormValue('brand', e.target.value)} data={brandData} />
                          <div className="invalid-feedback">{get(formData, 'brand.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("transactions.brandProduct")}<span className="asterisk">*</span>
                            <a href="" className="" data-toggle="modal" data-target="#addProduct" data-backdrop="static" data-keyboard="false"><i className="zmdi zmdi-plus-circle f-s-16"></i></a>
                          </label>
                          <Select name="product" value={get(formData, 'product.value')} onChange={(e) => setFormValue('product', e.target.value)} data={brandProductsData} />
                          <div className="invalid-feedback">{get(formData, 'product.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("creations.subProduct")}<span className="asterisk">*</span>
                            <a href="" className="" data-toggle="modal" data-target="#addProduct" data-backdrop="static" data-keyboard="false"><i className="zmdi zmdi-plus-circle f-s-16"></i></a>
                          </label>
                          <Select name="subProduct" value={get(formData, 'subProduct.value')} onChange={(e) => setFormValue('subProduct', e.target.value)} data={brandSubProductsData} />
                          <div className="invalid-feedback">{get(formData, 'subProduct.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.Purity")}<span className="asterisk">*</span>
                            <a href="" className="" data-toggle="modal" data-target="#addProduct" data-backdrop="static" data-keyboard="false"><i className="zmdi zmdi-plus-circle f-s-16"></i></a>
                          </label>
                          <Select name="purity" value={get(formData, 'purity.value')} onChange={(e) => setFormValue('purity', e.target.value)} data={puritiesData} />
                          <div className="invalid-feedback">{get(formData, 'purity.error')}</div>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.quantity")}<span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="quantity"
                            name="quantity"
                            value={get(formData, 'quantity.value')}
                            onChange={(e) => setFormValue('quantity', e.target.value)}
                          />
                          <div className="invalid-feedback">{get(formData, 'quantity.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("transactions.weight")}<span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="weight"
                            name="weight"
                            value={get(formData, 'weight.value')}
                            onChange={(e) => setFormValue('weight', e.target.value)}
                          />
                          <div className="invalid-feedback">{get(formData, 'weight.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.rate")}<span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="rate"
                            name="rate"
                            value={get(formData, 'rate.value')}
                            onChange={(e) => setFormValue('rate', e.target.value)}
                            disabled={get(formData, 'rate.disabled')}
                          />
                          <div className="invalid-feedback">{get(formData, 'rate.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.amount")}
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="amount"
                            name="amount"
                            value={get(formData, 'amount.value')}
                            onChange={(e) => setFormValue('amount', e.target.value)}
                            disabled={get(formData, 'amount.disabled')}
                          />
                          <div className="invalid-feedback">{get(formData, 'amount.error')}</div>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("common.remarks")}
                          </label>
                          <textarea
                            type="text"
                            className="form-control"
                            name="remarks"
                            style={{ height: '30px' }}
                            value={get(formData, 'remarks.value')}
                            onChange={(e) => setFormValue('remarks', e.target.value)}
                          />
                          <div className="invalid-feedback">{get(formData, 'remarks.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("transactions.labelType")}
                            <span className="asterisk">*</span>
                          </label>
                          <select
                            className="form-control select"
                            name="labelType"
                            value={get(formData, "labelType.value")}
                            onChange={(e) =>
                              setFormValue('labelType', e.target.value)
                            }
                          >
                            <option value={""}>Select</option>
                            <option value={"barcode"}>Bar Code</option>
                            <option value={"qrcode"}>QR Code</option>
                          </select>
                          <div className="invalid-feedback">
                            {get(formData, "labelType.error")}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <label htmlFor="upload-button">
                      {image.preview ?
                        <img src={image.preview} alt="image" style={{ width: '128px', height: '128px', borderRadius: '50%' }} /> :
                        <div className="circle m-t-20">
                          <i className="zmdi zmdi-camera zmdi-hc-2x"></i>
                        </div>
                      }
                    </label>
                    <input
                      type="file"
                      id="upload-button"
                      style={{ display: 'none' }}
                      onChange={handleChange} />
                  </div>
                </div>
              </div>
              <div className="panel-footer text-center">
                <a href="#" className="btn btn-primary" onClick={saveForm}>
                  <i className="zmdi zmdi-check"></i> {t("common.save")}
                </a>
                <a href="#" className="btn btn-default" onClick={resetForm}>
                  <i className="zmdi zmdi-close"></i> {t("common.clear")}
                </a>
              </div>
            </div>
          </div>
        </div>
      </LoadingOverlay>
    </div>
  );
};

export default withTranslation()(BrandedBarcode);
