import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import SearchSelect from "../../../components/Form/SearchSelect";
import Select from "../../../components/Form/Select";
import { fetchMainGroups } from '../../../redux/actions/mainGroupActions';
import { fetchSuppliers } from '../../../redux/actions/Admin/supplierActions';
import { fetchStoneColors } from "../../../redux/actions/creations/stoneColorActions";
import { fetchStoneClarities } from "../../../redux/actions/creations/stoneClarityActions";
import { fetchStonePolishes } from "../../../redux/actions/creations/stonePolishActions";
import {
    fetchStoneGroups,
    fetchStonesByStoneGroup,
    fetchStoneSizesByStoneGroup,
} from "../../../redux/actions/creations/stoneGroupActions";
import UOMElement from "../../../components/UOMElement";
import { gemstoneBarcodeForm } from "../Constants";
import { UOM_TYPE, GRAM_TO_KARAT, KARAT_TO_GRAM, GST_PERCENT } from "../../../utils/commonConstants";
import { fetchGemstoneBarcodeLots, fetchGemstoneSaleRates, saveGemstoneBarcode } from "../../../redux/actions/transactions/barcode/gemstoneBarcodeActions";
import DatePicker from "../../../components/Form/DatePicker";

export const GemstoneBarcode = (props) => {
    const { t } = props;
    const [formData, setFromData] = useState(cloneDeep(gemstoneBarcodeForm));
    const [viewForm, setToggleView] = useState(true);
    let [isStoneGroupDiamond, setIsStoneGroupDiamond] = useState(false);
    const [rates, setRates] = useState([]);
    const [lotData, setLotData] = useState([]);
    const [lotDataDdl, setLotDataDdl] = useState([]);
    const [selectedLot, setSelectedLot] = useState("");
    const [image, setImage] = useState({
        preview: "",
        raw: ""
    })
    const dispatch = useDispatch();

    const mainGroups = useSelector(state => state.MainGroupReducer.allGroups)
    const suppliers = useSelector(state => state.supplierReducer.allSuppliers)
    const stoneGroups = useSelector(
        (state) => state.stoneGroupReducer.allStoneGroups
    );
    const stones = useSelector(
        (state) => state.stoneGroupReducer.allStonesByStoneGroup
    );
    const stoneColors = useSelector(
        (state) => state.stoneColorReducer.allStoneColors
    );
    const stoneClarities = useSelector(
        (state) => state.stoneClarityReducer.allStoneClarities
    );
    const stonePolishes = useSelector(
        (state) => state.stonePolishReducer.allStonePolishes
    );
    const stoneSizes = useSelector(
        (state) => state.stoneGroupReducer.allStoneSizesByStoneGroup
    );
    const allGemstoneBarcodeRates = useSelector(state => state.gemstoneBarcodeReducer.allGemstoneBarcodeRates)
    const allGemstoneBarcodeLots = useSelector(state => state.gemstoneBarcodeReducer.allGemstoneBarcodeLots);
    const gemstoneBarcodeInfo = useSelector(state => state.gemstoneBarcodeReducer.gemstoneBarcodeInfo);

    const mainGroupData = mainGroups.data ? mainGroups.data.map((group) => {
        return {
            id: group.id,
            name: group.groupName,
            shortCode: group.shortCode,
        }
    }) : [];
    const suppliersData = suppliers.data ? suppliers.data.map((supplier) => {
        return {
            id: supplier.id,
            name: supplier.partyName
        }
    }) : [];
    const stoneGroupData = stoneGroups.data
        ? stoneGroups.data.map((group) => {
            return {
                id: group.id,
                name: group.groupName,
                shortCode: group.shortCode,
                diamond: group.diamond,
            };
        })
        : [];

    const stoneData = stones.data
        ? stones.data.map((stone) => {
            return {
                id: stone.id,
                name: stone.stoneName,
                shortCode: stone.shortCode,
            };
        })
        : [];


    const stoneColorData = stoneColors.data
        ? stoneColors.data.map((color) => {
            return {
                id: color.id,
                name: color.colorName,
                shortCode: color.shortCode,
            };
        })
        : [];
    const stoneClarityData = stoneClarities.data
        ? stoneClarities.data.map((clarity) => {
            return {
                id: clarity.id,
                name: clarity.clarityName,
                shortCode: clarity.shortCode,
            };
        })
        : [];
    const stonePolishData = stonePolishes.data
        ? stonePolishes.data.map((polish) => {
            return {
                id: polish.id,
                name: polish.polishName,
                shortCode: polish.shortCode,
            };
        })
        : [];

    const stoneSizeData = stoneSizes.data
        ? stoneSizes.data.map((size) => {
            return {
                id: size.id,
                name: size.stoneSize,
                shortCode: size.shortCode,
            };
        })
        : [];

    const partyRatesData = allGemstoneBarcodeRates.data ? allGemstoneBarcodeRates.data.map((partyRate) => {

        return {
            id: partyRate.id,
            stoneSizeId: partyRate.stoneSize ? partyRate.stoneSize.id : "",
            minRate: partyRate.minRate,
            maxRate: partyRate.maxRate,
            uom: partyRate.uom,
            noWeight: partyRate.noWeight,
            fixedRate: partyRate.fixedRate
        }
    }) : [];

    const setFormValue = (field, value) => {
        setRates([]);
        formData[field]['value'] = value;
        setFromData({ ...formData });
    }
    const resetForm = () => {
        setRates([]);
        map(formData, function (value, key, object) {
            formData[key]['value'] = ''
        });
        setFromData({ ...formData });
    }

    const validateField = (fieldObj) => {
        let errorObj = { isValid: true, error: null };
        if (fieldObj.required && !fieldObj.value) {
            errorObj.isValid = false;
            errorObj.error = "Please fill this field";
        } else if (fieldObj.value && fieldObj.value.length < fieldObj.min) {
            errorObj.isValid = false;
            errorObj.error = `Please enter min length ${fieldObj.min}`;
        } else if (fieldObj.value && fieldObj.value.length > fieldObj.max) {
            errorObj.isValid = false;
            errorObj.error = `Please enter max length ${fieldObj.max}`;
        } else if (fieldObj.value && (fieldObj.value < fieldObj.minValue || fieldObj.value > fieldObj.maxValue)) {
            errorObj.isValid = false;
            errorObj.error = `Please enter value between ${fieldObj.minValue} and ${fieldObj.maxValue}`;
        }
        return errorObj;
    };

    const isStoneADiamond = (selectedStoneGroupId) => {
        let selectedStoneGroup = stoneGroupData.find(ele => ele.id === selectedStoneGroupId);
        return selectedStoneGroup ? selectedStoneGroup.diamond : false;
    }

    const toggleView = () => {
        const formNode = document.getElementById('form');
        formNode.classList.toggle('collapse-panel');
        setToggleView(!viewForm);
    }

    const onSelectChange = (e) => {
        setFormValue(e.nativeEvent.target.name, e.target.value);
    };

    const onLotChange = (e) => {
        setFormValue("lotNo", e.value);
    }

    useEffect(() => {
        formData.weightPerGram['disabled'] = false;
        formData.weightPerKarat['disabled'] = false;
        let uomValue = get(formData, 'uom.value');
        if (uomValue) {
            if (uomValue === UOM_TYPE.GRAM || uomValue === UOM_TYPE.PIECE) {
                formData.weightPerGram['disabled'] = false;
                formData.weightPerKarat['disabled'] = true;
            } else if (uomValue === UOM_TYPE.KARAT) {
                formData.weightPerKarat['disabled'] = false;
                formData.weightPerGram['disabled'] = true;
            }
        }
        let uom = get(formData, "gemstoneCategoryId.value") && get(formData, "gemstoneSubCategoryId.value") && partyRatesData.filter((ele) => ele.uom === uomValue && ele.stoneSizeId === get(formData, "stoneSizeId.value"))[0];
        if (uom) {
            setFormValue("rate", uom.maxRate);
            formData.rate['minValue'] = uom.minRate;
            formData.rate['maxValue'] = uom.maxRate;
            formData.rate['disabled'] = uom.fixedRate ? true : false;
            // setformDataData({ ...formDataData });
        } else {
            setFormValue("rate", 0)
            formData.rate['disabled'] = false;
        }
    }, [get(formData, 'uom.value')]);

    useEffect(() => {
        let quantity = get(formData, 'quantity.value');
        let weightPerGram = get(formData, 'weightPerGram.value');
        let weightPerKarat = get(formData, 'weightPerKarat.value');
        // let weight = get(formData, 'weight.value');
        let rate = get(formData, 'rate.value');
        let uom = get(formData, 'uom.value');
        let amount = 0;
        if (uom) {
            if (uom === UOM_TYPE.PIECE) {
                setFormValue("weightPerKarat", Math.round(weightPerGram * GRAM_TO_KARAT));
                amount = Math.round(quantity * rate);
            } else if (uom === UOM_TYPE.GRAM) {
                setFormValue("weightPerKarat", Math.round(weightPerGram * GRAM_TO_KARAT));
                amount = Math.round(weightPerGram * rate);
            } else if (uom === UOM_TYPE.KARAT) {
                setFormValue("weightPerGram", Math.round(weightPerKarat * KARAT_TO_GRAM));
                amount = Math.round(weightPerKarat * rate);
            }
        }
        setFormValue("amount", amount);
    }, [get(formData, 'uom.value'), get(formData, 'quantity.value'), get(formData, 'weightPerGram.value'), get(formData, 'weightPerKarat.value')])

    useEffect(() => {
        dispatch(fetchSuppliers());
        dispatch(fetchMainGroups());
        dispatch(fetchStoneGroups());
    }, [dispatch]);

    useEffect(() => {
        dispatch(fetchStonesByStoneGroup(get(formData, "gemstoneCategoryId.value")));
        setIsStoneGroupDiamond(isStoneADiamond(get(formData, "gemstoneCategoryId.value")));
        dispatch(
            fetchStoneSizesByStoneGroup(get(formData, "gemstoneCategoryId.value"))
        );
        if (get(formData, "gemstoneCategoryId.value") && isStoneADiamond(get(formData, "gemstoneCategoryId.value"))) {
            dispatch(fetchStoneColors());
            dispatch(fetchStoneClarities());
            dispatch(fetchStonePolishes());
        }
    }, [get(formData, "gemstoneCategoryId.value")]);

    const onSaveClick = () => {
        let isFormValid = true;
        map(formData, function (value, key, object) {
            const isValid = validateField(value);
            if (!isValid.isValid) {
                isFormValid = false;
            }
            formData[key] = { ...value, ...isValid };
        });
        if (!isFormValid)
            return;
        dispatch(saveGemstoneBarcode(formData, image));
    };

    const onClearClick = () => {
        map(formData, function (value, key, object) {
            formData[key]["value"] = "";
        });
        setFromData({ ...formData });
        setImage({
            preview: "",
            raw: ""
        });
        setLotData([]);
        setLotDataDdl([]);
        setSelectedLot("");
    };

    const handleChange = (e) => {
        if (e.target.files.length) {
            setImage({
                preview: URL.createObjectURL(e.target.files[0]),
                raw: e.target.files[0]
            })
        }
    }

    useEffect(() => {
        if (get(formData, "partyNameId.value") !== "" && get(formData, "mainGroupId.value") !== "")
            dispatch(
                fetchGemstoneBarcodeLots(get(formData, "partyNameId.value"), get(formData, "mainGroupId.value"), get(formData, "date.value"))
            );
    }, [get(formData, "partyNameId.value"), get(formData, "mainGroupId.value"), get(formData, "date.value")]);

    useEffect(() => {
        if (get(formData, "stoneSizeId.value") !== "" && get(formData, "gemstoneSubCategoryId.value") !== "")
            dispatch(
                fetchGemstoneSaleRates(get(formData, "gemstoneSubCategoryId.value"), get(formData, "stoneSizeId.value"))
            );
    }, [get(formData, "stoneSizeId.value"), get(formData, "gemstoneSubCategoryId.value")]);

    useEffect(() => {
        if (allGemstoneBarcodeLots && allGemstoneBarcodeLots.status && allGemstoneBarcodeLots.status.toLowerCase() === "success") {
            const data = [...allGemstoneBarcodeLots.data];
            setLotData(data);
            const ddlData = [];
            data.forEach(item => {
                ddlData.push({ label: item.lotNo, value: item.lotNo });
            });
            setLotDataDdl(ddlData);
            setFormValue("lotDate", "");
        }
    }, [allGemstoneBarcodeLots]);

    useEffect(() => {
        if (lotData.length > 0) {
            const dataSelected = lotData.filter(val => val.lotNo === get(formData, "lotNo.value"))[0];
            setSelectedLot(dataSelected);
            setFormValue("lotDate", dataSelected.purchaseDate);
            setFormValue("partyId", dataSelected.partyId);
        }
    }, [get(formData, "lotNo.value")]);

    useEffect(() => {
        if (gemstoneBarcodeInfo && gemstoneBarcodeInfo.status && gemstoneBarcodeInfo.status.toLowerCase() === "success") {
            onClearClick();
        }
    }, [gemstoneBarcodeInfo]);

    return (
        <div className="panel panel-default hero-panel">
            <div className="panel-heading">
                <h1 className="panel-title">Gemstone Barcode </h1>
                <div className="panel-options">
                    <a href="#" className="text-info collapseBtn" title="Show Add Form">
                        {viewForm ? (
                            <i
                                className="zmdi zmdi-minus-circle  zmdi-hc-2x"
                                onClick={() => {
                                    toggleView();
                                }}
                            ></i>
                        ) : (
                            <i
                                className="zmdi zmdi-plus-circle zmdi-hc-2x"
                                onClick={() => {
                                    toggleView();
                                }}
                            ></i>
                        )}
                    </a>
                </div>
            </div>
            <div className="panel-body">
                <div className="panel update-panel form-pnel-container" id="form">
                    <div className="panel-body">
                        <div className="row">
                            <div className="col-md-3">
                                <label htmlFor="">{t("transactions.mainGroup")}</label>
                                <Select
                                    name="mainGroupId"
                                    value={get(formData, "mainGroupId.value")}
                                    onChange={(e) => onSelectChange(e)}
                                    data={mainGroupData}
                                />
                                <div className="invalid-feedback">
                                    {get(formData, "mainGroupId.error")}
                                </div>
                            </div>
                            <div className="col-md-2">
                                <label htmlFor="">{t("common.supplier")}</label>
                                <Select
                                    name="partyNameId"
                                    value={get(formData, "partyNameId.value")}
                                    onChange={(e) => onSelectChange(e)}
                                    data={suppliersData}
                                    placeholder={t("common.supplier")}
                                />
                                <div className="invalid-feedback">
                                    {get(formData, "partyNameId.error")}
                                </div>
                            </div>
                            <div className="col-md-4">

                                <label htmlFor="">{t("common.date")}</label>
                                <DatePicker
                                    value={get(formData, "date.value")}
                                    onChange={(value) => setFormValue("date", value)}
                                />
                                <div className="invalid-feedback">
                                    {get(formData, "date.error")}
                                </div>
                            </div>
                            <div className="col-md-3">
                                <label htmlFor="">
                                    Lot No.
                                </label>
                                <SearchSelect options={lotDataDdl} placeholder="Option"
                                    name="lotNo"
                                    value={lotDataDdl.filter(val => val.value === get(formData, "lotNo.value"))}
                                    onChange={(e) => onLotChange(e)}
                                    name="lotNo" />
                                <div className="invalid-feedback">
                                    {get(formData, "lotNo.error")}
                                </div>
                            </div>

                        </div>
                        <div class="m-t-10 m-b-10">
                            <div class="row">
                                <div class="col-md-6 text-center">
                                    <div class="bg-light ">
                                        <div class="panel-heading p-l-0 ">
                                            <h4 class="panel-title">Actual Details</h4>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-4 m-b-0">
                                                <label class="">UOM</label>
                                                <p class="m-b-0 text-info">{selectedLot !== "" ? `per ${selectedLot.uom}` : `--`}</p>
                                            </div>
                                            <div class="form-group col-md-4 m-b-0">
                                                <label for="">Qty</label>
                                                <p class="m-b-0 text-info">{selectedLot !== "" ? `${selectedLot.quantity}` : `--`}</p>
                                            </div>
                                            <div class="form-group col-md-4 m-b-0">
                                                <label for="">Weight</label>
                                                <p class="m-b-0 text-info">{selectedLot !== "" ? `${selectedLot.weightPerGram} gms` : `--`}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 text-center">
                                    <div class="bg-light ">
                                        <div class="panel-heading p-l-0">
                                            <h4 class="panel-title"> Pending Details</h4>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-4 m-b-0">
                                                <label class="">UOM</label>
                                                <p class="m-b-0 text-info">{selectedLot !== "" ? `per ${selectedLot.uom}` : `--`}</p>
                                            </div>
                                            <div class="form-group col-md-4 m-b-0">
                                                <label for="">Qty</label>
                                                <p class="m-b-0 text-info">{selectedLot !== "" ? selectedLot.quantity - selectedLot.createdBarCodeQuantity : `--`}</p>
                                            </div>
                                            <div class="form-group col-md-4 m-b-0">
                                                <label for="">Weight</label>
                                                <p class="m-b-0 text-info">{selectedLot !== "" ? `${selectedLot.weightPerGram - selectedLot.createdBarCodeWeight} gms` : `--`}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-t-30">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div className="form-group">
                                                <label htmlFor="">{t("transactions.gemStoneCategory")}
                                                    <span className="asterisk">*</span><a
                                                        href="#"
                                                        id="brand"
                                                        data-toggle="modal"
                                                    >
                                                        <i className="zmdi zmdi-plus-circle f-s-16"></i>
                                                    </a>
                                                </label>
                                                <Select
                                                    name="gemstoneCategoryId"
                                                    value={get(formData, "gemstoneCategoryId.value")}
                                                    onChange={(e) => onSelectChange(e)}
                                                    data={stoneGroupData}
                                                />
                                                <div className="invalid-feedback">
                                                    {get(formData, "gemstoneCategoryId.error")}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div className="form-group">
                                                <label htmlFor="">{t("transactions.gemStoneSubCategory")}
                                                    <span className="asterisk">*</span>
                                                    <a
                                                        href="#"
                                                        id="brand"
                                                        data-toggle="modal"
                                                    >
                                                        <i className="zmdi zmdi-plus-circle f-s-16"></i>
                                                    </a></label>
                                                <Select
                                                    name="gemstoneSubCategoryId"
                                                    value={get(formData, "gemstoneSubCategoryId.value")}
                                                    onChange={(e) => onSelectChange(e)}
                                                    data={stoneData}
                                                />
                                                <div className="invalid-feedback">
                                                    {get(formData, "gemstoneSubCategoryId.error")}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div className="form-group">
                                                <label htmlFor="">
                                                    {t("common.stone")} {t("common.size")}
                                                </label>
                                                <Select
                                                    name="stoneSizeId"
                                                    value={get(formData, "stoneSizeId.value")}
                                                    onChange={(e) =>
                                                        onSelectChange(e)
                                                    }
                                                    data={stoneSizeData}
                                                />
                                                <div className="invalid-feedback">
                                                    {get(formData, "stoneSizeId.error")}
                                                </div>
                                            </div>
                                        </div>
                                        <div className={`${get(formData, "gemstoneCategoryId.value") && isStoneGroupDiamond ? '' : 'hide'}`}>
                                            <div class="col-md-4">
                                                <div className="form-group">
                                                    <label htmlFor="">
                                                        {t("common.stone")} {t("common.color")}
                                                    </label>
                                                    <Select
                                                        name="stoneColorId"
                                                        value={get(formData, "stoneColorId.value")}
                                                        onChange={(e) =>
                                                            onSelectChange(e)
                                                        }
                                                        data={stoneColorData}
                                                    />
                                                    <div className="invalid-feedback">
                                                        {get(formData, "stoneColorId.error")}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div className="form-group">
                                                    <label htmlFor="">
                                                        {t("common.stone")} {t("common.clarity")}
                                                    </label>
                                                    <Select
                                                        name="stoneClarityId"
                                                        value={get(formData, "stoneClarityId.value")}
                                                        onChange={(e) =>
                                                            onSelectChange(e)
                                                        }
                                                        data={stoneClarityData}
                                                    />
                                                    <div className="invalid-feedback">
                                                        {get(formData, "stoneClarityId.error")}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div className="form-group">
                                                    <label htmlFor="">
                                                        {t("common.stone")} {t("common.polish")}
                                                    </label>
                                                    <Select
                                                        name="stonePolishId"
                                                        value={get(formData, "stonePolishId.value")}
                                                        onChange={(e) =>
                                                            onSelectChange(e)
                                                        }
                                                        data={stonePolishData}
                                                    />
                                                    <div className="invalid-feedback">
                                                        {get(formData, "stonePolishId.error")}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div className="form-group">
                                                <label htmlFor="">
                                                    {t("common.uom")}
                                                    <span className="asterisk">*</span>
                                                </label>
                                                <UOMElement name="uom" value={get(formData, 'uom.value')} onChange={(e) => setFormValue('uom', e.target.value)} />
                                                <div className="invalid-feedback">
                                                    {get(formData, "uom.error")}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div className="form-group">
                                                <label htmlFor="">
                                                    {t("common.quantity")}
                                                    <span className="asterisk">*</span>
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    placeholder="quantity"
                                                    name="quantity"
                                                    value={get(formData, "quantity.value")}
                                                    onChange={(e) =>
                                                        setFormValue("quantity", e.target.value)
                                                    }
                                                />
                                                <div className="invalid-feedback">
                                                    {get(formData, "quantity.error")}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div className="form-group">
                                                <label htmlFor="">
                                                    {t("transactions.weight")} ({t("common.gram")})
                                                    <span className="asterisk">*</span>
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    placeholder="weightPerGram"
                                                    name="weightPerGram"
                                                    value={get(formData, "weightPerGram.value")}
                                                    onChange={(e) =>
                                                        setFormValue("weightPerGram", e.target.value)
                                                    }
                                                    disabled={get(formData, 'weightPerGram.disabled')}
                                                />
                                                <div className="invalid-feedback">
                                                    {get(formData, "weightPerGram.error")}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div className="form-group">
                                                <label htmlFor="">
                                                    {t("transactions.weight")} ({t("common.karat")})
                                                    <span className="asterisk">*</span>
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    placeholder="weightPerKarat"
                                                    name="weightPerKarat"
                                                    value={get(formData, "weightPerKarat.value")}
                                                    onChange={(e) =>
                                                        setFormValue("weightPerKarat", e.target.value)
                                                    }
                                                    disabled={get(formData, 'weightPerKarat.disabled')}
                                                />
                                                <div className="invalid-feedback">
                                                    {get(formData, "weightPerKarat.error")}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div className="form-group">
                                                <label htmlFor="">
                                                    {t("common.rate")}
                                                    <span className="asterisk">*</span>
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    placeholder="rate"
                                                    name="rate"
                                                    value={get(formData, "rate.value")}
                                                    onChange={(e) =>
                                                        setFormValue("rate", e.target.value)
                                                    }
                                                    disabled={get(formData, 'rate.disabled')}
                                                />
                                                <div className="invalid-feedback">
                                                    {get(formData, "rate.error")}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div className="form-group">
                                                <label htmlFor="">
                                                    {t("common.amount")}
                                                    <span className="asterisk">*</span>
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    placeholder="amount"
                                                    name="amount"
                                                    value={get(formData, "amount.value")}
                                                    onChange={(e) =>
                                                        setFormValue("amount", e.target.value)
                                                    }
                                                    disabled={get(formData, 'amount.disabled')}
                                                />
                                                <div className="invalid-feedback">
                                                    {get(formData, "amount.error")}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div className="form-group">
                                                <label htmlFor="">
                                                    {t("transactions.labelType")}
                                                    <span className="asterisk">*</span>
                                                </label>
                                                <select
                                                    className="form-control select"
                                                    name="labelType"
                                                    value={get(formData, "labelType.value")}
                                                    onChange={(e) =>
                                                        setFormValue('labelType', e.target.value)
                                                    }
                                                >
                                                    <option value={""}>Select</option>
                                                    <option value={"barcode"}>Bar Code</option>
                                                    <option value={"qrcode"}>QR Code</option>
                                                </select>
                                                <div className="invalid-feedback">
                                                    {get(formData, "labelType.error")}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div className="form-group">
                                                <label htmlFor="">
                                                    {t("common.remarks")}
                                                    <span className="asterisk">*</span>
                                                </label>
                                                <textarea
                                                    type="text"
                                                    className="form-control"
                                                    name="remarks"
                                                    value={get(formData, "remarks.value")}
                                                    style={{ height: "30px" }}
                                                    onChange={(e) =>
                                                        setFormValue("remarks", e.target.value)
                                                    }
                                                />
                                                <div className="invalid-feedback">
                                                    {get(formData, "remarks.error")}
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label htmlFor="upload-button">
                                        {image.preview ?
                                            <img src={image.preview} alt="logo" style={{ width: '128px', height: '128px', borderRadius: '50%' }} /> :
                                            <div className="circle m-t-20">
                                                <i className="zmdi zmdi-camera zmdi-hc-2x"></i>
                                            </div>
                                        }
                                    </label>
                                    <input
                                        type="file"
                                        id="upload-button"
                                        style={{ display: 'none' }}
                                        onChange={handleChange} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-center">
                        <a href="#" onClick={onSaveClick} class="btn btn-primary"><i class="zmdi zmdi-check"></i>Save</a>
                        <a href="#" onClick={onClearClick} class="btn btn-default">Clear</a> </div>
                </div>
            </div>
        </div >
    );
};
export default withTranslation()(GemstoneBarcode);
