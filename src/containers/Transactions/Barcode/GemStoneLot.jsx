import React, { Component, useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { makeStyles } from "@material-ui/core";
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";

import { gemstoneLotSearchForm, gemstoneLotCreationForm } from "../Constants";
import { fetchSuppliers } from "../../../redux/actions/Admin/supplierActions";
import {
  fetchStoneGroups,
  fetchStonesByStoneGroup,
} from "../../../redux/actions/creations/stoneGroupActions";
import {
  fetchMainGroups,
} from "../../../redux/actions/mainGroupActions";
import { fetchEmployeesList } from '../../../redux/actions/Admin/employeeActions';
import { fetchGemstoneLotInvoices, fetchGemstoneLotDetails, saveGemstoneLot } from "../../../redux/actions/transactions/barcode/gemstoneLotActions";
import { fetchStoneColors } from "../../../redux/actions/creations/stoneColorActions";
import { fetchStoneClarities } from "../../../redux/actions/creations/stoneClarityActions";
import { fetchStonePolishes } from "../../../redux/actions/creations/stonePolishActions";
import StoneDetailsLot from "../StoneDetails/StoneDetailsLot";
import "../../../assets/css/search-select.css";
import CustomGrid from "../../../components/CustomGrid";
import Select from "../../../components/Form/Select";
import SearchSelect from "../../../components/Form/SearchSelect";
import UOMElement from "../../../components/UOMElement";
import DatePicker from "../../../components/Form/DatePicker";

const useStyles = makeStyles((theme) => ({}));

export const GemStoneLot = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const purchaseDetailsColumns = [
    {
      id: "purchaseDate",
      label: "Date",
      isEdit: false,
      isSort: true,
    },
    {
      id: "invoiceNo",
      label: "Invoice No",
      isEdit: false,
      isSort: true,
    },
    {
      id: "receiptNo",
      label: "RC No",
      isEdit: false,
      isSort: true,
    },
    {
      id: "supplier",
      label: "Supplier",
      isEdit: false,
      isSort: true,
    },
    {
      id: "categoryName",
      label: "Category",
      isEdit: false,
      isSort: true,
    },
    {
      id: "subCategoryName",
      label: "Sub Category",
      isEdit: false,
      isSort: true,
    },
    {
      id: "stoneColorName",
      label: "Color",
      isEdit: false,
      isSort: true,
    },
    {
      id: "stoneClarityName",
      label: "Clarity",
      isEdit: false,
      isSort: true,
    },
    {
      id: "stonePolishName",
      label: "Polish",
      isEdit: false,
      isSort: true,
    },
    {
      id: "uom",
      label: "UOM",
      isEdit: false,
      isSort: true,
    },
    {
      id: "quantity",
      label: "QTY",
      isEdit: false,
      isSort: true,
    },
    {
      id: "weightPerGram",
      label: "Weight (Gram)",
      isEdit: false,
      isSort: true,
    },
    {
      id: "weightPerKarat",
      label: "Weight (Karat)",
      isEdit: false,
      isSort: true,
    },
  ];


  const selectedListColumns = [
    {
      id: "employee",
      label: "Employee",
      isEdit: false,
      isSort: true,
    },
    {
      id: "receiptNo",
      label: "RC No",
      isEdit: false,
      isSort: true,
    },
    {
      id: "categoryName",
      label: "Category",
      isEdit: false,
      isSort: true,
    },
    {
      id: "subCategoryName",
      label: "Sub Category",
      isEdit: false,
      isSort: true,
    },
    {
      id: "stoneColorName",
      label: "Color",
      isEdit: false,
      isSort: true,
    },
    {
      id: "stoneClarityName",
      label: "Clarity",
      isEdit: false,
      isSort: true,
    },
    {
      id: "stonePolishName",
      label: "Polish",
      isEdit: false,
      isSort: true,
    },
    {
      id: "uom",
      label: "UOM",
      isEdit: false,
      isSort: true,
    },
    {
      id: "quantity",
      label: "QTY",
      isEdit: false,
      isSort: true,
    },
    {
      id: "weightPerGram",
      label: "Weight (Gram)",
      isEdit: false,
      isSort: true,
    },
    {
      id: "weightPerKarat",
      label: "Weight (Karat)",
      isEdit: false,
      isSort: true,
    },
  ];

  const { t } = props;
  const [formData, setFromData] = useState(cloneDeep(gemstoneLotSearchForm));
  const [formDataLotCreation, setFormDataLotCreation] = useState(
    cloneDeep(gemstoneLotCreationForm)
  );
  const [selectedList, setSelectedList] = useState([]);
  const [selectedItem, setSelectedItem] = useState("");
  const [gemstoneLotPurchaseDetails, setGemstoneLotPurchaseDetails] = useState([]);
  const [selectedItems, setSelectedItems] = useState([]);
  const [selectedItemIndexValue, setSelectedItemIndexValue] = useState(0);
  const [selectedItemToEdit, setSelectedItemToEdit] = useState("");
  const [allGemstoneLotsInvoicesData, setAllGemstoneLotsInvoicesData] = useState([]);
  let [isStoneGroupDiamond, setIsStoneGroupDiamond] = useState(false);

  const suppliers = useSelector((state) => state.supplierReducer.allSuppliers);
  const stoneGroups = useSelector(
    (state) => state.stoneGroupReducer.allStoneGroups
  );
  const stones = useSelector(
    (state) => state.stoneGroupReducer.allStonesByStoneGroup
  );
  const employees = useSelector(state => state.employeeReducer.allEmployeesList);
  const allGemstoneLotsInvoices = useSelector(state => state.gemstoneLotReducer.allGemstoneLotsInvoices);
  const allGemstoneLotDetails = useSelector(state => state.gemstoneLotReducer.allGemstoneLotDetails);
  const stoneColors = useSelector(
    (state) => state.stoneColorReducer.allStoneColors
  );
  const stoneClarities = useSelector(
    (state) => state.stoneClarityReducer.allStoneClarities
  );
  const stonePolishes = useSelector(
    (state) => state.stonePolishReducer.allStonePolishes
  );
  const gemstoneLotInfo = useSelector(state => state.gemstoneLotReducer.gemstoneLotInfo);

  useEffect(() => {
    let gemstoneLotPurchaseDetailsData = [];
    if (allGemstoneLotDetails.data) {
      let i = 0;
      const dataToBind = { ...allGemstoneLotDetails.data };

      dataToBind.stoneDetails.forEach(val1 => {
        i += 1;
        const item = {};
        item.invoiceNo = dataToBind.invoiceNo;
        item.purchaseDate = dataToBind.invoiceDate;
        item.isDeleted = dataToBind.isDeleted;
        item.purchaseType = dataToBind.purchaseType;
        item.partyId = dataToBind.partyId;
        item.mainGroup = "";
        item.groupName = "";
        item.uom = val1.uom;
        item.quantity = val1.quantity - val1.createdLotQuantity;
        item.weightPerGram = `${val1.weightPerGram - parseFloat(val1.createdLotWeight)}g`;
        item.weightPerKarat = `${val1.weightPerKarat}g`;
        item.id = dataToBind.id;
        item.categoryId = val1.stone && val1.stone.stoneGroup ? val1.stone.stoneGroup.id : "";
        item.subCategoryId = val1.stone ? val1.stone.id : "";
        item.stoneColorId = val1.stoneColor ? val1.stoneColor.id : "";
        item.stoneClarityId = val1.stoneClarity ? val1.stoneClarity.id : "";
        item.stonePolishId = val1.stonePolish ? val1.stonePolish.id : "";
        item.categoryName = val1.stone && val1.stone.stoneGroup ? val1.stone.stoneGroup.groupName : "";
        item.subCategoryName = val1.stone ? val1.stone.stoneName : "";
        item.stoneColorName = val1.stoneColor ? val1.stoneColor.colorName : "";
        item.stoneClarityName = val1.stoneClarity ? val1.stoneClarity.clarityName : "";
        item.stonePolishName = val1.stonePolish ? val1.stonePolish.polishName : "";
        item.stoneSizeId = val1.stoneSize ? val1.stoneSize.id : "";
        item.indexVal = i;
        item.receiptNo = dataToBind.receiptNo;
        item.createdLotQuantity = val1.createdLotQuantity;
        item.createdLotWeight = val1.createdLotWeight;
        item.purchaseId = val1.id;
        item.supplier = suppliersData.filter(val => val.id === get(formData, "partyNameId.value")).length > 0 ? suppliersData.filter(val => val.id === get(formData, "partyNameId.value"))[0].name : ``;
        gemstoneLotPurchaseDetailsData.push(item);
      });

      setGemstoneLotPurchaseDetails(gemstoneLotPurchaseDetailsData);
    }
  }, [allGemstoneLotDetails]);


  const setFormValue = (field, value) => {
    formData[field]["value"] = value;
    setFromData({ ...formData });
  };

  const setFormValueLotCreation = (field, value) => {
    formDataLotCreation[field]["value"] = value;
    setFormDataLotCreation({ ...formDataLotCreation });
  };

  const suppliersData = suppliers.data
    ? suppliers.data.map((supplier) => {
      return {
        id: supplier.id,
        name: supplier.partyName,
      };
    })
    : [];

  const stoneGroupData = stoneGroups.data
    ? stoneGroups.data.map((group) => {
      return {
        id: group.id,
        name: group.groupName,
        shortCode: group.shortCode,
        diamond: group.diamond,
      };
    })
    : [];

  const stoneData = stones.data
    ? stones.data.map((stone) => {
      return {
        id: stone.id,
        name: stone.stoneName,
        shortCode: stone.shortCode,
      };
    })
    : [];


  const stoneColorData = stoneColors.data
    ? stoneColors.data.map((color) => {
      return {
        id: color.id,
        name: color.colorName,
        shortCode: color.shortCode,
      };
    })
    : [];
  const stoneClarityData = stoneClarities.data
    ? stoneClarities.data.map((clarity) => {
      return {
        id: clarity.id,
        name: clarity.clarityName,
        shortCode: clarity.shortCode,
      };
    })
    : [];
  const stonePolishData = stonePolishes.data
    ? stonePolishes.data.map((polish) => {
      return {
        id: polish.id,
        name: polish.polishName,
        shortCode: polish.shortCode,
      };
    })
    : [];

  const isStoneADiamond = (selectedStoneGroupId) => {
    let selectedStoneGroup = stoneGroupData.find(ele => ele.id === selectedStoneGroupId);
    return selectedStoneGroup ? selectedStoneGroup.diamond : false;
  }

  const employeesData = employees.data ? employees.data.map((employee) => {
    return {
      id: employee.id,
      name: employee.firstName + " " + employee.middleName + " " + employee.lastName,
    }
  }) : [];

  const validateField = (fieldObj) => {
    let errorObj = { isValid: true, error: null }
    if (fieldObj.required && !fieldObj.value && fieldObj.value !== 0) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
    else if (fieldObj.value && parseInt(fieldObj.value) > parseInt(fieldObj.maxValue)) { errorObj.isValid = false; errorObj.error = `Value should be less than ${fieldObj.maxValue}` }
    else if (fieldObj.value && parseInt(fieldObj.value) < parseInt(fieldObj.minValue)) { errorObj.isValid = false; errorObj.error = `Value should be greater than ${fieldObj.minValue}` }
    else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
    else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }
    return errorObj;
  }

  useEffect(() => {
    dispatch(fetchSuppliers());
    dispatch(fetchStoneGroups());
    dispatch(fetchMainGroups());
    dispatch(fetchEmployeesList());
  }, [dispatch]);

  useEffect(() => {
    dispatch(fetchStonesByStoneGroup(get(formDataLotCreation, "gemstoneCategoryId.value")));
    setIsStoneGroupDiamond(isStoneADiamond(get(formDataLotCreation, "gemstoneCategoryId.value")));
    if (get(formDataLotCreation, "gemstoneCategoryId.value") && isStoneADiamond(get(formDataLotCreation, "gemstoneCategoryId.value"))) {
      dispatch(fetchStoneColors());
      dispatch(fetchStoneClarities());
      dispatch(fetchStonePolishes());
    }
  }, [get(formDataLotCreation, "gemstoneCategoryId.value")]);

  useEffect(() => {
    if (get(formData, 'partyNameId.value') !== "" && get(formData, "date.value") !== null && get(formData, "purchaseType.value") !== "") {
      dispatch(fetchGemstoneLotInvoices(get(formData, 'partyNameId.value'), get(formData, "purchaseType.value"), get(formData, "date.value")));
    }
  }, [get(formData, 'partyNameId.value'), get(formData, "date.value"), get(formData, "purchaseType.value")]);

  const onSelectChangeSearch = (e) => {
    setFormValue(e.nativeEvent.target.name, e.target.value);
  };

  const onSelectChange = (e) => {
    setFormValueLotCreation(e.nativeEvent.target.name, e.target.value);
  };

  const onInvoiceChange = (e) => {
    setFormValue("invoiceId", e.value);
  }

  const onFilterClick = () => {
    let isFormValid = true;
    map(formData, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      formData[key] = { ...value, ...isValid };
    });
    setFromData({ ...formData });
    if (!isFormValid)
      return;
    clearForm();
    setSelectedList([]);
    setSelectedItem("");
    dispatch(fetchGemstoneLotDetails(get(formData, "invoiceId.value")));
  }

  const onChangePurchaseItem = (item) => {
    setSelectedItemToEdit("");
    const selectedItem = gemstoneLotPurchaseDetails.filter(val => val.indexVal === item)[0];
    setSelectedItem(selectedItem);
    formDataLotCreation["gemstoneCategoryId"]["value"] = selectedItem.categoryId;
    formDataLotCreation["gemstoneSubCategoryId"]["value"] = selectedItem.subCategoryId;
    formDataLotCreation["stoneColorId"]["value"] = selectedItem.stoneColorId;
    formDataLotCreation["stoneClarityId"]["value"] = selectedItem.stoneClarityId;
    formDataLotCreation["stonePolishId"]["value"] = selectedItem.stonePolishId;
    formDataLotCreation["uom"]["value"] = selectedItem.uom;
    formDataLotCreation["qty"]["value"] = selectedItem.quantity;
    formDataLotCreation["qty"]["maxValue"] = selectedItem.quantity;
    formDataLotCreation["qty"]["minValue"] = "0";
    formDataLotCreation["weightPerGram"]["value"] = selectedItem.weightPerGram.replace('g', '');
    formDataLotCreation["weightPerGram"]["maxValue"] = selectedItem.weightPerGram.replace('g', '');
    formDataLotCreation["weightPerGram"]["minValue"] = "0";
    formDataLotCreation["weightPerKarat"]["value"] = selectedItem.weightPerKarat.replace('g', '');
    formDataLotCreation["weightPerKarat"]["maxValue"] = selectedItem.weightPerKarat.replace('g', '');
    formDataLotCreation["weightPerKarat"]["minValue"] = "0";
    setFormDataLotCreation({ ...formDataLotCreation });
  };

  const clearForm = () => {
    formDataLotCreation["gemstoneCategoryId"]["value"] = "";
    formDataLotCreation["gemstoneSubCategoryId"]["value"] = "";
    formDataLotCreation["stoneColorId"]["value"] = "";
    formDataLotCreation["stoneClarityId"]["value"] = "";
    formDataLotCreation["stonePolishId"]["value"] = "";
    formDataLotCreation["uom"]["value"] = "";
    formDataLotCreation["qty"]["value"] = "";
    formDataLotCreation["weightPerGram"]["value"] = "";
    formDataLotCreation["weightPerKarat"]["value"] = "";
    formDataLotCreation["remarks"]["value"] = "";
    formDataLotCreation["employeeId"]["value"] = "";
    setSelectedItem("");
    setSelectedItemToEdit("");
    setFormDataLotCreation({ ...formDataLotCreation });
  }

  const addToList = () => {
    let isFormValid = true;
    map(formDataLotCreation, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      formDataLotCreation[key] = { ...value, ...isValid };
    });
    setFormDataLotCreation({ ...formDataLotCreation });
    if (!isFormValid)
      return;
    const dataToChange = [...gemstoneLotPurchaseDetails];
    if (selectedItemToEdit === "") {
      const itemSelected = JSON.parse(JSON.stringify(selectedItem));
      const originalQty = parseInt(itemSelected.quantity) ? parseInt(itemSelected.quantity) : 0;
      const originalWeight = parseFloat(itemSelected.weightPerGram) ? parseFloat(itemSelected.weightPerGram.replace("g", "")) : 0;
      if (itemSelected.rowColorStyle)
        delete itemSelected.rowColorStyle;
      if (itemSelected.isRadioDisable)
        delete itemSelected.isRadioDisable;
      if (get(formDataLotCreation, "qty.value") !== "" && get(formDataLotCreation, "weightPerGram.value") !== "" && parseInt(get(formDataLotCreation, "qty.value")) <= originalQty && parseFloat(get(formDataLotCreation, "weightPerGram.value")) <= originalWeight) {
        itemSelected.quantity = get(formDataLotCreation, "qty.value");
        itemSelected.weightPerGram = `${get(formDataLotCreation, "weightPerGram.value")}g`;
        itemSelected.employee = employeesData.filter(val => val.id === get(formDataLotCreation, "employeeId.value")).length > 0 ? employeesData.filter(val => val.id === get(formDataLotCreation, "employeeId.value"))[0].name : "";
        itemSelected.employeeId = get(formDataLotCreation, "employeeId.value");
        itemSelected.remarks = get(formDataLotCreation, "remarks.value");
        itemSelected.selectedItemIndex = selectedItemIndexValue + 1;
        setSelectedItemIndexValue(selectedItemIndexValue + 1);
        dataToChange.forEach(item => {
          if (item.indexVal === itemSelected.indexVal) {
            item.quantity = originalQty - parseInt(get(formDataLotCreation, "qty.value"));
            item.weightPerGram = `${originalWeight - parseFloat(get(formDataLotCreation, "weightPerGram.value"))}g`;
            formDataLotCreation["weightPerGram"]["maxValue"] = originalWeight - parseFloat(get(formDataLotCreation, "weightPerGram.value"));
            formDataLotCreation["qty"]["maxValue"] = originalQty - parseInt(get(formDataLotCreation, "qty.value"));
            if (originalQty - parseInt(get(formDataLotCreation, "qty.value")) === 0 || originalWeight - parseInt(get(formDataLotCreation, "weightPerGram.value")) === 0) {
              item.rowColorStyle = "redColor";
              item.isRadioDisable = true;
            }
            else {
              item.rowColorStyle = "orangeColor";
              item.isRadioDisable = true;
            }
          }
        });
        formDataLotCreation["qty"]["value"] = "";
        formDataLotCreation["weightPerGram"]["value"] = "";
        formDataLotCreation["weightPerKarat"]["value"] = "";
        formDataLotCreation["remarks"]["value"] = "";
        formDataLotCreation["employeeId"]["value"] = "";
        setSelectedItemToEdit("");
        setSelectedList([...selectedList, itemSelected]);
        setGemstoneLotPurchaseDetails(dataToChange);
      }
    }
    else {
      const itemSelected = selectedList.filter(val => val.selectedItemIndex === selectedItemToEdit)[0];
      const initialQty = parseInt(dataToChange.filter(val => val.indexVal === itemSelected.indexVal)[0].quantity);
      const initialWeight = parseFloat(dataToChange.filter(val => val.indexVal === itemSelected.indexVal)[0].weightPerGram.replace("g", ""));
      const originalQty = parseInt(itemSelected.quantity) ? parseInt(itemSelected.quantity) : 0;
      const originalWeight = parseFloat(itemSelected.weightPerGram) ? parseFloat(itemSelected.weightPerGram.replace("g", "")) : 0;
      const currentQty = get(formDataLotCreation, "qty.value") !== "" ? parseInt(get(formDataLotCreation, "qty.value")) : 0;
      const currentWeight = get(formDataLotCreation, "weightPerGram.value") !== "" ? parseFloat(get(formDataLotCreation, "weightPerGram.value")) : 0;
      const weightDeviation = currentWeight - originalWeight;
      const qtyDeviation = currentQty - originalQty;
      const itemsList = [...selectedList];
      if (currentQty !== 0 && currentWeight !== 0 && initialQty - qtyDeviation >= 0 && initialWeight - weightDeviation >= 0) {
        itemsList.forEach(item => {
          if (item.selectedItemIndex === itemSelected.selectedItemIndex) {
            item.quantity = get(formDataLotCreation, "qty.value");
            item.weightPerGram = `${get(formDataLotCreation, "weightPerGram.value")}g`;
            item.employee = employeesData.filter(val => val.id === get(formDataLotCreation, "employeeId.value")).length > 0 ? employeesData.filter(val => val.id === get(formDataLotCreation, "employeeId.value"))[0].name : "";
            item.employeeId = get(formDataLotCreation, "employeeId.value");
            item.remarks = get(formDataLotCreation, "remarks.value");
          }
        });
        dataToChange.forEach(item => {
          if (item.indexVal === itemSelected.indexVal) {
            item.quantity = initialQty - qtyDeviation;
            item.weight = `${initialWeight - weightDeviation}g`;
            formDataLotCreation["weightPerGram"]["maxValue"] = initialWeight - weightDeviation;
            formDataLotCreation["qty"]["maxValue"] = initialQty - qtyDeviation;
            if (initialQty - qtyDeviation === 0 || initialWeight - weightDeviation === 0) {
              item.rowColorStyle = "redColor";
              item.isRadioDisable = true;
            }
            else {
              item.rowColorStyle = "orangeColor";
              item.isRadioDisable = true;
            }
          }
        });
        setSelectedList(itemsList);
        formDataLotCreation["qty"]["value"] = "";
        formDataLotCreation["weightPerGram"]["value"] = "";
        formDataLotCreation["weightPerKarat"]["value"] = "";
        formDataLotCreation["remarks"]["value"] = "";
        formDataLotCreation["employeeId"]["value"] = "";
        setSelectedItemToEdit("");
        setGemstoneLotPurchaseDetails(dataToChange);
      }
    }
  };

  const onItemsChange = (items) => {
    setSelectedItems(items);
  };

  const getUpdatedQty = (val, id, items) => {
    let changedQty = parseInt(val);
    items.filter(value => value.indexVal === id).forEach(item => {
      changedQty += parseInt(item.quantity);
    })
    return changedQty;
  };

  const getUpdatedWeight = (val, id, items) => {
    let changedWeight = parseFloat(val.replace('g', ''));
    items.filter(value => value.indexVal === id).forEach(item => {
      changedWeight += parseFloat(item.weightPerGram.replace('g', ''));
    })
    return `${changedWeight}g`;
  };

  const deleteSelectedItems = () => {
    const selectedDataToRemove = selectedList.filter(val => selectedItems.indexOf(val.selectedItemIndex) >= 0);
    const dataToChange = [...gemstoneLotPurchaseDetails];
    const filteredData = selectedList.filter(val => selectedItems.indexOf(val.selectedItemIndex) < 0);
    dataToChange.forEach(item => {
      item.quantity = getUpdatedQty(item.quantity, item.indexVal, selectedDataToRemove);
      item.weight = getUpdatedWeight(item.weight, item.indexVal, selectedDataToRemove);
      formDataLotCreation["weightPerGram"]["maxValue"] = getUpdatedWeight(item.weight, item.indexVal, selectedDataToRemove).replace('g', '');
      formDataLotCreation["qty"]["maxValue"] = getUpdatedQty(item.quantity, item.indexVal, selectedDataToRemove);
      setFormDataLotCreation({ ...formDataLotCreation });
      if (item.quantity === 0 || parseFloat(item.weightPerGram.replace('g', '')) === 0) {
        item.rowColorStyle = "redColor";
        item.isRadioDisable = true;
      }
      else if (item.quantity !== 0 && parseFloat(item.weightPerGram.replace('g', '')) !== 0) {
        if (filteredData.filter(val => val.indexVal === item.indexVal).length > 0) {
          item.rowColorStyle = "orangeColor";
          if (item.isRadioDisable)
            delete item.isRadioDisable;
        }
        else {
          if (item.rowColorStyle)
            delete item.rowColorStyle;
          if (item.isRadioDisable)
            delete item.isRadioDisable;
        }
      }

    });
    setGemstoneLotPurchaseDetails(dataToChange);
    setSelectedList(filteredData);
    setSelectedItems([]);
  };

  const onEditClickList = (val) => {
    setSelectedItemToEdit(val);
    const selectedItem = selectedList.filter(item => item.selectedItemIndex === val)[0];
    formDataLotCreation["gemstoneCategoryId"]["value"] = selectedItem.categoryId;
    formDataLotCreation["gemstoneSubCategoryId"]["value"] = selectedItem.subCategoryId;
    formDataLotCreation["stoneColorId"]["value"] = selectedItem.stoneColorId;
    formDataLotCreation["stoneClarityId"]["value"] = selectedItem.stoneClarityId;
    formDataLotCreation["stonePolishId"]["value"] = selectedItem.stonePolishId;
    formDataLotCreation["uom"]["value"] = selectedItem.uom;
    formDataLotCreation["qty"]["value"] = selectedItem.quantity;
    formDataLotCreation["weightPerGram"]["value"] = selectedItem.weightPerGram.replace('g', '');
    formDataLotCreation["weightPerKarat"]["value"] = selectedItem.weightPerKarat.replace('g', '');
    formDataLotCreation["employeeId"]["value"] = selectedItem.employeeId;
    formDataLotCreation["remarks"]["value"] = selectedItem.remarks;
    setFormDataLotCreation({ ...formDataLotCreation });
  }

  const onSaveClick = () => {
    const dataToInsert = [...selectedList];
    dataToInsert.forEach(val => {
      val.weightPerGram = parseFloat(val.weightPerGram.replace("g", ""));
      val.weightPerKarat = parseFloat(val.weightPerKarat.replace("g", ""));
    })
    dispatch(saveGemstoneLot(dataToInsert));
  };

  const onClearClick = () => {
    clearForm();
    setSelectedList([]);
    setGemstoneLotPurchaseDetails([]);
    setAllGemstoneLotsInvoicesData([]);
    formData["purchaseType"]["value"] = "";
    formData["partyNameId"]["value"] = "";
    formData["date"]["value"] = null;
    formData["invoiceId"]["value"] = "";
  }


  useEffect(() => {
    if (allGemstoneLotsInvoices.data)
      setAllGemstoneLotsInvoicesData(allGemstoneLotsInvoices.data.map((invc) => {
        return {
          value: invc.recieptNo,
          label: invc.invoiceNo === null ? invc.recieptNo : invc.recieptNo + " - " + invc.invoiceNo,
        }
      }));
  }, [allGemstoneLotsInvoices]);


  useEffect(() => {
    if (gemstoneLotInfo && gemstoneLotInfo.status && gemstoneLotInfo.status.toLowerCase() === "success") {
      onClearClick();
    }
  }, [gemstoneLotInfo]);

  return (
    <div className="panel panel-default hero-panel">
      <div className="panel-heading">
        <h1 className="panel-title">
          {t("transactions.gemstoneLot")} {t("common.creation")}
        </h1>
      </div>
      <div className="panel-body">
        <div className="panel-body p-t-0">
          <div class="bg-light p-15 p-b-0">
            <div className="row">
              <div className="col-md-3">
                <label for="">Purchase Type</label>
                <div className="form-group no-min-height">
                  <div className="radio radio-inline radio-primary m-b-0">
                    <input
                      type="radio"
                      name="purchaseType"
                      value="Approval"
                      checked={
                        get(formData, "purchaseType.value") === "Approval"
                      }
                      onChange={(e) => setFormValue("purchaseType", "Approval")}
                    />
                    <label htmlFor="">{t("transactions.approval")}</label>
                  </div>
                  <div className="radio radio-inline radio-primary m-b-0">
                    <input
                      type="radio"
                      name="purchaseType"
                      value="Invoice"
                      checked={
                        get(formData, "purchaseType.value") === "Invoice"
                      }
                      onChange={(e) => setFormValue("purchaseType", "Invoice")}
                    />
                    <label htmlFor="">{t("transactions.invoice")}</label>
                  </div>
                  <div className="invalid-feedback">
                    {get(formData, "purchaseType.error")}
                  </div>
                </div>
              </div>
              <div className="form-group col-md-2">
                <label htmlFor="">{t("common.supplier")}</label>
                <Select
                  name="partyNameId"
                  value={get(formData, "partyNameId.value")}
                  onChange={(e) => onSelectChangeSearch(e)}
                  data={suppliersData}
                  placeholder={t("common.supplier")}
                />
                <div className="invalid-feedback">
                  {get(formData, "partyNameId.error")}
                </div>
              </div>
              <div className="form-group col-md-4">
                <label htmlFor="">{t("common.date")}</label>
                <DatePicker
                  value={get(formData, "date.value")}
                  onChange={(value) => setFormValue("date", value)}
                />
                <div className="invalid-feedback">
                  {get(formData, "date.error")}
                </div>
              </div>
              <div className="form-group col-md-2">
                <label htmlFor="">
                  {t("transactions.invoice")} / {t("transactions.rcNo")}
                </label>
                <SearchSelect options={allGemstoneLotsInvoicesData} placeholder="Option"
                  name="invoiceId"
                  value={allGemstoneLotsInvoicesData.filter(val => val.value === get(formData, "invoiceId.value"))}
                  onChange={(e) => onInvoiceChange(e)} />
                <div className="invalid-feedback">
                  {get(formData, "invoiceId.error")}
                </div>
              </div>
              <div className="form-group col-md-1">
                <button class="btn btn-primary btn-sm m-t-22" onClick={onFilterClick}>Filter</button>
              </div>
            </div>
          </div>
          <div class="border p-15 m-b-20">
            <h3 class="panel-title">
              {t("transactions.purchase")} {t("common.details")}
            </h3>
            <CustomGrid
              gridData={gemstoneLotPurchaseDetails}
              columns={purchaseDetailsColumns}
              isRadio={true}
              isCheckbox={false}
              uniqueId="indexVal"
              sortColumn="receiptNo"
              isEdit={false}
              isDelete={false}
              defaultRowsPerPage={5}
              fnOnChangeRadioItem={onChangePurchaseItem}
            />
          </div>
          <div class="">
            <div class="">
              <h4 class="panel-title">{t("common.details")}</h4>
            </div>
            <div className="row">
              <div className="col-md-4">
                <label htmlFor="">{t("transactions.gemStoneCategory")}
                  <a
                    href="#"
                    id="brand"
                    data-toggle="modal"
                  >
                    <i className="zmdi zmdi-plus-circle f-s-16"></i>
                  </a></label>
                <Select
                  name="gemstoneCategoryId"
                  value={get(formDataLotCreation, "gemstoneCategoryId.value")}
                  onChange={(e) => onSelectChange(e)}
                  data={stoneGroupData}
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "gemstoneCategoryId.error")}
                </div>
              </div>
              <div className="form-group col-md-4">
                <label htmlFor="">{t("transactions.gemStoneSubCategory")}
                  <a
                    href="#"
                    id="brand"
                    data-toggle="modal"
                  >
                    <i className="zmdi zmdi-plus-circle f-s-16"></i>
                  </a></label>
                <Select
                  name="gemstoneSubCategoryId"
                  value={get(formDataLotCreation, "gemstoneSubCategoryId.value")}
                  onChange={(e) => onSelectChange(e)}
                  data={stoneData}
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "gemstoneSubCategoryId.error")}
                </div>
              </div>
              <div className={`${get(formDataLotCreation, "gemstoneCategoryId.value") && isStoneGroupDiamond ? '' : 'hide'}`}>
                <div className="col-sm-6 col-md-3 col-lg-3">
                  <div className="form-group">
                    <label htmlFor="">
                      {t("common.stone")} {t("common.color")}
                    </label>
                    <Select
                      name="stoneColorId"
                      value={get(formDataLotCreation, "stoneColorId.value")}
                      onChange={(e) =>
                        onSelectChange(e)
                      }
                      data={stoneColorData}
                    />
                    <div className="invalid-feedback">
                      {get(formDataLotCreation, "stoneColorId.error")}
                    </div>
                  </div>
                </div>
                <div className="col-sm-6 col-md-3 col-lg-3">
                  <div className="form-group">
                    <label htmlFor="">
                      {t("common.stone")} {t("common.clarity")}
                    </label>
                    <Select
                      name="stoneClarityId"
                      value={get(formDataLotCreation, "stoneClarityId.value")}
                      onChange={(e) =>
                        onSelectChange(e)
                      }
                      data={stoneClarityData}
                    />
                    <div className="invalid-feedback">
                      {get(formDataLotCreation, "stoneClarityId.error")}
                    </div>
                  </div>
                </div>
                <div className="col-sm-6 col-md-3 col-lg-3">
                  <div className="form-group">
                    <label htmlFor="">
                      {t("common.stone")} {t("common.polish")}
                    </label>
                    <Select
                      name="stonePolishId"
                      value={get(formDataLotCreation, "stonePolishId.value")}
                      onChange={(e) =>
                        onSelectChange(e)
                      }
                      data={stonePolishData}
                    />
                    <div className="invalid-feedback">
                      {get(formDataLotCreation, "stonePolishId.error")}
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-2">
                <label htmlFor="">
                  {t("common.uom")}
                  <span className="asterisk">*</span>
                </label>
                <UOMElement name="uom" value={get(formDataLotCreation, 'uom.value')} onChange={(e) => setFormValueLotCreation('uom', e.target.value)} />
                <div className="invalid-feedback">
                  {get(formData, "uom.error")}
                </div>

              </div>
              <div className="form-group col-md-2">
                <label htmlFor="">{t("transactions.qty")}</label>
                <input
                  type="number"
                  className="form-control"
                  placeholder="Qty"
                  name="qty"
                  value={get(formDataLotCreation, "qty.value")}
                  onChange={(e) =>
                    setFormValueLotCreation("qty", e.target.value)
                  }
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "qty.error")}
                </div>
              </div>

              <div className="form-group col-md-2">
                <label htmlFor="">
                  {t("transactions.weight")} ({t("common.gram")})
                </label>
                <input
                  type="number"
                  className="form-control"
                  name="weightPerGram"
                  value={get(formDataLotCreation, "weightPerGram.value")}
                  onChange={(e) =>
                    setFormValueLotCreation("weightPerGram", e.target.value)
                  }
                  disabled={get(formDataLotCreation, 'uom.value').toLowerCase() === "karat"}
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "weightPerGram.error")}
                </div>
              </div>
              <div className="form-group col-md-2">
                <label htmlFor="">
                  {t("transactions.weight")} ({t("common.karat")})
                </label>
                <input
                  type="number"
                  className="form-control"
                  name="weightPerKarat"
                  value={get(formDataLotCreation, "weightPerKarat.value")}
                  onChange={(e) =>
                    setFormValueLotCreation("weightPerKarat", e.target.value)
                  }
                  disabled={get(formDataLotCreation, 'uom.value').toLowerCase() === "gram"}
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "weightPerKarat.error")}
                </div>
              </div>
              <div className="col-md-3">
                <label htmlFor="">{t("common.employee")}</label>
                <Select
                  name="employeeId"
                  value={get(formDataLotCreation, "employeeId.value")}
                  onChange={(e) => onSelectChange(e)}
                  data={employeesData}
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "employeeId.error")}
                </div>
              </div>
              <div className="col-md-3">
                <div className="form-group">
                  <label htmlFor="">{t("common.remarks")}</label>
                  <textarea
                    type="text"
                    className="form-control"
                    placeholder="Remarks"
                    style={{ height: "30px" }}
                    name="remarks"
                    value={get(formDataLotCreation, "remarks.value")}
                    onChange={(e) =>
                      setFormValueLotCreation("remarks", e.target.value)
                    }
                  />
                  <div className="invalid-feedback">
                    {get(formDataLotCreation, "remarks.error")}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="panel-footer text-center">
            <a href="#" class="btn btn-primary" disabled={selectedItem === "" && selectedItemToEdit === ""} href="javascript:void(0)" onClick={addToList}>
              <i class="zmdi zmdi-playlist-plus"></i> {selectedItemToEdit === "" ? `Add to list` : `Update`}
            </a>{" "}
            <a href="javascript:void(0)" class="btn btn-default" onClick={clearForm}>
              Clear
            </a>{" "}
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Selected List</h3>
              <div class="panel-options ">
                <a href="javascript:void(0)" class="btn btn-default btn-sm" onClick={deleteSelectedItems} disabled={selectedItems.length === 0}>
                  Remove
                </a>
              </div>
            </div>
            <CustomGrid
              gridData={selectedList}
              columns={selectedListColumns}
              isRadio={false}
              isCheckbox={true}
              uniqueId="selectedItemIndex"
              sortColumn="employee"
              isEdit={true}
              isDelete={false}
              defaultRowsPerPage={5}
              onCheckboxChange={onItemsChange}
              setEditId={onEditClickList}
            />
          </div>
        </div>
      </div>
      <div class="panel-footer text-center">
        {" "}
        <a href="javascript:void(0)" disabled={selectedList.length === 0} class="btn btn-primary " onClick={onSaveClick} >
          Save
        </a>{" "}
        <a href="javascript:void(0)" disabled={selectedList.length === 0} class="btn btn-default " onClick={onClearClick} >
          Clear
        </a>{" "}
      </div>
      <div
        className="modal fade"
        id="addNew"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="myModalLabel"
      >
        <StoneDetailsLot />
      </div>
    </div >
  );
};

export default withTranslation()(GemStoneLot);
