import React, { Component, useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { makeStyles } from "@material-ui/core";
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";

import { metalLotSearchForm, metalLotCreationForm } from "../Constants";
import { fetchSuppliers } from "../../../redux/actions/Admin/supplierActions";
import {
  fetchMainGroups,
  fetchProductsByMainGroup,
  fetchPuritiesByMainGroup,
} from "../../../redux/actions/mainGroupActions";
import { fetchEmployees } from '../../../redux/actions/Admin/employeeActions';
import { fetchMetalLotInvoices, fetchMetalLotDetails, saveMetalLot } from "../../../redux/actions/transactions/barcode/metalLotActions";
import {
  fetchSubProductsByProduct,
} from "../../../redux/actions/creations/productActions";
import {
  fetchBrands,
} from "../../../redux/actions/creations/brandActions";
import StoneDetailsLot from "../StoneDetails/StoneDetailsLot";
import "../../../assets/css/search-select.css";
import CustomGrid from "../../../components/CustomGrid";
import Select from "../../../components/Form/Select";
import SearchSelect from "../../../components/Form/SearchSelect";
import DatePicker from "../../../components/Form/DatePicker";

const useStyles = makeStyles((theme) => ({}));

export const MetalLot = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const mainGroups = useSelector((state) => state.MainGroupReducer.allGroups);
  const products = useSelector(
    (state) => state.MainGroupReducer.allProductsByMainGroup
  );
  const subProducts = useSelector(
    (state) => state.productReducer.allSubProductsByProduct
  );
  const purities = useSelector(
    (state) => state.MainGroupReducer.allPuritiesByMainGroup
  );

  const suppliers = useSelector((state) => state.supplierReducer.allSuppliers);

  const employees = useSelector(state => state.employeeReducer.allEmployees);

  const allMetalLotsInvoices = useSelector(state => state.metalLotReducer.allMetalLotsInvoices);
  const allMetalLotDetails = useSelector(state => state.metalLotReducer.allMetalLotDetails);
  const metalLotInfo = useSelector(state => state.metalLotReducer.metalLotInfo);

  const purchaseDetailsColumns = [
    {
      id: "purchaseDate",
      label: "Date",
      isEdit: false,
      isSort: true,
    },
    {
      id: "invoiceNo",
      label: "Invoice No",
      isEdit: false,
      isSort: true,
    },
    {
      id: "receiptNo",
      label: "RC No",
      isEdit: false,
      isSort: true,
    },
    {
      id: "supplier",
      label: "Supplier",
      isEdit: false,
      isSort: true,
    },
    {
      id: "groupName",
      label: "Main Category",
      isEdit: false,
      isSort: true,
    },
    {
      id: "productName",
      label: "Product Name",
      isEdit: false,
      isSort: true,
    },
    {
      id: "quantity",
      label: "QTY",
      isEdit: false,
      isSort: true,
    },
    {
      id: "grossWeight",
      label: "Gross Weight",
      isEdit: false,
      isSort: true,
    },
    {
      id: "stoneWeight",
      label: "ST.Weight",
      isEdit: false,
      isSort: true,
    },
    {
      id: "netWeight",
      label: "Net Weight",
      isEdit: false,
      isSort: true,
    },
  ];


  const selectedListColumns = [
    {
      id: "employee",
      label: "Employee",
      isEdit: false,
      isSort: true,
    },
    {
      id: "receiptNo",
      label: "RC No",
      isEdit: false,
      isSort: true,
    },
    {
      id: "groupName",
      label: "Main Category",
      isEdit: false,
      isSort: true,
    },
    {
      id: "productName",
      label: "Product Name",
      isEdit: false,
      isSort: true,
    },
    {
      id: "quantity",
      label: "QTY",
      isEdit: false,
      isSort: true,
    },
    {
      id: "grossWeight",
      label: "Gross Weight",
      isEdit: false,
      isSort: true,
    },
    {
      id: "stoneWeight",
      label: "ST.Weight",
      isEdit: false,
      isSort: true,
    },
    {
      id: "netWeight",
      label: "Net Weight",
      isEdit: false,
      isSort: true,
    },
  ];

  const { t } = props;
  const [formData, setFromData] = useState(cloneDeep(metalLotSearchForm));
  const [formDataLotCreation, setFormDataLotCreation] = useState(
    cloneDeep(metalLotCreationForm)
  );
  const [selectedList, setSelectedList] = useState([]);
  const [selectedStoneDetails, setSelectedStoneDetails] = useState([]);
  const [selectedItem, setSelectedItem] = useState("");
  const [metalLotPurchaseDetails, setMetalLotPurchaseDetails] = useState([]);
  const [selectedItems, setSelectedItems] = useState([]);
  const [selectedItemIndexValue, setSelectedItemIndexValue] = useState(0);
  const [selectedItemToEdit, setSelectedItemToEdit] = useState("");
  const [allMetalLotsInvoicesData, setAllMetalLotsInvoicesData] = useState([]);

  const clearForm = () => {
    formDataLotCreation["mainGroupId"]["value"] = "";
    formDataLotCreation["productId"]["value"] = "";
    formDataLotCreation["purityId"]["value"] = "";
    formDataLotCreation["subProductId"]["value"] = "";
    formDataLotCreation["qty"]["value"] = "";
    formDataLotCreation["grossWeight"]["value"] = "";
    formDataLotCreation["stoneWeight"]["value"] = "";
    formDataLotCreation["netWeight"]["value"] = "";
    formDataLotCreation["remarks"]["value"] = "";
    formDataLotCreation["employeeId"]["value"] = "";
    setSelectedStoneDetails([]);
    setSelectedItem("");
    setSelectedItemToEdit("");
    setFormDataLotCreation({ ...formDataLotCreation });
  }

  useEffect(() => {
    const calculateStoneWeight = (data) => {
      let weight = 0;
      data.forEach(item => {
        weight += item.weightPerGram || 0 + item.weightPerKarat || 0;
      });
      return weight;
    }

    const bindDataToStone = (data) => {
      data.forEach(item => {
        item.stoneName = item.stone.stoneName;
        item.stoneGroupName = item.stone.stoneGroup.groupName;
      })
      return data;
    };

    let metalLotPurchaseDetailsData = [];
    if (allMetalLotDetails.data) {
      let i = 0;
      const dataToBind = { ...allMetalLotDetails.data };
      dataToBind.productDetails.forEach(val1 => {
        i += 1;
        const item = {};
        item.invoiceNo = dataToBind.invoiceNo;
        item.purchaseDate = dataToBind.invoiceDate;
        item.isDeleted = dataToBind.isDeleted;
        item.tenantCode = dataToBind.tenantCode;
        item.purchaseType = dataToBind.purchaseType;
        item.partyId = dataToBind.partyId;
        item.mainGroup = val1.subProduct.product.mainGroup.id;
        item.groupName = val1.subProduct.product.mainGroup.groupName;
        item.productName = val1.subProduct.product.productName;
        item.stoneDetails = bindDataToStone(val1.stoneDetails);
        item.quantity = val1.quantity - parseInt(val1.createdLotQuantity);
        item.grossWeight = `${val1.grossWeight - parseFloat(val1.createdLotWeight)}g`;
        item.stoneWeight = `${calculateStoneWeight(val1.stoneDetails)}g`
        item.netWeight = `${val1.grossWeight - val1.createdLotWeight - calculateStoneWeight(val1.stoneDetails)}g`;
        item.productId = val1.subProduct.product.id;
        item.subProductId = val1.subProduct.id;
        item.purityId = val1.purity.id;
        item.productSizeId = val1.productSize.id;
        item.id = dataToBind.id;
        item.indexVal = i;
        item.receiptNo = dataToBind.receiptNo;
        item.purchaseId = val1.id;
        item.supplier = suppliersData.filter(val => val.id === get(formData, "partyNameId.value"))[0].name;
        metalLotPurchaseDetailsData.push(item);
      });
      setMetalLotPurchaseDetails(metalLotPurchaseDetailsData);
    }
  }, [allMetalLotDetails]);

  const setFormValue = (field, value) => {
    formData[field]["value"] = value;
    setFromData({ ...formData });
  };

  const onInvoiceChange = (e) => {
    setFormValue("invoiceId", e.value);
  }

  const setFormValueLotCreation = (field, value) => {
    formDataLotCreation[field]["value"] = value;
    setFormDataLotCreation({ ...formDataLotCreation });
  };

  const suppliersData = suppliers.data
    ? suppliers.data.map((supplier) => {
      return {
        id: supplier.id,
        name: supplier.partyName,
      };
    })
    : [];
  const mainGroupData = mainGroups.data
    ? mainGroups.data.map((group) => {
      return {
        id: group.id,
        name: group.groupName,
        shortCode: group.shortCode,
      };
    })
    : [];
  const productsData = products.data
    ? products.data.map((product) => {
      return {
        id: product.id,
        name: product.productName,
        shortCode: product.shortCode,
      };
    })
    : [];
  const subProductsData = subProducts.data
    ? subProducts.data.map((subProduct) => {
      return {
        id: subProduct.id,
        name: subProduct.subProductName,
        shortCode: subProduct.shortCode,
      };
    })
    : [];
  const puritiesData = purities.data
    ? purities.data.map((purity) => {
      return {
        id: purity.id,
        name: purity.purity,
      };
    })
    : [];

  const employeesData = employees.data ? employees.data.map((employee) => {
    return {
      id: employee.id,
      name: employee.firstName + " " + employee.middleName + " " + employee.lastName,
    }
  }) : [];

  const validateField = (fieldObj) => {
    let errorObj = { isValid: true, error: null }
    if (fieldObj.required && !fieldObj.value && fieldObj.value !== 0) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
    else if (fieldObj.value && parseInt(fieldObj.value) > parseInt(fieldObj.maxValue)) { errorObj.isValid = false; errorObj.error = `Value should be less than ${fieldObj.maxValue}` }
    else if (fieldObj.value && parseInt(fieldObj.value) < parseInt(fieldObj.minValue)) { errorObj.isValid = false; errorObj.error = `Value should be greater than ${fieldObj.minValue}` }
    else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
    else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }
    return errorObj;
  }

  useEffect(() => {
    dispatch(fetchMainGroups());
    dispatch(fetchSuppliers());
    dispatch(fetchBrands());
    dispatch(fetchEmployees());
  }, [dispatch]);

  useEffect(() => {
    dispatch(
      fetchProductsByMainGroup(get(formDataLotCreation, "mainGroupId.value"))
    );
    dispatch(
      fetchPuritiesByMainGroup(get(formDataLotCreation, "mainGroupId.value"))
    );
  }, [get(formDataLotCreation, "mainGroupId.value")]);

  useEffect(() => {
    dispatch(
      fetchSubProductsByProduct(get(formDataLotCreation, "productId.value"))
    );
  }, [get(formDataLotCreation, "productId.value")]);

  useEffect(() => {
    if (get(formData, 'partyNameId.value') !== "" && get(formData, "date.value") !== null && get(formData, "purchaseType.value") !== "") {
      dispatch(fetchMetalLotInvoices(get(formData, 'partyNameId.value'), get(formData, "purchaseType.value"), get(formData, "date.value")));
    }
  }, [get(formData, 'partyNameId.value'), get(formData, "date.value"), get(formData, "purchaseType.value")]);

  useEffect(() => {
    let weight = 0;
    selectedStoneDetails.forEach(item => {
      weight += item.weightPerGram || 0 + item.weightPerKarat || 0;
    });
    setFormValueLotCreation("stoneWeight", weight);
    setFormValueLotCreation("netWeight", get(formDataLotCreation, "grossWeight.value") - weight);

  }, [selectedStoneDetails, get(formDataLotCreation, "grossWeight.value")]);

  const onSelectChangeSearch = (e) => {
    setFormValue(e.nativeEvent.target.name, e.target.value);
  };

  const onSelectChange = (e) => {
    setFormValueLotCreation(e.nativeEvent.target.name, e.target.value);
  };

  const onFilterClick = () => {
    let isFormValid = true;
    map(formData, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      formData[key] = { ...value, ...isValid };
    });
    setFromData({ ...formData });
    if (!isFormValid)
      return;
    clearForm();
    dispatch(fetchMetalLotDetails(get(formData, "invoiceId.value")));
  }

  const onChangePurchaseItem = (item) => {
    setSelectedItemToEdit("");
    const selectedItem = metalLotPurchaseDetails.filter(val => val.indexVal === item)[0];
    setSelectedItem(selectedItem);
    formDataLotCreation["mainGroupId"]["value"] = selectedItem.mainGroup;
    formDataLotCreation["productId"]["value"] = selectedItem.productId;
    formDataLotCreation["purityId"]["value"] = selectedItem.purityId;
    formDataLotCreation["subProductId"]["value"] = selectedItem.subProductId;
    formDataLotCreation["qty"]["value"] = selectedItem.quantity;
    formDataLotCreation["qty"]["maxValue"] = selectedItem.quantity;
    formDataLotCreation["qty"]["minValue"] = "0";
    formDataLotCreation["grossWeight"]["value"] = selectedItem.grossWeight.replace('g', '');
    formDataLotCreation["grossWeight"]["maxValue"] = selectedItem.grossWeight.replace('g', '');
    formDataLotCreation["grossWeight"]["minValue"] = "0";
    formDataLotCreation["stoneWeight"]["value"] = selectedItem.stoneWeight.replace('g', '');
    formDataLotCreation["stoneWeight"]["maxValue"] = selectedItem.stoneWeight.replace('g', '');
    formDataLotCreation["stoneWeight"]["minValue"] = "0";
    formDataLotCreation["netWeight"]["value"] = selectedItem.netWeight.replace('g', '');
    setSelectedStoneDetails(selectedItem.stoneDetails);
    setFormDataLotCreation({ ...formDataLotCreation });
  };

  const addToList = () => {
    let isFormValid = true;
    map(formDataLotCreation, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      formDataLotCreation[key] = { ...value, ...isValid };
    });
    setFormDataLotCreation({ ...formDataLotCreation });
    if (!isFormValid)
      return;
    const dataToChange = [...metalLotPurchaseDetails];
    if (selectedItemToEdit === "") {
      const itemSelected = JSON.parse(JSON.stringify(selectedItem));
      const originalQty = parseInt(itemSelected.quantity) ? parseInt(itemSelected.quantity) : 0;
      const originalGrossWeight = parseFloat(itemSelected.grossWeight) ? parseFloat(itemSelected.grossWeight.replace("g", "")) : 0;
      const originalStoneWeight = parseFloat(itemSelected.stoneWeight) ? parseFloat(itemSelected.stoneWeight.replace("g", "")) : 0;
      const originalNetWeight = parseFloat(itemSelected.netWeight) ? parseFloat(itemSelected.netWeight.replace("g", "")) : 0;
      if (itemSelected.rowColorStyle)
        delete itemSelected.rowColorStyle;
      if (itemSelected.isRadioDisable)
        delete itemSelected.isRadioDisable;
      if (get(formDataLotCreation, "qty.value") !== "" && get(formDataLotCreation, "grossWeight.value") !== "" && parseInt(get(formDataLotCreation, "qty.value")) <= originalQty && parseFloat(get(formDataLotCreation, "grossWeight.value")) <= originalGrossWeight && parseFloat(get(formDataLotCreation, "netWeight.value")) <= originalNetWeight && parseFloat(get(formDataLotCreation, "stoneWeight.value")) <= originalStoneWeight) {
        itemSelected.quantity = get(formDataLotCreation, "qty.value");
        itemSelected.grossWeight = `${get(formDataLotCreation, "grossWeight.value")}g`;
        itemSelected.netWeight = `${get(formDataLotCreation, "netWeight.value")}g`;
        itemSelected.stoneWeight = `${get(formDataLotCreation, "stoneWeight.value")}g`;
        itemSelected.employee = employeesData.filter(val => val.id === get(formDataLotCreation, "employeeId.value")).length > 0 ? employeesData.filter(val => val.id === get(formDataLotCreation, "employeeId.value"))[0].name : "";
        itemSelected.employeeId = get(formDataLotCreation, "employeeId.value");
        itemSelected.remarks = get(formDataLotCreation, "remarks.value");
        itemSelected.selectedItemIndex = selectedItemIndexValue + 1;
        setSelectedItemIndexValue(selectedItemIndexValue + 1);
        dataToChange.forEach(item => {
          if (item.indexVal === itemSelected.indexVal) {
            item.quantity = originalQty - parseInt(get(formDataLotCreation, "qty.value"));
            item.grossWeight = `${originalGrossWeight - parseFloat(get(formDataLotCreation, "grossWeight.value"))}g`;
            item.stoneWeight = `${originalStoneWeight - parseFloat(get(formDataLotCreation, "stoneWeight.value"))}g`;
            item.netWeight = `${originalNetWeight - parseFloat(get(formDataLotCreation, "netWeight.value"))}g`;
            if (originalQty - parseInt(get(formDataLotCreation, "qty.value")) === 0 || originalGrossWeight - parseFloat(get(formDataLotCreation, "grossWeight.value")) === 0) {
              item.rowColorStyle = "redColor";
              item.isRadioDisable = true;
            }
            else {
              item.rowColorStyle = "orangeColor";
              item.isRadioDisable = true;
            }
          }
        });
        setSelectedList([...selectedList, itemSelected]);
        setMetalLotPurchaseDetails(dataToChange);
        formDataLotCreation["qty"]["value"] = "";
        formDataLotCreation["grossWeight"]["value"] = "";
        formDataLotCreation["stoneWeight"]["value"] = "";
        formDataLotCreation["netWeight"]["value"] = "";
        formDataLotCreation["remarks"]["value"] = "";
        formDataLotCreation["employeeId"]["value"] = "";
        setSelectedItemToEdit("");
      }
    }
    else {
      const itemSelected = selectedList.filter(val => val.selectedItemIndex === selectedItemToEdit)[0];
      const initialQty = parseInt(dataToChange.filter(val => val.indexVal === itemSelected.indexVal)[0].quantity);
      const initialGrossWeight = parseFloat(dataToChange.filter(val => val.indexVal === itemSelected.indexVal)[0].grossWeight.replace("g", ""));
      const initialNetWeight = parseFloat(dataToChange.filter(val => val.indexVal === itemSelected.indexVal)[0].netWeight.replace("g", ""));
      const initialStoneWeight = parseFloat(dataToChange.filter(val => val.indexVal === itemSelected.indexVal)[0].stoneWeight.replace("g", ""));
      const originalQty = parseInt(itemSelected.quantity) ? parseInt(itemSelected.quantity) : 0;
      const originalGrossWeight = parseFloat(itemSelected.weight) ? parseFloat(itemSelected.grossWeight.replace("g", "")) : 0;
      const originalNetWeight = parseFloat(itemSelected.weight) ? parseFloat(itemSelected.netWeight.replace("g", "")) : 0;
      const originalStoneWeight = parseFloat(itemSelected.weight) ? parseFloat(itemSelected.stoneWeight.replace("g", "")) : 0;
      const currentQty = get(formDataLotCreation, "qty.value") !== "" ? parseInt(get(formDataLotCreation, "qty.value")) : 0;
      const currentGrossWeight = get(formDataLotCreation, "grossWeight.value") !== "" ? parseFloat(get(formDataLotCreation, "grossWeight.value")) : 0;
      const currentNetWeight = get(formDataLotCreation, "netWeight.value") !== "" ? parseFloat(get(formDataLotCreation, "netWeight.value")) : 0;
      const currentStoneWeight = get(formDataLotCreation, "stoneWeight.value") !== "" ? parseFloat(get(formDataLotCreation, "stoneWeight.value")) : 0;
      const grossWeightDeviation = currentGrossWeight - originalGrossWeight;
      const netWeightDeviation = currentNetWeight - originalNetWeight;
      const stoneWeightDeviation = currentStoneWeight - originalStoneWeight;
      const qtyDeviation = currentQty - originalQty;
      const itemsList = [...selectedList];
      if (currentQty !== 0 && currentGrossWeight !== 0 && initialQty - qtyDeviation >= 0 && initialGrossWeight - grossWeightDeviation >= 0 && initialNetWeight - netWeightDeviation >= 0 && initialStoneWeight - stoneWeightDeviation >= 0) {
        itemsList.forEach(item => {
          if (item.selectedItemIndex === itemSelected.selectedItemIndex) {
            item.quantity = get(formDataLotCreation, "qty.value");
            item.grossWeight = `${get(formDataLotCreation, "grossWeight.value")}g`;
            item.netWeight = `${get(formDataLotCreation, "netWeight.value")}g`;
            item.stoneWeight = `${get(formDataLotCreation, "stoneWeight.value")}g`;
            item.employee = employeesData.filter(val => val.id === get(formDataLotCreation, "employeeId.value")).length > 0 ? employeesData.filter(val => val.id === get(formDataLotCreation, "employeeId.value"))[0].name : "";
            item.employeeId = get(formDataLotCreation, "employeeId.value");
            item.remarks = get(formDataLotCreation, "remarks.value");
          }
        });
        dataToChange.forEach(item => {
          if (item.indexVal === itemSelected.indexVal) {
            item.quantity = initialQty - qtyDeviation;
            item.grossWeight = `${initialGrossWeight - grossWeightDeviation}g`;
            item.netWeight = `${initialNetWeight - netWeightDeviation}g`;
            item.stoneWeight = `${initialStoneWeight - stoneWeightDeviation}g`;
            if (initialQty - qtyDeviation === 0 || initialGrossWeight - grossWeightDeviation === 0) {
              item.rowColorStyle = "redColor";
              item.isRadioDisable = true;
            }
            else {
              item.rowColorStyle = "orangeColor";
              item.isRadioDisable = true;
            }
          }
        });
        setSelectedList(itemsList);
        setMetalLotPurchaseDetails(dataToChange);
        formDataLotCreation["qty"]["value"] = "";
        formDataLotCreation["grossWeight"]["value"] = "";
        formDataLotCreation["stoneWeight"]["value"] = "";
        formDataLotCreation["netWeight"]["value"] = "";
        formDataLotCreation["remarks"]["value"] = "";
        formDataLotCreation["employeeId"]["value"] = "";
        setSelectedItemToEdit("");
      }
    }
  }

  const onStoneDetailsEdited = (data) => {
    setSelectedStoneDetails(data);
  };

  const onItemsChange = (items) => {
    setSelectedItems(items);
  };


  const onEditClickList = (val) => {
    setSelectedItemToEdit(val);
    const selectedItem = selectedList.filter(item => item.selectedItemIndex === val)[0];
    formDataLotCreation["mainGroupId"]["value"] = selectedItem.mainGroup;
    formDataLotCreation["productId"]["value"] = selectedItem.productId;
    formDataLotCreation["purityId"]["value"] = selectedItem.purityId;
    formDataLotCreation["subProductId"]["value"] = selectedItem.subProductId;
    formDataLotCreation["qty"]["value"] = selectedItem.quantity;
    formDataLotCreation["grossWeight"]["value"] = selectedItem.grossWeight.replace('g', '');
    formDataLotCreation["stoneWeight"]["value"] = selectedItem.stoneWeight.replace('g', '');
    formDataLotCreation["netWeight"]["value"] = selectedItem.netWeight.replace('g', '');
    setSelectedStoneDetails(selectedItem.stoneDetails);
    formDataLotCreation["employeeId"]["value"] = selectedItem.employeeId;
    formDataLotCreation["remarks"]["value"] = selectedItem.remarks;
    setFormDataLotCreation({ ...formDataLotCreation });
  }

  const getUpdatedQty = (val, id, items) => {
    let changedQty = parseInt(val);
    items.filter(value => value.indexVal === id).forEach(item => {
      changedQty += parseInt(item.quantity);
    })
    return changedQty;
  };

  const getUpdatedGrossWeight = (val, id, items) => {
    let changedWeight = parseFloat(val.replace('g', ''));
    items.filter(value => value.indexVal === id).forEach(item => {
      changedWeight += parseFloat(item.grossWeight.replace('g', ''));
    })
    return `${changedWeight}g`;
  };

  const getUpdatedNetWeight = (val, id, items) => {
    let changedWeight = parseFloat(val.replace('g', ''));
    items.filter(value => value.indexVal === id).forEach(item => {
      changedWeight += parseFloat(item.netWeight.replace('g', ''));
    })
    return `${changedWeight}g`;
  };

  const getUpdatedStoneWeight = (val, id, items) => {
    let changedWeight = parseFloat(val.replace('g', ''));
    items.filter(value => value.indexVal === id).forEach(item => {
      changedWeight += parseFloat(item.stoneWeight.replace('g', ''));
    })
    return `${changedWeight}g`;
  };

  const deleteSelectedItems = () => {
    const selectedDataToRemove = selectedList.filter(val => selectedItems.indexOf(val.selectedItemIndex) >= 0);
    const dataToChange = [...metalLotPurchaseDetails];
    const filteredData = selectedList.filter(val => selectedItems.indexOf(val.selectedItemIndex) < 0);
    dataToChange.forEach(item => {
      item.quantity = getUpdatedQty(item.quantity, item.indexVal, selectedDataToRemove);
      item.grossWeight = getUpdatedGrossWeight(item.grossWeight, item.indexVal, selectedDataToRemove);
      item.netWeight = getUpdatedNetWeight(item.netWeight, item.indexVal, selectedDataToRemove);
      item.stoneWeight = getUpdatedStoneWeight(item.stoneWeight, item.indexVal, selectedDataToRemove);
      if (item.quantity === 0 || parseFloat(item.grossWeight.replace('g', '')) === 0) {
        item.rowColorStyle = "redColor";
        item.isRadioDisable = true;
      }
      else if (item.quantity !== 0 && parseFloat(item.grossWeight.replace('g', '')) !== 0) {
        if (filteredData.filter(val => val.indexVal === item.indexVal).length > 0) {
          item.rowColorStyle = "orangeColor";
          if (item.isRadioDisable)
            delete item.isRadioDisable;
        }
        else {
          if (item.rowColorStyle)
            delete item.rowColorStyle;
          if (item.isRadioDisable)
            delete item.isRadioDisable;
        }
      }

    });
    setMetalLotPurchaseDetails(dataToChange);
    setSelectedList(filteredData);
    setSelectedItems([]);
  };


  const onSaveClick = () => {
    const dataToInsert = [...selectedList];
    dataToInsert.forEach(val => {
      val.netWeight = parseFloat(val.netWeight.replace("g", ""));
      val.grossWeight = parseFloat(val.grossWeight.replace("g", ""));
      val.stoneWeight = parseFloat(val.stoneWeight.replace("g", ""));
    })
    dispatch(saveMetalLot(dataToInsert));
  };

  useEffect(() => {
    if (allMetalLotsInvoices.data)
      setAllMetalLotsInvoicesData(allMetalLotsInvoices.data.map((invc) => {
        return {
          value: invc.recieptNo,
          label: invc.invoiceNo === null ? invc.recieptNo : invc.recieptNo + " - " + invc.invoiceNo,
        }
      }));
  }, [allMetalLotsInvoices]);

  const onClearClick = () => {
    clearForm();
    setSelectedList([]);
    setMetalLotPurchaseDetails([]);
    formData["purchaseType"]["value"] = "";
    formData["partyNameId"]["value"] = "";
    formData["date"]["value"] = null;
    formData["invoiceId"]["value"] = "";
    setAllMetalLotsInvoicesData([]);
  }


  useEffect(() => {
    if (metalLotInfo && metalLotInfo.status && metalLotInfo.status.toLowerCase() === "success") {
      onClearClick();
    }
  }, [metalLotInfo]);

  return (
    <div className="panel panel-default hero-panel">
      <div className="panel-heading">
        <h1 className="panel-title">
          {t("transactions.metalLot")} {t("common.creation")}
        </h1>
      </div>
      <div className="panel-body">
        <div className="panel-body p-t-0">
          <div class="bg-light p-15 p-b-0">
            <div className="row">
              <div className="col-md-3">
                <label for="">Purchase Type</label>
                <div className="form-group no-min-height">
                  <div className="radio radio-inline radio-primary m-b-0">
                    <input
                      type="radio"
                      name="purchaseType"
                      value="Approval"
                      checked={
                        get(formData, "purchaseType.value") === "Approval"
                      }
                      onChange={(e) => setFormValue("purchaseType", "Approval")}
                    />
                    <label htmlFor="">{t("transactions.approval")}</label>
                  </div>
                  <div className="radio radio-inline radio-primary m-b-0">
                    <input
                      type="radio"
                      name="purchaseType"
                      value="Invoice"
                      checked={
                        get(formData, "purchaseType.value") === "Invoice"
                      }
                      onChange={(e) => setFormValue("purchaseType", "Invoice")}
                    />
                    <label htmlFor="">{t("transactions.invoice")}</label>
                  </div>
                  <div className="invalid-feedback">
                    {get(formData, "purchaseType.error")}
                  </div>
                </div>
              </div>
              <div className="form-group col-md-2">
                <label htmlFor="">{t("common.supplier")}</label>
                <Select
                  name="partyNameId"
                  value={get(formData, "partyNameId.value")}
                  onChange={(e) => onSelectChangeSearch(e)}
                  data={suppliersData}
                  placeholder={t("common.supplier")}
                />
                <div className="invalid-feedback">
                  {get(formData, "partyNameId.error")}
                </div>
              </div>
              <div className="form-group col-md-4">
                <label htmlFor="">{t("common.date")}</label>
                <DatePicker
                  value={get(formData, "date.value")}
                  onChange={(value) => setFormValue("date", value)}
                />
                <div className="invalid-feedback">
                  {get(formData, "date.error")}
                </div>
              </div>
              <div className="form-group col-md-2">
                <label htmlFor="">
                  {t("transactions.invoice")} / {t("transactions.rcNo")}
                </label>
                <SearchSelect options={allMetalLotsInvoicesData} placeholder="Option"
                  value={allMetalLotsInvoicesData.filter(val => val.value === get(formData, "invoiceId.value"))}
                  onChange={(e) => onInvoiceChange(e)}
                  name="invoiceId" />
                <div className="invalid-feedback">
                  {get(formData, "invoiceId.error")}
                </div>
              </div>
              <div className="form-group col-md-1">
                <button onClick={onFilterClick} class="btn btn-primary btn-sm m-t-22">Filter</button>
              </div>
            </div>
          </div>
          <div class="border p-15 m-b-20">
            <h3 class="panel-title">
              {t("transactions.purchase")} {t("common.details")}
            </h3>
            <CustomGrid
              gridData={metalLotPurchaseDetails}
              columns={purchaseDetailsColumns}
              isRadio={true}
              isCheckbox={false}
              uniqueId="indexVal"
              sortColumn="receiptNo"
              isEdit={false}
              isDelete={false}
              defaultRowsPerPage={5}
              fnOnChangeRadioItem={onChangePurchaseItem}
            />
          </div>
          <div class="">
            <div class="">
              <h4 class="panel-title">{t("common.details")}</h4>
            </div>
            <div className="row">
              <div className="col-md-3">
                <label htmlFor="">{t("transactions.mainGroup")}</label>
                <Select
                  name="mainGroupId"
                  value={get(formDataLotCreation, "mainGroupId.value")}
                  onChange={(e) => onSelectChange(e)}
                  data={mainGroupData}
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "mainGroupId.error")}
                </div>
              </div>
              <div className="col-md-3">
                <label htmlFor="">{t("transactions.productName")}</label>
                <Select
                  name="productId"
                  value={get(formDataLotCreation, "productId.value")}
                  onChange={(e) => onSelectChange(e)}
                  data={productsData}
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "productId.error")}
                </div>
              </div>
              <div className="col-md-2">
                <label htmlFor="">{t("common.Purity")}</label>
                <Select
                  name="purityId"
                  value={get(formDataLotCreation, "purityId.value")}
                  onChange={(e) => onSelectChange(e)}
                  data={puritiesData}
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "purityId.error")}
                </div>
              </div>
              <div className="col-md-2">
                <label htmlFor="">{t("transactions.subProduct")}</label>
                <Select
                  name="subProductId"
                  value={get(formDataLotCreation, "subProductId.value")}
                  onChange={(e) => onSelectChange(e)}
                  data={subProductsData}
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "subProductId.error")}
                </div>
              </div>
              <div className="form-group col-md-2">
                <label htmlFor="">{t("transactions.qty")}</label>
                <input
                  type="number"
                  className="form-control"
                  placeholder="Qty"
                  name="qty"
                  value={get(formDataLotCreation, "qty.value")}
                  onChange={(e) =>
                    setFormValueLotCreation("qty", e.target.value)
                  }
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "qty.error")}
                </div>
              </div>
              <div className="form-group col-md-2">
                <label htmlFor="">
                  {t("transactions.gross")} {t("transactions.weight")}
                </label>
                <input
                  type="number"
                  className="form-control"
                  placeholder="Gross Weight"
                  name="grossWeight"
                  value={get(formDataLotCreation, "grossWeight.value")}
                  onChange={(e) =>
                    setFormValueLotCreation("grossWeight", e.target.value)
                  }
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "grossWeight.error")}
                </div>
              </div>
              <div className="col-md-2">
                <div className="form-group">
                  <label htmlFor="">
                    {t("common.stone")} {t("common.Weight")}
                    {get(formDataLotCreation, "stoneWeight.value") > 0 ?
                      <a
                        href="#"
                        id="StoneDetailsLot"
                        data-toggle="modal"
                        data-target="#addNew"
                      >
                        <i className="zmdi zmdi-plus-circle f-s-16"></i>
                      </a> : ``}
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Stone Weight"
                    name="stoneWeight"
                    disabled
                    value={get(formDataLotCreation, "stoneWeight.value")}
                  />
                  <div className="invalid-feedback">
                    {get(formDataLotCreation, "stoneWeight.error")}
                  </div>
                </div>
              </div>
              <div className="form-group col-md-2">
                <label htmlFor="">
                  {t("transactions.net")} {t("transactions.weight")}
                </label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Net Weight"
                  name="netWeight"
                  disabled
                  value={get(formDataLotCreation, "netWeight.value")}
                  onChange={(e) =>
                    setFormValueLotCreation("netWeight", e.target.value)
                  }
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "netWeight.error")}
                </div>
              </div>
              <div className="col-md-2">
                <label htmlFor="">{t("common.employee")}</label>
                <Select
                  name="employeeId"
                  value={get(formDataLotCreation, "employeeId.value")}
                  onChange={(e) => onSelectChange(e)}
                  data={employeesData}
                />
                <div className="invalid-feedback">
                  {get(formDataLotCreation, "employeeId.error")}
                </div>
              </div>
              <div className="col-md-2">
                <div className="form-group">
                  <label htmlFor="">{t("common.remarks")}</label>
                  <textarea
                    type="text"
                    className="form-control"
                    placeholder="Remarks"
                    style={{ height: "30px" }}
                    name="remarks"
                    value={get(formDataLotCreation, "remarks.value")}
                    onChange={(e) =>
                      setFormValueLotCreation("remarks", e.target.value)
                    }
                  />
                  <div className="invalid-feedback">
                    {get(formDataLotCreation, "remarks.error")}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="panel-footer text-center">
            <a class="btn btn-primary" disabled={selectedItem === "" && selectedItemToEdit === ""} href="javascript:void(0)" onClick={addToList}>
              <i class="zmdi zmdi-playlist-plus"></i> {selectedItemToEdit === "" ? `Add to list` : `Update`}
            </a>{" "}
            <a href="javascript:void(0)" class="btn btn-default" onClick={clearForm}>
              Clear
            </a>{" "}
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Selected List</h3>
              <div class="panel-options ">
                <a href="javascript:void(0)" class="btn btn-default btn-sm" onClick={deleteSelectedItems} disabled={selectedItems.length === 0}>
                  Remove
                </a>
              </div>
            </div>
            <CustomGrid
              gridData={selectedList}
              columns={selectedListColumns}
              isRadio={false}
              isCheckbox={true}
              uniqueId="selectedItemIndex"
              sortColumn="employee"
              isEdit={true}
              isDelete={false}
              defaultRowsPerPage={5}
              onCheckboxChange={onItemsChange}
              setEditId={onEditClickList}
            />
          </div>
        </div>
      </div>
      <div class="panel-footer text-center">
        {" "}
        <a href="javascript:void(0)" disabled={selectedList.length === 0} class="btn btn-primary " onClick={onSaveClick} >
          Save
        </a>{" "}
        <a href="javascript:void(0)" disabled={selectedList.length === 0} class="btn btn-default " onClick={onClearClick} >
          Clear
        </a>{" "}
      </div>
      <div
        className="modal fade"
        id="addNew"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="myModalLabel"
      >
        <StoneDetailsLot stoneDetails={selectedStoneDetails} onStoneDetailsChange={onStoneDetailsEdited} />
      </div>
    </div>
  );
};

export default withTranslation()(MetalLot);
