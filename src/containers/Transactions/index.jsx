import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import Menu from "./Menu";
import { get } from "lodash";
import MainGroup from './Billing/MainGroup';
import Product from './Billing/Product'
import SubProduct from './Billing/SubProduct'
import ProductSize from './Billing/ProductSize'
import SaleWastage from './Billing/SaleWastage'
import PartyWastage from './Billing/PartyWastage'
import MetalPurchase from './Inventory/MetalPurchase'
import BrandedPurchase from './Inventory/BrandedPurchase'
import GemstonePurchase from './Inventory/GemstonePurchase'
import MetalLot from './Barcode/MetalLot';
import BrandedLot from './Barcode/BrandedLot';
import GemStoneLot from './Barcode/GemStoneLot';
import MetalBarcode from './Barcode/MetalBarcode';
import BrandedBarcode from './Barcode/BrandedBarcode';
import GemstoneBarcode from './Barcode/GemstoneBarcode';
import DeleteBarcode from './Barcode/DeleteBarcode';
import ReprintBarcode from './Barcode/ReprintBarcode';

export const Transactions = (props) => {
  const { t } = props;
  const pathname = get(props, "history.location.pathname");
  const [viewMenu, setMenuView] = React.useState(true);
  const renderRightSideComponent = () => {
    switch (pathname) {
      case "/transactions/mainGroup":
        return <MainGroup />;
      case "/transactions/product/name":
        return <Product />;
      case "/transactions/product/sub":
        return <SubProduct />;
      case "/transactions/product/size":
        return <ProductSize />;
      case "/transactions/product/sale-wastage":
        return <SaleWastage />;
      case "/transactions/product/party-wastage":
        return <PartyWastage />;
      case "/transactions/inventory/metal-purchase":
        return <MetalPurchase />;
      case "/transactions/inventory/branded-purchase":
        return <BrandedPurchase />;
      case "/transactions/inventory/gemstone-purchase":
        return <GemstonePurchase />;
      case "/transactions/barcode/metal-lot":
        return <MetalLot />;
      case "/transactions/barcode/branded-lot":
        return <BrandedLot />;
      case "/transactions/barcode/gemstone-lot":
        return <GemStoneLot />;
      case "/transactions/barcode/metal-barcode":
        return <MetalBarcode />;
      case "/transactions/barcode/branded-barcode":
        return <BrandedBarcode />;
      case "/transactions/barcode/gemstone-barcode":
        return <GemstoneBarcode />;
      case "/transactions/barcode/delete":
        return <DeleteBarcode />;
      case "/transactions/barcode/reprint":
        return <ReprintBarcode />;
      default:
        return <div>{pathname}</div>;
    }
  };
  const onToggleMenu = () => {
    const menuToggle = !viewMenu;
    console.log({ menuToggle });
    setMenuView(menuToggle)
  }
  return (
    <div className="innerContainer">
      <div className={viewMenu ? "innerLeftNav" : "innerLeftNav clicked"}>
        <Menu pathname={pathname} onToggleMenu={onToggleMenu} />
      </div>
      <div className={viewMenu ? "innerRightCntnt" : "innerRightCntnt widthChange"}>
        <a onClick={() => { onToggleMenu() }} className="btn-collapse">
          <i className={viewMenu ? "zmdi zmdi-chevron-left" : "zmdi zmdi-chevron-right"}></i>
        </a>
        {renderRightSideComponent()}
      </div>
    </div>
  );
};

export default withTranslation()(Transactions);
