import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { stoneDetailsForm, stoneDetailsColumns } from "../Constants";
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import {
  saveDetails,
  editDetails,
  deleteDetails,
  saveStoneDetailsForm
} from "../../../redux/actions/transactions/stoneDetailsActions";
import { fetchStoneGroups, fetchStonesByStoneGroup, fetchStoneSizesByStoneGroup } from '../../../redux/actions/creations/stoneGroupActions'
import { fetchStoneTypes } from '../../../redux/actions/creations/stoneTypeActions'
import { fetchStoneColors } from '../../../redux/actions/creations/stoneColorActions'
import { fetchStoneClarities } from '../../../redux/actions/creations/stoneClarityActions'
import { fetchStonePolishes } from '../../../redux/actions/creations/stonePolishActions'
import { fetchPartyStoneRates } from "../../../redux/actions/creations/partyStoneRateActions";
import { UOM_TYPE, GRAM_TO_KARAT, KARAT_TO_GRAM, GST_PERCENT } from "../../../utils/commonConstants";
import UOMElement from "../../../components/UOMElement";
import Grid from "../../../components/Grid";
import Select from "../../../components/Form/Select";
import $ from 'jquery'; // <-to import jquery



import Swal from "sweetalert2";

export const StoneDetails = (props) => {
  const { t, showCreateForm = true } = props;
  const { fetchPartyNameById, partyId } = props;
  const [stoneForm, setStoneForm] = useState(cloneDeep(stoneDetailsForm));
  const [save, setSave] = useState(false);
  const [viewForm, setToggleView] = useState(true);
  const [showUpdate, setShowUpdate] = useState(false);
  let [isStoneGroupDiamond, setIsStoneGroupDiamond] = useState(false);
  const stoneDetails = useSelector(state => state.stoneDetailsReducer.stoneDetails);
  let [selected, setSelected] = useState([]);
  const dispatch = useDispatch();
  const stoneTypes = useSelector(state => state.stoneTypeReducer.allStoneTypes)
  const stoneGroups = useSelector(state => state.stoneGroupReducer.allStoneGroups)
  const stones = useSelector(state => state.stoneGroupReducer.allStonesByStoneGroup)
  const stoneSizes = useSelector(state => state.stoneGroupReducer.allStoneSizesByStoneGroup)
  const stoneColors = useSelector(state => state.stoneColorReducer.allStoneColors)
  const stoneClarities = useSelector(state => state.stoneClarityReducer.allStoneClarities)
  const stonePolishes = useSelector(state => state.stonePolishReducer.allStonePolishes)


  const stoneGroupData = stoneGroups.data
    ? stoneGroups.data.map((group) => {
      return {
        id: group.id,
        name: group.groupName,
        shortCode: group.shortCode,
        diamond: group.diamond,
      };
    })
    : [];
  const stoneData = stones.data ? stones.data.map((stone) => {
    return {
      id: stone.id,
      name: stone.stoneName,
      shortCode: stone.shortCode
    }
  }) : [];
  const stoneSizeData = stoneSizes.data ? stoneSizes.data.map((size) => {
    return {
      id: size.id,
      name: size.stoneSize,
      shortCode: size.shortCode
    }
  }) : [];
  const stoneColorData = stoneColors.data ? stoneColors.data.map((color) => {
    return {
      id: color.id,
      name: color.colorName,
      shortCode: color.shortCode
    }
  }) : [];
  const stoneClarityData = stoneClarities.data ? stoneClarities.data.map((clarity) => {
    return {
      id: clarity.id,
      name: clarity.clarityName,
      shortCode: clarity.shortCode
    }
  }) : [];
  const stonePolishData = stonePolishes.data ? stonePolishes.data.map((polish) => {
    return {
      id: polish.id,
      name: polish.polishName,
      shortCode: polish.shortCode
    }
  }) : [];

  const isStoneADiamond = (selectedStoneGroupId) => {
    let selectedStoneGroup = stoneGroupData.find(ele => ele.id === selectedStoneGroupId);
    return selectedStoneGroup ? selectedStoneGroup.diamond : false;
  }

  const stoneDetailsData = stoneDetails ? stoneDetails.map((stone) => {
    return {
      id: stone.id,
      stoneTypeId: stone.stoneTypeId,
      stoneType: stone.stoneType,
      stoneGroupId: stone.stoneGroupId,
      stoneGroup: stone.stoneGroup,
      stoneId: stone.stoneId,
      stone: stone.stone,
      isDiamond: isStoneADiamond(stone.stoneGroupId),
      stoneSizeId: stone.stoneSizeId,
      stoneSize: stone.stoneSize,
      stoneColorId: stone.stoneColorId,
      stoneColor: stone.stoneColor,
      stoneClarityId: stone.stoneClarityId,
      stoneClarity: stone.stoneClarity,
      stonePolishId: stone.stonePolishId,
      stonePolish: stone.stonePolish,
      uom: stone.uom,
      quantity: stone.quantity,
      weightPerGram: stone.weightPerGram,
      rate: stone.rate,
      amount: stone.amount,
      isNoWeight: stone.isNoWeight
    }
  }) : [];

  const partyRates = useSelector(state => state.partyStoneRateReducer.allPartyStoneRates)

  const partyRatesData = partyRates.data ? partyRates.data.map((partyRate) => {
    let stoneGroup = partyRate.stone.stoneGroup;
    return {
      id: partyRate.id,
      partyId: partyRate.partyId,
      party: fetchPartyNameById(partyRate.partyId),
      stoneTypeId: stoneGroup.stoneType.id,
      stoneType: stoneGroup.stoneType.typeName,
      groupId: stoneGroup.id,
      stoneGroup: stoneGroup.groupName,
      isDiamond: stoneGroup.diamond,
      stoneId: partyRate.stone.id,
      stone: partyRate.stone.stoneName,
      stoneSizeId: partyRate.stoneSize ? partyRate.stoneSize.id : "",
      stoneSize: partyRate.stoneSize ? partyRate.stoneSize.stoneSize : "",
      stoneColorId: partyRate.stoneColor ? partyRate.stoneColor.id : "",
      stoneColor: partyRate.stoneColor ? partyRate.stoneColor.colorName : "",
      stoneClarityId: partyRate.stoneClarity ? partyRate.stoneClarity.id : "",
      stoneClarity: partyRate.stoneClarity ? partyRate.stoneClarity.clarityName : "",
      stonePolishId: partyRate.stonePolish ? partyRate.stonePolish.id : "",
      stonePolish: partyRate.stonePolish ? partyRate.stonePolish.polishName : "",
      minRate: partyRate.minRate,
      maxRate: partyRate.maxRate,
      uom: partyRate.uom,
      noWeight: partyRate.noWeight,
      fixedRate: partyRate.fixedRate
    }
  }) : [];

  useEffect(() => {
    setStoneFormValue("uom", "")
    setStoneFormValue("rate", 0)
    dispatch(fetchPartyStoneRates(partyId, get(stoneForm, "stoneId.value"), get(stoneForm, "stoneSizeId.value")));
  }, [partyId, get(stoneForm, "stoneId.value"), get(stoneForm, "stoneSizeId.value")])

  useEffect(() => {
    stoneForm.weightPerGram['disabled'] = false;
    stoneForm.weightPerKarat['disabled'] = false;
    let uomValue = get(stoneForm, 'uom.value');
    if (uomValue) {
      if (uomValue === UOM_TYPE.GRAM || uomValue === UOM_TYPE.PIECE) {
        stoneForm.weightPerGram['disabled'] = false;
        stoneForm.weightPerKarat['disabled'] = true;
      } else if (uomValue === UOM_TYPE.KARAT) {
        stoneForm.weightPerKarat['disabled'] = false;
        stoneForm.weightPerGram['disabled'] = true;
      }
    }
    let uom = partyId && get(stoneForm, "stoneGroupId.value") && get(stoneForm, "stoneId.value") && partyRatesData.filter((ele) => ele.uom === uomValue && ele.stoneSizeId === get(stoneForm, "stoneSizeId.value"))[0];
    if (uom) {
      setStoneFormValue("rate", uom.maxRate);
      stoneForm.rate['minValue'] = uom.minRate;
      stoneForm.rate['maxValue'] = uom.maxRate;
      stoneForm.rate['disabled'] = uom.fixedRate ? true : false;
      // setStoneFormData({ ...stoneFormData });
    } else {
      setStoneFormValue("rate", 0)
      stoneForm.rate['disabled'] = false;
    }
  }, [get(stoneForm, 'uom.value')])

  const setStoneFormValue = (field, value) => {
    stoneForm[field]["value"] = value;
    setStoneForm({ ...stoneForm });
  };
  const resetStoneForm = () => {
    setShowUpdate(false);
    map(stoneForm, function (value, key, object) {
      if (key === 'isNoWeight') {
        document.getElementById("isNoWeight").checked = false;
        setStoneFormValue('isNoWeight', false)
      }
      else {
        stoneForm[key]["value"] = "";
      }
    });
    setStoneForm({ ...stoneForm });
    setIsStoneGroupDiamond(false);
  };

  useEffect(() => {
    let quantity = get(stoneForm, 'quantity.value');
    let weightPerGram = get(stoneForm, 'weightPerGram.value');
    let weightPerKarat = get(stoneForm, 'weightPerKarat.value');
    // let weight = get(stoneForm, 'weight.value');
    let rate = get(stoneForm, 'rate.value');
    let uom = get(stoneForm, 'uom.value');
    let amount = 0;
    let gstAmount = 0;
    let totalAmount = amount + gstAmount;
    if(uom) {
      if(uom === UOM_TYPE.PIECE) {
        setStoneFormValue("weightPerKarat", Math.round(weightPerGram * GRAM_TO_KARAT));
        amount = Math.round(quantity * rate);
      } else if(uom === UOM_TYPE.GRAM) {
        setStoneFormValue("weightPerKarat", Math.round(weightPerGram * GRAM_TO_KARAT));
        amount = Math.round(weightPerGram * rate);
      } else if(uom === UOM_TYPE.KARAT) {
        setStoneFormValue("weightPerGram", Math.round(weightPerKarat * KARAT_TO_GRAM));
        amount = Math.round(weightPerKarat * rate);
      }
    }
    gstAmount = Math.round(amount * GST_PERCENT / 100);
    totalAmount = amount + gstAmount;
    setStoneFormValue("amount", amount);
    setStoneFormValue("gstAmount", gstAmount);
    setStoneFormValue("totalAmount", totalAmount);
  }, [get(stoneForm, 'uom.value'), get(stoneForm, 'quantity.value'),get(stoneForm, 'weightPerGram.value'), get(stoneForm, 'weightPerKarat.value')])

  const saveStoneDetails = () => {
    let isFormValid = true;
    let date = new Date();
    let time = date.getTime();
    setStoneFormValue("id", time);
    map(stoneForm, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      stoneForm[key] = { ...value, ...isValid };
      setStoneForm({ ...stoneForm });
    });
    if (isFormValid) {
      let data = {};
      map(stoneForm, function (value, key, object) {
        data = {
          ...data,
          [key]: value.value,
        };
      });
      Swal.fire({
        title: "Confirm Saving Stone Details Info.!",
        text: "Continue to save / Cancel to stop",
        icon: "info",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Continue",
      }).then((result) => {
        if (result.isConfirmed) {
          console.log(data);
          let stoneDetailsData = {
            id: data.id,
            isDeleted: false,
            stone: {
              id: data.stoneId
            },
            stoneSize: {
              id: data.stoneSizeId
            },
            stoneColor: {
              id: data.stoneColorId
            },
            stoneClarity: {
              id: data.stoneClarityId
            },
            stonePolish: {
              id: data.stonePolishId
            },
            quantity: data.quantity,
            uom: data.uom,
            weightPerGram: data.weightPerGram,
            rate: data.rate,
            amount: data.amount,
            isNoWeight: data.isNoWeight
          }
          console.log(stoneDetailsData)
          dispatch(saveDetails(data, stoneDetailsData));
          resetStoneForm();
        }
      });
    }
  };

  const updateStoneDetails = () => {
    let isFormValid = true;
    map(stoneForm, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      stoneForm[key] = { ...value, ...isValid };
      setStoneForm({ ...stoneForm });
    });
    if (isFormValid) {
      let data = {};
      map(stoneForm, function (value, key, object) {
        data = {
          ...data,
          [key]: value.value,
        };
      });
      Swal.fire({
        title: "Confirm Saving Stone Details Info.!",
        text: "Continue to save / Cancel to stop",
        icon: "info",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Continue",
      }).then((result) => {
        if (result.isConfirmed) {
          dispatch(editDetails(data));
          resetStoneForm();
        }
      });
    }
  };

  const validateField = (fieldObj) => {
    let errorObj = { isValid: true, error: null };
    if (fieldObj.required && !fieldObj.value) {
      errorObj.isValid = false;
      errorObj.error = "Please fill this field";
    } else if (fieldObj.value && fieldObj.value.length < fieldObj.min) {
      errorObj.isValid = false;
      errorObj.error = `Please enter min length ${fieldObj.min}`;
    } else if (fieldObj.value && fieldObj.value.length > fieldObj.max) {
      errorObj.isValid = false;
      errorObj.error = `Please enter max length ${fieldObj.max}`;
    }
    return errorObj;
  };

  const deleteStoneDetails = (data) => {
    console.log(data);
    let selectedIds;
    if (data instanceof Array && data.length > 0) {
      selectedIds = data.map((ele) => {
        return ele.id;
      });
    } else {
      selectedIds = data.id;
    }
    Swal.fire({
      title: "Confirm Deleting Stone Details.!",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        console.log(selectedIds);
        dispatch(deleteDetails(selectedIds));
      }
    });
  };

  const editAction = (ele) => {
    setShowUpdate(true);
    setStoneFormValue("id", ele.id);
    setStoneFormValue("stoneGroupId", ele.stoneGroupId);
    setStoneFormValue("stoneGroup", ele.stoneGroup);
    setIsStoneGroupDiamond(ele.isDiamond);
    setStoneFormValue("stoneId", ele.stoneId);
    setStoneFormValue("stone", ele.stone);
    setStoneFormValue("stoneSizeId", ele.stoneSizeId);
    setStoneFormValue("stoneSize", ele.stoneSize);
    setStoneFormValue("stoneColorId", ele.stoneColorId);
    setStoneFormValue("stoneColor", ele.stoneColor);
    setStoneFormValue("stoneClarityId", ele.stoneClarityId);
    setStoneFormValue("stoneClarity", ele.stoneClarity);
    setStoneFormValue("stonePolishId", ele.stonePolishId);
    setStoneFormValue("stonePolish", ele.stonePolish);
    setStoneFormValue("uom", ele.uom);
    setStoneFormValue("quantity", ele.quantity);
    setStoneFormValue("weightPerGram", ele.weightPerGram);
    setStoneFormValue("rate", ele.rate);
    setStoneFormValue("amount", ele.amount);
    document.getElementById("isNoWeight").checked = ele.isNoWeight;
    setStoneFormValue("isNoWeight", ele.isNoWeight);
  };

  const saveForm = () => {
    // let isFormValid = true;
    // let date = new Date();
    // let time = date.getTime();
    // setStoneFormValue("id", time);
    // map(stoneForm, function (value, key, object) {
    //   const isValid = validateField(value);
    //   if (!isValid.isValid) {
    //     isFormValid = false;
    //   }
    //   stoneForm[key] = { ...value, ...isValid };
    //   setStoneForm({ ...stoneForm });
    // });
    // if (isFormValid) {
    //   let data = {};
    //   map(stoneForm, function (value, key, object) {
    //     data = {
    //       ...data,
    //       [key]: value.value,
    //     };
    //   });
    //   saveStoneDetails()
    //   dispatch(saveStoneDetailsForm(true))      
    // }
    if (stoneDetailsData.length > 0) {
      dispatch(saveStoneDetailsForm(true))
      $('.close').click();
    } else {
      Swal.fire({
        title: "Confirm Saving Empty Details.!",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, Save it!",
      }).then((result) => {
        if (result.isConfirmed) {
          $('.close').click();
        }
      });
    }
  }

  useEffect(() => {
    dispatch(fetchStoneTypes());
    dispatch(fetchStoneGroups());
  }, [])


  useEffect(() => {
    setStoneFormValue("uom", "")
    setStoneFormValue("rate", 0)
    dispatch(fetchStonesByStoneGroup(get(stoneForm, 'stoneGroupId.value')));
    dispatch(fetchStoneSizesByStoneGroup(get(stoneForm, 'stoneGroupId.value')));
    if (get(stoneForm, "stoneGroupId.value") && isStoneADiamond(get(stoneForm, "stoneGroupId.value"))) {
      dispatch(fetchStoneColors());
      dispatch(fetchStoneClarities());
      dispatch(fetchStonePolishes());
    }
  }, [get(stoneForm, 'stoneGroupId.value')])



  const stoneGroupOnChange = (e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    resetElementsOnStoneGroupChange();
    setStoneFormValue('stoneGroupId', e.target.value);
    setStoneFormValue('stoneGroup', label);
    setIsStoneGroupDiamond(isStoneADiamond(e.target.value));
  }

  const resetElementsOnStoneGroupChange = () => {
    setStoneFormValue('stoneColorId', '')
    setStoneFormValue('stoneColor', '')
    setStoneFormValue('stoneClarityId', '')
    setStoneFormValue('stoneClarity', '')
    setStoneFormValue('stonePolishId', '')
    setStoneFormValue('stonePolish', '')
  }

  const selectOnChange = (idKey, labelKey, e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    setStoneFormValue(idKey, e.target.value)
    setStoneFormValue(labelKey, label)
  }

  return (
    <div className="modal-dialog modal-lg" role="dialog" id="stoneDetailsModal"  >
      {console.log("{get(stoneForm, 'stoneTypeId.error')} :::: ", get(stoneForm, 'stoneTypeId.error'))}
      <div className="modal-content" >
        <div className="modal-header">
          <button
            type="button"
            className="close"
            data-dismiss="modal"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 className="modal-title" id="myModalLabel">
            {showCreateForm ? `Add Stone Details` : `Stone Details`}
          </h4>
        </div>
        {showCreateForm ? (
          <div className="modal-body">
            <div className="panel panel-default">
              <div className="panel-body">
                <div className="row">
                  <div className="col-md-3 col-lg-2">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("common.stone")} {t("common.group")}<span className="asterisk">*</span>
                      </label>
                      <Select name="stoneGroupId" value={get(stoneForm, 'stoneGroupId.value')} onChange={(e) => stoneGroupOnChange(e)} data={stoneGroupData} />
                      <div className="invalid-feedback">{get(stoneForm, 'stoneGroupId.error')}</div>
                    </div>
                  </div>
                  <div className="col-md-3 col-lg-2">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("common.stone")} {t("common.name")}<span className="asterisk">*</span>
                      </label>
                      <Select name="stoneId" value={get(stoneForm, 'stoneId.value')} onChange={(e) => selectOnChange('stoneId', 'stone', e)} data={stoneData} />
                      <div className="invalid-feedback">{get(stoneForm, 'stoneId.error')}</div>
                    </div>
                  </div>
                  <div className="col-md-3 col-lg-2">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("common.stone")} {t("common.size")}<span className="asterisk">*</span>
                      </label>
                      <Select name="stoneSizeId" value={get(stoneForm, 'stoneSizeId.value')} onChange={(e) => selectOnChange('stoneSizeId', 'stoneSize', e)} data={stoneSizeData} />
                      <div className="invalid-feedback">{get(stoneForm, 'stoneSizeId.error')}</div>
                    </div>
                  </div>
                  <div className={`${get(stoneForm, 'stoneGroupId.value') && isStoneGroupDiamond ? '' : 'hide'}`}>
                    <div className="col-md-3 col-lg-2">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.stone")} {t("common.color")}<span className="asterisk">*</span>
                        </label>
                        <Select name="stoneColorId" value={get(stoneForm, 'stoneColorId.value')} onChange={(e) => selectOnChange('stoneColorId', 'stoneColor', e)} data={stoneColorData} />
                        <div className="invalid-feedback">{get(stoneForm, 'stoneColorId.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-3 col-lg-2">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.stone")} {t("common.clarity")}<span className="asterisk">*</span>
                        </label>
                        <Select name="stoneClarityId" value={get(stoneForm, 'stoneClarityId.value')} onChange={(e) => selectOnChange('stoneClarityId', 'stoneClarity', e)} data={stoneClarityData} />
                        <div className="invalid-feedback">{get(stoneForm, 'stoneClarityId.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-3 col-lg-2">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.stone")} {t("common.polish")}<span className="asterisk">*</span>
                        </label>
                        <Select name="stonePolishId" value={get(stoneForm, 'stonePolishId.value')} onChange={(e) => selectOnChange('stonePolishId', 'stonePolish', e)} data={stonePolishData} />
                        <div className="invalid-feedback">{get(stoneForm, 'stonePolishId.error')}</div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-3 col-lg-2">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("common.uom")}
                        <span className="asterisk">*</span>
                      </label>
                      <UOMElement name="uom" value={get(stoneForm, 'uom.value')} onChange={(e) => setStoneFormValue('uom', e.target.value)} />
                      <div className="invalid-feedback">
                        {get(stoneForm, "uom.error")}
                      </div>
                    </div>
                  </div>

                  <div className="col-md-3 col-lg-2">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("common.quantity")}<span className="asterisk">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="quantity"
                        name="quantity"
                        value={get(stoneForm, 'quantity.value')}
                        onChange={(e) => setStoneFormValue('quantity', e.target.value)}
                      />
                      <div className="invalid-feedback">{get(stoneForm, 'quantity.error')}</div>
                    </div>
                  </div>
                  {/* <div className="col-md-3 col-lg-2">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("transactions.weight")}<span className="asterisk">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="weight"
                        name="weight"
                        value={get(stoneForm, 'weight.value')}
                        onChange={(e) => setStoneFormValue('weight', e.target.value)}
                      />
                      <div className="invalid-feedback">{get(stoneForm, 'weight.error')}</div>
                    </div>
                  </div> */}
                  <div className="col-md-3 col-lg-2">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("transactions.weight")} ({t("common.gram")})
                        <span className="asterisk">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="weightPerGram"
                        name="weightPerGram"
                        value={get(stoneForm, "weightPerGram.value")}
                        onChange={(e) =>
                          setStoneFormValue("weightPerGram", e.target.value)
                        }
                        disabled={get(stoneForm, 'weightPerGram.disabled')}
                      />
                      <div className="invalid-feedback">
                        {get(stoneForm, "weightPerGram.error")}
                      </div>
                    </div>
                  </div>
                  <div className="col-md-3 col-lg-2">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("transactions.weight")} ({t("common.karat")})
                        <span className="asterisk">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="weightPerKarat"
                        name="weightPerKarat"
                        value={get(stoneForm, "weightPerKarat.value")}
                        onChange={(e) =>
                          setStoneFormValue("weightPerKarat", e.target.value)
                        }
                        disabled={get(stoneForm, 'weightPerKarat.disabled')}
                      />
                      <div className="invalid-feedback">
                        {get(stoneForm, "weightPerKarat.error")}
                      </div>
                    </div>
                  </div>
                  <div className="col-md-3 col-lg-2">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("common.rate")}<span className="asterisk">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="rate"
                        name="rate"
                        value={get(stoneForm, 'rate.value')}
                        onChange={(e) => setStoneFormValue('rate', e.target.value)}
                      />
                      <div className="invalid-feedback">{get(stoneForm, 'rate.error')}</div>
                    </div>
                  </div>
                  <div className="col-md-3 col-lg-2">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("common.amount")}<span className="asterisk">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="amount"
                        name="amount"
                        value={get(stoneForm, 'amount.value')}
                        onChange={(e) => setStoneFormValue('amount', e.target.value)} 
                        disabled
                      />
                      <div className="invalid-feedback">{get(stoneForm, 'amount.error')}</div>
                    </div>
                  </div>
                  <div className="col-md-3 col-lg-2">
                    <div className="form-group">
                      <div className="checkbox checkbox-primary checkbox-inline">
                        <input
                          type="checkbox"
                          name="isNoWeight"
                          id="isNoWeight"
                          defaultChecked={get(stoneForm, 'isNoWeight.value')}
                          onChange={(e) => setStoneFormValue('isNoWeight', !stoneForm.isNoWeight.value)} />
                        <label htmlFor="">{t("transactions.noWeight")}</label>
                      </div>
                    </div>
                  </div>

                  <div className="col-md-3 col-lg-2">
                    <a href="#" className="btn btn-primary" onClick={saveStoneDetails}>
                      <i className="zmdi zmdi-playlist-plus"></i> {t("common.addToList")}
                    </a>
                  </div>
                  {showUpdate ?
                    <div className="col-md-3 col-lg-2">
                      <a href="#" className="btn btn-primary" onClick={updateStoneDetails}>
                        <i className="zmdi zmdi-refresh"></i> {t("common.update")}
                      </a>
                    </div>
                    :
                    ``}

                </div>
              </div>
            </div>
          </div>
        ) : (
          ``
        )}

        <div className="panel-body">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">Stone Details List</h3>
              <div className="panel-options">
                <p>
                  {selected.length > 0 ?
                    <a href="#" className="btn btn-info btn-sm" onClick={() => { deleteStoneDetails(selected) }}>
                      <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                    </a> :
                    null}
                </p>
              </div>
            </div>
            <div className="panel-body">
              <div className="ag-grid-wrapper">
                <Grid colDefs={stoneDetailsColumns} rowData={stoneDetailsData} selectedRows={setSelected} actions={{ isRequired: true, editAction: editAction, deleteAction: deleteStoneDetails }} flex />
              </div>
            </div>
          </div>
        </div>
        {showCreateForm ? (
          <div className="modal-footer">
            <a href="#" className="btn btn-primary" onClick={saveForm}>
              <i className="zmdi zmdi-check"></i> {t("common.save")}
            </a>
          </div>
        ) : (
          ``
        )}
      </div>
    </div>
  );
};

export default withTranslation()(StoneDetails);
