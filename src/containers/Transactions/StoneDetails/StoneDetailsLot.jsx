import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { stoneDetailsForm } from "../Constants";
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import $ from 'jquery';

import {
  saveDetails,
  editDetails,
  deleteDetails,
} from "../../../redux/actions/transactions/stoneDetailsActions";
import CustomGrid from "../../../components/CustomGrid";
import Swal from "sweetalert2";


const stoneDetailsColumns = [
  {
    id: "stoneGroupName",
    label: "Stone",
    isEdit: false,
    isSort: true,
  },
  {
    id: "stoneName",
    label: "Stone Name",
    isEdit: false,
    isSort: true,
  },
  {
    id: "uom",
    label: "UOM",
    isEdit: false,
    isSort: true,
  },
  {
    id: "quantity",
    label: "Pieces",
    isEdit: false,
    isSort: true,
  },
  {
    id: "weightPerGram",
    label: "Weight (Gram)",
    isEdit: true,
    isSort: true,
  },
  {
    id: "weightPerKarat",
    label: "Weight (Karat)",
    isEdit: false,
    isSort: true,
  },
  {
    id: "rate",
    label: "Rate",
    isEdit: false,
    isSort: true,
  },
  {
    id: "amount",
    label: "Amount",
    isEdit: false,
    isSort: true,
  },
];

export const StoneDetailsLot = (props) => {
  const { t = true } = props;
  const [stoneForm, setStoneForm] = useState(cloneDeep(stoneDetailsForm));
  let [selected, setSelected] = useState([]);
  let [stoneDetailsData, setStoneDetailsData] = useState([]);
  const dispatch = useDispatch();

  const setStoneFormValue = (field, value) => {
    stoneForm[field]["value"] = value;
    setStoneForm({ ...stoneForm });
  };
  const resetStoneForm = () => {
    map(stoneForm, function (value, key, object) {
      stoneForm[key]["value"] = "";
    });
    setStoneForm({ ...stoneForm });
  };

  const saveStoneDetails = () => {
    let isFormValid = true;
    let date = new Date();
    let time = date.getTime();
    setStoneFormValue("id", time);
    map(stoneForm, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      stoneForm[key] = { ...value, ...isValid };
    });
    if (isFormValid) {
      let data = {};
      map(stoneForm, function (value, key, object) {
        data = {
          ...data,
          [key]: value.value,
        };
      });
      Swal.fire({
        title: "Confirm Saving Stone Details Info.!",
        text: "Continue to save / Cancel to stop",
        icon: "info",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Continue",
      }).then((result) => {
        if (result.isConfirmed) {
          dispatch(saveDetails(data));
          resetStoneForm();
        }
      });
    }
  };


  const validateField = (fieldObj) => {
    let errorObj = { isValid: true, error: null };
    if (fieldObj.required && !fieldObj.value) {
      errorObj.isValid = false;
      errorObj.error = "Please fill this field";
    } else if (fieldObj.value && fieldObj.value.length < fieldObj.min) {
      errorObj.isValid = false;
      errorObj.error = `Please enter min length ${fieldObj.min}`;
    } else if (fieldObj.value && fieldObj.value.length > fieldObj.max) {
      errorObj.isValid = false;
      errorObj.error = `Please enter max length ${fieldObj.max}`;
    }
    return errorObj;
  };

  const deleteStoneDetails = (data) => {
    let selectedIds;
    if (data instanceof Array && data.length > 0) {
      selectedIds = data.map((ele) => {
        return ele.id;
      });
    } else {
      selectedIds = data.id;
    }
    Swal.fire({
      title: "Confirm Deleting Stone Details.!",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deleteDetails(selectedIds));
      }
    });
  };

  const editedData = (data) => {
    setStoneDetailsData(data);
  };

  const onSave = () => {
    props.onStoneDetailsChange(stoneDetailsData);
    $('.close').click();
  };

  useEffect(() => {
    setStoneDetailsData(props.stoneDetails);
  }, [props.stoneDetails]);

  return (
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button
            type="button"
            class="close"
            data-dismiss="modal"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="myModalLabel">
            Stone Details
          </h4>
        </div>


        <div className="panel-body">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">Stone Details List</h3>
              <div className="panel-options">
                <p>
                  {selected.length > 0 ? (
                    <a
                      href="#"
                      className="btn btn-info btn-sm"
                      onClick={() => {
                        deleteStoneDetails(selected);
                      }}
                    >
                      <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                    </a>
                  ) : null}
                </p>
              </div>
            </div>
            <div className="panel-body">
              <div className="ag-grid-wrapper">
                <CustomGrid
                  gridData={stoneDetailsData || []}
                  columns={stoneDetailsColumns}
                  isRadio={false}
                  isCheckbox={false}
                  uniqueId="id"
                  sortColumn="stoneGroupName"
                  isEdit={false}
                  isDelete={false}
                  defaultRowsPerPage={5}
                  onDataEdited={editedData}
                />
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <a href="javascript:void(0)" class="btn btn-primary" onClick={onSave}>
            <i class="zmdi zmdi-check"></i> Save
          </a>
        </div>
      </div>
    </div>
  );
};

export default withTranslation()(StoneDetailsLot);
