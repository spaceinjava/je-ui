import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { stoneGroupColumns, stoneGroupForm } from "../Constants";
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../../components/Grid";
import Swal from 'sweetalert2'
import Select from "../../../components/Form/Select";
import { saveStoneGroup, fetchStoneGroups, deleteStoneGroups } from '../../../redux/actions/creations/stoneGroupActions'
import { fetchStoneTypes } from '../../../redux/actions/creations/stoneTypeActions'
import LoadingOverlay from 'react-loading-overlay';

export const StoneGroup = (props) => {
  const { t } = props;
  const [formData, setFromData] = useState(cloneDeep(stoneGroupForm));
  const [viewForm, setToggleView] = useState(true);
  let [selected, setSelected] = useState([]);
  const dispatch = useDispatch();
  const stoneTypes = useSelector(state => state.stoneTypeReducer.allStoneTypes)
  const stoneGroups = useSelector(state => state.stoneGroupReducer.allStoneGroups)
  const loader = useSelector(state => state.stoneGroupReducer.loading);
  const stoneTypeData = stoneTypes.data ? stoneTypes.data.map((type) => {
    return {
      id: type.id,
      name: type.typeName,
      shortCode: type.shortCode,
    }
  }) : [];
  const stoneGroupData = stoneGroups.data ? stoneGroups.data.map((group) => {
    return {
      id: group.id,
      typeId: group.stoneType.id,
      typeName: group.stoneType.typeName,
      groupName: group.groupName,
      shortCode: group.shortCode,
      diamond: group.diamond
    }
  }) : [];

  const setFormValue = (field, value) => {
    formData[field]['value'] = value;
    setFromData({ ...formData });
  }
  const resetForm = () => {
    map(formData, function (value, key, object) {
      if (key === 'diamond') {
        document.getElementById("diamond").checked = false;
        setFormValue('diamond', false)
      }
      else {
        formData[key]['value'] = '';
      }
    });
    setFromData({ ...formData });
  }
  const saveForm = () => {
    let isFormValid = true;
    map(formData, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      formData[key] = { ...value, ...isValid }
    });
    if (isFormValid) {
      let data = {};
      map(formData, function (value, key, object) {
        data = {
          ...data,
          [key]: value.value
        }
      });
      Swal.fire({
        title: 'Confirm Saving Stone Group Info.!',
        text: 'Continue to save / Cancel to stop',
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Continue'
      }).then((result) => {
        if (result.isConfirmed) {
          dispatch(saveStoneGroup(data))
            .then(() => {
              resetForm()
            })
        }
      })
    }
  }
  const validateField = (fieldObj) => {
    let errorObj = { isValid: true, error: null }
    if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
    else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
    else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }

    return errorObj;
  }

  const editAction = (ele) => {
    setFormValue('id', ele.id)
    setFormValue('stoneType', ele.typeId)
    setFormValue('groupName', ele.groupName)
    setFormValue('shortCode', ele.shortCode)
    document.getElementById("diamond").checked = ele.diamond;
    setFormValue('diamond', ele.diamond)
  }

  const deleteAction = (data) => {
    let selectedIds;
    if (data instanceof Array && data.length > 0) {
      selectedIds = data.map((ele) => {
        return ele.id
      })
    } else {
      selectedIds = data.id;
    }
    Swal.fire({
      title: 'Confirm Deleting Stone Group.!',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deleteStoneGroups(selectedIds.toString()))
          .then(() => {
            selected = []
            setSelected(selected);
          })
      }
    })
  }

  useEffect(() => {
    dispatch(fetchStoneTypes())
    dispatch(fetchStoneGroups())
  }, [])

  const toggleView = () => {
    const formNode = document.getElementById('form');
    formNode.classList.toggle('collapse-panel');
    setToggleView(!viewForm);
  }

  return (
    <div className="panel panel-default hero-panel">
      <LoadingOverlay
        active={loader}
        spinner
        text='Loading...'
      >
        <div className="panel-heading">
          <h1 className="panel-title">{t("common.stone")} {t("common.group")} {t("common.creation")}</h1>
          <div className="panel-options">
            <a
              href="#"
              className="text-info collapseBtn"
              title="Show Add Form"
            >
              {viewForm ?
                <i className="zmdi zmdi-minus-circle  zmdi-hc-2x" onClick={() => { toggleView() }}>
                </i> :
                <i className="zmdi zmdi-plus-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                </i>}
            </a>
          </div>
        </div>
        <div className="panel-body">
          <div className="panel update-panel form-pnel-container" id="form">
            <div className="text-danger">* {t("common.requiredFieldsMsg")} </div>
            <div className="panel update-panel">
              <div className="panel-body ">
                <div className="row">
                  <div className="col-sm-6 col-md-3 col-lg-3">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("creations.stoneType")}<span className="asterisk">*</span>
                      </label>
                      <Select name="stoneType" value={get(formData, 'stoneType.value')} onChange={(e) => setFormValue('stoneType', e.target.value)} data={stoneTypeData} />
                      <div className="invalid-feedback">{get(formData, 'stoneType.error')}</div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-3 col-lg-3">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("common.shortCode")}<span className="asterisk">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="code"
                        name="shortCode"
                        value={get(formData, 'shortCode.value')}
                        onChange={(e) => setFormValue('shortCode', e.target.value)}
                      />
                      <div className="invalid-feedback">{get(formData, 'shortCode.error')}</div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-3 col-lg-3">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("creations.stoneGroup")} {t("common.name")}<span className="asterisk">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="name"
                        name="groupName"
                        value={get(formData, 'groupName.value')}
                        onChange={(e) => setFormValue('groupName', e.target.value)}
                      />
                      <div className="invalid-feedback">{get(formData, 'groupName.error')}</div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-3 col-lg-3">
                    <div className="form-group">
                      <div className="checkbox checkbox-primary">
                        <input
                          type="checkbox"
                          name="diamond"
                          id="diamond"
                          defaultChecked={get(formData, 'diamond.value')}
                          onChange={(e) => setFormValue('diamond', !formData.diamond.value)} />
                        <label htmlFor="">{t("common.diamond")}</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="panel-footer text-center">
              <a href="#" className="btn btn-primary fire" onClick={saveForm}>
                <i className="zmdi zmdi-check"></i> {t("common.save")}
              </a>
              <a href="#" className="btn btn-default" onClick={resetForm}>
                <i className="zmdi zmdi-close"></i> {t("common.clear")}
              </a>
            </div>
          </div>

          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">Stone Group List</h3>
              <div className="panel-options">
                <p>
                  {selected.length > 0 ?
                    <a href="#" className="btn btn-info btn-sm" onClick={() => deleteAction(selected)} >
                      <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                    </a> :
                    null}
                </p>
              </div>
            </div>
            <div className="panel-body">
              <div className="ag-grid-wrapper">
                <Grid colDefs={stoneGroupColumns} rowData={stoneGroupData} selectedRows={setSelected} actions={{ isRequired: true, editAction: editAction, deleteAction: deleteAction }} />
              </div>
            </div>
          </div>
        </div>
      </LoadingOverlay>
    </div>
  );
};

export default withTranslation()(StoneGroup);
