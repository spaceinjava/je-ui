import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { partyStoneRateColumns, partyStoneRateForm } from "../Constants";
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../../components/Grid";
import Swal from 'sweetalert2'
import Select from "../../../components/Form/Select";
import { fetchStoneGroupsByType, fetchStonesByStoneGroup, fetchStoneSizesByStoneGroup } from '../../../redux/actions/creations/stoneGroupActions'
import { fetchStoneTypes } from '../../../redux/actions/creations/stoneTypeActions'
import { fetchSuppliers } from '../../../redux/actions/Admin/supplierActions'
import { fetchStoneColors } from '../../../redux/actions/creations/stoneColorActions'
import { fetchStoneClarities } from '../../../redux/actions/creations/stoneClarityActions'
import { fetchStonePolishes } from '../../../redux/actions/creations/stonePolishActions'
import { savePartyStoneRate, fetchPartyStoneRates, deletePartyStoneRates } from '../../../redux/actions/creations/partyStoneRateActions'
import LoadingOverlay from 'react-loading-overlay';
import UOMElement from "../../../components/UOMElement";

export const PartyStoneRate = (props) => {
  const { t } = props;
  const [formData, setFromData] = useState(cloneDeep(partyStoneRateForm));
  const [viewForm, setToggleView] = useState(true);
  let [selected, setSelected] = useState([]);
  const dispatch = useDispatch();
  const suppliers = useSelector(state => state.supplierReducer.allSuppliers)
  const stoneTypes = useSelector(state => state.stoneTypeReducer.allStoneTypes)
  const stoneGroups = useSelector(state => state.stoneGroupReducer.allStoneGroupsByType)
  const stones = useSelector(state => state.stoneGroupReducer.allStonesByStoneGroup)
  const stoneSizes = useSelector(state => state.stoneGroupReducer.allStoneSizesByStoneGroup)
  const stoneColors = useSelector(state => state.stoneColorReducer.allStoneColors)
  const stoneClarities = useSelector(state => state.stoneClarityReducer.allStoneClarities)
  const stonePolishes = useSelector(state => state.stonePolishReducer.allStonePolishes)
  const partyRates = useSelector(state => state.partyStoneRateReducer.allPartyStoneRates)
  const loader = useSelector(state => state.partyStoneRateReducer.loading);

  const suppliersData = suppliers.data ? suppliers.data.map((supplier) => {
    return {
      id: supplier.id,
      name: supplier.partyName
    }
  }) : [];
  const stoneTypeData = stoneTypes.data ? stoneTypes.data.map((type) => {
    return {
      id: type.id,
      name: type.typeName,
      shortCode: type.shortCode
    }
  }) : [];
  const stoneGroupData = stoneGroups.data ? stoneGroups.data.map((group) => {
    return {
      id: group.id,
      name: group.groupName,
      shortCode: group.shortCode,
      diamond: group.diamond
    }
  }) : [];
  const stoneData = stones.data ? stones.data.map((stone) => {
    return {
      id: stone.id,
      name: stone.stoneName,
      shortCode: stone.shortCode
    }
  }) : [];
  const stoneSizeData = stoneSizes.data ? stoneSizes.data.map((size) => {
    return {
      id: size.id,
      name: size.stoneSize,
      shortCode: size.shortCode
    }
  }) : [];
  const stoneColorData = stoneColors.data ? stoneColors.data.map((color) => {
    return {
      id: color.id,
      name: color.colorName,
      shortCode: color.shortCode
    }
  }) : [];
  const stoneClarityData = stoneClarities.data ? stoneClarities.data.map((clarity) => {
    return {
      id: clarity.id,
      name: clarity.clarityName,
      shortCode: clarity.shortCode
    }
  }) : [];
  const stonePolishData = stonePolishes.data ? stonePolishes.data.map((polish) => {
    return {
      id: polish.id,
      name: polish.polishName,
      shortCode: polish.shortCode
    }
  }) : [];

  const fetchPartyNameById = (partyId) => {
    let selectedParty = suppliersData.find(ele => ele.id === partyId);
    return selectedParty ? selectedParty.name : "";
  }

  const isStoneADiamond = (selectedStoneGroupId) => {
    let selectedStoneGroup = stoneGroupData.find(ele => ele.id === selectedStoneGroupId);
    return selectedStoneGroup ? selectedStoneGroup.diamond : false;
  }

  const partyRatesData = partyRates.data ? partyRates.data.map((partyRate) => {
    let stoneGroup = partyRate.stone.stoneGroup;
    return {
      id: partyRate.id,
      partyId: partyRate.partyId,
      party: fetchPartyNameById(partyRate.partyId),
      stoneTypeId: stoneGroup.stoneType.id,
      stoneType: stoneGroup.stoneType.typeName,
      groupId: stoneGroup.id,
      stoneGroup: stoneGroup.groupName,
      isDiamond: stoneGroup.diamond,
      stoneId: partyRate.stone.id,
      stone: partyRate.stone.stoneName,
      stoneSizeId: partyRate.stoneSize ? partyRate.stoneSize.id : "",
      stoneSize: partyRate.stoneSize ? partyRate.stoneSize.stoneSize : "",
      stoneColorId: partyRate.stoneColor ? partyRate.stoneColor.id : "",
      stoneColor: partyRate.stoneColor ? partyRate.stoneColor.colorName : "",
      stoneClarityId: partyRate.stoneClarity ? partyRate.stoneClarity.id : "",
      stoneClarity: partyRate.stoneClarity ? partyRate.stoneClarity.clarityName : "",
      stonePolishId: partyRate.stonePolish ? partyRate.stonePolish.id : "",
      stonePolish: partyRate.stonePolish ? partyRate.stonePolish.polishName : "",
      minRate: partyRate.minRate,
      maxRate: partyRate.maxRate,
      uom: partyRate.uom,
      noWeight: partyRate.noWeight,
      fixedRate: partyRate.fixedRate
    }
  }) : [];

  const setFormValue = (field, value) => {
    formData[field]['value'] = value;
    setFromData({ ...formData });
  }
  const resetForm = () => {
    map(formData, function (value, key, object) {
      if (key === 'noWeight') {
        document.getElementById("noWeight").checked = false;
        setFormValue('noWeight', false)
      } else if (key === 'fixedRate') {
        document.getElementById("fixedRate").checked = false;
        setFormValue('fixedRate', false)
      }
      else {
        formData[key]['value'] = '';
      }
    });
    setFromData({ ...formData });
    setIsStoneGroupDiamond(false);
  }
  const saveForm = () => {
    let isFormValid = true;
    map(formData, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      formData[key] = { ...value, ...isValid }
    });
    if (isFormValid) {
      let data = {};
      map(formData, function (value, key, object) {
        data = {
          ...data,
          [key]: value.value
        }
      });
      Swal.fire({
        title: 'Confirm Saving Party Stone Rate Info.!',
        text: 'Continue to save / Cancel to stop',
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Continue'
      }).then((result) => {
        if (result.isConfirmed) {
          dispatch(savePartyStoneRate(data))
            .then(() => {
              resetForm()
            })
        }
      })
    }
  }
  const validateField = (fieldObj) => {
    let errorObj = { isValid: true, error: null }
    if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
    else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
    else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }

    return errorObj;
  }

  const editAction = (ele) => {
    setFormValue('id', ele.id)
    setFormValue('partyId', ele.partyId)
    setFormValue('party', ele.party)
    setFormValue('stoneType', ele.stoneTypeId)
    setFormValue('stoneGroup', ele.groupId)
    setIsStoneGroupDiamond(ele.isDiamond);
    setFormValue('stone', ele.stoneId)
    setFormValue('stoneSize', ele.stoneSizeId ? ele.stoneSizeId : "")
    setFormValue('stoneColor', ele.stoneColorId ? ele.stoneColorId : "")
    setFormValue('stoneClarity', ele.stoneClarityId ? ele.stoneClarityId : "")
    setFormValue('stonePolish', ele.stonePolishId ? ele.stonePolishId : "")
    setFormValue('minRate', ele.minRate)
    setFormValue('maxRate', ele.maxRate)
    setFormValue('uom', ele.uom)
    document.getElementById("noWeight").checked = ele.noWeight;
    setFormValue('noWeight', ele.noWeight)
    document.getElementById("fixedRate").checked = ele.fixedRate;
    setFormValue('fixedRate', ele.fixedRate)
  }

  const deleteAction = (data) => {
    let selectedIds;
    if (data instanceof Array && data.length > 0) {
      selectedIds = data.map((ele) => {
        return ele.id
      })
    } else {
      selectedIds = data.id;
    }
    Swal.fire({
      title: 'Confirm Deleting Party Stone Rate.!',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deletePartyStoneRates(selectedIds.toString()))
          .then(() => {
            selected = []
            setSelected(selected);
            setFormValue('id', '')
          })
      }
    })
  }

  useEffect(() => {
    dispatch(fetchSuppliers());
    dispatch(fetchStoneTypes());
    dispatch(fetchStoneColors());
    dispatch(fetchStoneClarities());
    dispatch(fetchStonePolishes());
    dispatch(fetchPartyStoneRates());
  }, [])

  useEffect(() => {
    dispatch(fetchStoneGroupsByType(get(formData, 'stoneType.value')));
  }, [get(formData, 'stoneType.value')])

  useEffect(() => {
    dispatch(fetchStonesByStoneGroup(get(formData, "stoneGroup.value")));
    dispatch(fetchStoneSizesByStoneGroup(get(formData, "stoneGroup.value")));
  }, [get(formData, "stoneGroup.value")]);

  const toggleView = () => {
    const formNode = document.getElementById('form');
    formNode.classList.toggle('collapse-panel');
    setToggleView(!viewForm);
  }

  const partyNameChange = (e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    setFormValue("partyId", e.target.value);
    setFormValue("party", label);
  };

  let [isStoneGroupDiamond, setIsStoneGroupDiamond] = useState(false);

  const stoneGroupOnChange = (e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    resetElementsOnStoneGroupChange();
    if (e.target.value) {
      setFormValue('stoneGroup', e.target.value);
    }
    setIsStoneGroupDiamond(isStoneADiamond(e.target.value));
  }

  const resetElementsOnStoneGroupChange = () => {
    setFormValue('stoneColor', '')
    setFormValue('stoneClarity', '')
    setFormValue('stonePolish', '')
  }

  return (
    <div className="panel panel-default hero-panel">
      <LoadingOverlay
        active={loader}
        spinner
        text='Loading...'
      >
        <div className="panel-heading">
          <h1 className="panel-title">{t("common.Party")} {t("common.stone")} {t("common.rate")} {t("common.creation")}</h1>
          <div className="panel-options">
            <a
              href="#"
              className="text-info collapseBtn"
              title="Show Add Form"
            >
              {viewForm ?
                <i className="zmdi zmdi-minus-circle  zmdi-hc-2x" onClick={() => { toggleView() }}>
                </i> :
                <i className="zmdi zmdi-plus-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                </i>}
            </a>
          </div>
        </div>
        <div className="panel-body">
          <div className="panel update-panel form-pnel-container" id="form">
            <div className="text-danger">* {t("common.requiredFieldsMsg")} </div>
            <div className="panel update-panel">
              <div className="panel-body ">
                <div className="row">
                  <div className="col-sm-6 col-md-3 col-lg-3">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("common.Party")}<span className="asterisk">*</span>
                      </label>
                      <Select name="partyId" value={get(formData, 'partyId.value')} onChange={(e) => partyNameChange(e)} data={suppliersData} />
                      <div className="invalid-feedback">{get(formData, 'partyId.error')}</div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-3 col-lg-3">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("common.stone")} {t("common.type")}<span className="asterisk">*</span>
                      </label>
                      <Select name="stoneType" value={get(formData, 'stoneType.value')} onChange={(e) => setFormValue('stoneType', e.target.value)} data={stoneTypeData} />
                      <div className="invalid-feedback">{get(formData, 'stoneType.error')}</div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-3 col-lg-3">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("common.stone")} {t("common.group")}<span className="asterisk">*</span>
                      </label>
                      <Select name="stoneGroup" value={get(formData, 'stoneGroup.value')} onChange={(e) => stoneGroupOnChange(e)} data={stoneGroupData} />
                      <div className="invalid-feedback">{get(formData, 'stoneGroup.error')}</div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-3 col-lg-3">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("common.stone")} {t("common.name")}<span className="asterisk">*</span>
                      </label>
                      <Select name="stone" value={get(formData, 'stone.value')} onChange={(e) => setFormValue('stone', e.target.value)} data={stoneData} />
                      <div className="invalid-feedback">{get(formData, 'stone.error')}</div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-3 col-lg-3">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("common.stone")} {t("common.size")}
                      </label>
                      <Select name="stoneSize" value={get(formData, 'stoneSize.value')} onChange={(e) => setFormValue('stoneSize', e.target.value)} data={stoneSizeData} />
                      <div className="invalid-feedback">{get(formData, 'stoneSize.error')}</div>
                    </div>
                  </div>
                  <div className={`${get(formData, "stoneGroup.value") && isStoneGroupDiamond ? '' : 'hide'}`}>
                    <div className="col-sm-6 col-md-3 col-lg-3">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.stone")} {t("common.color")}<span className="asterisk">*</span>
                        </label>
                        <Select name="stoneColor" value={get(formData, 'stoneColor.value')} onChange={(e) => setFormValue('stoneColor', e.target.value)} data={stoneColorData} />
                        <div className="invalid-feedback">{get(formData, 'stoneColor.error')}</div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-3 col-lg-3">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.stone")} {t("common.clarity")}<span className="asterisk">*</span>
                        </label>
                        <Select name="stoneClarity" value={get(formData, 'stoneClarity.value')} onChange={(e) => setFormValue('stoneClarity', e.target.value)} data={stoneClarityData} />
                        <div className="invalid-feedback">{get(formData, 'stoneClarity.error')}</div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-3 col-lg-3">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.stone")} {t("common.polish")}<span className="asterisk">*</span>
                        </label>
                        <Select name="stonePolish" value={get(formData, 'stonePolish.value')} onChange={(e) => setFormValue('stonePolish', e.target.value)} data={stonePolishData} />
                        <div className="invalid-feedback">{get(formData, 'stonePolish.error')}</div>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-3 col-lg-3">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("common.uom")}<span className="asterisk">*</span>
                      </label>
                      <UOMElement name="uom" value={get(formData, 'uom.value')} onChange={(e) => setFormValue('uom', e.target.value)} />
                      <div className="invalid-feedback">{get(formData, 'uom.error')}</div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-3 col-lg-3">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("creations.minRate")}<span className="asterisk">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="minRate"
                        name="minRate"
                        value={get(formData, 'minRate.value')}
                        onChange={(e) => setFormValue('minRate', e.target.value)}
                      />
                      <div className="invalid-feedback">{get(formData, 'minRate.error')}</div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-3 col-lg-3">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("creations.maxRate")}<span className="asterisk">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="maxRate"
                        name="maxRate"
                        value={get(formData, 'maxRate.value')}
                        onChange={(e) => setFormValue('maxRate', e.target.value)}
                      />
                      <div className="invalid-feedback">{get(formData, 'maxRate.error')}</div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-3 col-lg-3">
                    <div className="form-group">
                      <div className="checkbox checkbox-primary">
                        <input
                          type="checkbox"
                          name="noWeight"
                          id="noWeight"
                          defaultChecked={get(formData, 'noWeight.value')}
                          onChange={(e) => setFormValue('noWeight', !formData.noWeight.value)} />
                        <label htmlFor="">{t("creations.noWeight")}</label>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-3 col-lg-3">
                    <div className="form-group">
                      <div className="checkbox checkbox-primary">
                        <input
                          type="checkbox"
                          name="fixedRate"
                          id="fixedRate"
                          defaultChecked={get(formData, 'fixedRate.value')}
                          onChange={(e) => setFormValue('fixedRate', !formData.fixedRate.value)} />
                        <label htmlFor="">{t("creations.fixedRate")}</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="panel-footer text-center">
              <a href="#" className="btn btn-primary fire" onClick={saveForm}>
                <i className="zmdi zmdi-check"></i> {t("common.save")}
              </a>
              <a href="#" className="btn btn-default" onClick={resetForm}>
                <i className="zmdi zmdi-close"></i> {t("common.clear")}
              </a>
            </div>
          </div>

          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">Party Rates List</h3>
              <div className="panel-options">
                <p>
                  {selected.length > 0 ?
                    <a href="#" className="btn btn-info btn-sm" onClick={() => deleteAction(selected)} >
                      <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                    </a> :
                    null}
                </p>
              </div>
            </div>
            <div className="panel-body">
              <div className="ag-grid-wrapper">
                <Grid colDefs={partyStoneRateColumns} rowData={partyRatesData} selectedRows={setSelected} actions={{ isRequired: true, editAction: editAction, deleteAction: deleteAction }} flex={true} />
              </div>
            </div>
          </div>
        </div>
      </LoadingOverlay>
    </div>
  );
};

export default withTranslation()(PartyStoneRate);


