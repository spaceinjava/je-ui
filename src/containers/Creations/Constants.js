export const companiesColumns = [
    { headerName: "adminMenus.companies", field: "company" },
    { headerName: "common.address", field: "addressLineOne" },
    { headerName: "common.email", field: "email" },
    { headerName: "common.contact", field: "contact" },
    { headerName: "common.state", field: "state" },
    { headerName: "common.district", field: "district" },
    { headerName: "common.city", field: "city_town" },
    { headerName: "common.area", field: "area" },
    { headerName: "GST", field: "GST" },
    { headerName: "common.actions", field: "actions" }
]

export const departmentsColumns = [
    { headerName: "common.name", field: "name" },
    { headerName: "common.shortCode", field: "shortCode" }
]

export const designationColumns = [
    { headerName: "adminMenus.department", field: "deptName" },
    { headerName: "common.name", field: "name" },
    { headerName: "common.shortCode", field: "shortCode" }
]

export const mainGroupColumns = [
    { headerName: "creations.mainGroup", field: "groupName" },
    { headerName: "common.shortCode", field: "shortCode" }
]

export const brandColumns = [
    { headerName: "", field: "groupId", hide: true },
    { headerName: "creations.mainGroup", field: "groupName" },
    { headerName: "creations.brand", field: "brandName" },
    { headerName: "common.shortCode", field: "shortCode" }
]

export const brandProductColumns = [
    { headerName: "", field: "groupId", hide: true },
    { headerName: "creations.mainGroup", field: "groupName", hide: true },
    { headerName: "", field: "brandId", hide: true },
    { headerName: "creations.brand", field: "brandName" },
    { headerName: "creations.product", field: "productName" },
    { headerName: "common.shortCode", field: "shortCode" },
    { headerName: "common.gst", field: "gst" },
    { headerName: "creations.tray", field: "tray" }
]

export const brandSubProductColumns = [
    { headerName: "", field: "brandId", hide: true },
    { headerName: "creations.brand", field: "brandName" },
    { headerName: "", field: "productId", hide: true },
    { headerName: "creations.product", field: "productName" },
    { headerName: "common.shortCode", field: "shortCode" },
    { headerName: "creations.subProduct", field: "subProductName" }
]

export const floorColumns = [
    { headerName: "creations.floorName", field: "floorName" },
    { headerName: "common.shortCode", field: "shortCode" }
]

export const floorForm = {
    id: { value: '' },
    floorName: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    shortCode: { value: '', required: true, min: 3, max: 3, isValid: false, error: null },
}

export const counterForm = {
    id: { value: '' },
    floor: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    counterName: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    shortCode: { value: '', required: true, min: 3, max: 3, isValid: false, error: null },
}

export const counterColumns = [
    { headerName: "", field: "floorId", hide: true },
    { headerName: "common.floor", field: "floorName" },
    { headerName: "common.counter", field: "counterName" },
    { headerName: "common.shortCode", field: "shortCode" }
]

export const purityForm = {
    id: { value: '' },
    mainGroup: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    purity: { value: '', required: true, min: 1, max: 50, isValid: false, error: null },
}

export const purityColumns = [
    { headerName: "", field: "groupId", hide: true },
    { headerName: "creations.mainGroup", field: "groupName" },
    { headerName: "common.Purity", field: "purity" },
]

export const bankColumns = [
    { headerName: "creations.bankName", field: "bankName" },
    { headerName: "common.shortCode", field: "shortCode" }
]

export const bankBranchColumns = [
    { headerName: "creations.bankName", field: "bankName" },
    { headerName: "creations.branchName", field: "branchName" },
    { headerName: "common.shortCode", field: "shortCode" }
]

export const gstForm = {
    id: { value: '' },
    metalType: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    percent: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
}

export const gstColumns = [
    { headerName: "", field: "metalId", hide: true },
    { headerName: "creations.metalType", field: "metalType" },
    { headerName: "common.gst", field: "percent" }
]

export const hsnForm = {
    id: { value: '' },
    mainGroup: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    hsnCode: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    description: { value: '', required: true, min: 1, max: 50, isValid: false, error: null },
}


export const hsnColumns = [
    { headerName: "", field: "groupId", hide: true },
    { headerName: "creations.mainGroup", field: "groupName" },
    { headerName: "creations.hsnCode", field: "hsnCode"},
    { headerName: "common.description", field: "description" }
]

export const stoneGroupColumns = [
    { headerName: "", field: "typeId", hide: true },
    { headerName: "creations.stoneType", field: "typeName" },
    { headerName: "creations.stoneGroup", field: "groupName" },
    { headerName: "common.shortCode", field: "shortCode" },
    { headerName: "common.diamond", field: "diamond" }
]

export const stoneColumns = [
    { headerName: "", field: "typeId", hide: true },
    { headerName: "creations.stoneType", field: "typeName" },
    { headerName: "", field: "groupId", hide: true },
    { headerName: "creations.stoneGroup", field: "groupName" },
    { headerName: "common.stone", field: "stoneName" },
    { headerName: "common.shortCode", field: "shortCode" }
]

export const stoneSizeColumns = [
    { headerName: "", field: "stoneTypeId", hide: true },
    { headerName: "creations.stoneType", field: "stoneType" },
    { headerName: "", field: "groupId", hide: true },
    { headerName: "creations.stoneGroup", field: "groupName" },
    { headerName: "common.size", field: "size" },
    { headerName: "common.shortCode", field: "shortCode" }
]

export const stoneColorColumns = [
    { headerName: "common.color", field: "name" },
    { headerName: "common.shortCode", field: "shortCode" }
]

export const stoneClarityColumns = [
    { headerName: "common.clarity", field: "name" },
    { headerName: "common.shortCode", field: "shortCode" }
]

export const stonePolishColumns = [
    { headerName: "common.polish", field: "name" },
    { headerName: "common.shortCode", field: "shortCode" }
]

export const companiesForm = {
    company: { value: '', required: true, type: 'text', min: 3, max: 20, isValid: false, error: null },
    companyShortName: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null },
    email: { value: '', required: true, type: 'email', min: 3, max: 30, isValid: false, error: null },
    phoneNumber: { value: '', required: true, type: 'number', min: 6, max: 10, isValid: false, error: null },
    alternativePhoneNumber: { value: '', required: false, type: 'number', min: 6, max: 10, isValid: false, error: null },
    addressLineOne: { value: '', required: true, type: 'text', min: 3, max: 100, isValid: false, error: null },
    addressLineTwo: { value: '', required: true, type: 'text', min: 3, max: 100, isValid: false, error: null },
    state: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null },
    district: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null },
    cityTown: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null },
    areaVillage: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null },
    pincode: { value: '', required: true, type: 'number', min: 6, max: 6, isValid: false, error: null },
    gstNumber: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null },
    logo: { value: null, required: true, type: 'file', min: 3, max: 10, isValid: false, error: null },
    isBranch: { value: false, required: true, type: 'bool', min: 3, max: 10, isValid: false, error: null },
}

export const departmentForm = {
    id: { value: '' },
    name: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    shortCode: { value: '', required: true, min: 2, max: 5, isValid: false, error: null }
}

export const designationForm = {
    id: { value: '' },
    deptName: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    name: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    shortCode: { value: '', required: true, min: 2, max: 5, isValid: false, error: null }
}

export const mainGroupForm = {
    id: { value: '' },
    groupName: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    shortCode: { value: '', required: true, min: 2, max: 5, isValid: false, error: null }
}

export const brandForm = {
    id: { value: '' },
    mainGroup: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    brandName: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    shortCode: { value: '', required: true, min: 2, max: 5, isValid: false, error: null }
}

export const brandProductForm = {
    id: { value: '' },
    mainGroup: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    brand: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    productName: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    shortCode: { value: '', required: true, min: 3, max: 3, isValid: false, error: null },
    gst: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    tray: { value: false, required: false, min: 3, max: 50, isValid: false, error: null }
}

export const brandSubProductForm = {
    id: { value: '' },
    brand: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    brandProduct: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    subProductName: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    shortCode: { value: '', required: true, min: 3, max: 3, isValid: false, error: null }
}

export const stoneGroupForm = {
    id: { value: '' },
    groupName: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    shortCode: { value: '', required: true, min: 3, max: 3, isValid: false, error: null },
    stoneType: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    diamond: { value: false, required: false, min: 3, max: 10, isValid: false, error: null }
}

export const stoneForm = {
    id: { value: '' },
    stoneName: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    shortCode: { value: '', required: true, min: 3, max: 3, isValid: false, error: null },
    stoneType: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    stoneGroup: { value: '', required: true, min: 2, max: 50, isValid: false, error: null }
}

export const stoneSizeForm = {
    id: { value: '' },
    size: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    shortCode: { value: '', required: true, min: 3, max: 3, isValid: false, error: null },
    stoneType: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    stoneGroup: { value: '', required: true, min: 2, max: 50, isValid: false, error: null }
}

export const stoneColorForm = {
    id: { value: '' },
    name: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    shortCode: { value: '', required: true, min: 3, max: 3, isValid: false, error: null }
}

export const stoneClarityForm = {
    id: { value: '' },
    name: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    shortCode: { value: '', required: true, min: 3, max: 3, isValid: false, error: null }
}

export const stonePolishForm = {
    id: { value: '' },
    name: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    shortCode: { value: '', required: true, min: 3, max: 3, isValid: false, error: null }
}

export const partyStoneRateForm = {
    id: { value: '' },
    partyId: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    party: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    stoneType: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    stoneGroup: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    stone: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    stoneSize: { value: '', required: false, min: 2, max: 50, isValid: false, error: null },
    stoneColor: { value: '', required: false, min: 2, max: 50, isValid: false, error: null },
    stoneClarity: { value: '', required: false, min: 2, max: 50, isValid: false, error: null },
    stonePolish: { value: '', required: false, min: 2, max: 50, isValid: false, error: null },
    minRate: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    maxRate: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    uom: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    noWeight: { value: false, required: false, min: 3, max: 10, isValid: false, error: null },
    fixedRate: { value: false, required: false, min: 3, max: 10, isValid: false, error: null }
}

export const partyStoneRateColumns = [
    { headerName: "", field: "partyId", hide: true },
    { headerName: "common.Party", field: "party" },
    { headerName: "", field: "stoneTypeId", hide: true },
    { headerName: "creations.stoneType", field: "stoneType" },
    { headerName: "", field: "groupId", hide: true },
    { headerName: "creations.stoneGroup", field: "stoneGroup" },
    { headerName: "", field: "stoneId", hide: true },
    { headerName: "common.stone", field: "stone" },
    { headerName: "", field: "stoneSizeId", hide: true },
    { headerName: "creations.stoneSize", field: "stoneSize" },
    { headerName: "", field: "stoneColorId", hide: true },
    { headerName: "creations.stoneColor", field: "stoneColor" },
    { headerName: "", field: "stoneClarityId", hide: true },
    { headerName: "creations.stoneClarity", field: "stoneClarity" },
    { headerName: "", field: "stonePolishId", hide: true },
    { headerName: "creations.stonePolish", field: "stonePolish" },
    { headerName: "creations.minRate", field: "minRate" },
    { headerName: "creations.maxRate", field: "maxRate" },
    { headerName: "common.uom", field: "uom" },
    { headerName: "creations.noWeight", field: "noWeight" },
    { headerName: "creations.fixedRate", field: "fixedRate" }
]

export const saleStoneRateForm = {
    id: { value: '' },
    stoneType: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    stoneGroup: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    stone: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    stoneSize: { value: '', required: false, min: 2, max: 50, isValid: false, error: null },
    stoneColor: { value: '', required: false, min: 2, max: 50, isValid: false, error: null },
    stoneClarity: { value: '', required: false, min: 2, max: 50, isValid: false, error: null },
    stonePolish: { value: '', required: false, min: 2, max: 50, isValid: false, error: null },
    minRate: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    maxRate: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    uom: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    noWeight: { value: false, required: false, min: 3, max: 10, isValid: false, error: null },
    saleRate: { value: false, required: false, min: 3, max: 10, isValid: false, error: null },
    barcodeRate: { value: false, required: false, min: 3, max: 10, isValid: false, error: null },
    updateStock: { value: false, required: false, min: 3, max: 10, isValid: false, error: null }
}

export const saleStoneRateColumns = [
    { headerName: "", field: "stoneTypeId", hide: true },
    { headerName: "creations.stoneType", field: "stoneType" },
    { headerName: "", field: "groupId", hide: true },
    { headerName: "creations.stoneGroup", field: "stoneGroup" },
    { headerName: "", field: "stoneId", hide: true },
    { headerName: "common.stone", field: "stone" },
    { headerName: "", field: "stoneSizeId", hide: true },
    { headerName: "creations.stoneSize", field: "stoneSize" },
    { headerName: "", field: "stoneColorId", hide: true },
    { headerName: "creations.stoneColor", field: "stoneColor" },
    { headerName: "", field: "stoneClarityId", hide: true },
    { headerName: "creations.stoneClarity", field: "stoneClarity" },
    { headerName: "", field: "stonePolishId", hide: true },
    { headerName: "creations.stonePolish", field: "stonePolish" },
    { headerName: "creations.minRate", field: "minRate" },
    { headerName: "creations.maxRate", field: "maxRate" },
    { headerName: "common.uom", field: "uom" },
    { headerName: "creations.noWeight", field: "noWeight" },
    { headerName: "common.sale", field: "saleRate" },
    { headerName: "common.barcode", field: "barcodeRate" }
]

export const productForm = {
    id: { value: '' },
    mainGroup: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    productName: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    shortCode: { value: '', required: true, min: 3, max: 3, isValid: false, error: null }
}

export const productColumns = [
    { headerName: "", field: "groupId", hide: true },
    { headerName: "creations.mainGroup", field: "groupName" },
    { headerName: "creations.productName", field: "productName" },
    { headerName: "common.shortCode", field: "shortCode" }
]

export const subProductForm = {
    id: { value: '' },
    mainGroup: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    product: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    subProductName: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    shortCode: { value: '', required: true, min: 3, max: 3, isValid: false, error: null }
}

export const subProductColumns = [
    { headerName: "", field: "groupId", hide: true },
    { headerName: "creations.mainGroup", field: "groupName" },
    { headerName: "", field: "productId", hide: true },
    { headerName: "creations.product", field: "productName" },
    { headerName: "creations.subProductName", field: "subProductName" },
    { headerName: "common.shortCode", field: "shortCode" }
]

export const productSizeForm = {
    id: { value: '' },
    mainGroup: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    product: { value: '', required: true, min: 3, max: 50, isValid: false, error: null },
    productSize: { value: '', required: true, min: 1, max: 10, isValid: false, error: null },
}

export const productSizeColumns = [
    { headerName: "", field: "groupId", hide: true },
    { headerName: "creations.mainGroup", field: "groupName" },
    { headerName: "", field: "productId", hide: true },
    { headerName: "creations.product", field: "productName" },
    { headerName: "creations.productSize", field: "productSize" },
]

export const partyWastageForm = {
    id: { value: '' },
    partyId: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    party: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    mainGroup: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    product: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    subProduct: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    purity: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    productSize: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    touchValue: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    touchPercent: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    mcPerGram: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    mcPerDir: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    wastagePerGram: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    wastagePerDir: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    // mcOn: { value: '', required: true, min: 3, max: 10, isValid: false, error: null },
    // wastageOn: { value: '', required: true, min: 3, max: 10, isValid: false, error: null },
}

export const partyWastageColumns = [
    { headerName: "", field: "partyId", hide: true },
    { headerName: "common.Party", field: "party" },
    { headerName: "", field: "mainGroupId", hide: true },
    { headerName: "creations.mainGroup", field: "mainGroup" },
    { headerName: "", field: "productId", hide: true },
    { headerName: "creations.product", field: "product" },
    { headerName: "", field: "subProductId", hide: true },
    { headerName: "creations.subProduct", field: "subProduct" },
    { headerName: "", field: "purityId", hide: true },
    { headerName: "common.Purity", field: "purity" },
    { headerName: "", field: "productSizeId", hide: true },
    { headerName: "common.size", field: "productSize" },
    { headerName: "creations.touchValue", field: "touchValue" },
    { headerName: "creations.touchPercent", field: "touchPercent" },
    { headerName: "creations.mcgm", field: "mcPerGram" },
    { headerName: "creations.mcDir", field: "mcPerDir" },
    { headerName: "creations.wastgaeGm", field: "wastagePerGram" },
    { headerName: "creations.wastageDir", field: "wastagePerDir" },
    // { headerName: "creations.mcCalcOn", field: "mcOn" },
    // { headerName: "creations.wastageCalcOn", field: "wastageOn" },
]

export const saleWastageForm = {
    id: { value: '' },
    mainGroup: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    product: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    subProduct: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    purity: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    productSize: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    fromWeight: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    toWeight: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    minWstPerGram: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    maxWstPerGram: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    minDirWst: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    maxDirWst: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    minMcPerGram: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    maxMcPerGram: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    minDirMc: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    maxDirMc: { value: '', required: true, min: 1, max: 20, isValid: false, error: null },
    mcOn: { value: '', required: true, min: 3, max: 10, isValid: false, error: null },
    wastageOn: { value: '', required: true, min: 3, max: 10, isValid: false, error: null },
    incWstMc: { value: false, required: false, min: 3, max: 10, isValid: false, error: null }
}

export const saleWastageColumns = [
    { headerName: "", field: "mainGroupId", hide: true },
    { headerName: "creations.mainGroup", field: "mainGroup" },
    { headerName: "", field: "productId", hide: true },
    { headerName: "creations.product", field: "product" },
    { headerName: "", field: "subProductId", hide: true },
    { headerName: "creations.subProduct", field: "subProduct" },
    { headerName: "", field: "purityId", hide: true },
    { headerName: "common.Purity", field: "purity" },
    { headerName: "", field: "productSizeId", hide: true },
    { headerName: "common.size", field: "productSize" },
    { headerName: "creations.fromWeight", field: "fromWeight" },
    { headerName: "creations.toWeight", field: "toWeight" },
    { headerName: "creations.minWstPerGram", field: "minWstPerGram" },
    { headerName: "creations.maxWstPerGram", field: "maxWstPerGram" },
    { headerName: "creations.minDirWst", field: "minDirWst" },
    { headerName: "creations.maxDirWst", field: "maxDirWst" },
    { headerName: "creations.minMcPerGram", field: "minMcPerGram" },
    { headerName: "creations.maxMcPerGram", field: "maxMcPerGram" },
    { headerName: "creations.minDirMc", field: "minDirMc" },
    { headerName: "creations.maxDirMc", field: "maxDirMc" },
    { headerName: "creations.mcCalcOn", field: "mcOn" },
    { headerName: "creations.wastageCalcOn", field: "wastageOn" },
    { headerName: "creations.incWstMc", field: "incWstMc" }
]

export const rateForm = {
    mainGroup: { value: '', required: true, min: 2, max: 50, isValid: false, error: null },
    perGramRate: { value: '', required: true, min: 1, max: 20, isValid: false, error: null }
}

export const rateColumns = [
    { headerName: "", field: "mainGroupId", hide: true },
    { headerName: "creations.mainGroup", field: "mainGroup" },
    { headerName: "common.perGramRate", field: "perGramRate" },
    { headerName: "", field: "purityId", hide: true },
    { headerName: "common.Purity", field: "purity" },
    { headerName: "common.rate", field: "purityRate" },
    { headerName: "common.display", field: "display" }
]
