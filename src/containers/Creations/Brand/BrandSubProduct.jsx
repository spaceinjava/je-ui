import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { brandSubProductForm, brandSubProductColumns } from '../Constants';
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../../components/Grid";
import Select from "../../../components/Form/Select";
import { fetchBrands } from '../../../redux/actions/creations/brandActions'
import { fetchBrandProductsByBrand } from '../../../redux/actions/creations/brandProductActions'
import { saveBrandSubProduct, fetchBrandSubProducts, deleteBrandSubProducts } from "../../../redux/actions/creations/brandSubProductActions";
import Swal from 'sweetalert2'
import LoadingOverlay from 'react-loading-overlay';

export const BrandSubProduct = (props) => {
    const { t } = props;
    const [formData, setFromData] = useState(cloneDeep(brandSubProductForm));
    const [viewForm, setToggleView] = useState(true);
    let [selected, setSelected] = useState([]);
    const dispatch = useDispatch();
    const brands = useSelector(state => state.brandReducer.allBrands)
    const brandProducts = useSelector(state => state.brandProductReducer.allBrandProductsByBrand)
    const brandSubProducts = useSelector(state => state.brandSubProductReducer.allBrandSubProducts)
    const loader = useSelector(state => state.brandSubProductReducer.loading);

    const brandData = brands.data ? brands.data.map((brand) => {
        return {
            id: brand.id,
            name: brand.brandName,
            shortCode: brand.shortCode,
        }
    }) : [];

    const brandProductsData = brandProducts.data ? brandProducts.data.map((product) => {
        return {
            id: product.id,
            name: product.productName,
            shortCode: product.shortCode,
        }
    }) : [];

    const brandSubProductsData = brandSubProducts.data ? brandSubProducts.data.map((subProduct) => {
        return {
            id: subProduct.id,
            subProductName: subProduct.subProductName,
            shortCode: subProduct.shortCode,
            productId: subProduct.brandProduct.id,
            productName: subProduct.brandProduct.productName,
            brandId: subProduct.brandProduct.brand.id,
            brandName: subProduct.brandProduct.brand.brandName,
        }
    }) : [];

    const setFormValue = (field, value) => {
        formData[field]['value'] = value;
        setFromData({ ...formData });
    }
    const resetForm = () => {
        map(formData, function (value, key, object) {
            formData[key]['value'] = ''
        });
        setFromData({ ...formData });
    }
    const saveForm = () => {
        let isFormValid = true;
        map(formData, function (value, key, object) {
            const isValid = validateField(value);
            if (!isValid.isValid) {
                isFormValid = false;
            }
            formData[key] = { ...value, ...isValid }
        });
        if (isFormValid) {
            let data = {};
            map(formData, function (value, key, object) {
                data = {
                    ...data,
                    [key]: value.value
                }
            });
            Swal.fire({
                title: 'Confirm Saving Brand Sub Product Info.!',
                text: 'Continue to save / Cancel to stop',
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Continue'
            }).then((result) => {
                if (result.isConfirmed) {
                    dispatch(saveBrandSubProduct(data))
                        .then(() => {
                            resetForm()
                        })
                }
            })
        }
    }

    const validateField = (fieldObj) => {
        let errorObj = { isValid: true, error: null }
        if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
        else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
        else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }
        return errorObj;
    }

    const editAction = (ele) => {
        setFormValue('id', ele.id)
        setFormValue('subProductName', ele.subProductName)
        setFormValue('shortCode', ele.shortCode)
        setFormValue('brand', ele.brandId)
        setFormValue('brandProduct', ele.productId)
    }

    const deleteAction = (data) => {
        let selectedIds;
        if (data instanceof Array && data.length > 0) {
            selectedIds = data.map((ele) => {
                return ele.id
            })
        } else {
            selectedIds = data.id;
        }
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(deleteBrandSubProducts(selectedIds.toString()))
                    .then(() => {
                        selected = []
                        setSelected(selected);
                    })
            }
        })
    }

    useEffect(() => {
        dispatch(fetchBrands());
        dispatch(fetchBrandSubProducts());
    }, [])

    useEffect(() => {
        dispatch(fetchBrandProductsByBrand(get(formData, 'brand.value')));
    }, [get(formData, 'brand.value')])

    const toggleView = () => {
        const formNode = document.getElementById('form');
        formNode.classList.toggle('collapse-panel');
        setToggleView(!viewForm);
    }

    return (
        <div className="panel panel-default hero-panel">
            <LoadingOverlay
                active={loader}
                spinner
                text='Loading...'
            >
                <div className="panel-heading">
                    <h1 className="panel-title">{t("common.brand")} {t("common.sub")} {t("creations.product")} {t("common.creation")}</h1>
                    <div className="panel-options">
                        <a
                            href="#"
                            className="text-info collapseBtn"
                            title="Show Add Form"
                        >
                            {viewForm ?
                                <i className="zmdi zmdi-minus-circle  zmdi-hc-2x" onClick={() => { toggleView() }}>
                                </i> :
                                <i className="zmdi zmdi-plus-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                                </i>}
                        </a>
                    </div>
                </div>
                <div className="panel-body">
                    <div className="panel update-panel form-pnel-container" id="form">
                        <div className="text-danger">* {t("common.requiredFieldsMsg")} </div>
                        <div className="panel update-panel">
                            <div className="panel-body ">
                                <div className="row">
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("common.brand")}<span className="asterisk">*</span>
                                            </label>
                                            <Select name="brand" value={get(formData, 'brand.value')} onChange={(e) => setFormValue('brand', e.target.value)} data={brandData} />
                                            <div className="invalid-feedback">{get(formData, 'brand.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("common.brand")} {t("creations.product")}<span className="asterisk">*</span>
                                            </label>
                                            <Select name="brandProduct" value={get(formData, 'brandProduct.value')} onChange={(e) => setFormValue('brandProduct', e.target.value)} data={brandProductsData} />
                                            <div className="invalid-feedback">{get(formData, 'brandProduct.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("common.brand")} {t("common.sub")} {t("creations.product")}<span className="asterisk">*</span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="name"
                                                name="subProductName"
                                                value={get(formData, 'subProductName.value')}
                                                onChange={(e) => setFormValue('subProductName', e.target.value)}
                                            />
                                            <div className="invalid-feedback">{get(formData, 'subProductName.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("common.shortCode")}<span className="asterisk">*</span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="code"
                                                name="shortCode"
                                                value={get(formData, 'shortCode.value')}
                                                onChange={(e) => setFormValue('shortCode', e.target.value)}
                                            />
                                            <div className="invalid-feedback">{get(formData, 'shortCode.error')}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="panel-footer text-center">
                            <a href="#" className="btn btn-primary" onClick={saveForm}>
                                <i className="zmdi zmdi-check"></i> {t("common.save")}
                            </a>
                            <a href="#" className="btn btn-default" onClick={resetForm}>
                                <i className="zmdi zmdi-close"></i> {t("common.clear")}
                            </a>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">Brand Sub Products List</h3>
                            <div className="panel-options">
                                <p>
                                    {selected.length > 0 ?
                                        <a href="#" className="btn btn-info btn-sm" onClick={() => { deleteAction(selected) }}>
                                            <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                                        </a> :
                                        null}
                                </p>
                            </div>
                        </div>
                        <div className="panel-body">
                            <div className="ag-grid-wrapper">
                                <Grid colDefs={brandSubProductColumns} rowData={brandSubProductsData} selectedRows={setSelected} actions={{ isRequired: true, editAction: editAction, deleteAction: deleteAction }} />
                            </div>
                        </div>
                    </div>
                </div>
            </LoadingOverlay>
        </div>
    );
};

export default withTranslation()(BrandSubProduct);
