import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { brandProductForm, brandProductColumns } from '../Constants';
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../../components/Grid";
import Select from "../../../components/Form/Select";
import { fetchMainGroups } from '../../../redux/actions/mainGroupActions'
import { fetchBrandsByMainGroup } from '../../../redux/actions/creations/brandActions'
import { saveBrandProduct, fetchBrandProducts, deleteBrandProducts } from "../../../redux/actions/creations/brandProductActions";
import Swal from 'sweetalert2'
import LoadingOverlay from 'react-loading-overlay';

export const BrandProduct = (props) => {
    const { t } = props;
    const [formData, setFromData] = useState(cloneDeep(brandProductForm));
    const [viewForm, setToggleView] = useState(true);
    let [selected, setSelected] = useState([]);
    const dispatch = useDispatch();
    const mainGroups = useSelector(state => state.MainGroupReducer.allGroups)
    const groupBrands = useSelector(state => state.brandReducer.allMainGroupBrands)
    const brandProducts = useSelector(state => state.brandProductReducer.allBrandProducts)
    const loader = useSelector(state => state.brandProductReducer.loading);
    const groupData = mainGroups.data ? mainGroups.data.map((group) => {
        return {
            id: group.id,
            name: group.groupName,
            shortCode: group.shortCode,
        }
    }) : [];
    const groupBrandData = groupBrands.data ? groupBrands.data.map((brand) => {
        return {
            id: brand.id,
            name: brand.brandName,
            shortCode: brand.shortCode,
        }
    }) : [];
    const brandProductsData = brandProducts.data ? brandProducts.data.map((product) => {
        return {
            id: product.id,
            productName: product.productName,
            shortCode: product.shortCode,
            brandId: product.brand.id,
            brandName: product.brand.brandName,
            groupId: product.brand.mainGroup.id,
            groupName: product.brand.mainGroup.groupName,
            gst: product.gst,
            tray: product.tray,
        }
    }) : [];
    const setFormValue = (field, value) => {
        formData[field]['value'] = value;
        setFromData({ ...formData });
    }
    const resetForm = () => {
        map(formData, function (value, key, object) {
            if (key === 'tray') {
                document.getElementById("tray").checked = false;
                setFormValue('tray', false)
            }
            else {
                formData[key]['value'] = '';
            }
        });
        setFromData({ ...formData });
    }
    const saveForm = () => {
        let isFormValid = true;
        map(formData, function (value, key, object) {
            const isValid = validateField(value);
            if (!isValid.isValid) {
                isFormValid = false;
            }
            formData[key] = { ...value, ...isValid }
        });
        if (isFormValid) {
            let data = {};
            map(formData, function (value, key, object) {
                data = {
                    ...data,
                    [key]: value.value
                }
            });
            Swal.fire({
                title: 'Confirm Saving Brand Product Info.!',
                text: 'Continue to save / Cancel to stop',
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Continue'
            }).then((result) => {
                if (result.isConfirmed) {
                    dispatch(saveBrandProduct(data))
                        .then(() => {
                            resetForm()
                        })
                }
            })
        }
    }

    const validateField = (fieldObj) => {
        let errorObj = { isValid: true, error: null }
        if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
        else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
        else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }
        return errorObj;
    }

    const editAction = (ele) => {
        setFormValue('id', ele.id)
        setFormValue('productName', ele.productName)
        setFormValue('shortCode', ele.shortCode)
        setFormValue('brand', ele.brandId)
        setFormValue('mainGroup', ele.groupId)
        setFormValue('gst', ele.gst)
        document.getElementById("tray").checked = ele.tray;
        setFormValue('tray', ele.tray)
    }

    const deleteAction = (data) => {
        let selectedIds;
        if (data instanceof Array && data.length > 0) {
            selectedIds = data.map((ele) => {
                return ele.id
            })
        } else {
            selectedIds = data.id;
        }
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(deleteBrandProducts(selectedIds.toString()))
                    .then(() => {
                        selected = []
                        setSelected(selected);
                    })
            }
        })
    }

    useEffect(() => {
        dispatch(fetchMainGroups());
        dispatch(fetchBrandProducts());
    }, [])

    useEffect(() => {
        dispatch(fetchBrandsByMainGroup(get(formData, 'mainGroup.value')));
    }, [get(formData, 'mainGroup.value')])

    const toggleView = () => {
        const formNode = document.getElementById('form');
        formNode.classList.toggle('collapse-panel');
        setToggleView(!viewForm);
    }

    return (
        <div className="panel panel-default hero-panel">
            <LoadingOverlay
                active={loader}
                spinner
                text='Loading...'
            >
                <div className="panel-heading">
                    <h1 className="panel-title">{t("common.brand")} {t("creations.product")} {t("common.creation")}</h1>
                    <div className="panel-options">
                        <a
                            href="#"
                            className="text-info collapseBtn"
                            title="Show Add Form"
                        >
                            {viewForm ?
                                <i className="zmdi zmdi-minus-circle  zmdi-hc-2x" onClick={() => { toggleView() }}>
                                </i> :
                                <i className="zmdi zmdi-plus-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                                </i>}
                        </a>
                    </div>
                </div>
                <div className="panel-body">
                    <div className="panel update-panel form-pnel-container" id="form">
                        <div className="text-danger">* {t("common.requiredFieldsMsg")} </div>
                        <div className="panel update-panel">
                            <div className="panel-body ">
                                <div className="row">
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("creations.mainGroup")}<span className="asterisk">*</span>
                                            </label>
                                            <Select name="mainGroup" value={get(formData, 'mainGroup.value')} onChange={(e) => setFormValue('mainGroup', e.target.value)} data={groupData} />
                                            <div className="invalid-feedback">{get(formData, 'mainGroup.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("common.brand")}<span className="asterisk">*</span>
                                            </label>
                                            <Select name="brand" value={get(formData, 'brand.value')} onChange={(e) => setFormValue('brand', e.target.value)} data={groupBrandData} />
                                            <div className="invalid-feedback">{get(formData, 'brand.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("common.brand")} {t("creations.product")}<span className="asterisk">*</span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="name"
                                                name="productName"
                                                value={get(formData, 'productName.value')}
                                                onChange={(e) => setFormValue('productName', e.target.value)}
                                            />
                                            <div className="invalid-feedback">{get(formData, 'productName.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("common.shortCode")}<span className="asterisk">*</span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="code"
                                                name="shortCode"
                                                value={get(formData, 'shortCode.value')}
                                                onChange={(e) => setFormValue('shortCode', e.target.value)}
                                            />
                                            <div className="invalid-feedback">{get(formData, 'shortCode.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("common.gst")}<span className="asterisk">*</span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="gst"
                                                name="gst"
                                                value={get(formData, 'gst.value')}
                                                onChange={(e) => setFormValue('gst', e.target.value)}
                                            />
                                            <div className="invalid-feedback">{get(formData, 'gst.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <div className="checkbox checkbox-primary">
                                                <input
                                                    type="checkbox"
                                                    name="tray"
                                                    id="tray"
                                                    defaultChecked={get(formData, 'tray.value')}
                                                    onChange={(e) => setFormValue('tray', !formData.tray.value)} />
                                                <label htmlFor="">{t("creations.tray")}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="panel-footer text-center">
                            <a href="#" className="btn btn-primary" onClick={saveForm}>
                                <i className="zmdi zmdi-check"></i> {t("common.save")}
                            </a>
                            <a href="#" className="btn btn-default" onClick={resetForm}>
                                <i className="zmdi zmdi-close"></i> {t("common.clear")}
                            </a>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">Brand Products List</h3>
                            <div className="panel-options">
                                <p>
                                    {selected.length > 0 ?
                                        <a href="#" className="btn btn-info btn-sm" onClick={() => { deleteAction(selected) }}>
                                            <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                                        </a> :
                                        null}
                                </p>
                            </div>
                        </div>
                        <div className="panel-body">
                            <div className="ag-grid-wrapper">
                                <Grid colDefs={brandProductColumns} rowData={brandProductsData} selectedRows={setSelected} actions={{ isRequired: true, editAction: editAction, deleteAction: deleteAction }} />
                            </div>
                        </div>
                    </div>
                </div>
            </LoadingOverlay>
        </div>
    );
};

export default withTranslation()(BrandProduct);
