import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import SecondMenu from "./../../components/SecondMenu";

export const Menu = (props) => {
  const { t } = props;
  const [viewMenu, setMenuView] = React.useState(true);
  const productMenuOptions = [
    {
      name: "mainGroup",
      active: false,
      href: "/creations/mainGroup",
      title: t("creations.mainGroup"),
    },
    {
      name: "product",
      active: false,
      href: "/creations/product/name",
      title: t("creations.product"),
    },
    {
      name: "subProduct",
      active: false,
      href: "/creations/product/sub",
      title: t("creations.subProduct"),
    },
    {
      name: "productSize",
      active: false,
      href: "/creations/product/size",
      title: t("creations.productSize"),
    },
    {
      name: "saleWastage",
      active: false,
      href: "/creations/product/sale-wastage",
      title: t("creations.saleWastage"),
    },
    {
      name: "partyWastage",
      active: false,
      href: "/creations/product/party-wastage",
      title: t("creations.partyWastage"),
    }
  ];
  const stoneMenuOptions = [
    {
      name: "stoneGroup",
      active: false,
      href: "/creations/stone/group",
      title: t("common.stone") + " " + t("common.group"),
    },
    {
      name: "stoneName",
      active: false,
      href: "/creations/stone/name",
      title: t("common.stone") + " " + t("common.name"),
    },
    {
      name: "stoneSize",
      active: false,
      href: "/creations/stone/size",
      title: t("common.stone") + " " + t("common.size"),
    },
    {
      name: "stoneColor",
      active: false,
      href: "/creations/stone/color",
      title: t("common.stone") + " " + t("common.color"),
    },
    {
      name: "stoneClarity",
      active: false,
      href: "/creations/stone/clarity",
      title: t("common.stone") + " " + t("common.clarity"),
    },
    {
      name: "stonePolish",
      active: false,
      href: "/creations/stone/polish",
      title: t("common.stone") + " " + t("common.polish"),
    },
    {
      name: "partyRate",
      active: false,
      href: "/creations/stone/party-rate",
      title: t("common.Party") + " " + t("common.stone") + " " + t("common.rate"),
    },
    {
      name: "saleRate",
      active: false,
      href: "/creations/stone/sale-rate",
      title: t("common.sale") + " " + t("common.stone") + " " + t("common.rate"),
    }
  ];
  const brandMenuOptions = [
    {
      name: "brand",
      active: false,
      href: "/creations/brand/creation",
      title: t("common.brand"),
    },
    {
      name: "brand",
      active: false,
      href: "/creations/brand/product",
      title: t("common.brand") + " " + t("creations.product"),
    },
    {
      name: "brand",
      active: false,
      href: "/creations/brand/sub-product",
      title: t("common.brand") + " " + t("common.sub") + " " + t("creations.product"),
    }
  ];

  const otherMenuOptions = [
    {
      name: "floor",
      active: false,
      href: "/creations/floor",
      title: t("common.floor"),
    },
    {
      name: "counter",
      active: false,
      href: "/creations/counter",
      title: t("common.counter"),
    },
    {
      name: "purity",
      active: false,
      href: "/creations/purity",
      title: t("common.Purity"),
    },
    {
      name: "rate",
      active: false,
      href: "/creations/rate",
      title: t("common.rate"),
    },
    {
      name: "gst",
      active: false,
      href: "/creations/gst",
      title: t("creations.gst"),
    },
    {
      name: "hsn",
      active: false,
      href: "/creations/hsn",
      title: t("common.hsn") + " " + t("common.codes") ,
    },
  ]

  const stoneMenu = () => {
    let active = false;
    return {
      id: "stoneMasters",
      name: t("common.stone") + " " + t("creations.master"),
      menuItems: stoneMenuOptions.map(eachMenu => {
        if (eachMenu.href.includes(props.pathname)) {
          active = true;
        }
        return { ...eachMenu, active: eachMenu.href.includes(props.pathname) }
      }),
      active
    };
  }

  const brandMenu = () => {
    let active = false;
    return {
      id: "brandMasters",
      name: t("common.brand") + " " + t("creations.master"),
      menuItems: brandMenuOptions.map(eachMenu => {
        if (eachMenu.href.includes(props.pathname)) {
          active = true;
        }
        return { ...eachMenu, active: eachMenu.href.includes(props.pathname) }
      }),
      active
    };
  }

  const productMenu = () => {
    let active = false;
    let menuItems = productMenuOptions.map(eachMenu => {
      if (eachMenu.href.includes(props.pathname)) {
        active = true;
      }
      return { ...eachMenu, active: eachMenu.href.includes(props.pathname) }
    });
    // let brandItems = brandMenu();
    // active = brandItems.active || active;
    // menuItems.push(brandItems)
    return {
      id: "productMasters",
      name: t("creations.product") + " " + t("creations.master"),
      menuItems: menuItems,
      active
    };
  }

  const otherMenu = () => {
    let active = false;
    let menuItems = otherMenuOptions.map(eachMenu => {
      if (eachMenu.href.includes(props.pathname)) {
        active = true;
      }
      return { ...eachMenu, active: eachMenu.href.includes(props.pathname) }
    });
    return {
      id: "otherMasters",
      name: t("common.other") + " " + t("creations.master"),
      menuItems: menuItems,
      active
    };
  }

  const menuOptions = [
    productMenu(),
    stoneMenu(),
    brandMenu(),
    otherMenu()
  ];

  return (
    <div className="left-nav-holder">
      <ul className="left-subnav" id="sidebar">
        <SecondMenu menuOptions={menuOptions} />
      </ul>
    </div>
  );
};

export default withTranslation()(Menu);
