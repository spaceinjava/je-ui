import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { rateForm, rateColumns } from '../Constants';
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../../components/Grid";
import Select from "../../../components/Form/Select";
import { fetchMainGroups } from '../../../redux/actions/mainGroupActions'
import { saveRate, fetchRates, fetchCalculatedRates } from "../../../redux/actions/creations/rateActions";
import Swal from 'sweetalert2'
import Switch from 'react-switch';
import LoadingOverlay from 'react-loading-overlay';

export const Rate = (props) => {
    const { t } = props;
    const [formData, setFromData] = useState(cloneDeep(rateForm));
    const [viewForm, setToggleView] = useState(true);
    let [selected, setSelected] = useState([]);
    const [rates, setRates] = useState([]);
    const [checkClicked, setCheckClicked] = useState(false);
    const [editClicked, setEditClicked] = useState(false);
    const dispatch = useDispatch();
    const mainGroups = useSelector(state => state.MainGroupReducer.allGroups)
    const calculatedRates = useSelector(state => state.rateReducer.calculatedRates)
    const loader = useSelector(state => state.rateReducer.loading)
    const allRates = useSelector(state => state.rateReducer.allRates)

    const mainGroupData = mainGroups.data ? mainGroups.data.map((group) => {
        return {
            id: group.id,
            name: group.groupName,
            shortCode: group.shortCode,
        }
    }) : [];
    let calculatedRatesData = calculatedRates.data ? calculatedRates.data.map((rate) => {
        return {
            id: rate.id,
            isDeleted: rate.isDeleted,
            tenantCode: rate.tenantCode,
            mainGroup: rate.mainGroup,
            perGramRate: rate.perGramRate,
            purity: rate.purity,
            purityRate: rate.purityRate,
            display: rate.display
        }
    }) : [];
    const allRatesData = allRates.data ? allRates.data.map((rate) => {
        return {
            id: rate.id,
            isDeleted: rate.isDeleted,
            tenantCode: rate.tenantCode,
            mainGroupId: rate.mainGroup.id,
            mainGroup: rate.mainGroup.groupName,
            perGramRate: rate.perGramRate,
            purityId: rate.purity.id,
            purity: rate.purity.purity,
            purityRate: rate.purityRate,
            display: rate.display
        }
    }) : [];

    const setFormValue = (field, value) => {
        setRates([]);
        formData[field]['value'] = value;
        setFromData({ ...formData });
    }
    const resetForm = () => {
        setRates([]);
        map(formData, function (value, key, object) {
            formData[key]['value'] = ''
        });
        setFromData({ ...formData });
    }
    const saveForm = () => {
        Swal.fire({
            title: 'Confirm Saving Rates.!',
            text: 'Continue to save / Cancel to stop',
            icon: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Continue'
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(saveRate(rates))
                    .then(() => {
                        resetForm();
                    })
            }
        })
    }

    const validateField = (fieldObj) => {
        let errorObj = { isValid: true, error: null }
        if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
        else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
        else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }
        return errorObj;
    }

    const fetchPurityRates = () => {
        let isFormValid = true;
        map(formData, function (value, key, object) {
            const isValid = validateField(value);
            if (!isValid.isValid) {
                isFormValid = false;
            }
            formData[key] = { ...value, ...isValid }
        });
        if (isFormValid) {
            let data = {};
            map(formData, function (value, key, object) {
                data = {
                    ...data,
                    [key]: value.value
                }
            });
            if (allRatesData.find(ele => ele.mainGroupId === data.mainGroup)) {
                Swal.fire({
                    title: 'MainGroup has rates already associated.!',
                    text: 'Do you want to override with latest.?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Continue'
                }).then((result) => {
                    if (result.isConfirmed) {
                        dispatch(fetchCalculatedRates(data.mainGroup, data.perGramRate))
                            .then(() => setCheckClicked(true))
                    }
                })
            } else {
                dispatch(fetchCalculatedRates(data.mainGroup, data.perGramRate))
                    .then(() => setCheckClicked(true))
            }
        }
    }

    const displayChange = (nextChecked, event, id) => {
        rates[id].display = nextChecked;
        setRates([...rates]);
    }

    const editAction = (ele) => {
        dispatch(fetchCalculatedRates(ele.mainGroupId)).then(() => setEditClicked(true))
        setFormValue('mainGroup', ele.mainGroupId)
        setFormValue('perGramRate', ele.perGramRate)
    }

    useEffect(() => {
        setRates(calculatedRatesData);
        setEditClicked(false);
    }, [editClicked]);

    useEffect(() => {
        setRates(calculatedRatesData);
        setCheckClicked(false);
    }, [checkClicked]);

    useEffect(() => {
        dispatch(fetchMainGroups());
        dispatch(fetchRates());
    }, [])

    const toggleView = () => {
        const formNode = document.getElementById('form');
        formNode.classList.toggle('collapse-panel');
        setToggleView(!viewForm);
    }

    return (
        <div className="panel panel-default hero-panel">
            <LoadingOverlay
                active={loader}
                spinner
                text='Loading...'
            >
                <div className="panel-heading">
                    <h1 className="panel-title">{t("common.rate")} {t("common.creation")}</h1>
                    <div className="panel-options">
                        <a
                            href="#"
                            className="text-info collapseBtn"
                            title="Show Add Form"
                        >
                            {viewForm ?
                                <i className="zmdi zmdi-minus-circle  zmdi-hc-2x" onClick={() => { toggleView() }}>
                                </i> :
                                <i className="zmdi zmdi-plus-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                                </i>}
                        </a>
                    </div>
                </div>
                <div className="panel-body">
                    <div className="panel update-panel form-pnel-container" id="form">
                        <div className="text-danger">* {t("common.requiredFieldsMsg")} </div>
                        <div className="panel update-panel">
                            <div className="panel-body ">
                                <div className="row">
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("creations.mainGroup")}<span className="asterisk">*</span>
                                            </label>
                                            <Select name="mainGroup" value={get(formData, 'mainGroup.value')} onChange={(e) => setFormValue('mainGroup', e.target.value)} data={mainGroupData} />
                                            <div className="invalid-feedback">{get(formData, 'mainGroup.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("common.rate")}<span className="asterisk">*</span> <span className="text-primary">1 Gram Value</span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="rate"
                                                name="perGramRate"
                                                value={get(formData, 'perGramRate.value')}
                                                onChange={(e) => setFormValue('perGramRate', e.target.value)}
                                            />
                                            <div className="invalid-feedback">{get(formData, 'perGramRate.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <a href="#" className="btn btn-primary" onClick={fetchPurityRates}>
                                                <i className="zmdi zmdi-check"></i> Check
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {rates.length > 0 ?
                            <div className="panel-body ">
                                <h4>Purity Details</h4>
                                <table id="" className="table table-bordered" style={{ "width": "350px" }}>
                                    <thead>
                                        <tr>
                                            <th>Display</th>
                                            <th>Purity</th>
                                            <th>Rate</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {rates.map((prd, index) =>
                                            <tr key={`tr${index}`}>
                                                <td style={{ "width": "20%" }}><Switch checked={prd.display} onChange={displayChange} id={index.toString()}
                                                    offColor="#ccc"
                                                    onColor="#2196F3"
                                                    // onHandleColor="#2693e6"
                                                    handleDiameter={16}
                                                    uncheckedIcon={false}
                                                    checkedIcon={false}
                                                    boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                                                    // activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                                                    height={20}
                                                    width={43}
                                                /></td>
                                                <td>{prd.purity.purity}</td>
                                                <td>{prd.purityRate}</td>
                                            </tr>
                                        )
                                        }
                                    </tbody>
                                </table>
                            </div>
                            :
                            <div />
                        }
                        <div className="panel-footer text-center">
                            {rates.length > 0 ? <a href="#" className="btn btn-primary" onClick={saveForm}>
                                <i className="zmdi zmdi-check"></i> {t("common.save")}
                            </a> : <></>}
                            <a href="#" className="btn btn-default" onClick={resetForm}>
                                <i className="zmdi zmdi-close"></i> {t("common.clear")}
                            </a>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">Rates List</h3>
                        </div>
                        <div className="panel-body">
                            <div className="ag-grid-wrapper">
                                <Grid colDefs={rateColumns} rowData={allRatesData} selectedRows={setSelected} actions={{ editAction: editAction }} />
                            </div>
                        </div>
                    </div>
                </div>
            </LoadingOverlay>
        </div>
    );
};
export default withTranslation()(Rate);
