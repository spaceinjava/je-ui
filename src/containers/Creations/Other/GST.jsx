import React, { Component, useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { gstForm, gstColumns } from '../Constants';
import Menu from "../Menu";
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../../components/Grid";
import Select from "../../../components/Form/Select";
import { fetchMetalType, saveGST, fetchGST, deleteGST } from '../../../redux/actions/creations/gstActions'
import Swal from 'sweetalert2'
import LoadingOverlay from 'react-loading-overlay';

export const GSTCreation = (props) => {
    const { t } = props;
    const [formData, setFromData] = useState(cloneDeep(gstForm));
    const [viewForm, setToggleView] = useState(true);
    let [selected, setSelected] = useState([]);
    const dispatch = useDispatch();
    const metalTypes = useSelector(state => state.gstReducer.metalTypes)
    const gst = useSelector(state => state.gstReducer.allgst)
    const loader = useSelector(state => state.gstReducer.loading)
    const metalData = metalTypes.data ? metalTypes.data.map((metal) => {
        return {
            id: metal.id,
            name: metal.metalType,
            shortCode: metal.shortCode,
        }
    }) : [];
    const gstData = gst.data ? gst.data.map((item) => {
        return {
            id: item.id,
            metalId: item.metalType.id,
            metalType: item.metalType.metalType,
            percent: item.percent,
        }
    }) : [];
    const setFormValue = (field, value) => {
        formData[field]['value'] = value;
        setFromData({ ...formData });
    }
    const resetForm = () => {
        map(formData, function (value, key, object) {
            formData[key]['value'] = ''
        });
        setFromData({ ...formData });
    }

    const saveForm = () => {
        let isFormValid = true;
        map(formData, function (value, key, object) {
            const isValid = validateField(value);
            if (!isValid.isValid) {
                isFormValid = false;
            }
            formData[key] = { ...value, ...isValid }
        });
        if (isFormValid) {
            let data = {};
            map(formData, function (value, key, object) {
                data = {
                    ...data,
                    [key]: value.value
                }
            });
            Swal.fire({
                title: 'Confirm Saving GST Info.!',
                text: 'Continue to save / Cancel to stop',
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Continue'
            }).then((result) => {
                if (result.isConfirmed) {
                    dispatch(saveGST(data))
                        .then(() => {
                            resetForm()
                        })
                }
            })
        }
    }

    const validateField = (fieldObj) => {
        let errorObj = { isValid: true, error: null }
        if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
        else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
        else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }
        return errorObj;
    }

    const editAction = (ele) => {
        setFormValue('id', ele.id)
        setFormValue('metalType', ele.metalId)
        setFormValue('percent', ele.percent)
    }

    const deleteAction = (data) => {
        let selectedIds;
        if (data instanceof Array && data.length > 0) {
            selectedIds = data.map((ele) => {
                return ele.id
            })
        } else {
            selectedIds = data.id;
        }
        Swal.fire({
            title: 'Confirm Deleting GST.!',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(deleteGST(selectedIds.toString()))
                    .then(() => {
                        selected = []
                        setSelected(selected);
                    })
            }
        })
    }

    useEffect(() => {
        dispatch(fetchMetalType());
        dispatch(fetchGST());
    }, [])

    const toggleView = () => {
        const formNode = document.getElementById('form');
        formNode.classList.toggle('collapse-panel');
        setToggleView(!viewForm);
    }

    return (
        <div className="panel panel-default hero-panel">
            <LoadingOverlay
                active={loader}
                spinner
                text='Loading...'
            >
                <div className="panel-heading">
                    <h1 className="panel-title">{t("creations.gst")} {t("common.creation")}</h1>
                    <div className="panel-options">
                        <a
                            href="#"
                            className="text-info collapseBtn"
                            title="Show Add Form"
                        >
                            {viewForm ?
                                <i className="zmdi zmdi-minus-circle  zmdi-hc-2x" onClick={() => { toggleView() }}>
                                </i> :
                                <i className="zmdi zmdi-plus-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                                </i>}
                        </a>
                    </div>
                </div>
                <div className="panel-body">
                    <div className="panel update-panel form-pnel-container" id="form">
                        <div className="text-danger">* {t("common.requiredFieldsMsg")} </div>
                        <div className="panel update-panel">
                            <div className="panel-body ">
                                <div className="row">
                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("common.metal")} {t("common.type")}<span className="asterisk">*</span>
                                            </label>
                                            <Select name="metalType" value={get(formData, 'metalType.value')} onChange={(e) => setFormValue('metalType', e.target.value)} data={metalData} />
                                            <div className="invalid-feedback">{get(formData, 'metalType.error')}</div>
                                        </div>
                                    </div>

                                    <div className="col-sm-6 col-md-3 col-lg-3">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("common.gst")}<span className="asterisk">*</span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder=""
                                                name="percent"
                                                value={get(formData, 'percent.value')}
                                                onChange={(e) => setFormValue('percent', e.target.value)}
                                            />
                                            <div className="invalid-feedback">{get(formData, 'percent.error')}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="panel-footer text-center">
                            <a href="#" className="btn btn-primary fire" onClick={saveForm}>
                                <i className="zmdi zmdi-check"></i> {t("common.save")}
                            </a>
                            <a href="#" className="btn btn-default" onClick={resetForm}>
                                <i className="zmdi zmdi-close"></i> {t("common.clear")}
                            </a>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">GST List</h3>
                            <div className="panel-options">
                                <p>
                                    {selected.length > 0 ?
                                        <a href="#" className="btn btn-info btn-sm" onClick={() => deleteAction(selected)} >
                                            <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                                        </a> :
                                        null}
                                </p>
                            </div>
                        </div>
                        <div className="panel-body">
                            <div className="ag-grid-wrapper">
                                <Grid colDefs={gstColumns} rowData={gstData} selectedRows={setSelected} actions={{ isRequired: true, editAction: editAction, deleteAction: deleteAction }} />
                            </div>
                        </div>
                    </div>
                </div>
            </LoadingOverlay>
        </div>
    );
};

export default withTranslation()(GSTCreation);
