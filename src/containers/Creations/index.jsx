import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import Menu from "./Menu";
import { get } from "lodash";
import MainGroup from './Products/MainGroup';
import Product from './Products/Product'
import SubProduct from './Products/SubProduct'
import ProductSize from './Products/ProductSize'
import SaleWastage from './Products/SaleWastage'
import PartyWastage from './Products/PartyWastage'
import Brand from './Brand/Brand'
import BrandProduct from './Brand/BrandProduct'
import BrandSubProduct from './Brand/BrandSubProduct'
import StoneGroup from './Stone/StoneGroup'
import StoneSize from "./Stone/StoneSize";
import StoneClarity from "./Stone/StoneClarity";
import StoneColor from "./Stone/StoneColor";
import StonePolish from "./Stone/StonePolish";
import Stone from "./Stone/Stone";
import Floor from './Other/Floor';
import Counter from './Other/Counter'
import Purity from './Other/Purity'
import Rate from './Other/Rate'
import GST from './Other/GST'
import HSNCodes from './Other/HSNCodes'
import PartyStoneRate from "./Stone/PartyStoneRate";
import SaleStoneRate from "./Stone/SaleStoneRate";

export const Creations = (props) => {
  const { t } = props;
  const pathname = get(props, "history.location.pathname");
  const [viewMenu, setMenuView] = React.useState(true);
  const renderRightSideComponent = () => {
    switch (pathname) {
      case "/creations/mainGroup":
        return <MainGroup />;
      case "/creations/product/name":
        return <Product />;
      case "/creations/product/sub":
        return <SubProduct />;
      case "/creations/product/size":
        return <ProductSize />;
      case "/creations/product/sale-wastage":
        return <SaleWastage />;
      case "/creations/product/party-wastage":
        return <PartyWastage />;
      case "/creations/brand/creation":
        return <Brand />;
      case "/creations/brand/product":
        return <BrandProduct />;
      case "/creations/brand/sub-product":
        return <BrandSubProduct />;
      case "/creations/stone/group":
        return <StoneGroup />;
      case "/creations/stone/name":
        return <Stone />;
      case "/creations/stone/size":
        return <StoneSize />;
      case "/creations/stone/color":
        return <StoneColor />;
      case "/creations/stone/clarity":
        return <StoneClarity />;
      case "/creations/stone/polish":
        return <StonePolish />;
      case "/creations/stone/party-rate":
        return <PartyStoneRate />;
      case "/creations/stone/sale-rate":
        return <SaleStoneRate />;
      case "/creations/floor":
        return <Floor />;
      case "/creations/counter":
        return <Counter />;
      case "/creations/purity":
        return <Purity />;
      case "/creations/rate":
        return <Rate />;
      case "/creations/gst":
        return <GST />;
      case "/creations/hsn":
        return <HSNCodes />;
      default:
        return <div>{pathname}</div>;
    }
  };
  const onToggleMenu = () => {
    const menuToggle = !viewMenu;
    console.log({ menuToggle });
    setMenuView(menuToggle)
  }
  return (
    <div className="innerContainer">
      <div className={viewMenu ? "innerLeftNav" : "innerLeftNav clicked"}>
        <Menu pathname={pathname} onToggleMenu={onToggleMenu} />
      </div>
      <div className={viewMenu ? "innerRightCntnt" : "innerRightCntnt widthChange"}>
        <a onClick={() => { onToggleMenu() }} className="btn-collapse">
          <i className={viewMenu ? "zmdi zmdi-chevron-left" : "zmdi zmdi-chevron-right"}></i>
        </a>
        {renderRightSideComponent()}
      </div>
    </div>
  );
};

export default withTranslation()(Creations);
