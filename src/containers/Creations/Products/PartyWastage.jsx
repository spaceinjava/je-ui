import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { partyWastageForm, partyWastageColumns } from '../Constants';
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../../components/Grid";
import Select from "../../../components/Form/Select";
import { fetchSuppliers } from '../../../redux/actions/Admin/supplierActions'
import { fetchMainGroups, fetchProductsByMainGroup, fetchPuritiesByMainGroup } from '../../../redux/actions/mainGroupActions'
import { fetchSubProductsByProduct, fetchSizesByProduct } from "../../../redux/actions/creations/productActions";
import { savePartyWastage, fetchPartyWastages, deletePartyWastages } from "../../../redux/actions/creations/partyWastageActions";
import Swal from 'sweetalert2'
import LoadingOverlay from 'react-loading-overlay';

export const PartyWastage = (props) => {
    const { t } = props;
    const [formData, setFromData] = useState(cloneDeep(partyWastageForm));
    let [selected, setSelected] = useState([]);
    const [viewForm, setToggleView] = useState(true);
    const dispatch = useDispatch();
    const suppliers = useSelector(state => state.supplierReducer.allSuppliers)
    const mainGroups = useSelector(state => state.MainGroupReducer.allGroups)
    const products = useSelector(state => state.MainGroupReducer.allProductsByMainGroup)
    const subProducts = useSelector(state => state.productReducer.allSubProductsByProduct)
    const productSizes = useSelector(state => state.productReducer.allSizesByProduct)
    const partyWastages = useSelector(state => state.partyWastageReducer.allPartyWastages)
    const loader = useSelector(state => state.partyWastageReducer.loading);
    const purities = useSelector(state => state.MainGroupReducer.allPuritiesByMainGroup)
    const suppliersData = suppliers.data ? suppliers.data.map((supplier) => {
        return {
            id: supplier.id,
            name: supplier.partyName
        }
    }) : [];
    const mainGroupData = mainGroups.data ? mainGroups.data.map((group) => {
        return {
            id: group.id,
            name: group.groupName,
            shortCode: group.shortCode,
        }
    }) : [];
    const productsData = products.data ? products.data.map((product) => {
        return {
            id: product.id,
            name: product.productName,
            shortCode: product.shortCode,
        }
    }) : [];
    const subProductsData = subProducts.data ? subProducts.data.map((subProduct) => {
        return {
            id: subProduct.id,
            name: subProduct.subProductName,
            shortCode: subProduct.shortCode,
        }
    }) : [];
    const puritiesData = purities.data ? purities.data.map((purity) => {
        return {
            id: purity.id,
            name: purity.purity,
        }
    }) : [];
    const productSizesData = productSizes.data ? productSizes.data.map((productSize) => {
        return {
            id: productSize.id,
            name: productSize.productSize
        }
    }) : [];

    const fetchPartyNameById = (partyId) => {
        let selectedParty = suppliersData.find(ele => ele.id === partyId);
        return selectedParty ? selectedParty.name : "";
    }

    const partyWastagesData = partyWastages.data ? partyWastages.data.map((partyWastage) => {
        const product = partyWastage.subProduct.product;
        return {
            id: partyWastage.id,
            partyId: partyWastage.partyId,
            party: fetchPartyNameById(partyWastage.partyId),
            mainGroupId: product.mainGroup.id,
            mainGroup: product.mainGroup.groupName,
            productId: product.id,
            product: product.productName,
            subProductId: partyWastage.subProduct.id,
            subProduct: partyWastage.subProduct.subProductName,
            productSizeId: partyWastage.productSize.id,
            productSize: partyWastage.productSize.productSize,
            purityId: partyWastage.purity.id,
            purity: partyWastage.purity.purity,
            touchValue: partyWastage.touchValue,
            touchPercent: partyWastage.touchPercent,
            mcPerGram: partyWastage.mcPerGram,
            mcPerDir: partyWastage.mcPerDir,
            wastagePerGram: partyWastage.wastagePerGram,
            wastagePerDir: partyWastage.wastagePerDir,
            // mcOn: partyWastage.mcOn,
            // wastageOn: partyWastage.wastageOn,
        }
    }) : [];
    const setFormValue = (field, value) => {
        formData[field]['value'] = value;
        setFromData({ ...formData });
    }
    const resetForm = () => {
        map(formData, function (value, key, object) {
            // if (key === 'mcOnGross') {
            //     document.getElementById("mcOnGross").checked = false;
            //     setFormValue('mcOnGross', false)
            // } else if (key === 'mcOnNet') {
            //     document.getElementById("mcOnNet").checked = false;
            //     setFormValue('mcOnNet', false)
            // } else if (key === 'wastageOnGross') {
            //     document.getElementById("wastageOnGross").checked = false;
            //     setFormValue('wastageOnGross', false)
            // } else if (key === 'wastageOnNet') {
            //     document.getElementById("wastageOnNet").checked = false;
            //     setFormValue('wastageOnNet', false)
            // } else {
                formData[key]['value'] = '';
            // }
        });
        setFromData({ ...formData });
    }
    const saveForm = () => {
        let isFormValid = true;
        map(formData, function (value, key, object) {
            const isValid = validateField(value);
            if (!isValid.isValid) {
                isFormValid = false;
            }
            formData[key] = { ...value, ...isValid }
        });
        if (isFormValid) {
            let data = {};
            map(formData, function (value, key, object) {
                data = {
                    ...data,
                    [key]: value.value
                }
            });
            Swal.fire({
                title: 'Confirm Saving Party Wastage Info.!',
                text: 'Continue to save / Cancel to stop',
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Continue'
            }).then((result) => {
                if (result.isConfirmed) {
                    dispatch(savePartyWastage(data))
                        .then(() => {
                            resetForm()
                        })
                }
            })
        }
    }

    const validateField = (fieldObj) => {
        let errorObj = { isValid: true, error: null }
        if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
        else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
        else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }
        return errorObj;
    }

    const editAction = (ele) => {
        setFormValue('id', ele.id)
        setFormValue('partyId', ele.partyId)
        setFormValue('party', ele.party)
        setFormValue('mainGroup', ele.mainGroupId)
        setFormValue('product', ele.productId)
        setFormValue('subProduct', ele.subProductId)
        setFormValue('productSize', ele.productSizeId)
        setFormValue('purity', ele.purityId)
        setFormValue('touchValue', ele.touchValue)
        setFormValue('touchPercent', ele.touchPercent)
        setFormValue('mcPerGram', ele.mcPerGram)
        setFormValue('mcPerDir', ele.mcPerDir)
        setFormValue('wastagePerGram', ele.wastagePerGram)
        setFormValue('wastagePerDir', ele.wastagePerDir)
        // document.getElementById("mcOn").checked = ele.mcOn;
        // setFormValue('mcOn', ele.mcOn)
        // document.getElementById("wastageOn").checked = ele.wastageOn;
        // setFormValue('wastageOn', ele.wastageOn)
    }

    const deleteAction = (data) => {
        let selectedIds;
        if (data instanceof Array && data.length > 0) {
            selectedIds = data.map((ele) => {
                return ele.id
            })
        } else {
            selectedIds = data.id;
        }
        Swal.fire({
            title: 'Confirm Deleting Party Wastage.!',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                dispatch(deletePartyWastages(selectedIds.toString()))
                    .then(() => {
                        selected = []
                        setSelected(selected);
                    })
            }
        })
    }

    useEffect(() => {
        dispatch(fetchSuppliers());
        dispatch(fetchMainGroups());
        dispatch(fetchPartyWastages());
    }, [])

    useEffect(() => {
        dispatch(fetchProductsByMainGroup(get(formData, 'mainGroup.value')));
        dispatch(fetchPuritiesByMainGroup(get(formData, 'mainGroup.value')));
    }, [get(formData, 'mainGroup.value')])

    useEffect(() => {
        dispatch(fetchSubProductsByProduct(get(formData, 'product.value')));
        dispatch(fetchSizesByProduct(get(formData, 'product.value')));
    }, [get(formData, 'product.value')])

    const toggleView = () => {
        const formNode = document.getElementById('form');
        formNode.classList.toggle('collapse-panel');
        setToggleView(!viewForm);
    }

    const partyNameChange = (e) => {
        let index = e.nativeEvent.target.selectedIndex;
        let label = e.nativeEvent.target[index].text;
        setFormValue("partyId", e.target.value);
        setFormValue("party", label);
    };

    const purityOnChange = (e) => {
        let index = e.nativeEvent.target.selectedIndex;
        let label = e.nativeEvent.target[index].text;
        setFormValue("purity", e.target.value);
        setFormValue("touchPercent", label);
    }

    return (
        <div className="panel panel-default hero-panel">
            <LoadingOverlay
                active={loader}
                spinner
                text='Loading...'
            >
                <div className="panel-heading">
                    <h1 className="panel-title">{t("creations.partyWastage")} {t("common.creation")}</h1>
                    <div className="panel-options">
                        <a
                            href="#"
                            className="text-info collapseBtn"
                            title="Show Add Form"
                        >
                            {viewForm ?
                                <i className="zmdi zmdi-minus-circle  zmdi-hc-2x" onClick={() => { toggleView() }}>
                                </i> :
                                <i className="zmdi zmdi-plus-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                                </i>}
                        </a>
                    </div>
                </div>
                <div className="panel-body">
                    <div className="panel update-panel form-pnel-container" id="form">
                        <div className="text-danger">* {t("common.requiredFieldsMsg")} </div>
                        <div className="panel update-panel">
                            <div className="panel-body ">
                                <div className="row">
                                    <div className="col-sm-6 col-md-4 col-lg-4">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("common.Party")}<span className="asterisk">*</span>
                                            </label>
                                            <Select name="partyId" value={get(formData, 'partyId.value')} onChange={(e) => partyNameChange(e)} data={suppliersData} />
                                            <div className="invalid-feedback">{get(formData, 'partyId.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-4 col-lg-4">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("creations.mainGroup")}<span className="asterisk">*</span>
                                            </label>
                                            <Select name="mainGroup" value={get(formData, 'mainGroup.value')} onChange={(e) => setFormValue('mainGroup', e.target.value)} data={mainGroupData} />
                                            <div className="invalid-feedback">{get(formData, 'mainGroup.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-4 col-lg-4">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("creations.productName")}<span className="asterisk">*</span>
                                            </label>
                                            <Select name="product" value={get(formData, 'product.value')} onChange={(e) => setFormValue('product', e.target.value)} data={productsData} />
                                            <div className="invalid-feedback">{get(formData, 'product.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-4 col-lg-4">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("creations.subProduct")}<span className="asterisk">*</span>
                                            </label>
                                            <Select name="subProduct" value={get(formData, 'subProduct.value')} onChange={(e) => setFormValue('subProduct', e.target.value)} data={subProductsData} />
                                            <div className="invalid-feedback">{get(formData, 'subProduct.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-4 col-lg-4">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("common.Purity")}<span className="asterisk">*</span>
                                            </label>
                                            <Select name="purity" value={get(formData, 'purity.value')} onChange={(e) => purityOnChange(e)} data={puritiesData} />
                                            <div className="invalid-feedback">{get(formData, 'purity.error')}</div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-md-4 col-lg-4">
                                        <div className="form-group">
                                            <label htmlFor="">
                                                {t("creations.productSize")}<span className="asterisk">*</span>
                                            </label>
                                            <Select name="productSize" value={get(formData, 'productSize.value')} onChange={(e) => setFormValue('productSize', e.target.value)} data={productSizesData} />
                                            <div className="invalid-feedback">{get(formData, 'productSize.error')}</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="panel panel-default">
                                    <div className="panel-heading">
                                        <h3 className="panel-title">{t("common.Wastage")} {t("common.Making")} {t("common.details")}</h3>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-6 col-md-3 col-lg-3">
                                            <div className="form-group">
                                                <label htmlFor="">
                                                    {t("creations.touchValue")}<span className="asterisk">*</span>
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    placeholder="touchValue"
                                                    name="touchValue"
                                                    value={get(formData, 'touchValue.value')}
                                                    onChange={(e) => setFormValue('touchValue', e.target.value)}
                                                />
                                                <div className="invalid-feedback">{get(formData, 'touchValue.error')}</div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6 col-md-3 col-lg-3">
                                            <div className="form-group">
                                                <label htmlFor="">
                                                    {t("creations.touchPercent")}<span className="asterisk">*</span>
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    placeholder="touchPercent"
                                                    name="touchPercent"
                                                    value={get(formData, 'touchPercent.value')}
                                                    onChange={(e) => setFormValue('touchPercent', e.target.value)}
                                                />
                                                <div className="invalid-feedback">{get(formData, 'touchPercent.error')}</div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6 col-md-3 col-lg-3">
                                            <div className="form-group">
                                                <label htmlFor="">
                                                    {t("creations.mcgm")}<span className="asterisk">*</span>
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    placeholder="mcPerGram"
                                                    name="mcPerGram"
                                                    value={get(formData, 'mcPerGram.value')}
                                                    onChange={(e) => setFormValue('mcPerGram', e.target.value)}
                                                />
                                                <div className="invalid-feedback">{get(formData, 'mcPerGram.error')}</div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6 col-md-3 col-lg-3">
                                            <div className="form-group">
                                                <label htmlFor="">
                                                    {t("creations.mcDir")}<span className="asterisk">*</span>
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    placeholder="mcPerDir"
                                                    name="mcPerDir"
                                                    value={get(formData, 'mcPerDir.value')}
                                                    onChange={(e) => setFormValue('mcPerDir', e.target.value)}
                                                />
                                                <div className="invalid-feedback">{get(formData, 'mcPerDir.error')}</div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6 col-md-3 col-lg-3">
                                            <div className="form-group">
                                                <label htmlFor="">
                                                    {t("creations.wastgaeGm")}<span className="asterisk">*</span>
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    placeholder="wastagePerGram"
                                                    name="wastagePerGram"
                                                    value={get(formData, 'wastagePerGram.value')}
                                                    onChange={(e) => setFormValue('wastagePerGram', e.target.value)}
                                                />
                                                <div className="invalid-feedback">{get(formData, 'wastagePerGram.error')}</div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6 col-md-3 col-lg-3">
                                            <div className="form-group">
                                                <label htmlFor="">
                                                    {t("creations.wastageDir")}<span className="asterisk">*</span>
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    placeholder="wastagePerDir"
                                                    name="wastagePerDir"
                                                    value={get(formData, 'wastagePerDir.value')}
                                                    onChange={(e) => setFormValue('wastagePerDir', e.target.value)}
                                                />
                                                <div className="invalid-feedback">{get(formData, 'wastagePerDir.error')}</div>
                                            </div>
                                        </div>
                                        {/* <div className="col-sm-6 col-md-3 col-lg-3">
                                            <div className="form-group">
                                                <label htmlFor="">{t("creations.mcCalcOn")}</label>
                                                <div className="radio radio-inline radio-primary">
                                                    <input
                                                        type="radio"
                                                        name="mcOn"
                                                        value="Gross"
                                                        checked={get(formData, 'mcOn.value') === "Gross"}
                                                        onChange={(e) => setFormValue('mcOn', "Gross")} />
                                                    <label htmlFor="">{t("common.gross")}</label>
                                                </div>
                                                <div className="radio radio-inline radio-primary">
                                                    <input
                                                        type="radio"
                                                        name="mcOn"
                                                        value="Net"
                                                        checked={get(formData, 'mcOn.value') === "Net"}
                                                        onChange={(e) => setFormValue('mcOn', "Net")} />
                                                    <label htmlFor="">{t("common.net")}</label>
                                                </div>
                                                <div className="invalid-feedback">{get(formData, 'mcOn.error')}</div>
                                            </div>
                                        </div>
                                        <div className="col-sm-6 col-md-3 col-lg-3">
                                            <div className="form-group">
                                                <label htmlFor="">{t("creations.wastageCalcOn")}</label>
                                                <div className="radio radio-inline radio-primary">
                                                    <input
                                                        type="radio"
                                                        name="wastageOn"
                                                        value="Gross"
                                                        checked={get(formData, 'wastageOn.value') === "Gross"}
                                                        onChange={(e) => setFormValue('wastageOn', "Gross")} />
                                                    <label htmlFor="">{t("common.gross")}</label>
                                                </div>
                                                <div className="radio radio-inline radio-primary">
                                                    <input
                                                        type="radio"
                                                        name="wastageOn"
                                                        value="Net"
                                                        checked={get(formData, 'wastageOn.value') === "Net"}
                                                        onChange={(e) => setFormValue('wastageOn', "Net")} />
                                                    <label htmlFor="">{t("common.net")}</label>
                                                </div>
                                                <div className="invalid-feedback">{get(formData, 'wastageOn.error')}</div>
                                            </div>
                                        </div> */}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="panel-footer text-center">
                            <a href="#" className="btn btn-primary fire" onClick={saveForm}>
                                <i className="zmdi zmdi-check"></i> {t("common.save")}
                            </a>
                            <a href="#" className="btn btn-default" onClick={resetForm}>
                                <i className="zmdi zmdi-close"></i> {t("common.clear")}
                            </a>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">Party Wastage List</h3>
                            <div className="panel-options">
                                <p>
                                    {selected.length > 0 ?
                                        <a href="#" className="btn btn-info btn-sm" onClick={() => deleteAction(selected)} >
                                            <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                                        </a> :
                                        null}
                                </p>
                            </div>
                        </div>
                        <div className="panel-body">
                            <div className="ag-grid-wrapper">
                                <Grid colDefs={partyWastageColumns} rowData={partyWastagesData} selectedRows={setSelected} actions={{ isRequired: true, editAction: editAction, deleteAction: deleteAction }} flex={true} />
                            </div>
                        </div>
                    </div>
                </div>
            </LoadingOverlay>
        </div>
    );
};

export default withTranslation()(PartyWastage);
