import React, { useEffect } from "react";
import logo from "./../../assets/images/logo.png";
import user from "./../../assets/images/user.jpg";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import { withTranslation } from "react-i18next";
import moment from "moment";
import { useDispatch } from "react-redux"
import { logout, getCookie } from "../../redux/actions/authActions"
import "./styles.css";

const href = "#";
export const Header = (props) => {
  const [language, setLanguage] = React.useState("English");
  const dispatch = useDispatch();
  const username = getCookie("username");
  const handleChange = (lang) => {
    let newlang = lang;
    //setLanguage(newlang);
    props.i18n.changeLanguage(newlang);
  };
  useEffect(() => {
    if(props.i18n.language === 'eng') {
      setLanguage("English");
    }
    if(props.i18n.language === 'hin') {
      setLanguage("हिंदी");
    }
    if(props.i18n.language === 'tel') {
      setLanguage("తెలుగు");
    }
  },[props.i18n.language]);
  return (
    <div className="mainHeader clearfix">
      <div className="headerLogo">
        <a href="index.html">
          <img src={logo} alt="Logo" />
        </a>
      </div>
      <div className="header-right">
        <ul className="headerCntnt">
          <li className="ratesScroller">
            <div
              id="carousel-example-generic"
              className="carousel slide"
              data-ride="carousel"
            >
              <div className="carousel-inner" role="listbox">
                <div className="item active">
                  {" "}
                  <span className="itemTitle">
                    <span>10g</span> of <span>24k gold</span> (99.9%)
                  </span>
                  <span className="itemCost">39,930.00</span>
                </div>
                <div className="item">
                  {" "}
                  <span className="itemTitle">
                    <span>10g</span> of <span>22k gold</span> (90.1%)
                  </span>
                  <span className="itemCost">38,150.00</span>
                </div>
              </div>
              <a
                className="left carousel-control"
                href="#carousel-example-generic"
                role="button"
                data-slide="prev"
              >
                {" "}
                <i className="zmdi zmdi-chevron-left"></i>{" "}
              </a>
              <a
                className="right carousel-control"
                href="#carousel-example-generic"
                role="button"
                data-slide="next"
              >
                {" "}
                <i className="zmdi zmdi-chevron-right"></i>{" "}
              </a>
            </div>
          </li>
          <li className="language">
            <div className="dropdown">
            <button className="btn btn-link dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              <i className="zmdi zmdi-translate"></i> 
              <span className="hidden-xs">{language}</span> 
              <span className="caret"></span> 
            </button>
            <ul className="dropdown-menu dropdown-right" aria-labelledby="dropdownMenu1">
                    <li ><a href="#" className="language-options" value="tel" onClick={() => handleChange("tel")} >తెలుగు</a></li>
                    <li ><a href="#" className="language-options" value="hin" onClick={() => handleChange("hin")}>हिंदी</a></li>
                    <li ><a href="#" className="language-options" value="eng" onClick={() => handleChange("eng")}>English</a></li>
                    
            </ul>
              {/* <Select
                labelId="language-chooser"
                id="language-chooser"
                value={language}
                label="Language"  
                onChange={handleChange}
                className="language-chooser"
              >
                <MenuItem className="language-options" value={"tel"}>
                  తెలుగు
                </MenuItem>
                <MenuItem className="language-options" value={"hin"}>
                  हिंदी
                </MenuItem>
                <MenuItem className="language-options" value={"eng"}>
                  English
                </MenuItem>
              </Select> */}
            </div>
          </li>
          <li className="date hidden-xs">
            {" "}
            <span className="hidden-xs">{moment().format("dddd")}, </span>
            {moment().format("MMM")} {moment().format("DD,YYYY")}{" "}
          </li>

          <li className="userInfo">
            <div className="dropdown">
              <button
                className="btn btn-link dropdown-toggle"
                type="button"
                id="dropdownMenu1"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="true"
              >
                <div className="userImg">
                  <img src={user} alt="Img" />
                </div>{" "}
                <span className="hidden-xs">{username}</span>{" "}
                <span className="caret"></span>{" "}
              </button>
              <ul
                className="dropdown-menu dropdown-right"
                aria-labelledby="dropdownMenu1"
              >
                <li>
                  <a href={href}>Settings</a>
                </li>
                <li>
                  <a href={href} onClick={() => dispatch(logout())} >Logout</a>
                </li>
              </ul>
            </div>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default withTranslation()(Header);
