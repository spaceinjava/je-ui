import React from "react";
import Header from "./Header";
import LeftNavigation from "./LeftNavigation";
export const Layout = (props) => {
  return (
    <div>
      <Header />
      <div className="main-wrapper">
        <LeftNavigation location={props.location}/>
        <div className="right-content">
          <div className="container-fluid">
            <div className="row">
                {props.children}
              </div>
            </div>
          </div>
        </div>
      </div>
  );
};

export default Layout;

// import React from "react";
// import Header from "./Header";
// import LeftNavigation from "./LeftNavigation";
// export const Layout = (props) => {
//   return (
//     <div>
//       <Header />
//       <div className="main-wrapper">
//       <LeftNavigation location={props.location}/>
//       <div className="right-content">
//         <div className="container-fluid">
          
//           <div className="row">
//               {props.children}
//             </div>
//           </div>
//         </div>
//         </div>
//       </div>
//   );
// };

// export default Layout;

