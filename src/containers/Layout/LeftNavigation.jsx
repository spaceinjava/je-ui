import React from "react";
import dashboard from "./../../assets/images/dashboard.png";
import admin from "./../../assets/images/admin.png";
import creations from "./../../assets/images/creations.png";
import transactions from "./../../assets/images/transactions.png";
import reports from "./../../assets/images/reports.png";
import utilities from "./../../assets/images/utilities.png";
import { withTranslation } from "react-i18next";
import { get } from "lodash";

export const LeftNavigation = (props) => {
  const { t } = props;
  const leftMenuOptions = [
    {
      name: "dashboard",
      active: false,
      href: "/dashboard",
      imgSrc: dashboard,
      title: t("leftMenus.dashboard"),
    },
    {
      name: "admin",
      active: false,
      href: "/_admin/branches",
      imgSrc: admin,
      title: t("leftMenus.admin"),
    },
    {
      name: "creations",
      active: false,
      href: "/creations/mainGroup",
      imgSrc: creations,
      title: t("leftMenus.creation"),
    },
    {
      name: "transactions",
      active: false,
      href: "/transactions/inventory/metal-purchase",
      imgSrc: transactions,
      title: t("leftMenus.transactions"),
    },
    {
      name: "reports",
      active: false,
      href: "/reports",
      imgSrc: reports,
      title: t("leftMenus.reports"),
    },
    {
      name: "utilities",
      active: false,
      href: "/utilities",
      imgSrc: utilities,
      title: t("leftMenus.utilities"),
    },
  ];
  let pathname = get(props, "location.pathname");
  pathname = pathname == "/" ? "dashboard" : pathname;
  return (
    <div className="left-nav">
      <ul className="iconTabs">
        {leftMenuOptions.map((eachMenu) => {
          return (
            <li key={`${eachMenu.name}_${eachMenu.href}_`}>
              <a
                href={eachMenu.href}
                className={pathname.includes(eachMenu.name) ? "active" : ""}
              >
                {" "}
                <span className="tabIcon">
                  <img
                    src={eachMenu.imgSrc}
                    width="96"
                    height="96"
                    alt="Dashboard"
                  />
                </span>{" "}
                <span className="tabTitle">{eachMenu.title}</span>
              </a>
            </li>
          );
        })}
      </ul>
    </div>
  );
};
export default withTranslation()(LeftNavigation);
