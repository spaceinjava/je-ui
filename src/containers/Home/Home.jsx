import React from 'react';
import "./Home.css";
import softwarepng from '../../assets/images/software.png';
import whyarewepng from '../../assets/images/why_we_are.jpg';
import favsvg from '../../assets/images/faq.svg';
import inventorysvg from '../../assets/images/icon/inventory.svg';
import salessvg from '../../assets/images/icon/sales.svg';
import purchasesvg from '../../assets/images/icon/purchase.svg';
import ordersvg from '../../assets/images/icon/order.svg';
import loyaltysvg from '../../assets/images/icon/loyalty.svg';
import branchsvg from '../../assets/images/icon/branch.svg';
import accountingsvg from '../../assets/images/icon/accounting.svg';
import billingsvg from '../../assets/images/icon/billing.svg';
import laboursvg from '../../assets/images/icon/labour.svg';
import printingsvg from '../../assets/images/icon/printing.svg';
import stocksvg from '../../assets/images/icon/stock.svg';
import goldsvg from '../../assets/images/icon/gold.svg';
import ringsvg from '../../assets/images/icon/ring.svg';
import repairsvg from '../../assets/images/icon/repair.svg';
import securesvg from '../../assets/images/icon/secure.svg';
import logolightpng from '../../assets/images/logo-light.png';

function Home() {
  return (
    <div id="home">
    <section className="bg-half-260 bg-home d-table w-100">
        <div className="bg-overlay2"></div>
        <div className="container">
            <div className="row align-items-center position-relative mt-5" style={{"z-index": "1"}}>
                <div className="col-lg-6 col-md-12">
                    <div className="title-heading mt-4 text-center text-lg-left">
                        <h1 className="heading mb-3 title-dark text-white">Jewel Easy</h1>
                        <p className="para-desc">Capture the true value of the cloud for your business</p>
                        <div className="mt-4">
                            <a href="/login" className="btn btn-success"><i className="mdi mdi-account"></i>Login</a>
                        </div>
                    </div>
                </div>

                <div className="col-lg-6 col-md-12 mt-4 pt-2">
                    <div className="position-relative">
                        <div className="software_hero">
                            <img src={softwarepng} className="img-fluid d-block" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section className="section bg-white">
        <div className="container">
            <div className="row">
                <div className="col-md-12 pb-5 text-left py-3">
                    <h3>In today’s dynamic ecosystem, <span className="text-primary font-weight-bold">“One can see the surge of 85% among organizations who look forward to seamless work-flow management through the cloud-computing platform for most of the business functions”. </span></h3>
                    <h3>One can clearly witness the transitioning of getting into the cloud platform from other conventional modes of digital technologies in place.
                    </h3>
                </div>

                <div className="col-lg-5 col-md-5  text-center">
                    <div className="position-relative">
                        <img src={whyarewepng} className="img-fluid " alt="" style={{"height": "260px"}} />
                    </div>
                </div>

                <div className="col-lg-7 col-md-7">
                    <div className="section-title">
                        <h3 className="">Why JewelEasy?</h3>
                        <p className="text-left"><span className="text-primary font-weight-bold">Jewel Easy</span> is a proprietary product application being is conceived and designed for Jewellery business which is exclusively available on the cloud platform. The software becomes
                            a ready application for GST ready inventory and also incorporates an accounting module with it which helps the user in completing a commercial transaction. </p>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section className="section py-3 bg-white">
        <div className="container">
            <div className="row">
                <div className="col-lg-7 col-md-7">
                    <div className="section-title">
                        <h3 className="title mb-4"> <span className="text-primary">SpaceIn Solutions</span> has emerged as a humble service provider </h3>
                        <p className="text-muted">We have transformed itself as a beacon of hope for most of the small/medium (SME’S) and large scale businesses that become organized in the arrangement of data management systems.</p>
                        <p>Instead of transitioning ourselves as a service provider, we become the trusted business partner for enterprises that indeed seeks to have a shift from the conventional methods of business process to the digitized cloud version.
                        </p>
                    </div>
                </div>
                <div className="col-lg-5 col-md-5">
                    <div className="position-relative">
                        <img src={favsvg} className="rounded img-fluid mx-auto d-block" alt="" />
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section className="section bg-light">
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-ṃd-12 ">
                    <div className="section-title text-center">
                        <h3 className="title ">JewelEasy Key Features</h3>
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-md-3 col-12 mt-5">
                    <div className="features text-center">
                        <div className="image position-relative d-inline-block">
                            <img src={inventorysvg} className="avatar avatar-small" alt="" />
                        </div>
                        <div className="content mt-4">
                            <h4 className="title-2">Inventory management</h4>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 col-12 mt-5">
                    <div className="features text-center">
                        <div className="image position-relative d-inline-block">
                            <img src={salessvg} className="avatar avatar-small" alt="" />
                        </div>
                        <div className="content mt-4">
                            <h4 className="title-2">Sales Management</h4>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 col-12 mt-5">
                    <div className="features text-center">
                        <div className="image position-relative d-inline-block">
                            <img src={purchasesvg} className="avatar avatar-small" alt="" />
                        </div>
                        <div className="content mt-4">
                            <h4 className="title-2">Purchase Management</h4>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 col-12 mt-5">
                    <div className="features text-center">
                        <div className="image position-relative d-inline-block">
                            <img src={ordersvg} className="avatar avatar-small" alt="" />
                        </div>
                        <div className="content mt-4">
                            <h4 className="title-2">Order Management</h4>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 col-12 mt-5">
                    <div className="features text-center">
                        <div className="image position-relative d-inline-block">
                            <img src={loyaltysvg} className="avatar avatar-small" alt="" />
                        </div>
                        <div className="content mt-4">
                            <h4 className="title-2">Customer Loyalty Program</h4>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 col-12 mt-5">
                    <div className="features text-center">
                        <div className="image position-relative d-inline-block">
                            <img src={branchsvg} className="avatar avatar-small" alt="" />
                        </div>
                        <div className="content mt-4">
                            <h4 className="title-2">Branch Management</h4>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 col-12 mt-5">
                    <div className="features text-center">
                        <div className="image position-relative d-inline-block">
                            <img src={accountingsvg} className="avatar avatar-small" alt="" />
                        </div>
                        <div className="content mt-4">
                            <h4 className="title-2">Account Management</h4>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 col-12 mt-5">
                    <div className="features text-center">
                        <div className="image position-relative d-inline-block">
                            <img src={billingsvg} className="avatar avatar-small" alt="" />
                        </div>
                        <div className="content mt-4">
                            <h4 className="title-2">Fastest billing and tagging facility</h4>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 col-12 mt-5">
                    <div className="features text-center">
                        <div className="image position-relative d-inline-block">
                            <img src={laboursvg} className="avatar avatar-small" alt="" />
                        </div>
                        <div className="content mt-4">
                            <h4 className="title-2">Labor Bill</h4>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 col-12 mt-5">
                    <div className="features text-center">
                        <div className="image position-relative d-inline-block">
                            <img src={printingsvg} className="avatar avatar-small" alt="" />
                        </div>
                        <div className="content mt-4">
                            <h4 className="title-2">Printing images on invoices</h4>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 col-12 mt-5">
                    <div className="features text-center">
                        <div className="image position-relative d-inline-block">
                            <img src={stocksvg} className="avatar avatar-small" alt="" />
                        </div>
                        <div className="content mt-4">
                            <h4 className="title-2">Analyzing stock with images</h4>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 col-12 mt-5">
                    <div className="features text-center">
                        <div className="image position-relative d-inline-block">
                            <img src={goldsvg} className="avatar avatar-small" alt="" />
                        </div>
                        <div className="content mt-4">
                            <h4 className="title-2">Gold scheme Management</h4>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 col-12 mt-5">
                    <div className="features text-center">
                        <div className="image position-relative d-inline-block">
                            <img src={ringsvg} className="avatar avatar-small" alt="" />
                        </div>
                        <div className="content mt-4">
                            <h4 className="title-2">Old metal management Management</h4>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 col-12 mt-5">
                    <div className="features text-center">
                        <div className="image position-relative d-inline-block">
                            <img src={repairsvg} className="avatar avatar-small" alt="" />
                        </div>
                        <div className="content mt-4">
                            <h4 className="title-2">Repairing Management</h4>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 col-12 mt-5">
                    <div className="features text-center">
                        <div className="image position-relative d-inline-block">
                            <img src={securesvg} className="avatar avatar-small" alt="" />
                        </div>
                        <div className="content mt-4">
                            <h4 className="title-2">Security management</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </section>

    <section className="section bg-white" id="contact">
        <div className="container">
                    <div className="text-center">
                        <h3 className="title mb-4">Get In Touch !</h3>
                        <p className="para-desc">Start working with <span className="text-primary font-weight-bold">SpaceIn Solutions</span> that can provide everything you need to generate awareness, drive traffic, connect.</p>
                    </div>

            <div className="row">
                <div className="col-lg-5 col-md-6 ">
                    <div className="card" data-aos="fade-up" data-aos-duration="1000">
                        <div className="card-body">
                            <div className="custom-form bg-white">
                                <div id="message"></div>
                                <form method="post" action="php/contact.php" name="contact-form" id="contact-form">
                                    <div className="row">
                                        <div className="col-lg-6">
                                            <div className="form-group position-relative">
                                                <label>Your Name <span className="text-danger">*</span></label>
                                                <i data-feather="user" className="fea icon-sm icons"></i>
                                                <input name="name" id="name" type="text" className="form-control pl-5" placeholder="First Name :" />
                                            </div>
                                        </div>
                                        <div className="col-lg-6">
                                            <div className="form-group position-relative">
                                                <label>Your Email <span className="text-danger">*</span></label>
                                                <i data-feather="mail" className="fea icon-sm icons"></i>
                                                <input name="email" id="email2" type="email" className="form-control pl-5" placeholder="Your email :" />
                                            </div>
                                        </div>
                                        <div className="col-lg-12">
                                            <div className="form-group position-relative">
                                                <label>Comments</label>
                                                <i data-feather="message-circle" className="fea icon-sm icons"></i>
                                                <textarea name="comments" id="comments" rows="4" className="form-control pl-5" placeholder="Your Message :"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-12 text-center">
                                            <input type="submit" id="submit" name="send" className="submitBnt btn btn-primary btn-block" value="Send Message" />
                                            <div id="simple-msg"></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-lg-7 col-md-6 ">
                    <div className="title-heading ml-lg-4 ">
                        <h4 className="mb-4 aos-init aos-animate" data-aos="fade-up " data-aos-duration="1000 ">Contact Details</h4>
                        <p className="text-muted aos-init" data-aos="fade-up " data-aos-duration="1400 ">Start working with <span className="text-primary font-weight-bold ">SpaceIn Solutions</span> that can provide everything you need to generate awareness, drive traffic, connect.</p>
                                <div className="contact-detail d-flex align-items-center mt-3 aos-init" data-aos="fade-up " data-aos-duration="1200 ">
                                    <div className="icon ">
                                        <i data-feather="mail " className="fea icon-m-md text-dark mr-3 "></i>
                                    </div>
                                    <div className="content overflow-hidden d-block ">
                                        <h6 className="font-weight-bold mb-0 ">Email</h6>
                                        <a href="mailto:info@spaceinsolutions.com " className="text-primary ">info@spaceinsolutions.com</a>
                                    </div>
                                </div>
                                <div className="contact-detail d-flex align-items-center mt-3 aos-init" data-aos="fade-up " data-aos-duration="1400 ">
                                    <div className="icon ">
                                        <i data-feather="phone " className="fea icon-m-md text-dark mr-3 "></i>
                                    </div>
                                    <div className="content overflow-hidden d-block ">
                                        <h6 className="font-weight-bold mb-0 ">Phone</h6>
                                        <a href="tel:+152534-468-854 " className="text-primary ">+152 534-468-854</a>
                                    </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" className="btn btn-icon btn-soft-primary back-to-top pull-right"><i data-feather="arrow-up" className="mdi mdi-arrow-up"></i></a>


        </div>
    </section>
   
   
    </div>
  );
}
export default Home;
