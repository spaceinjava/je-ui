import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { designationForm, designationColumns } from "./Constants";
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../components/Grid";
import Swal from 'sweetalert2'
import Select from "../../components/Form/Select";
import { fetchDepartments } from '../../redux/actions/departmentActions'
import { saveDesignation, fetchDesignations, deleteDesignations } from "../../redux/actions/designationActions";
import LoadingOverlay from 'react-loading-overlay';

export const Designation = (props) => {
  const { t } = props;
  const [formData, setFromData] = useState(cloneDeep(designationForm));
  const [viewForm, setToggleView] = useState(true);
  let [selected, setSelected] = useState([]);
  const dispatch = useDispatch();
  const departments = useSelector(state => state.departmentReducer.allDepartments)
  const designations = useSelector(state => state.designationReducer.allDesignations)
  const loader = useSelector(state => state.designationReducer.loading);
  const departmentData = departments.data ? departments.data.map((department) => {
    return {
      id: department.id,
      name: department.departmentName,
      shortCode: department.shortCode,
    }
  }) : [];
  const designationData = designations.data ? designations.data.map((designation) => {
    return {
      id: designation.id,
      departmentId: designation.department.id,
      departmentName: designation.department.departmentName,
      name: designation.designationName,
      shortCode: designation.shortCode,
    }
  }) : [];
  const setFormValue = (field, value) => {
    formData[field]['value'] = value;
    setFromData({ ...formData });
  }
  const resetForm = () => {
    map(formData, function (value, key, object) {
      formData[key]['value'] = ''
    });
    setFromData({ ...formData });
  }
  const saveForm = () => {
    let isFormValid = true;
    map(formData, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      formData[key] = { ...value, ...isValid }
    });
    if (isFormValid) {
      let data = {};
      map(formData, function (value, key, object) {
        data = {
          ...data,
          [key]: value.value
        }
      });
      Swal.fire({
        title: 'Confirm Saving Designation Info.!',
        text: 'Continue to save / Cancel to stop',
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Continue'
      }).then((result) => {
        if (result.isConfirmed) {
          dispatch(saveDesignation(data))
            .then(() => {
              resetForm()
            })
        }
      })
    }
  }

  const validateField = (fieldObj) => {
    let errorObj = { isValid: true, error: null }
    if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
    else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
    else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }
    return errorObj;
  }

  const editAction = (ele) => {
    setFormValue('id', ele.id)
    setFormValue('department', ele.departmentId)
    setFormValue('name', ele.name)
    setFormValue('shortCode', ele.shortCode)
  }

  const deleteAction = (data) => {
    let selectedIds;
    if (data instanceof Array && data.length > 0) {
      selectedIds = data.map((ele) => {
        return ele.id
      })
    } else {
      selectedIds = data.id;
    }
    Swal.fire({
      title: 'Confirm Deleting Designation.!',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deleteDesignations(selectedIds.toString()))
          .then(() => {
            selected = []
            setSelected(selected);
          })
      }
    })
  }

  useEffect(() => {
    dispatch(fetchDepartments());
    dispatch(fetchDesignations());
  }, [])

  const toggleView = () => {
    const formNode = document.getElementById('form');
    formNode.classList.toggle('collapse-panel');
    setToggleView(!viewForm);
  }

  return (
    <div className="panel panel-default hero-panel">
      <LoadingOverlay
        active={loader}
        spinner
        text='Loading...'
      >
        <div className="panel-heading">
          <h1 className="panel-title">{t("adminMenus.designation")} {t("common.creation")}</h1>
          <div className="panel-options">
            <a
              href="#"
              className="text-info collapseBtn"
              title="Show Add Form"
            >
              {viewForm ?
                <i className="zmdi zmdi-minus-circle  zmdi-hc-2x" onClick={() => { toggleView() }}>
                </i> :
                <i className="zmdi zmdi-plus-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                </i>}
            </a>
          </div>
        </div>
        <div className="panel-body">
          <div className="panel update-panel form-pnel-container" id="form">
            <div className="text-danger">* {t("common.requiredFieldsMsg")} </div>
            <div className="panel update-panel">
              <div className="panel-body ">
                <div className="row">
                  <div className="col-sm-6 col-md-4 col-lg-3">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("adminMenus.department")} {t("common.name")}<span className="asterisk">*</span>
                      </label>
                      <Select name="department" value={get(formData, 'department.value')} onChange={(e) => setFormValue('department', e.target.value)} data={departmentData} />
                      <div className="invalid-feedback">{get(formData, 'department.error')}</div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-4 col-lg-3">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("adminMenus.designation")} {t("common.name")}<span className="asterisk">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="name"
                        name="name"
                        value={get(formData, 'name.value')}
                        onChange={(e) => setFormValue('name', e.target.value)}
                      />
                      <div className="invalid-feedback">{get(formData, 'name.error')}</div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-md-4 col-lg-3">
                    <div className="form-group">
                      <label htmlFor="">
                        {t("adminMenus.designation")} {t("common.shortCode")}<span className="asterisk">*</span>
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="shortCode"
                        name="shortCode"
                        value={get(formData, 'shortCode.value')}
                        onChange={(e) => setFormValue('shortCode', e.target.value)}
                      />
                      <div className="invalid-feedback">{get(formData, 'shortCode.error')}</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="panel-footer text-center">
              <a href="#" className="btn btn-primary fire" onClick={saveForm}>
                <i className="zmdi zmdi-check"></i> {t("common.save")}
              </a>
              <a href="#" className="btn btn-default" onClick={resetForm}>
                <i className="zmdi zmdi-close"></i> {t("common.clear")}
              </a>
            </div>
          </div>
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">{t("adminMenus.designation")} {t("common.list")}</h3>
              <div className="panel-options">
                <p>
                  {selected.length > 0 ?
                    <a href="#" className="btn btn-info btn-sm" onClick={() => deleteAction(selected)} >
                      <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                    </a> :
                    null}
                </p>
              </div>
            </div>
            <div className="panel-body">
              <div className="ag-grid-wrapper">
                <Grid colDefs={designationColumns} rowData={designationData} selectedRows={setSelected} actions={{ isRequired: true, editAction: editAction, deleteAction: deleteAction }} />
              </div>
            </div>
          </div>
        </div>
      </LoadingOverlay>
    </div>
  );
};

export default withTranslation()(Designation);
