import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import SecondMenu from "./../../components/SecondMenu";
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

export const Menu = (props) => {
  const { t } = props;
  const [viewMenu, setMenuView]= React.useState(true);
  const menuOptions = [
    // {
    //   name: "companies",
    //   active: true,
    //   href: "/_admin/companies",
    //   title: t("adminMenus.companies"),
    // },
    {
      name: "branches",
      active: false,
      href: "/_admin/branches",
      title: t("adminMenus.branches"),
    },
    {
        name: "department",
        active: false,
        href: "/_admin/department",
        title: t("adminMenus.department"),
    },
    {
      name: "designation",
      active: false,
      href: "/_admin/designation",
      title: t("adminMenus.designation"),
    },
    {
      name: "employees",
      active: false,
      href: "/_admin/employees",
      title: t("adminMenus.employees"),
    },
    {
      name: "suppliers",
      active: false,
      href: "/_admin/suppliers",
      title: t("adminMenus.suppliers"),
    },
  ];
  let updatedMenuOptions= menuOptions.map(eachMenu=>{
      return {...eachMenu, active:eachMenu.href.includes(props.pathname)}
  })
  return (
    <div className="left-nav-holder">
    <ul className="left-subnav" id="sidebar">
    <SecondMenu menuOptions={updatedMenuOptions} />
    </ul>
  </div>
  );
};

export default withTranslation()(Menu);
