export const companiesColumns = [
    { headerName: "adminMenus.companies", field: "company" },
    { headerName: "common.address", field: "addressLineOne" },
    { headerName: "common.email", field: "email" },
    { headerName: "common.contact", field: "contact" },
    { headerName: "common.state", field: "state" },
    { headerName: "common.district", field: "district" },
    { headerName: "common.city", field: "city_town" },
    { headerName: "common.area", field: "area" },
    { headerName: "GST", field: "GST" },
    { headerName: "common.actions", field: "actions" }
]

export const branchesColumns = [
    { headerName: "branches.branchName", field: "branchName" },
    { headerName: "common.shortCode", field: "shortCode" },
    { headerName: "common.email", field: "email" },
    { headerName: "common.phone", field: "contactNum" },
    { headerName: "common.alternate", field: "alternateContactNum" },
    { headerName: "common.pincode", field: "pincode" },
    { headerName: "common.state", field: "state" },
    { headerName: "common.district", field: "district" },
    { headerName: "common.city", field: "city" },
    { headerName: "common.area", field: "area" },
    { headerName: "common.address1", field: "addressLine1" },
    { headerName: "common.address2", field: "addressLine2" },    
    { headerName: "creations.gst", field: "gstNo" },
    { headerName: "branches.isHeadBranch", field: "isHeadBranch" },
]

export const employeeColumns = [ 
    { headerName: "employees.employeeId", field: "employeeId" },
    { headerName: "employees.employeeName", field: "firstName" },
    { headerName: "branches.branchName", field: "branchName" },
    { headerName: "", field: "branchId", hide: true },
    { headerName: "adminMenus.department", field: "department" },
    { headerName: "", field: "departmentId", hide: true },
    { headerName: "adminMenus.designation", field: "designation" },
    { headerName: "", field: "designationId", hide: true },
    { headerName: "common.email", field: "email" },
    { headerName: "common.phone", field: "mobile" },
    { headerName: "creations.dob", field: "dob" },
    { headerName: "common.alternate", field: "emergencyContactNum" },
    { headerName: "employees.aadharNum", field: "aadharNum" },
    { headerName: "employees.panNum", field: "panNum" },
    { headerName: "common.pincode", field: "pincode" },
    { headerName: "common.state", field: "state" },
    { headerName: "common.district", field: "district" },
    { headerName: "common.city", field: "city" },
    { headerName: "common.area", field: "area" },
    { headerName: "common.address1", field: "addressLine1" },
    { headerName: "common.address2", field: "addressLine2" },    
    { headerName: "employees.loginEnabled", field: "loginEnabled" },
]

export const departmentsColumns = [
    { headerName: "common.name", field: "name" },
    { headerName: "common.shortCode", field: "shortCode" }
]

export const designationColumns = [
    { headerName: "", field: "departmentId", hide: true },
    { headerName: "adminMenus.department", field: "departmentName" },
    { headerName: "common.name", field: "name" },
    { headerName: "common.shortCode", field: "shortCode" }
]

export const companiesForm = {
    company: { value: '', required: true, type: 'text', min: 3, max: 20, isValid: false, error: null },
    companyShortName: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null },
    email: { value: '', required: true, type: 'email', min: 3, max: 30, isValid: false, error: null },
    phoneNumber: { value: '', required: true, type: 'number', min: 6, max: 10, isValid: false, error: null },
    alternativePhoneNumber: { value: '', required: false, type: 'number', min: 6, max: 10, isValid: false, error: null },
    addressLineOne: { value: '', required: true, type: 'text', min: 3, max: 100, isValid: false, error: null },
    addressLineTwo: { value: '', required: true, type: 'text', min: 3, max: 100, isValid: false, error: null },
    state: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null },
    district: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null },
    cityTown: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null },
    areaVillage: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null },
    pincode: { value: '', required: true, type: 'number', min: 6, max: 6, isValid: false, error: null },
    gstNumber: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null },
    logo: { value: null, required: true, type: 'file', min: 3, max: 10, isValid: false, error: null },
    isBranch: { value: false, required: true, type: 'bool', min: 3, max: 10, isValid: false, error: null },
    BankList : {}
}

export const employeesForm = {
    id: { value: '' },
    photoUrl: { value: '', required: false, type: 'text',  isValid: false, error: null },
    branchName: { value: '', required: true, type: 'text', min: 3, max: 50, isValid: false, error: null },
    department: { value: '', required: true, type: 'text', min: 3, max: 50, isValid: false, error: null },
    designation: { value: '', required: true, type: 'text', min: 3, max: 50, isValid: false, error: null },
    generateId: { value: false, required: false, type: 'text', min: 3, max: 30, isValid: false, error: null },
    employeeId: { value: '', required: false, type: 'text', min: 3, max: 20, isValid: false, error: null },
    firstName: { value: '', required: true, type: 'text', min: 3, max: 20, isValid: false, error: null },
    middleName: { value: '', required: false, type: 'text', min: 3, max: 20, isValid: false, error: null },
    lastName: { value: '', required: true, type: 'text', min: 3, max: 20, isValid: false, error: null },
    dob: { value: '', required: true, type: 'text', min: 3, max: 20, isValid: false, error: null },
    gender: { value: '', required: true, type: 'text', min: 3, max: 20, isValid: false, error: null },
    loginEnabled: { value: false, required: false, type: 'text', min: 3, max: 20, isValid: false, error: null },
    contactNumber: { value: '', required: true, type: 'text', min: 3, max: 20, isValid: false, error: null },
    emgContactNumber: { value: '', required: true, type: 'text', min: 3, max: 20, isValid: false, error: null },
    email: { value: '', required: false, type: 'email', min: 3, max: 30, isValid: false, error: null },
    aadharNo: { value: '', required: true, type: 'email', min: 3, max: 30, isValid: false, error: null },
    panNumber: { value: '', required: true, type: 'email', min: 3, max: 30, isValid: false, error: null },
    addressLine1: { value: '', required: false, type: 'text', min: 3, max: 100, isValid: false, error: null },
    addressLine2: { value: '', required: false, type: 'text', min: 3, max: 100, isValid: false, error: null },
    pincode: { value: '', required: true, type: 'number', min: 3, max: 30, isValid: false, error: null },
    area: { value: '', required: true, type: 'text', min: 3, max: 30, isValid: false, error: null },
    city: { value: '', required: true, type: 'text', min: 3, max: 30, isValid: false, error: null },
    district: { value: '', required: true, type: 'text', min: 3, max: 30, isValid: false, error: null },
    state: { value: '', required: true, type: 'text', min: 3, max: 30, isValid: false, error: null },
    addressId: { value: '' },
    bankDetailsId: { value: '' },
    accountHolderName: { value: '', required: true, type: 'text', min: 3, max: 100, isValid: false, error: null },
    accountNumber: { value: '', required: true, type: 'text', min: 3, max: 100, isValid: false, error: null },
    salary: { value: '', required: true, type: 'text', min: 3, max: 100, isValid: false, error: null },
    ifscCode: { value: '', required: true, type: 'text', min: 3, max: 100, isValid: false, error: null },
    bankName: { value: '', required: true, type: 'text', min: 3, max: 100, isValid: false, error: null },
    bankBranch: { value: '', required: true, type: 'text', min: 3, max: 100, isValid: false, error: null },
}

export const addressForm = {
    id: {value: ''},
    addressLine1: { value: '', required: true, type: 'text', min: 3, max: 100, isValid: false, error: null },
    addressLine2: { value: '', required: true, type: 'text', min: 3, max: 100, isValid: false, error: null },
    pincode: { value: '', required: true, type: 'number', min: 6, max: 6, isValid: false, error: null },
    area: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null },
    city: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null },
    district: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null },
    state: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null }
}

export const branchesForm = {
    id: { value: '' },
    branchName: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null },
    shortCode: { value: '', required: true, type: 'text', min: 3, max: 3, isValid: false, error: null },
    email: { value: '', required: true, type: 'email', min: 3, max: 30, isValid: false, error: null },
    contactNum: { value: '', required: true, type: 'number', min: 6, max: 10, isValid: false, error: null },
    alternateContactNum: { value: '', required: false, type: 'number', min: 6, max: 10, isValid: false, error: null },
    gstNo: { value: '', required: true, type: 'text', min: 3, max: 10, isValid: false, error: null },
    isHeadBranch: { value: false, required: false, type: 'text', min: 3, max: 30, isValid: false, error: null },
    addressLine1: { value: '', required: false, type: 'text', min: 3, max: 100, isValid: false, error: null },
    addressLine2: { value: '', required: false, type: 'text', min: 3, max: 100, isValid: false, error: null },
    pincode: { value: '', required: true, type: 'number', min: 3, max: 30, isValid: false, error: null },
    area: { value: '', required: true, type: 'text', min: 3, max: 30, isValid: false, error: null },
    city: { value: '', required: true, type: 'text', min: 3, max: 30, isValid: false, error: null },
    district: { value: '', required: true, type: 'text', min: 3, max: 30, isValid: false, error: null },
    state: { value: '', required: true, type: 'text', min: 3, max: 30, isValid: false, error: null },
    addressId: { value: '' },
    BankList : {}
}

export const departmentForm = {
    id: {value: ''},
    name: {value: '', required: true, min: 3, max: 50, isValid: false, error: null},
    shortCode: {value: '', required: true, min: 2, max: 5, isValid: false, error: null}
}

export const designationForm = {
    id: {value: ''},
    department: {value: '', required: true, min: 3, max: 50, isValid: false, error: null},
    name: {value: '', required: true, min: 3, max: 50, isValid: false, error: null},
    shortCode: {value: '', required: true, min: 2, max: 5, isValid: false, error: null}
}

export const bankDetailsForm = {
    id: { value: '' },
    ifscCode: { value: '', required: true, type: 'number', min: 1, max: 20, isValid: false, error: null },
    bankName: { value: '', required: true, type: 'text', min: 1, max: 20, isValid: false, error: null },
    bankBranch: { value: '', required: true, type: 'text', min: 1, max: 20, isValid: false, error: null },
    accountNum: { value: '', required: true, type: 'number', min: 1, max: 20, isValid: false, error: null },
    accountHolderName: { value: '', required: true, type: 'number', min: 1, max: 30, isValid: false, error: null },
}

export const supplierForm = {
    id: { value: '' },
    partyType: { value: '', required: true, type: 'text', min: 1, max: 50, isValid: false, error: null },
    partyTypeId: { value: '', required: true, type: 'text', min: 1, max: 50, isValid: false, error: null },
    partyName: { value: '', required: true, type: 'text', min: 1, max: 20, isValid: false, error: null },
    shortCode: { value: '', required: true, type: 'text', min: 1, max: 20, isValid: false, error: null },
    email: { value: '', required: true, type: 'text', min: 3, max: 30, isValid: false, error: null },
    contactNum: { value: '', required: true, type: 'text', min: 6, max: 12, isValid: false, error: null },
    addressId: { value: '' },
    addressLine1: { value: '', required: true, type: 'text', min: 3, max: 100, isValid: false, error: null },
    addressLine2: { value: '', required: true, type: 'text', min: 3, max: 100, isValid: false, error: null },
    pincode: { value: '', required: true, type: 'text', min: 3, max: 30, isValid: false, error: null },
    area: { value: '', required: true, type: 'text', min: 3, max: 30, isValid: false, error: null },
    city: { value: '', required: true, type: 'text', min: 3, max: 30, isValid: false, error: null },
    district: { value: '', required: true, type: 'text', min: 3, max: 30, isValid: false, error: null },
    state: { value: '', required: true, type: 'text', min: 3, max: 30, isValid: false, error: null },
    gstNum: { value: '', required: true, type: 'text', min: 3, max: 30, isValid: false, error: null },
    hallmarkChargesPerPc: { value: '', required: false, type: 'text', min: 3, max: 30, isValid: false, error: null },
    isSmith: { value: false, required: false, type: 'checkbox', min: 3, max: 30, isValid: false, error: null },
    isBranded: { value: false, required: false, type: 'checkbox', min: 3, max: 30, isValid: false, error: null },
    isGemStone: { value: false, required: false, type: 'checkbox', min: 3, max: 30, isValid: false, error: null },
    isTestingDealer: { value: false, required: false, type: 'checkbox', min: 3, max: 30, isValid: false, error: null },
    logoUrl: { value: '', required: false, type: 'text', isValid: false, error: null },
    bankDetails : {},
    supplierDetails: {},
}

export const DEALER_PARTY_TYPE = "DEALER";
export const HALLMARK_PARTY_TYPE = "HALLMARK";
export const MELTING_PARTY_TYPE = "MELTING";

export const supplierDetailsForm = {
    id: { value: '' },
    mainGroup: { value: '', required: true, type: 'text', min: 1, max: 50, isValid: false, error: null },
    mainGroupId: { value: '', required: true, type: 'text', min: 1, max: 50, isValid: false, error: null },
    openingWeight: { value: '', required: true, type: 'text', min: 1, max: 20, isValid: false, error: null },
    openingAmount: { value: '', required: true, type: 'text', min: 1, max: 20, isValid: false, error: null },
}

export const supplierColumns = [
    { headerName: "common.Party", field: "partyType" },
    { headerName: "common.name", field: "partyName" },
    { headerName: "common.shortCode", field: "shortCode" },
    { headerName: "creations.gst", field: "gstNum" },
    { headerName: "common.email", field: "email" },
    { headerName: "common.contact", field: "contactNum" },
    { headerName: "common.area", field: "area" },
    { headerName: "common.city", field: "city" },
    { headerName: "common.state", field: "state" },
]
