import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { branchesColumns, branchesForm, bankDetailsForm } from "./Constants";
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../components/Grid";
import Swal from 'sweetalert2'
import Select from "../../components/Form/Select";
import { saveBranch, fetchBranches, deleteBranches } from '../../redux/actions/Admin/branchActions'
import { fetchStoneTypes } from '../../redux/actions/creations/stoneTypeActions'
import { fetchPartyTypes } from '../../redux/actions/creations/partyTypeActions'
import { fetchMainGroups } from '../../redux/actions/mainGroupActions'
import { fetchAddress } from '../../redux/actions/addressActions'
import { fetchBanks } from '../../redux/actions/bankActions'
import CircularProgress from "@material-ui/core/CircularProgress";

import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

import LoadingOverlay from 'react-loading-overlay';

export const Branches = (props) => {
  const { t } = props;
  const [formData, setFormData] = useState(cloneDeep(branchesForm));
  const [viewForm, setToggleView] = useState(true);
  let [selected, setSelected] = useState([]);
  const dispatch = useDispatch();
  const [open, setOpen] = React.useState(false);
  const [options, setOptions] = React.useState([]);
  const [edit, setEdit] = useState(false)

  const [bankDetails, setBankDetails] = useState([cloneDeep(bankDetailsForm)])
  const loading = open && options.length === 0;
  const branches = useSelector(state => state.branchReducer.allBranches)
  const branchLoader = useSelector(state => state.branchReducer.loading);
  let Addresses = useSelector(state => state.addressReducer.allAddresses);
  const bankDetailData = useSelector(state => state.bankReducer.allBanks)
  const loader = useSelector(state => state.addressReducer.loading);

  const branchData = branches.data ? branches.data.map((branch) => {
    return {
      id: branch.id,
      branchName: branch.branchName,
      shortCode: branch.shortCode,
      email: branch.email,
      contactNum: branch.contactNum,
      alternateContactNum: branch.alternateContactNum,
      pincode: branch.address.pincode,
      state: branch.address.state,
      district: branch.address.district,
      city: branch.address.city,
      area: branch.address.area,
      addressLine1: branch.address.addressLine1,
      addressLine2: branch.address.addressLine2,
      addressId: branch.address.id,
      gstNo: branch.gstNo,
      isHeadBranch: branch.isHeadBranch,
      bankDetails: branch.bankDetails
    }
  }) : [];

  useEffect(() => {
    setFormData(formData);
  }, [formData]);

  useEffect(() => {
    if (bankDetailData) {

      bankDetails.map((bank, index) => {
        if (bankDetailData.ifsc === bank.ifscCode.value.toLocaleUpperCase()) {
          setBankElements(index, 'bankName', bankDetailData.bank)
          setBankElements(index, 'bankBranch', bankDetailData.branch)
        }
      })

    }
  }, [bankDetailData])

  useEffect(() => {
    console.log(Addresses)
    if (Addresses && Addresses.length > 0) {
      if (loader) {
        console.log('loading..')
      } else {
        setOptions(Addresses)
      }
    }

  })

  const getOptionSelected = (option) => {
    if (option && option.pincode.length === 6) {
      setFormValue('state', option.state)
      setFormValue('district', option.district)
      setFormValue('city', option.mandal)
      setFormValue('area', option.village)
    }
  }

  const setFormValue = (field, value) => {
    formData[field]['value'] = value;
    setFormData({ ...formData });
  }

  const resetForm = () => {
    map(formData, function (value, key, object) {
      if (key === 'logo') {
        document.getElementById("logo").value = "";
      }
      else {
        formData[key]['value'] = '';
      }
    });
    setFormData({ ...formData });
  }

  const saveForm = () => {
    console.log(formData)
    let isBankDetailsValid = true;
    bankDetails.map((ele, index) => {
      map(ele, function (value, key, object) {
        const isValid = validateField(value);
        if (!isValid.isValid) {
          isBankDetailsValid = false;
        }
        object[key] = { ...value, ...isValid }
        const list = [...bankDetails];
        setBankDetails(list)
      })
    });
    setFormValue('BankList', bankDetails)
    let isFormValid = true;
    map(formData, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      formData[key] = { ...value, ...isValid }
      setFormData(formData)
    });
    console.log("!!!!!!!!!!!!!!!!!", formData)
    if (isBankDetailsValid && isFormValid) {
      let data = {};
      map(formData, function (value, key, object) {
        data = {
          ...data,
          [key]: value.value
        }
      });

      console.log("!!!!!!!!!!!!!!!!!", data)

      Swal.fire({
        title: 'Confirm Saving Branch Info.!',
        text: 'Continue to save / Cancel to stop',
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Continue'
      }).then((result) => {
        if (result.isConfirmed) {
          dispatch(saveBranch(data))
            .then(() => {
              resetForm()
              setBankDetails([cloneDeep(bankDetailsForm)])
            })
        }
      })
    }
  }

  const validateField = (fieldObj) => {
    let errorObj = { isValid: true, error: null }
    if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
    else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
    else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }

    return errorObj;
  }

  const editAction = (ele) => {
    console.log(ele)
    setFormValue('id', ele.id)
    setFormValue('branchName', ele.branchName)
    setFormValue('shortCode', ele.shortCode)
    setFormValue('email', ele.email)
    setFormValue('contactNum', ele.contactNum)
    setFormValue('alternateContactNum', ele.alternateContactNum)
    setFormValue('gstNo', ele.gstNo)
    setFormValue('addressLine1', ele.addressLine1)
    setFormValue('addressLine2', ele.addressLine2)
    setFormValue('area', ele.area)
    setFormValue('city', ele.city)
    setFormValue('district', ele.district)
    setFormValue('state', ele.state)
    setFormValue('pincode', ele.pincode)
    setFormValue('addressId', ele.addressId)
    document.getElementById("isHeadBranch").checked = ele.isHeadBranch;
    setFormValue('isHeadBranch', ele.isHeadBranch)


    // ele.bankDetails.map((bank, index) => {
    //   if(index < ele.bankDetails.length) {
    //     setBankDetails(
    //       [...bankDetails, cloneDeep(bankDetailsForm)]
    //     )
    //   }
    // })
    var list = [];
    for (let i = 0; i < ele.bankDetails.length; i++) {
      list.push(cloneDeep(bankDetailsForm))
    }

    ele.bankDetails.map((bank, index) => {
      console.log(index)
      console.log(bank.id)
      console.log(list)

      list[index]['id']['value'] = bank.id;
      list[index]['id']['isValid'] = true;
      list[index]['accountHolderName']['value'] = bank.accountHolderName;
      list[index]['accountHolderName']['isValid'] = true;
      list[index]['accountNum']['value'] = bank.accountNum;
      list[index]['accountNum']['isValid'] = true;
      list[index]['bankBranch']['value'] = bank.bankBranch;
      list[index]['bankBranch']['isValid'] = true;
      list[index]['bankName']['value'] = bank.bankName;
      list[index]['bankName']['isValid'] = true;
      list[index]['ifscCode']['value'] = bank.ifscCode;
      list[index]['ifscCode']['isValid'] = true;
      setBankDetails(list)
      console.log(list)
    })

    // let isFormValid = true;
    // ele.bankDetails.map((item) => 
    // map(item, function (value, key, object) {
    //   console.log(value)
    //   console.log(key)
    //   console.log(object)
    //   const isValid = validateField(value);
    //   if (!isValid.isValid) {
    //     isFormValid = false;
    //   }
    //   object[key] = { value, ...isValid }
    //   const list = [...ele.bankDetails];
    //   console.log(list)
    //   setBankDetails(list)
    //   console.log(bankDetails)
    // }))


  }

  const deleteAction = (data) => {
    let selectedIds;
    if (data instanceof Array && data.length > 0) {
      selectedIds = data.map((ele) => {
        return ele.id
      })
    } else {
      selectedIds = data.id;
    }
    Swal.fire({
      title: 'Confirm Deleting Branch.!',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deleteBranches(selectedIds.toString()))
          .then(() => {
            selected = []
            setSelected(selected);
          })
      }
    })
  }

  useEffect(() => {
    dispatch(fetchBranches());
  }, [])

  const bankDetailsonChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...bankDetails];
    list[index][name]['value'] = value;
    list[index][name]['error'] = null;
    setBankDetails(list)
  }

  const addBank = () => {
    let isFormValid = true;
    map(bankDetails[bankDetails.length - 1], function (value, key, object) {
      console.log(value)
      console.log(key)
      console.log(object)
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      object[key] = { ...value, ...isValid }
      const list = [...bankDetails];
      setBankDetails(list)
      console.log(list)
    })
    if (isFormValid) {
      setBankDetails(
        [...bankDetails, cloneDeep(bankDetailsForm)]
      )
    }
  }

  const removeBank = (index) => {
    const list = [...bankDetails];
    list.splice(index, 1);
    setBankDetails(list)
  }

  const toggleView = () => {
    const formNode = document.getElementById('form');
    formNode.classList.toggle('collapse-panel');
    setToggleView(!viewForm);
  }


  const onChangeHandle = async value => {
    console.log(value)
    if (value.length === 6) {
      setFormValue('pincode', value)
      dispatch(fetchAddress(value))
    }
    else {
      setOptions([]);
    }
  };

  React.useEffect(() => {
    if (!open) {
      setOptions([]);
    }
  }, [open]);

  const onIfscChange = (value, index) => {

    if (value.length <= 11) {
      setBankElements(index, 'ifscCode', value);
    }
    if (value.length === 11) {
      dispatch(fetchBanks(value.toLocaleUpperCase()))
    }
    if (value.length < 11) {
      setBankElements(index, 'bankName', '')
      setBankElements(index, 'bankBranch', '')
    }
  }

  const setBankElements = (index, name, value) => {
    console.log(index)
    const list = [...bankDetails];
    console.log(list)
    list[index][name]['value'] = value;
    console.log(list)
    setBankDetails(list)
  }

  return (
    <div className="panel panel-default hero-panel">
      <LoadingOverlay
        active={branchLoader}
        spinner
        text='Loading...'
      >
        <div className="panel-heading">
          <h1 className="panel-title">{t("adminMenus.branches")} </h1>
          <div className="panel-options">
            <a
              href="#"
              className="text-info collapseBtn"
              title="Show Add Form"
            >
              {viewForm ?
                <i className="zmdi zmdi-minus-circle  zmdi-hc-2x" onClick={() => { toggleView() }}>
                </i> :
                <i className="zmdi zmdi-plus-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                </i>}
            </a>
          </div>
        </div>
        <div className="panel-body">
          <div className="panel update-panel form-pnel-container" id="form">
            <form id="companyDetailsForm">
              <div className="panel-body  p-t-0">
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h3 className="panel-title">
                      {t("common.company")} {t("common.details")}
                    </h3>
                  </div>
                  <div className="row">
                    <div className="col-sm-6 col-md-3 col-lg-3">
                      <div className="form-group">
                        <label htmlFor="">{t("branches.branchName")}<span className="asterisk">*</span></label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder=""
                          name="branchName"
                          value={get(formData, 'branchName.value')}
                          onChange={(e) => setFormValue('branchName', e.target.value)}
                        />
                        <div className="invalid-feedback">{get(formData, 'branchName.error')}</div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-3 col-lg-3">
                      <div className="form-group">
                        <label htmlFor="">{t("common.shortCode")}<span className="asterisk">*</span></label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder=""
                          name="shortCode"
                          value={get(formData, 'shortCode.value')}
                          onChange={(e) => setFormValue('shortCode', e.target.value)}
                        />
                        <div className="invalid-feedback">{get(formData, 'shortCode.error')}</div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-3 col-lg-3">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.email")}<span className="asterisk">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="email"
                          name="email"
                          value={get(formData, 'email.value')}
                          onChange={(e) => setFormValue('email', e.target.value)}
                        />
                        <div className="invalid-feedback">{get(formData, 'email.error')}</div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-3 col-lg-3">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.phone")} {t("common.number")}<span className="asterisk">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="contactNum"
                          name="contactNum"
                          value={get(formData, 'contactNum.value')}
                          onChange={(e) => setFormValue('contactNum', e.target.value)}
                        />
                        <div className="invalid-feedback">{get(formData, 'contactNum.error')}</div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-3 col-lg-3">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.alternate")} {t("common.phone")} {t("common.number")}<span className="asterisk">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="alternateContactNum"
                          name="alternateContactNum"
                          value={get(formData, 'alternateContactNum.value')}
                          onChange={(e) => setFormValue('alternateContactNum', e.target.value)}
                        />
                        <div className="invalid-feedback">{get(formData, 'alternateContactNum.error')}</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h3 className="panel-title">{t("common.address")}</h3>
                  </div>
                  <div className="panel-body">
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">Pincode<span className="asterisk">*</span></label>
                        <Autocomplete
                          id="pincode"
                          open={open}

                          onOpen={() => {
                            setOpen(true);
                          }}
                          onClose={() => {
                            setOpen(false);
                          }}
                          options={options}
                          filterOptions={(x) => x}
                          autoComplete
                          includeInputInList
                          filterSelectedOptions
                          onChange={(event, value) => getOptionSelected(value)}
                          getOptionLabel={option => option.pincode ? option.pincode : get(formData, 'pincode.value')}
                          value={get(formData, 'pincode.value')}
                          loading={loading}
                          renderOption={(option) => <span>{option.village}, {option.mandal}, {option.district},  {option.state} , {option.pincode}</span>}
                          renderInput={params => {
                            const inputProps = params.inputProps;
                            inputProps.autoComplete = 'new-password';
                            return (
                              <TextField

                                {...params}
                                onChange={ev => {
                                  if (ev.target.value !== "" || ev.target.value !== null) {
                                    onChangeHandle(ev.target.value);
                                  }
                                }}
                                InputProps={{
                                  ...params.InputProps,
                                  endAdornment: (
                                    <React.Fragment>
                                      {loading ? (
                                        <CircularProgress color="inherit" size={20} />
                                      ) : null}
                                      {params.InputProps.endAdornment}
                                    </React.Fragment>
                                  )
                                }}

                              />
                            );
                          }
                          }
                        />
                        <div className="invalid-feedback">{get(formData, 'pincode.error')}</div>
                      </div>

                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">State<span className="asterisk">*</span></label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="state"
                          name="state"
                          value={get(formData, 'state.value')}
                          disabled
                        />
                        <div className="invalid-feedback">{get(formData, 'state.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">District<span className="asterisk">*</span></label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="district"
                          name="district"
                          value={get(formData, 'district.value')}
                          disabled
                        />
                        <div className="invalid-feedback">{get(formData, 'district.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">City<span className="asterisk">*</span></label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="city"
                          name="city"
                          value={get(formData, 'city.value')}
                          disabled
                        />
                        <div className="invalid-feedback">{get(formData, 'city.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">Area/Village<span className="asterisk">*</span></label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="area"
                          name="area"
                          value={get(formData, 'area.value')}
                          disabled
                        />
                        <div className="invalid-feedback">{get(formData, 'area.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">Line 1</label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder=""
                          name="addressLine1"
                          value={get(formData, 'addressLine1.value')}
                          onChange={(e) => setFormValue('addressLine1', e.target.value)}
                        />
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">Line 2</label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder=""
                          name="addressLine2"
                          value={get(formData, 'addressLine2.value')}
                          onChange={(e) => setFormValue('addressLine2', e.target.value)}
                        />
                      </div>
                    </div>

                  </div>
                </div>


                <div className="panel panel-default">
                  <div className="panel-heading position-relative">
                    <h3 className="panel-title">{t("common.bank")} {t("common.details")}</h3>
                    <span className="addBtn btn btn-primary btn-sm mdi mdi-plus" onClick={addBank}></span>
                  </div>
                  {bankDetails && bankDetails.map((bank, index) => {
                    return (
                      <div key={`bankDetails_${index}`} className="bg-light p-10 m-b-10 position-relative">
                        {bankDetails.length > 1 && <span className="removeBtn btn btn-danger btn-sm mdi mdi-close" onClick={() => removeBank(index)}></span>}
                        <div className="row">
                          <div className="col-md-3">
                            <div className="form-group">
                              <label htmlFor="">{t("common.account")} {t("common.number")}</label>
                              <input
                                type="text"
                                className="form-control"
                                name="accountNum"
                                placeholder="accountNum"
                                onChange={e => bankDetailsonChange(e, index)}
                                value={bank.accountNum.value}
                              />
                              <div className="invalid-feedback">{bank.accountNum.error}</div>
                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="form-group">
                              <label htmlFor="">{t("common.accountholder")} {t("common.name")}</label>
                              <input
                                type="text"
                                className="form-control"
                                name="accountHolderName"
                                placeholder="accountHolderName"
                                onChange={e => bankDetailsonChange(e, index)}
                                value={bank.accountHolderName.value}
                              />
                              <div className="invalid-feedback">{bank.accountHolderName.error}</div>
                            </div>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-3">
                            <div className="form-group">
                              <label htmlFor="">{t("common.ifsc")} {t("common.code")}</label>
                              <input type="text"
                                className="form-control"
                                value={bank.ifscCode.value}
                                onChange={(e) => onIfscChange(e.target.value, index)}
                                style={{ textTransform: 'uppercase' }}
                              />
                              <div className="invalid-feedback">{bank.ifscCode.error}</div>
                              {/* {!bank.ifscCode.error && ((bank.ifscCode.value.length < 11 && bank.ifscCode.value.length > 0) && <div className="invalid-feedback">IFSC code should be 11 digits</div>)} */}

                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="form-group">
                              <label htmlFor="">{t("creations.bankName")}</label>
                              <input
                                type="text"
                                className="form-control"
                                name="bankName"
                                placeholder="bankName"
                                onChange={e => bankDetailsonChange(e, index)}
                                value={bank.bankName.value}
                                disabled
                              />
                              <div className="invalid-feedback">{bank.bankName.error}</div>
                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="form-group">
                              <label htmlFor="">{t("creations.branchName")}</label>
                              <input
                                type="text"
                                className="form-control"
                                name="bankBranch"
                                placeholder="bankBranch"
                                onChange={e => bankDetailsonChange(e, index)}
                                value={bank.bankBranch.value}
                                disabled
                              />
                              <div className="invalid-feedback">{bank.bankBranch.error}</div>
                            </div>
                          </div>

                        </div>
                      </div>
                    )
                  })}
                </div>

                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h3 className="panel-title">{t("common.others")} </h3>
                  </div>
                  <div className="panel-body">
                    <div className="col-sm-6 col-md-3 col-lg-3">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("creations.gst")} {t("common.number")}<span className="asterisk">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="gstNo"
                          name="gstNo"
                          value={get(formData, 'gstNo.value')}
                          onChange={(e) => setFormValue('gstNo', e.target.value)}
                        />
                        <div className="invalid-feedback">{get(formData, 'gstNo.error')}</div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-3 col-lg-3">
                      <div className="form-group">
                        <label htmlFor="">&nbsp;</label>
                        <div className="checkbox checkbox-primary">
                          <input
                            type="checkbox"
                            name="isHeadBranch"
                            id="isHeadBranch"
                            defaultChecked={get(formData, 'isHeadBranch.value')}
                            onChange={(e) => setFormValue('isHeadBranch', !formData.isHeadBranch.value)} />
                          <label htmlFor="">{t("branches.isHeadBranch")}</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="panel-footer text-center">
                  <a href="#" className="btn btn-primary fire" onClick={saveForm}>
                    <i className="zmdi zmdi-check"></i> {t("common.save")}
                  </a>
                  <a href="#" className="btn btn-default" onClick={resetForm}>
                    <i className="zmdi zmdi-close"></i> {t("common.clear")}
                  </a>
                </div>
              </div>
            </form>
          </div>

          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">{t("adminMenus.branches")} {t("common.list")}</h3>
              <div className="panel-options">
                <p>
                  {selected.length > 0 ?
                    <a href="#" className="btn btn-info btn-sm" onClick={() => deleteAction(selected)} >
                      <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                    </a> :
                    null}
                </p>
              </div>
            </div>
            <div className="panel-body">
              <div className="ag-grid-wrapper">
                <Grid colDefs={branchesColumns} rowData={branchData} selectedRows={setSelected} actions={{ isRequired: true, editAction: editAction, deleteAction: deleteAction }} flex={true} />
              </div>
            </div>
          </div>
        </div>
      </LoadingOverlay>
    </div>
  );
};

export default withTranslation()(Branches);
