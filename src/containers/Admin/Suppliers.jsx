import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import {
  supplierColumns, supplierForm, bankDetailsForm, supplierDetailsForm,
  DEALER_PARTY_TYPE, HALLMARK_PARTY_TYPE, MELTING_PARTY_TYPE
} from "./Constants";
import { get, map, cloneDeep } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import Grid from "../../components/Grid";
import Swal from 'sweetalert2'
import Select from "../../components/Form/Select";
import { saveSupplier, fetchSuppliers, deleteSuppliers } from '../../redux/actions/Admin/supplierActions'
import { fetchPartyTypes } from '../../redux/actions/creations/partyTypeActions'
import { fetchMainGroups } from '../../redux/actions/mainGroupActions'

import { fetchAddress } from '../../redux/actions/addressActions'
import { fetchBanks } from '../../redux/actions/bankActions'

import CircularProgress from "@material-ui/core/CircularProgress";
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import LoadingOverlay from 'react-loading-overlay';

export const Suppliers = (props) => {
  const { t } = props;
  const [formData, setFromData] = useState(cloneDeep(supplierForm));
  const [viewForm, setToggleView] = useState(true);
  let [selected, setSelected] = useState([]);
  const dispatch = useDispatch();
  const partyTypes = useSelector(state => state.partyTypeReducer.allPartyTypes)
  const mainGroups = useSelector(state => state.MainGroupReducer.allGroups)

  const [bankDetails, setBankDetails] = useState([cloneDeep(bankDetailsForm)])
  const [supplierDetails, setSupplierDetails] = useState([cloneDeep(supplierDetailsForm)])

  const [open, setOpen] = React.useState(false);
  const [options, setOptions] = React.useState([]);

  const [image, setImage] = useState({
    preview: "",
    raw: ""
  })

  const loading = open && options.length === 0;

  let Addresses = useSelector(state => state.addressReducer.allAddresses);
  const bankDetailData = useSelector(state => state.bankReducer.allBanks)
  const suppliers = useSelector(state => state.supplierReducer.allSuppliers)
  const supplierLoader = useSelector(state => state.supplierReducer.loading);
  const loader = useSelector(state => state.addressReducer.loading);

  useEffect(() => {
    if (bankDetailData) {
      bankDetails.map((bank, index) => {
        if (bankDetailData.ifsc === bank.ifscCode.value.toLocaleUpperCase()) {
          setBankElements(index, 'bankName', bankDetailData.bank)
          setBankElements(index, 'bankBranch', bankDetailData.branch)
        }
      })
    }
  }, [bankDetailData])

  useEffect(() => {
    if (Addresses && Addresses.length > 0) {
      if (loader) {
        console.log('loading..')
      } else {
        setOptions(Addresses)
      }
    }
  })

  useEffect(() => {
    setFromData(formData);
  }, [formData]);

  const suppliersData = suppliers.data ? suppliers.data.map((supplier) => {
    let supplierDetails = supplier.supplierDetails ? supplier.supplierDetails.map((details) => {
      return {
        id: details.id,
        mainGroupId: details.mainGroupId,
        mainGroup: details.mainGroup,
        openingWeight: details.openingWeight,
        openingAmount: details.openingAmount
      }
    }) : [];
    let bankDetails = supplier.bankDetails ? supplier.bankDetails.map((bank) => {
      return {
        id: bank.id,
        ifscCode: bank.ifscCode,
        bankName: bank.bankName,
        bankBranch: bank.bankBranch,
        accountNum: bank.accountNum,
        accountHolderName: bank.accountHolderName
      }
    }) : [];
    return {
      id: supplier.id,
      partyType: supplier.partyType,
      partyTypeId: supplier.partyTypeId,
      partyName: supplier.partyName,
      shortCode: supplier.shortCode,
      email: supplier.email,
      contactNum: supplier.contactNum,
      addressId: supplier.address.id,
      addressLine1: supplier.address.addressLine1,
      addressLine2: supplier.address.addressLine2,
      pincode: supplier.address.pincode,
      area: supplier.address.area,
      city: supplier.address.city,
      district: supplier.address.district,
      state: supplier.address.state,
      gstNum: supplier.gstNum,
      hallmarkChargesPerPc: supplier.hallmarkChargesPerPc,
      isSmith: supplier.isSmith,
      isBranded: supplier.isBranded,
      isGemStone: supplier.isGemStone,
      isTestingDealer: supplier.isTestingDealer,
      logoUrl: supplier.logoUrl,
      bankDetails: bankDetails,
      supplierDetails: supplierDetails
    }
  }) : [];

  const partyTypeData = partyTypes.data ? partyTypes.data.map((type) => {
    return {
      id: type.id,
      name: type.partyType
    }
  }) : [];

  const mainGroupData = mainGroups.data ? mainGroups.data.map((group) => {
    return {
      id: group.id,
      name: group.groupName,
      shortCode: group.shortCode,
    }
  }) : [];

  const [partyTypeText, setPartyTypeText] = useState();

  const partyTypeChange = (e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    console.log(e.target.value)
    console.log(label)
    resetElementsOnPartyTypeChange();
    setFormValue('partyTypeId', e.target.value)
    setFormValue('partyType', label)
    setPartyTypeText(label);
  }

  const resetElementsOnPartyTypeChange = () => {
    setFormValue('hallmarkChargesPerPc', '');
    document.getElementById("isTestingDealer").checked = false;
    setFormValue('isTestingDealer', false);
    document.getElementById("isSmith").checked = false;
    setFormValue('isSmith', false);
    document.getElementById("isBranded").checked = false;
    setFormValue('isBranded', false);
    document.getElementById("isGemStone").checked = false;
    setFormValue('isGemStone', false);
  }

  const getOptionSelected = (option) => {
    if (option && option.pincode.length === 6) {
      setFormValue('state', option.state)
      setFormValue('district', option.district)
      setFormValue('city', option.mandal)
      setFormValue('area', option.village)
    }
  }

  const onChangeHandle = async value => {
    if (value.length === 6) {
      setFormValue('pincode', value)
      dispatch(fetchAddress(value))
    }
    else {
      setOptions([]);
    }
  };

  const onIfscChange = (value, index) => {
    if (value.length <= 11) {
      setBankElements(index, 'ifscCode', value);
    }
    if (value.length === 11) {
      dispatch(fetchBanks(value.toLocaleUpperCase()))
    }
    if (value.length < 11) {
      // resetting bank values 
      setBankElements(index, 'bankName', '')
      setBankElements(index, 'bankBranch', '')
    }
  }

  const setFormValue = (field, value) => {
    formData[field]['value'] = value;
    setFromData({ ...formData });
  }
  const resetForm = () => {
    map(formData, function (value, key, object) {
      if (key === 'isSmith') {
        document.getElementById("isSmith").checked = false;
        setFormValue('isSmith', false)
      } else if (key === 'isBranded') {
        document.getElementById("isBranded").checked = false;
        setFormValue('isBranded', false)
      } else if (key === 'isGemStone') {
        document.getElementById("isGemStone").checked = false;
        setFormValue('isGemStone', false)
      } else if (key === 'isTestingDealer') {
        document.getElementById("isTestingDealer").checked = false;
        setFormValue('isTestingDealer', false)
      } else {
        formData[key]['value'] = '';
      }
    });
    setImage({
      preview: "",
      raw: ""
    });
    setPartyTypeText('');
    setFromData({ ...formData });
    setBankDetails([cloneDeep(bankDetailsForm)]);
    setSupplierDetails([cloneDeep(supplierDetailsForm)]);
  }
  const saveForm = () => {
    let isSupplierDetailsValid = true;
    supplierDetails.map((ele, index) => {
      map(ele, function (value, key, object) {
        const isValid = validateField(value);
        if (!isValid.isValid) {
          isSupplierDetailsValid = false;
        }
        object[key] = { ...value, ...isValid }
        const list = [...supplierDetails];
        setSupplierDetails(list)
      })
    });
    setFormValue('supplierDetails', supplierDetails)
    let isBankDetailsValid = true;
    bankDetails.map((ele, index) => {
      map(ele, function (value, key, object) {
        const isValid = validateField(value);
        if (!isValid.isValid) {
          isBankDetailsValid = false;
        }
        object[key] = { ...value, ...isValid }
        const list = [...bankDetails];
        setBankDetails(list)
      })
    });
    setFormValue('bankDetails', bankDetails)
    let isFormValid = true;
    map(formData, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      formData[key] = { ...value, ...isValid }
    });
    setFromData(formData);
    if (isSupplierDetailsValid && isBankDetailsValid && isFormValid) {
      let data = {};
      map(formData, function (value, key, object) {
        data = {
          ...data,
          [key]: value.value
        }
        if (key === 'bankDetails' || key === 'supplierDetails') {
          let elementsList = value.value.map((ele) => {
            let element = {};
            map(ele, function (value, key1, object) {
              element = {
                ...element,
                [key1]: value.value,
                isDeleted: false
              }
            });
            return element;
          })
          data = {
            ...data,
            [key]: elementsList
          }
        }
      });
      data.logo = image.raw;
      Swal.fire({
        title: 'Confirm Saving Supplier Info.!',
        text: 'Continue to save / Cancel to stop',
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Continue'
      }).then((result) => {
        if (result.isConfirmed) {
          dispatch(saveSupplier(data))
            .then(() => {
              resetForm()
            })
        }
      })
    }
  }
  const validateField = (fieldObj) => {
    let errorObj = { isValid: true, error: null }
    if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
    else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
    else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }

    return errorObj;
  }

  const editAction = (ele) => {
    resetForm();
    setFormValue('id', ele.id)
    setFormValue('partyType', ele.partyType)
    setFormValue('partyTypeId', ele.partyTypeId)
    setPartyTypeText(ele.partyType);
    setFormValue('partyName', ele.partyName)
    setFormValue('shortCode', ele.shortCode)
    setFormValue('email', ele.email)
    setFormValue('contactNum', ele.contactNum)
    setFormValue('addressId', ele.addressId)
    setFormValue('addressLine1', ele.addressLine1)
    setFormValue('addressLine2', ele.addressLine2)
    setFormValue('pincode', ele.pincode)
    setFormValue('area', ele.area)
    setFormValue('city', ele.city)
    setFormValue('district', ele.district)
    setFormValue('state', ele.state)
    setFormValue('gstNum', ele.gstNum)
    setFormValue('hallmarkChargesPerPc', ele.hallmarkChargesPerPc)
    document.getElementById("isSmith").checked = ele.isSmith;
    setFormValue('isSmith', ele.isSmith)
    document.getElementById("isBranded").checked = ele.isBranded;
    setFormValue('isBranded', ele.isBranded)
    document.getElementById("isGemStone").checked = ele.isGemStone;
    setFormValue('isGemStone', ele.isGemStone)
    document.getElementById("isTestingDealer").checked = ele.isTestingDealer;
    setFormValue('isTestingDealer', ele.isTestingDealer)
    setFormValue('logoUrl', ele.logoUrl)
    setImage({
      preview: ele.logoUrl
    })
    bankDetails.length = 1;
    ele.bankDetails.map((bank, index) => {
      if (index < ele.bankDetails.length - 1) {
        bankDetails.push(cloneDeep(bankDetailsForm));
      }
      setBankElements(index, 'id', bank.id);
      setBankElements(index, 'ifscCode', bank.ifscCode);
      setBankElements(index, 'bankName', bank.bankName);
      setBankElements(index, 'bankBranch', bank.bankBranch);
      setBankElements(index, 'accountNum', bank.accountNum);
      setBankElements(index, 'accountHolderName', bank.accountHolderName);

    });
    supplierDetails.length = 1;
    ele.supplierDetails.map((details, index) => {
      if (index < ele.supplierDetails.length - 1) {
        supplierDetails.push(cloneDeep(supplierDetailsForm));
      }
      setSupplierDetailsElements(index, 'id', details.id);
      setSupplierDetailsElements(index, 'mainGroupId', details.mainGroupId);
      setSupplierDetailsElements(index, 'mainGroup', details.mainGroup);
      setSupplierDetailsElements(index, 'openingAmount', details.openingAmount);
      setSupplierDetailsElements(index, 'openingWeight', details.openingWeight);
    });
  }

  useEffect(() => {
    setBankDetails(bankDetails);
  }, [bankDetails]);

  const deleteAction = (data) => {
    let selectedIds;
    if (data instanceof Array && data.length > 0) {
      selectedIds = data.map((ele) => {
        return ele.id
      })
    } else {
      selectedIds = data.id;
    }
    Swal.fire({
      title: 'Confirm Deleting Supplier.!',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deleteSuppliers(selectedIds.toString()))
          .then(() => {
            selected = []
            setSelected(selected);
          })
      }
    })
  }

  useEffect(() => {
    dispatch(fetchPartyTypes());
    dispatch(fetchMainGroups());
    dispatch(fetchSuppliers())
  }, [])

  const bankDetailsonChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...bankDetails];
    list[index][name]['value'] = value;
    list[index][name]['error'] = null;
    setBankDetails(list)
  }

  const setBankElements = (index, name, value) => {
    const list = [...bankDetails];
    list[index][name]['value'] = value;
    setBankDetails(list)
  }

  const setSupplierDetailsElements = (index, name, value) => {
    const list = [...supplierDetails];
    list[index][name]['value'] = value;
    setSupplierDetails(list)
  }

  const supplierDetailsonChange = (e, index) => {
    let { name, value } = e.target;
    const list = [...supplierDetails];
    if (name === 'mainGroupId') {
      let selectedIndex = e.nativeEvent.target.selectedIndex;
      let label = e.nativeEvent.target[selectedIndex].text;
      if (list.some(ele => ele.mainGroupId.value === value)) {
        Swal.fire({
          position: "top-end",
          title: 'MainGroup already used.!',
          text: 'Please select other group',
          icon: 'info',
          showConfirmButton: false,
          timer: 2000
        })
        value = '';
        label = '';
      }
      list[index]['mainGroup']['value'] = label;
      list[index]['mainGroup']['error'] = null;
    }
    list[index][name]['value'] = value;
    list[index][name]['error'] = null;
    setSupplierDetails(list)
  }

  const addBank = () => {
    let isFormValid = true;
    map(bankDetails[bankDetails.length - 1], function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      object[key] = { ...value, ...isValid }
      const list = [...bankDetails];
      setBankDetails(list)
    })
    if (isFormValid) {
      setBankDetails(
        [...bankDetails, cloneDeep(bankDetailsForm)]
      )
    }
  }

  const removeBank = (index) => {
    const list = [...bankDetails];
    list.splice(index, 1);
    setBankDetails(list)
  }

  const addSupplier = () => {
    let isFormValid = true;
    map(supplierDetails[supplierDetails.length - 1], function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      object[key] = { ...value, ...isValid }
      const list = [...supplierDetails];
      setSupplierDetails(list)
    })
    if (isFormValid) {
      setSupplierDetails(
        [...supplierDetails, cloneDeep(supplierDetailsForm)]
      )
    }
  }

  const removeSupplier = (index) => {
    const list = [...supplierDetails];
    list.splice(index, 1);
    setSupplierDetails(list)
  }

  const toggleView = () => {
    const formNode = document.getElementById('form');
    formNode.classList.toggle('collapse-panel');
    setToggleView(!viewForm);
  }

  const handleChange = (e) => {
    if (e.target.files.length) {
      setImage({
        preview: URL.createObjectURL(e.target.files[0]),
        raw: e.target.files[0]
      })
    }
  }

  return (
    <>
      <div className="panel panel-default hero-panel">
      <LoadingOverlay
          active={supplierLoader}
          spinner
          text='Loading...'
        >
        <div className="panel-heading">
          <h1 className="panel-title">{t("adminMenus.suppliers")}</h1>
          <div className="panel-options">
            <a
              href="#"
              className="text-info collapseBtn"
              title="Show Add Form"
            >
              {viewForm ?
                <i className="zmdi zmdi-minus-circle  zmdi-hc-2x" onClick={() => { toggleView() }}>
                </i> :
                <i className="zmdi zmdi-plus-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                </i>}
            </a>
          </div>
        </div>
        <div className="panel-body">
          <div className="panel update-panel form-pnel-container" id="form">
            <form id="companyDetailsForm">
              <div className="panel-body  p-t-0">
                <div className="panel panel-default">
                  <div className="panel-heading">
                    {/* <h3 className="panel-title">
                      {t("common.Party")} {t("common.details")}
                    </h3> */}
                  </div>
                  <div className="row">
                    <div className="col-md-3">
                      <label htmlFor="upload-button">
                        {image.preview ?
                          <img src={image.preview} alt="logo" style={{ width: '128px', height: '128px', borderRadius: '50%' }} /> :
                          <div className="circle m-t-20">
                            <i className="zmdi zmdi-camera zmdi-hc-2x"></i>
                          </div>
                        }
                      </label>
                      <input
                        type="file"
                        id="upload-button"
                        style={{ display: 'none' }}
                        onChange={handleChange} />
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.Party")} {t("common.type")}<span className="asterisk">*</span>
                        </label>
                        <Select name="partyTypeId" value={get(formData, 'partyTypeId.value')} onChange={(e) => partyTypeChange(e)} data={partyTypeData} />
                        <div className="invalid-feedback">{get(formData, 'partyTypeId.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.Party")} {t("common.name")}<span className="asterisk">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="partyName"
                          name="partyName"
                          value={get(formData, 'partyName.value')}
                          onChange={(e) => setFormValue('partyName', e.target.value)}
                        />
                        <div className="invalid-feedback">{get(formData, 'partyName.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.Party")} {t("common.code")}<span className="asterisk">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="shortCode"
                          name="shortCode"
                          value={get(formData, 'shortCode.value')}
                          onChange={(e) => setFormValue('shortCode', e.target.value)}
                        />
                        <div className="invalid-feedback">{get(formData, 'shortCode.error')}</div>
                      </div>

                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.email")}<span className="asterisk">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="email"
                          name="email"
                          value={get(formData, 'email.value')}
                          onChange={(e) => setFormValue('email', e.target.value)}
                        />
                        <div className="invalid-feedback">{get(formData, 'email.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">
                          {t("common.phone")} {t("common.number")}<span className="asterisk">*</span>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="contactNum"
                          name="contactNum"
                          value={get(formData, 'contactNum.value')}
                          onChange={(e) => setFormValue('contactNum', e.target.value)}
                        />
                        <div className="invalid-feedback">{get(formData, 'contactNum.error')}</div>
                      </div>
                      {/* <div className="form-group">
                        <label htmlFor="">Address</label>
                        <input type="text" className="form-control" placeholder="" />
                      </div> */}
                    </div>
                  </div>

                  <div className="panel panel-default">
                    <div className="panel-heading">
                      <h3 className="panel-title">{t("common.address")}</h3>
                    </div>
                    <div className="panel-body">
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">Pincode<span className="asterisk">*</span></label>
                          <Autocomplete
                            id="pincode"
                            open={open}
                            onOpen={() => {
                              setOpen(true);
                            }}
                            onClose={() => {
                              setOpen(false);
                            }}
                            options={options}
                            filterOptions={(x) => x}
                            autoComplete
                            includeInputInList
                            filterSelectedOptions
                            onChange={(event, value) => getOptionSelected(value)}
                            getOptionLabel={option => option.pincode ? option.pincode : get(formData, 'pincode.value')}
                            value={get(formData, 'pincode.value')}
                            loading={loading}
                            renderOption={(option) => <span>{option.village}, {option.mandal}, {option.district},  {option.state} , {option.pincode}</span>}
                            renderInput={params => {
                              const inputProps = params.inputProps;
                              inputProps.autoComplete = 'new-password';
                              return (
                                <TextField
                                  {...params}
                                  onChange={ev => {
                                    if (ev.target.value !== "" || ev.target.value !== null) {
                                      onChangeHandle(ev.target.value);
                                    }
                                  }}
                                  InputProps={{
                                    ...params.InputProps,
                                    endAdornment: (
                                      <React.Fragment>
                                        {loading ? (
                                          <CircularProgress color="inherit" size={20} />
                                        ) : null}
                                        {params.InputProps.endAdornment}
                                      </React.Fragment>
                                    )
                                  }}

                                />
                              );
                            }
                            }
                          />
                          {/* <div className="invalid-feedback">{get(formData, 'pincode.error')}</div> */}
                        </div>

                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">State<span className="asterisk">*</span></label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="state"
                            name="state"
                            value={get(formData, 'state.value')}
                            disabled
                          />
                          <div className="invalid-feedback">{get(formData, 'state.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">District<span className="asterisk">*</span></label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="district"
                            name="district"
                            value={get(formData, 'district.value')}
                            disabled
                          />
                          <div className="invalid-feedback">{get(formData, 'district.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">City<span className="asterisk">*</span></label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="city"
                            name="city"
                            value={get(formData, 'city.value')}
                            disabled
                          />
                          <div className="invalid-feedback">{get(formData, 'city.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">Area/Village<span className="asterisk">*</span></label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="area"
                            name="area"
                            value={get(formData, 'area.value')}
                            disabled
                          />
                          <div className="invalid-feedback">{get(formData, 'area.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">Line 1</label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder=""
                            name="addressLine1"
                            value={get(formData, 'addressLine1.value')}
                            onChange={(e) => setFormValue('addressLine1', e.target.value)}
                          />
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">Line 2</label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder=""
                            name="addressLine2"
                            value={get(formData, 'addressLine2.value')}
                            onChange={(e) => setFormValue('addressLine2', e.target.value)}
                          />
                        </div>
                      </div>

                    </div>
                  </div>

                  <div className="panel panel-default">
                    <div className="panel-heading position-relative">
                      <h3 className="panel-title">{t("common.other")} {t("common.details")}</h3>
                      <span className="addBtn btn btn-primary btn-sm mdi mdi-plus" onClick={addSupplier}></span>
                    </div>

                    {supplierDetails && supplierDetails.map((ele, index) => {
                      return (
                        <div key={`supplierDetails_${index}`} className="bg-light p-10 m-b-10 position-relative">
                          {supplierDetails.length > 1 && <div className="removeBtn btn btn-danger btn-sm mdi mdi-close" onClick={() => removeSupplier(index)}></div>}
                          <div className="row">
                            <div className="col-md-3">
                              <div className="form-group">
                                <label htmlFor="">
                                  {t("creations.mainGroup")}<span className="asterisk">*</span>
                                </label>
                                <Select name="mainGroupId" value={ele.mainGroupId.value} onChange={e => supplierDetailsonChange(e, index)} data={mainGroupData} />
                                <div className="invalid-feedback">{ele.mainGroupId.error}</div>
                              </div>
                            </div>
                            <div className="col-md-3">
                              <div className="form-group">
                                <label htmlFor="">
                                  {t("suppliers.openingWeight")}<span className="asterisk">*</span>
                                </label>
                                <input
                                  type="text"
                                  className="form-control"
                                  placeholder="openingWeight"
                                  name="openingWeight"
                                  value={ele.openingWeight.value}
                                  onChange={e => supplierDetailsonChange(e, index)}
                                />
                                <div className="invalid-feedback">{ele.openingWeight.error}</div>
                              </div>
                            </div>
                            <div className="col-md-3">
                              <div className="form-group">
                                <label htmlFor="">
                                  {t("suppliers.openingAmount")}<span className="asterisk">*</span>
                                </label>
                                <input
                                  type="text"
                                  className="form-control"
                                  placeholder="openingAmount"
                                  name="openingAmount"
                                  value={ele.openingAmount.value}
                                  onChange={e => supplierDetailsonChange(e, index)}
                                />
                                <div className="invalid-feedback">{ele.openingAmount.error}</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      )
                    })}
                    <div className="row">
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">
                            {t("creations.gst")} {t("common.number")}<span className="asterisk">*</span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="gstNum"
                            name="gstNum"
                            value={get(formData, 'gstNum.value')}
                            onChange={(e) => setFormValue('gstNum', e.target.value)}
                          />
                          <div className="invalid-feedback">{get(formData, 'gstNum.error')}</div>
                        </div>
                      </div>
                      <div className={`col-md-4 ${partyTypeText && partyTypeText === HALLMARK_PARTY_TYPE ? '' : 'hide'}`}>
                        <div className="form-group">
                          <label htmlFor="">{t("suppliers.hallmarkChargesPerPc")}</label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="hallmarkChargesPerPc"
                            name="hallmarkChargesPerPc"
                            value={get(formData, 'hallmarkChargesPerPc.value')}
                            onChange={(e) => setFormValue('hallmarkChargesPerPc', e.target.value)}
                          />
                          <div className="invalid-feedback">{get(formData, 'hallmarkChargesPerPc.error')}</div>
                        </div>
                      </div>
                      <div className={`col-md-3 ${partyTypeText && partyTypeText === MELTING_PARTY_TYPE ? '' : 'hide'}`}>
                        <div className="form-group">
                          <label htmlFor="">&nbsp;</label>
                          <div className="checkbox checkbox-primary">
                            <input
                              type="checkbox"
                              name="isTestingDealer"
                              id="isTestingDealer"
                              defaultChecked={get(formData, 'isTestingDealer.value')}
                              onChange={(e) => setFormValue('isTestingDealer', !formData.isTestingDealer.value)} />
                            <label htmlFor="">{t("suppliers.isTestingDealer")}</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className={`row ${partyTypeText && partyTypeText === DEALER_PARTY_TYPE ? '' : 'hide'}`}>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">&nbsp;</label>
                          <div className="checkbox checkbox-primary">
                            <input
                              type="checkbox"
                              name="isSmith"
                              id="isSmith"
                              defaultChecked={get(formData, 'isSmith.value')}
                              onChange={(e) => setFormValue('isSmith', !formData.isSmith.value)} />
                            <label htmlFor="">{t("suppliers.isSmith")}</label>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">&nbsp;</label>
                          <div className="checkbox checkbox-primary">
                            <input
                              type="checkbox"
                              name="isBranded"
                              id="isBranded"
                              defaultChecked={get(formData, 'isBranded.value')}
                              onChange={(e) => setFormValue('isBranded', !formData.isBranded.value)} />
                            <label htmlFor="">{t("suppliers.isBranded")}</label>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">&nbsp;</label>
                          <div className="checkbox checkbox-primary">
                            <input
                              type="checkbox"
                              name="isGemStone"
                              id="isGemStone"
                              defaultChecked={get(formData, 'isGemStone.value')}
                              onChange={(e) => setFormValue('isGemStone', !formData.isGemStone.value)} />
                            <label htmlFor="">{t("suppliers.isGemStone")}</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="panel panel-default">
                    <div className="panel-heading position-relative">
                      <h3 className="panel-title">{t("common.bank")} {t("common.details")}</h3>
                      <span className="addBtn btn btn-primary btn-sm mdi mdi-plus" onClick={addBank}></span>
                    </div>
                    {bankDetails && bankDetails.map((bank, index) => {
                      return (
                        <div key={`bankDetails_${index}`} className="bg-light p-10 m-b-10 position-relative">
                          {bankDetails.length > 1 && <span className="removeBtn btn btn-danger btn-sm mdi mdi-close" onClick={() => removeBank(index)}></span>}
                          <div className="row">
                            <div className="col-md-3">
                              <div className="form-group">
                                <label htmlFor="">{t("common.account")} {t("common.number")}</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="accountNum"
                                  placeholder="accountNum"
                                  onChange={e => bankDetailsonChange(e, index)}
                                  value={bank.accountNum.value}
                                />
                                <div className="invalid-feedback">{bank.accountNum.error}</div>
                              </div>
                            </div>
                            <div className="col-md-3">
                              <div className="form-group">
                                <label htmlFor="">{t("common.accountholder")} {t("common.name")}</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="accountHolderName"
                                  placeholder="accountHolderName"
                                  onChange={e => bankDetailsonChange(e, index)}
                                  value={bank.accountHolderName.value}
                                />
                                <div className="invalid-feedback">{bank.accountHolderName.error}</div>
                              </div>
                            </div>
                            <div className="col-md-3">
                              <div className="form-group">
                                <label htmlFor="">{t("common.ifsc")} {t("common.code")}</label>
                                <input type="text"
                                  className="form-control"
                                  value={bank.ifscCode.value}
                                  onChange={(e) => onIfscChange(e.target.value, index)}
                                  style={{ textTransform: 'uppercase' }}
                                />
                                <div className="invalid-feedback">{bank.ifscCode.error}</div>
                                {!bank.ifscCode.error && ((bank.ifscCode.value.length < 11 && bank.ifscCode.value.length > 0) && <div className="invalid-feedback">IFSC code should be 11 digits</div>)}

                              </div>
                            </div>
                            <div className="col-md-3">
                              <div className="form-group">
                                <label htmlFor="">{t("creations.bankName")}</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="bankName"
                                  placeholder="bankName"
                                  onChange={e => bankDetailsonChange(e, index)}
                                  value={bank.bankName.value}
                                  disabled
                                />
                                <div className="invalid-feedback">{bank.bankName.error}</div>
                              </div>
                            </div>
                            <div className="col-md-3">
                              <div className="form-group">
                                <label htmlFor="">{t("creations.branchName")}</label>
                                <input
                                  type="text"
                                  className="form-control"
                                  name="bankBranch"
                                  placeholder="bankBranch"
                                  onChange={e => bankDetailsonChange(e, index)}
                                  value={bank.bankBranch.value}
                                  disabled
                                />
                                <div className="invalid-feedback">{bank.bankBranch.error}</div>
                              </div>
                            </div>

                          </div>
                        </div>
                      )
                    })}
                  </div>
                </div>

                <div className="panel-footer text-center">
                  <a href="#" className="btn btn-primary" onClick={saveForm}>
                    <i className="zmdi zmdi-check"></i> {t("common.save")}
                  </a>
                  <a href="#" className="btn btn-default" onClick={resetForm}>
                    <i className="zmdi zmdi-close"></i> {t("common.clear")}
                  </a>
                </div>
              </div>
            </form>
          </div>

          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">{t("adminMenus.suppliers")} {t("common.list")}</h3>
              <div className="panel-options">
                <p>
                  {selected.length > 0 ?
                    <a href="#" className="btn btn-info btn-sm" onClick={() => deleteAction(selected)} >
                      <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                    </a> :
                    null}
                </p>
              </div>
            </div>
            <div className="panel-body">
              <div className="ag-grid-wrapper">
                <Grid colDefs={supplierColumns} rowData={suppliersData} selectedRows={setSelected} actions={{ isRequired: true, editAction: editAction, deleteAction: deleteAction }} />
              </div>
            </div>
          </div>
        </div>
        </LoadingOverlay>
      </div>
    </>
  );
};

export default withTranslation()(Suppliers);
