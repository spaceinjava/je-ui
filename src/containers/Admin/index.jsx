import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import Menu from "./Menu";
import { get } from "lodash";
import Companies from "./Companies";
import Branches from "./Branches";
import Departments from "./Departments";
import Suppliers from "./Suppliers";
import Designation from "./Designation";
import Employees from "./Employees";
import CommonGrid from './../../components/Grid';
export const Admin = (props) => {
  const { t } = props;
  const pathname = get(props, "history.location.pathname");
  const [viewMenu, setMenuView]= React.useState(true);
  const renderRightSideComponent = () => {
    switch (pathname) {
      case "/_admin/companies":
        return <Companies />;
      case "/_admin/branches":
        return <Branches />;
      case "/_admin/department":
        return <Departments />;
      case "/_admin/suppliers":
        return <Suppliers />;
      case "/_admin/designation":
        return <Designation />;
      case "/_admin/employees":
        return <Employees />;
      default:
        return <div>{pathname}</div>;
    }
  };
  const onToggleMenu=()=>{
    const menuToggle= !viewMenu;
    console.log({menuToggle});
    setMenuView(menuToggle)
  }
  return (
    <div className="innerContainer">
      <div className={viewMenu ? "innerLeftNav" : "innerLeftNav clicked"}>
        <Menu pathname={pathname} onToggleMenu={onToggleMenu} />
      </div>
      <div className={viewMenu ? "innerRightCntnt" : "innerRightCntnt widthChange"}>
        <a onClick={() => { onToggleMenu() }} className="btn-collapse">
          <i className={viewMenu ? "zmdi zmdi-chevron-left" : "zmdi zmdi-chevron-right"}></i>
        </a>
        {renderRightSideComponent()}
      </div>
    </div>
  );
};

export default withTranslation()(Admin);
