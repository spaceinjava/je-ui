import React, { Component, useState } from "react";
import { withTranslation } from "react-i18next";
import Menu from "./Menu";
import { get, pick, map, cloneDeep } from "lodash";
import Grid from "./../../components/Grid";
import { companiesColumns, companiesForm } from "./Constants";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchAddress } from '../../redux/actions/addressActions'
import CircularProgress from "@material-ui/core/CircularProgress";
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
//import { ThingsProvider } from '../../components/Context/UserContext';

import $ from 'jquery';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

function sleep(delay = 0) {
  return new Promise((resolve) => {
    setTimeout(resolve, delay);
  });
}

export const Companies = (props) => {
  const { t } = props;
  const things = [
    { id: 1, name: "thing 1", length: 5 },
    { id: 2, name: "thing 2", length: 2 },
    { id: 3, name: "thing 3", length: 6 },
    { id: 4, name: "thing 4", length: 10 },
    { id: 5, name: "thing 5", length: 1 },
  ];
  const [formData, setFormData] = useState(cloneDeep(companiesForm));
  const [viewForm, setToggleView] = useState(true);
  const [pinCode, setPincode] = useState([])
  const [open, setOpen] = React.useState(false);
  const [options, setOptions] = React.useState([]);
  const [bankDetails, setBankDetails] = useState([{
    ifsccode: { value: '', required: true, type: 'number', min: 1, max: 20, isValid: false, error: null },
    bankName: { value: '', required: true, type: 'text', min: 1, max: 20, isValid: false, error: null },
    branch: { value: '', required: true, type: 'text', min: 1, max: 20, isValid: false, error: null },
    accountNumber: { value: '', required: true, type: 'number', min: 1, max: 6, isValid: false, error: null },
  }])
  const loading = open && options.length === 0;

  const dispatch = useDispatch();
  let Addresses = useSelector(state => state.addressReducer.allAddresses);
  const loader = useSelector(state => state.addressReducer.loading);

  

  useEffect(() => {
    setFormData(formData);
  }, [formData]);

  useEffect(() => {
    if (Addresses && Addresses.length > 0) {
      if (loader) {
        console.log('loading..')
      } else {
        setOptions(Addresses)
      }
    }

  })


  const setFormValue = (field, value) => {
    formData[field]['value'] = value;
    formData[field]['error'] = null;
    setFormData({ ...formData });
    //validateForm();
  }
  const resetForm = () => {
    map(formData, function (value, key, object) {
      if (key === 'logo') {
        document.getElementById("logo").value = "";
      }
      if (key === 'isBranch') {
        document.getElementById("isBranch").checked = false;
        setFormValue('isBranch', false)
      }
      else {
        formData[key]['value'] = '';
      }
    });

    //add bank panel
    $("#addBank").onClick(function () {
      $(".bankPanel").show();
      $(".removeBtn").show();
    });

    console.log({ formData })
    setFormData({ ...formData });
    //document.getElementById("companyDetailsForm").reset();
  }

  const validateForm = () => {
    console.log({ formData });
    console.log(bankDetails)
    map(bankDetails[bankDetails.length - 1], function (value, key, object) {
      console.log({ value, key, object })
      const isValid = validateBank(value);
      const list = [...bankDetails];
      setBankDetails(list)
    })
    let isFormValid = true;
    map(formData, function (value, key, object) {
      console.log({ value, key, object })
      const isValid = validateField(value);
      if (!isValid) {
        isFormValid = false;
      }
      console.log({ isValid })
      formData[key] = { ...value, ...isValid }
    });
    setFormValue('BankList', bankDetails)
    if (isFormValid)
      console.log("is valid form ::::: ", { formData });
  }

  const validateField = (fieldObj) => {
    //console.log({fieldObj})
    let errorObj = { isValid: true, error: null }
    if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
    else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length${fieldObj.min}` }
    else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length${fieldObj.max}` }

    return errorObj;
  }
  const rowData = [
    {
      company: "joyAlukas",
      addressLineOne: "jubilee hills",
      email: "info@jy.com",
      state: "India"
    },
    {
      company: "josAlukas",
      addressLineOne: "Banjara hills",
      email: "info@js.com",
    },
    {
      company: "GRT",
      addressLineOne: "jubilee hills",
      email: "info@grt.com",
    },
  ];
  const selectedRows = (params) => {
    console.log({ params })
    if (params && params.length == 1) {
      map(params[0], function (value, key, object) {
        console.log("key ============ ", key);
        console.log("value =========== ", value);
        console.log("object =========== ", object);
        formData[key]['value'] = value
      });
      setFormData({ ...formData })
    } else {
      resetForm();
    }
  }


  console.log({ formData });
  const toggleView = () => {
    const formNode = document.getElementById('companyDetailsFormWrapper');
    formNode.classList.toggle('collapse-panel');
    setToggleView(!viewForm);
  }
  $(function () {
    var availableTags = [
      "ActionScript",
      "AppleScript",
      "Asp",
      "BASIC",
      "C",
      "C++",
      "Clojure",
      "COBOL",
      "ColdFusion",
      "Erlang",
      "Fortran",
      "Groovy",
      "Haskell",
      "Java",
      "JavaScript",
      "Lisp",
      "Perl",
      "PHP",
      "Python",
      "Ruby",
      "Scala",
      "Scheme"
    ];
  });

  const onChangeHandle = async value => {
    if (value.length === 6) {
      dispatch(fetchAddress(value))
    }
    else {
      setOptions([]);
    }
  };

  const getOptionSelected = (option, value) => {
    if (option.Name === value.Name) {
      setFormValue('state', option.Circle)
      setFormValue('district', option.District)
      setFormValue('cityTown', option.Division)
      setFormValue('areaVillage', option.Name)
    }
  }

  React.useEffect(() => {
    if (!open) {
      setOptions([]);
    }
  }, [open]);


  // Top 100 films as rated by IMDb users. http://www.imdb.com/chart/top
  const top100Films = [
    { title: 'The Shawshank Redemption', year: 1994 },
    { title: 'The Godfather', year: 1972 },
    { title: 'The Godfather: Part II', year: 1974 },
    { title: 'The Dark Knight', year: 2008 },
    { title: '12 Angry Men', year: 1957 },
    { title: "Schindler's List", year: 1993 },
    { title: 'Pulp Fiction', year: 1994 },
    { title: 'The Lord of the Rings: The Return of the King', year: 2003 },
    { title: 'The Good, the Bad and the Ugly', year: 1966 },
    { title: 'Fight Club', year: 1999 },
    { title: 'The Lord of the Rings: The Fellowship of the Ring', year: 2001 },
    { title: 'Star Wars: Episode V - The Empire Strikes Back', year: 1980 },
    { title: 'Forrest Gump', year: 1994 },
  ];

  const editAction = (ele) => {
    console.log("edit action in companies ...!!!! ", ele)
  }

  const deleteAction = (ele) => {
    console.log("delete action in companies ...!#### ", ele)
  }

  const bankDetailsonChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...bankDetails];
    list[index][name]['value'] = value;
    list[index][name]['error'] = null;
    setBankDetails(list)
  }

  const addBank = () => {
    console.log(bankDetails)
    let isFormValid = true;
    map(bankDetails[bankDetails.length - 1], function (value, key, object) {
      console.log({ value, key, object })
      const isValid = validateBank(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      console.log({ isValid })
      console.log(isFormValid)
      // bankDetails[key] = { ...value, ...isValid }
      const list = [...bankDetails];
      setBankDetails(list)
    })
    if (isFormValid) {
      setBankDetails(
        [...bankDetails, {
          ifsccode: { value: '', required: true, type: 'number', min: 1, max: 20, isValid: false, error: null },
          bankName: { value: '', required: true, type: 'text', min: 1, max: 20, isValid: false, error: null },
          branch: { value: '', required: true, type: 'text', min: 1, max: 20, isValid: false, error: null },
          accountNumber: { value: '', required: true, type: 'number', min: 1, max: 6, isValid: false, error: null },
        }]
      )
    }
  }

  const validateBank = (fieldObj) => {
    console.log({ fieldObj })
    let errorObj = { isValid: true, error: null }
    fieldObj.isValid = true;
    if (fieldObj.required && !fieldObj.value) { fieldObj.isValid = false; fieldObj.error = 'Please fill this field' }
    else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { fieldObj.isValid = false; fieldObj.error = `Please enter min length${fieldObj.min}` }
    else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { fieldObj.isValid = false; fieldObj.error = `Please enter max length${fieldObj.max}` }

    return fieldObj;
  }





  const removeBank = (index) => {
    const list = [...bankDetails];
    list.splice(index, 1);
    setBankDetails(list)
  }

  return (
    <>
      <div className="panel panel-default hero-panel">
        <div className="panel-heading">
          <h1 className="panel-title">{t("adminMenus.companies")} </h1>
          <div className="panel-options">
            <a
              href="#"
              className="text-info collapseBtn"
              title="Show Add Form"
            >
              <i className="zmdi zmdi-caret-up-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                {/* {viewForm ?  <ExpandLessIcon color="primary" style={{ fontSize: 40 }}/> : <ExpandMoreIcon color="primary" style={{ fontSize: 40 }} />} */}
              </i>
            </a>
          </div>
        </div>
        <div className="panel-body">
          <div className="panel update-panel form-pnel-container" id="companyDetailsFormWrapper">
            <form id="companyDetailsForm">
              <div className="panel-body  p-t-0">
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h3 className="panel-title">
                      {t("common.company")} {t("common.details")}
                    </h3>
                  </div>
                  <div className="row">
                    <div className="col-md-2">
                      <div className="form-group">
                        <label htmlFor="">Company Name*</label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder=""
                          name="company"
                          value={get(formData, 'company.value')}
                          onChange={(e) => setFormValue('company', e.target.value)}
                        />
                        <div className="invalid-feedback small">{get(formData, 'company.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-2">
                      <div className="form-group">
                        <label htmlFor="">Short Code*</label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder=""
                          name="companyShortName"
                          value={get(formData, 'companyShortName.value')}
                          onChange={(e) => setFormValue('companyShortName', e.target.value)}
                        />
                        <div className="invalid-feedback">{get(formData, 'companyShortName.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-2">
                      <div className="form-group">
                        <label htmlFor="">Email ID</label>
                        <input
                          type="mail"
                          className="form-control"
                          placeholder=""
                          name="email"
                          value={get(formData, 'email.value')}
                          onChange={(e) => setFormValue('email', e.target.value)}
                        />
                        <div className="invalid-feedback">{get(formData, 'email.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">Phone Number</label>
                        <input
                          type="number"
                          className="form-control"
                          placeholder=""
                          name="phoneNumber"
                          value={get(formData, 'phoneNumber.value')}
                          onChange={(e) => setFormValue('phoneNumber', e.target.value)}
                        />
                        <div className="invalid-feedback">{get(formData, 'phoneNumber.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">Alternative Phone Number</label>
                        <input
                          type="number"
                          className="form-control"
                          placeholder=""
                          name="alternativePhoneNumber"
                          value={get(formData, 'alternativePhoneNumber.value')}
                          onChange={(e) => setFormValue('alternativePhoneNumber', e.target.value)}
                        />
                        <div className="invalid-feedback">{get(formData, 'alternativePhoneNumber.error')}</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h3 className="panel-title">{t("common.address")}</h3>
                  </div>
                  <div className="row">
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">Pincode</label>
                        <Autocomplete
                          id="asynchronous-demo"
                          open={open}
                          onOpen={() => {
                            setOpen(true);
                          }}
                          onClose={() => {
                            setOpen(false);
                          }}
                          options={options}
                          filterOptions={(x) => x}
                          autoComplete
                          includeInputInList
                          filterSelectedOptions
                          getOptionSelected={(option, value) => getOptionSelected(option, value)}
                          getOptionLabel={option => option.Pincode}
                          loading={loading}
                          renderOption={(option) => <span>{option.Name}, {option.Division}, {option.District},  {option.Circle} , {option.Pincode}</span>}
                          renderInput={params => {
                            const inputProps = params.inputProps;
                            inputProps.autoComplete = 'new-password';
                            return (
                              <TextField
                                {...params}
                                onChange={ev => {
                                  if (ev.target.value !== "" || ev.target.value !== null) {
                                    onChangeHandle(ev.target.value);
                                  }
                                }}
                                InputProps={{
                                  ...params.InputProps,
                                  endAdornment: (
                                    <React.Fragment>
                                      {loading ? (
                                        <CircularProgress color="inherit" size={20} />
                                      ) : null}
                                      {params.InputProps.endAdornment}
                                    </React.Fragment>
                                  )
                                }}
                              />
                            );
                          }
                          }
                        />
                        <div className="invalid-feedback">{get(formData, 'pincode.error')}</div>
                      </div>

                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">State*</label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder=""
                          name="state"
                          value={get(formData, 'state.value')}
                          disabled
                        />
                        <div className="invalid-feedback">{get(formData, 'state.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">District*</label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder=""
                          name="district"
                          value={get(formData, 'district.value')}
                          disabled
                        />
                        <div className="invalid-feedback">{get(formData, 'district.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">City/Town*</label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder=""
                          name="cityTown"
                          value={get(formData, 'cityTown.value')}
                          disabled
                        />
                        <div className="invalid-feedback">{get(formData, 'cityTown.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">Area/Village*</label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder=""
                          name="areaVillage"
                          value={get(formData, 'areaVillage.value')}
                          disabled
                        />
                        <div className="invalid-feedback">{get(formData, 'areaVillage.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">Line 1</label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder=""
                          name="addressLineOne"
                          value={get(formData, 'addressLineOne.value')}
                          onChange={(e) => setFormValue('addressLineOne', e.target.value)}
                        />
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">Line 2</label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder=""
                          name="addressLineTwo"
                          value={get(formData, 'addressLineTwo.value')}
                          onChange={(e) => setFormValue('addressLineTwo', e.target.value)}
                        />
                      </div>
                    </div>

                  </div>
                </div>

                {bankDetails && bankDetails.map((bank, index) => {
                  { console.log(bank) }
                  return (
                    <div className="panel panel-default" key={index}>
                      <div className="panel-heading">
                        {index === 0 && <h3 className="panel-title">{t("common.bank")} {t("common.details")}</h3>}
                      </div>

                      <div className="p-3 bg-light position-relative">
                        <a href="" className="removeBtn btn btn-danger btn-sm mdi mdi-close hide"></a>
                        <div className="row">
                          <div className="col-md-3">
                            <div className="form-group">
                              <label htmlFor="">IFSC Code</label>
                              <input
                                type="text"
                                className="form-control"
                                name="ifsccode"
                                placeholder=""
                                onChange={e => bankDetailsonChange(e, index)}
                                value={bank.ifsccode.value}
                              />
                              <div className="invalid-feedback">{bank.ifsccode.error}</div>
                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="form-group">
                              <label htmlFor="">Bank Name*</label>
                              <select className="form-control select" name="bankName"
                                onChange={e => bankDetailsonChange(e, index)}
                                value={bank.bankName.value} >
                                <option></option>
                                <option>India</option>
                                <option>America</option>
                                <option>UK </option>
                                <option>USA</option>
                                <option>Finland</option>
                                <option>Denmark </option>
                                <option>Australia</option>
                                <option>Newzeland</option>
                              </select>
                              <div className="invalid-feedback">{bank.bankName.error}</div>
                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="form-group">
                              <label htmlFor="">Branch*</label>
                              <select className="form-control select" name="branch"
                                onChange={e => bankDetailsonChange(e, index)}
                                value={bank.branch.value} >
                                <option></option>
                                <option>India</option>
                                <option>America</option>
                              </select>
                              <div className="invalid-feedback">{bank.branch.error}</div>
                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="form-group">
                              <label htmlFor="">A/C Number</label>
                              <input
                                type="text"
                                className="form-control"
                                name="accountNumber"
                                placeholder=""
                                onChange={e => bankDetailsonChange(e, index)}
                                value={bank.accountNumber.value}
                              />
                              <div className="invalid-feedback">{bank.accountNumber.error}</div>
                            </div>
                          </div>
                        </div>
                      </div>
                      {bankDetails.length !== 1 &&
                        <div className="text-right mt-2">
                          <div className="btn btn-sm btn-info panel-options" id="addBank" alt="" onClick={() => removeBank(index)} >Remove Bank</div>
                        </div>}
                      {bankDetails.length - 1 === index &&
                        <div className="text-right mt-2">
                          <div className="btn btn-sm btn-info panel-options" id="addBank" alt="" onClick={addBank}>Add Bank</div>
                        </div>
                      }

                    </div>
                  )
                })}

                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h3 className="panel-title">{t("common.others")} </h3>
                  </div>
                  <div className="panel-body">
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">GST No</label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder=""
                          name="gstNumber"
                          value={get(formData, 'gstNumber.value')}
                          onChange={(e) => setFormValue('gstNumber', e.target.value)}
                        />
                        <div className="invalid-feedback">{get(formData, 'gstNumber.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="">Logo Upload</label>
                        <input
                          type="file"
                          className="form-control"
                          placeholder=""
                          name="logo"
                          id="logo"
                          value={get(formData, 'logo.value.file')}
                          onChange={(e) => setFormValue('logo', e.target.files[0])}
                        />
                        <div className="invalid-feedback">{get(formData, 'logo.error')}</div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor=""></label>
                        <div className="checkbox checkbox-primary">
                          <input
                            type="checkbox"
                            name="isBranch"
                            id="isBranch"
                            defaultChecked={get(formData, 'isBranch.value')}
                            onChange={(e) => setFormValue('isBranch', !formData.isBranch.value)} />
                          <label htmlFor="Gender_M">is Branch</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="panel-footer text-center">
                  <a href="#" className="btn btn-primary" onClick={() => { validateForm() }}>
                    <i className="zmdi zmdi-check"></i> Save
                  </a>
                  <a href="#" className="btn btn-primary">
                    <i className="zmdi zmdi-refresh"></i> Update
                  </a>
                  <a href="#" className="btn btn-default" onClick={() => { resetForm() }}>
                    <i className="zmdi zmdi-close"></i> Clear
                  </a>
                </div>
              </div>
            </form>
          </div>

          <div className="panel panel-default">
            <div className="panel-heading hidden">
              <h3 className="panel-title">Company List </h3>
            </div>
            <div className="panel-body">
              <p>
                <a href="#" className="btn btn-default">
                  <i className="zmdi zmdi-delete"></i> Delete
                </a>
              </p>

              <div className="ag-grid-wrapper">
                <Grid colDefs={companiesColumns} rowData={rowData} selectedRows={selectedRows} actions={{ isRequired: true, editAction: editAction, deleteAction: deleteAction }} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default withTranslation()(Companies);
