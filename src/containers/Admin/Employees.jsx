import React, { useState, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { get, map, cloneDeep } from "lodash";
import Grid from "./../../components/Grid";
import { employeeColumns, employeesForm } from "./Constants";
import { useDispatch, useSelector } from "react-redux";
import { fetchAddress } from '../../redux/actions/addressActions'
import { fetchBanks } from '../../redux/actions/bankActions'
import { fetchBranches } from '../../redux/actions/Admin/branchActions'
import { fetchDepartments } from '../../redux/actions/departmentActions'
import { fetchDesignations } from "../../redux/actions/designationActions";
import { saveEmployee, fetchEmployees, deleteEmployees } from '../../redux/actions/Admin/employeeActions'
import CircularProgress from "@material-ui/core/CircularProgress";
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Select from "../../components/Form/Select";
import Swal from 'sweetalert2'
import LoadingOverlay from 'react-loading-overlay';

export const Employees = (props) => {
  const { t } = props;
  const [formData, setFormData] = useState(cloneDeep(employeesForm));
  const [viewForm, setToggleView] = useState(true);
  let [selected, setSelected] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [options, setOptions] = React.useState([]);
  const [image, setImage] = useState({
    preview: "",
    raw: ""
  })
  const bankDetails = useSelector(state => state.bankReducer.allBanks)

  useEffect(() => {
    if (bankDetails) {
      setFormValue('bankName', bankDetails.bank)
      setFormValue('bankBranch', bankDetails.branch)
    }
  }, [bankDetails])

  useEffect(() => {
    dispatch(fetchBranches());
    dispatch(fetchDepartments());
    dispatch(fetchDesignations());
    dispatch(fetchEmployees())
  }, [])



  const loading = open && options.length === 0;

  const dispatch = useDispatch();
  const branches = useSelector(state => state.branchReducer.allBranches)
  const departments = useSelector(state => state.departmentReducer.allDepartments)
  const designations = useSelector(state => state.designationReducer.allDesignations)
  const employees = useSelector(state => state.employeeReducer.allEmployees)
  const employeeLoader = useSelector(state => state.employeeReducer.loading);
  let Addresses = useSelector(state => state.addressReducer.allAddresses);
  const loader = useSelector(state => state.addressReducer.loading);

  const employeesData = employees.data ? employees.data.map((employee) => {
    return {
      id: employee.id,
      employeeId: employee.employeeId,
      firstName: employee.firstName,
      middleName: employee.middleName,
      lastName: employee.lastName,
      gender: employee.gender,
      branchId: employee.branch.id,
      branchName: employee.branch.branchName,
      departmentId: employee.designation.department.id,
      department: employee.designation.department.departmentName,
      designationId: employee.designation.id,
      designation: employee.designation.designationName,
      email: employee.email,
      mobile: employee.mobile,
      dob: employee.dob,
      emergencyContactNum: employee.emergencyContactNum,
      aadharNum: employee.aadharNum,
      panNum: employee.panNum,
      addressId: employee.address.id,
      pincode: employee.address.pincode,
      state: employee.address.state,
      district: employee.address.district,
      city: employee.address.city,
      area: employee.address.area,
      addressLine1: employee.address.addressLine1,
      addressLine2: employee.address.addressLine2,
      loginEnabled: employee.loginEnabled,
      bankDetailsId: employee.bankDetails.id,
      accountHolderName: employee.bankDetails.accountHolderName,
      accountNumber: employee.bankDetails.accountNum,
      salary: employee.salary,
      ifscCode: employee.bankDetails.ifscCode,
      bankName: employee.bankDetails.bankName,
      bankBranch: employee.bankDetails.bankBranch,
      generateId: employee.generateId,
      photoUrl: employee.photoUrl
    }
  }) : [];

  const branchData = branches.data ? branches.data.map((branch) => {
    return {
      id: branch.id,
      name: branch.branchName,
      shortCode: branch.shortCode,
    }
  }) : [];

  const departmentData = departments.data ? departments.data.map((department) => {
    return {
      id: department.id,
      name: department.departmentName,
      shortCode: department.shortCode,
    }
  }) : [];

  const designationData = designations.data ? designations.data.map((designation) => {
    return {
      id: designation.id,
      name: designation.designationName,
      shortCode: designation.shortCode,
    }
  }) : [];

  const resetForm = () => {
    map(formData, function (value, key, object) {
      if (key === 'logo') {
        document.getElementById("logo").value = "";
      }
      if (key === 'isBranch') {
        document.getElementById("isBranch").checked = false;
        setFormValue('isBranch', false)
      }
      else {
        formData[key]['value'] = '';
      }
    });



    console.log({ formData })
    setFormData({ ...formData });
    //document.getElementById("companyDetailsForm").reset();
  }

  const setFormValue = (field, value) => {
    formData[field]['value'] = value;
    setFormData({ ...formData });
  }

  const saveForm = () => {
    let isFormValid = true;
    map(formData, function (value, key, object) {
      const isValid = validateField(value);
      if (!isValid.isValid) {
        isFormValid = false;
      }
      formData[key] = { ...value, ...isValid }
    });
    if (isFormValid) {
      let data = {};
      map(formData, function (value, key, object) {
        data = {
          ...data,
          [key]: value.value
        }
      });
      data.pic = image.raw;
      Swal.fire({
        title: 'Confirm Saving Employee Info.!',
        text: 'Continue to save / Cancel to stop',
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Continue'
      }).then((result) => {
        if (result.isConfirmed) {
          console.log(data)
          dispatch(saveEmployee(data))
            .then(() => {
              resetForm()
            })
        }
      })
    }
  }

  const validateField = (fieldObj) => {
    let errorObj = { isValid: true, error: null }
    if (fieldObj.required && !fieldObj.value) { errorObj.isValid = false; errorObj.error = 'Please fill this field' }
    else if (fieldObj.value && fieldObj.value.length < fieldObj.min) { errorObj.isValid = false; errorObj.error = `Please enter min length ${fieldObj.min}` }
    else if (fieldObj.value && fieldObj.value.length > fieldObj.max) { errorObj.isValid = false; errorObj.error = `Please enter max length ${fieldObj.max}` }
    return errorObj;
  }

  const rowData = [
    {
      company: "joyAlukas",
      addressLineOne: "jubilee hills",
      email: "info@jy.com",
      state: "India"
    },
    {
      company: "josAlukas",
      addressLineOne: "Banjara hills",
      email: "info@js.com",
    },
    {
      company: "GRT",
      addressLineOne: "jubilee hills",
      email: "info@grt.com",
    },
  ];
  const selectedRows = (params) => {
    console.log({ params })
    if (params && params.length == 1) {
      map(params[0], function (value, key, object) {
        console.log("key ============ ", key);
        console.log("value =========== ", value);
        console.log("object =========== ", object);
        formData[key]['value'] = value
      });
      setFormData({ ...formData })
    } else {
      resetForm();
    }
  }

  useEffect(() => {
    setFormData(formData);
  }, [formData]);

  useEffect(() => {
    if (Addresses && Addresses.length > 0) {
      if (loader) {
        console.log('loading..')
      } else {
        setOptions(Addresses)
      }
    }
  })


  console.log({ formData });
  const toggleView = () => {
    const formNode = document.getElementById('form');
    formNode.classList.toggle('collapse-panel');
    setToggleView(!viewForm);
  }


  const onChangeHandle = async value => {
    console.log(value)
    if (value.length === 6) {
      setFormValue('pincode', value)
      dispatch(fetchAddress(value))
    }
    else {
      setOptions([]);
    }
  };

  const onIfscChange = value => {
    if (value.length <= 11) {
      setFormValue('ifscCode', value)
    }
    if (value.length === 11) {
      dispatch(fetchBanks(value.toLocaleUpperCase()))
    }
    if (value.length < 11) {
      setFormValue('bankName', '')
      setFormValue('bankBranch', '')
    }
  }

  const getOptionSelected = (option) => {
    if (option && option.pincode.length === 6) {
      setFormValue('state', option.state)
      setFormValue('district', option.district)
      setFormValue('city', option.mandal)
      setFormValue('area', option.village)
    }
  }



  const editAction = (ele) => {
    console.log("edit action in companies ...!!!! ", ele)
    setFormValue('id', ele.id)
    setFormValue('employeeId', ele.employeeId)
    setFormValue('photoUrl', ele.photoUrl)
    setImage({
      preview: ele.photoUrl,
    })
    setFormValue('branchName', ele.branchId)
    setFormValue('designation', ele.designationId)
    setFormValue('department', ele.departmentId)
    setFormValue('firstName', ele.firstName)
    setFormValue('middleName', ele.middleName)
    setFormValue('lastName', ele.lastName)
    setFormValue('dob', ele.dob)
    setFormValue('gender', ele.gender)
    document.getElementById("loginEnabled").checked = ele.loginEnabled;
    setFormValue('loginEnabled', ele.loginEnabled)
    setFormValue('contactNumber', ele.mobile)
    setFormValue('emgContactNumber', ele.emergencyContactNum)
    setFormValue('email', ele.email)
    setFormValue('aadharNo', ele.aadharNum)
    setFormValue('panNumber', ele.panNum)
    setFormValue('addressLine1', ele.addressLine1)
    setFormValue('addressLine2', ele.addressLine2)
    setFormValue('pincode', ele.pincode)
    setFormValue('area', ele.area)
    setFormValue('city', ele.city)
    setFormValue('district', ele.district)
    setFormValue('state', ele.state)
    setFormValue('addressId', ele.addressId)
    setFormValue('bankDetailsId', ele.bankDetailsId)
    setFormValue('accountHolderName', ele.accountHolderName)
    setFormValue('accountNumber', ele.accountNumber)
    setFormValue('salary', ele.salary)
    setFormValue('ifscCode', ele.ifscCode)
    setFormValue('bankName', ele.bankName)
    setFormValue('bankBranch', ele.bankBranch)
    document.getElementById("generateId").checked = false;
    // setFormValue('generateId', ele.generateId)
  }

  const deleteAction = (data) => {
    let selectedIds;
    if (data instanceof Array && data.length > 0) {
      selectedIds = data.map((ele) => {
        return ele.id
      })
    } else {
      selectedIds = data.id;
    }
    Swal.fire({
      title: 'Confirm Deleting Employeee.!',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deleteEmployees(selectedIds.toString()))
          .then(() => {
            selected = []
            setSelected(selected);
          })
      }
    })
  }

  const handleChange = (e) => {
    if (e.target.files.length) {
      setImage({
        preview: URL.createObjectURL(e.target.files[0]),
        raw: e.target.files[0]
      })
    }
  }

  return (
    <>
      <div className="panel panel-default hero-panel">
        <LoadingOverlay
          active={employeeLoader}
          spinner
          text='Loading...'
        >
          <div className="panel-heading">
            <h1 className="panel-title">{t("adminMenus.employees")} </h1>
            <div className="panel-options">
              <a
                href="#"
                className="text-info collapseBtn"
                title="Show Add Form"
              >
                {viewForm ?
                  <i className="zmdi zmdi-minus-circle  zmdi-hc-2x" onClick={() => { toggleView() }}>
                  </i> :
                  <i className="zmdi zmdi-plus-circle zmdi-hc-2x" onClick={() => { toggleView() }}>
                  </i>}
              </a>
            </div>
          </div>
          <div className="panel-body">
            <div className="panel update-panel form-pnel-container" id="form">
              <form id="companyDetailsForm">
                <div className="panel-body  p-t-0">
                  <div className="panel panel-default">
                    <div className="panel-heading">
                      <h3 className="panel-title">
                        {t("employees.personalDetails")}
                      </h3>
                    </div>
                    <div className="panel-body">

                      <div className="col-md-3">
                        <label htmlFor="upload-button">
                          {image.preview ?
                            <img src={image.preview} alt="profile pic" style={{ width: '128px', height: '128px', borderRadius: '50%' }} /> :
                            <div className="circle m-t-20">
                              <i className="zmdi zmdi-camera zmdi-hc-2x"></i>
                            </div>
                          }


                        </label>
                        <input
                          type="file"
                          id="upload-button"
                          style={{ display: 'none' }}
                          onChange={handleChange} />
                        {/* <div className="invalid-feedback">Upload Employee Profile Pic</div> */}
                      </div>


                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">Branch Name<span className="asterisk">*</span></label>
                          <Select name="branchName" value={get(formData, 'branchName.value')} onChange={(e) => setFormValue('branchName', e.target.value)} data={branchData} />
                          <div className="invalid-feedback">{get(formData, 'branchName.error')}</div>
                        </div>
                      </div>

                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">Department<span className="asterisk">*</span></label>
                          <Select name="department" value={get(formData, 'department.value')} onChange={(e) => setFormValue('department', e.target.value)} data={departmentData} />
                          <div className="invalid-feedback">{get(formData, 'department.error')}</div>
                        </div>
                      </div>

                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">Designation<span className="asterisk">*</span></label>
                          <Select name="designation" value={get(formData, 'designation.value')} onChange={(e) => setFormValue('designation', e.target.value)} data={designationData} />
                          <div className="invalid-feedback">{get(formData, 'designation.error')}</div>
                        </div>
                      </div>

                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">&nbsp;</label>
                          <div className="checkbox checkbox-primary">
                            <input type="checkbox"
                              name="generateId"
                              id="generateId"
                              defaultChecked={get(formData, 'generateId.value')}
                              onChange={(e) => setFormValue('generateId', !formData.generateId.value)}
                              disabled={get(formData, 'employeeId.value')} />
                            <label htmlFor="Checkbox1">Generate Employee ID </label>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">Employee ID<span className="asterisk">*</span></label>
                          <input name="employeeId"
                            type="text"
                            value={get(formData, 'employeeId.value')}
                            onChange={(e) => setFormValue('employeeId', e.target.value)}
                            className="form-control"
                            placeholder=""
                            disabled={get(formData, 'generateId.value')}
                          />
                          <div className="invalid-feedback">{get(formData, 'employeeId.error')}</div>
                        </div>
                      </div>

                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">First Name<span className="asterisk">*</span></label>
                          <input type="text"
                            className="form-control"
                            value={get(formData, 'firstName.value')}
                            onChange={(e) => setFormValue('firstName', e.target.value)} />
                          <div className="invalid-feedback">{get(formData, 'firstName.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">Middle Name</label>
                          <input type="text"
                            className="form-control"
                            value={get(formData, 'middleName.value')}
                            onChange={(e) => setFormValue('middleName', e.target.value)} />
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">Last Name<span className="asterisk">*</span></label>
                          <input type="text"
                            className="form-control"
                            placeholder=""
                            value={get(formData, 'lastName.value')}
                            onChange={(e) => setFormValue('lastName', e.target.value)} />
                          <div className="invalid-feedback">{get(formData, 'lastName.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">Date of Birth<span className="asterisk">*</span></label>
                          <input type="date"
                            className="form-control"
                            placeholder=""
                            value={get(formData, 'dob.value')}
                            onChange={(e) => setFormValue('dob', e.target.value)} />
                          <div className="invalid-feedback">{get(formData, 'dob.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3"></div>

                      <div className="col-md-3">
                        <div className="form-group">
                          <label className="control-label">Gender<span className="asterisk">*</span></label>
                          <div className="radio radio-inline radio-primary">
                            <input
                              type="radio"
                              name="Gender"
                              value="Male"
                              checked={get(formData, 'gender.value') === "Male"}
                              onChange={(e) => setFormValue('gender', "Male")} />
                            <label htmlFor="Gender_M">Male</label>
                          </div>
                          <div className="radio radio-inline radio-primary">
                            <input
                              type="radio"
                              name="Gender"
                              value="Female"
                              checked={get(formData, 'gender.value') === "Female"}
                              onChange={(e) => setFormValue('gender', "Female")} />
                            <label htmlFor="Gender_F">Female</label>
                          </div>
                          <div className="invalid-feedback">{get(formData, 'gender.error')}</div>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label htmlFor="">Login Enable/Disable</label>
                          <label className="switch">
                            <input type="checkbox"
                              name="loginEnabled"
                              id="loginEnabled"
                              defaultChecked={get(formData, 'loginEnabled.value')}
                              onChange={(e) => setFormValue('loginEnabled', !formData.loginEnabled.value)}
                            />
                            <span className="slider round"></span>
                          </label>
                        </div>
                      </div>
                    </div>

                    <div className="panel panel-default">
                      <div className="panel-heading">
                        <h3 className="panel-title">Contact Details </h3>
                      </div>
                      <div className="panel-body">
                        <div className="col-md-3">
                          <div className="form-group">
                            <label htmlFor="">Contact Number<span className="asterisk">*</span></label>
                            <input type="text"
                              className="form-control"
                              value={get(formData, 'contactNumber.value')}
                              onChange={(e) => setFormValue('contactNumber', e.target.value)} />
                            <div className="invalid-feedback">{get(formData, 'contactNumber.error')}</div>
                          </div>
                        </div>
                        <div className="col-md-3">
                          <div className="form-group">
                            <label htmlFor="">Emg. Contact No.<span className="asterisk">*</span></label>
                            <input type="text"
                              className="form-control"
                              value={get(formData, 'emgContactNumber.value')}
                              onChange={(e) => setFormValue('emgContactNumber', e.target.value)} />
                            <div className="invalid-feedback">{get(formData, 'emgContactNumber.error')}</div>
                          </div>
                        </div>
                        <div className="col-md-3">
                          <div className="form-group">
                            <label htmlFor="">Email ID</label>
                            <input type="text"
                              className="form-control"
                              value={get(formData, 'email.value')}
                              onChange={(e) => setFormValue('email', e.target.value)} />
                          </div>
                        </div>
                        <div className="col-md-3">
                          <div className="form-group">
                            <label htmlFor="">Aadhar No<span className="asterisk">*</span></label>
                            <input type="text"
                              className="form-control"
                              value={get(formData, 'aadharNo.value')}
                              onChange={(e) => setFormValue('aadharNo', e.target.value)} />
                            <div className="invalid-feedback">{get(formData, 'aadharNo.error')}</div>
                          </div>
                        </div>
                        <div className="col-md-3">
                          <div className="form-group">
                            <label htmlFor="">Pan No<span className="asterisk">*</span></label>
                            <input type="text"
                              className="form-control"
                              value={get(formData, 'panNumber.value')}
                              onChange={(e) => setFormValue('panNumber', e.target.value)} />
                            <div className="invalid-feedback">{get(formData, 'panNumber.error')}</div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="panel panel-default">
                      <div className="panel-heading">
                        <h3 className="panel-title">{t("common.address")}</h3>
                      </div>
                      <div className="panel-body">
                        <div className="col-md-3">
                          <div className="form-group">
                            <label htmlFor="">Pincode<span className="asterisk">*</span></label>
                            <Autocomplete
                              id="pincode"
                              open={open}

                              onOpen={() => {
                                setOpen(true);
                              }}
                              onClose={() => {
                                setOpen(false);
                              }}
                              options={options}
                              filterOptions={(x) => x}
                              autoComplete
                              includeInputInList
                              filterSelectedOptions
                              onChange={(event, value) => getOptionSelected(value)}
                              getOptionLabel={option => option.pincode ? option.pincode : get(formData, 'pincode.value')}
                              value={get(formData, 'pincode.value')}
                              loading={loading}
                              renderOption={(option) => <span>{option.village}, {option.mandal}, {option.district},  {option.state} , {option.pincode}</span>}
                              renderInput={params => {
                                const inputProps = params.inputProps;
                                inputProps.autoComplete = 'new-password';
                                return (
                                  <TextField

                                    {...params}
                                    onChange={ev => {
                                      if (ev.target.value !== "" || ev.target.value !== null) {
                                        onChangeHandle(ev.target.value);
                                      }
                                    }}
                                    InputProps={{
                                      ...params.InputProps,
                                      endAdornment: (
                                        <React.Fragment>
                                          {loading ? (
                                            <CircularProgress color="inherit" size={20} />
                                          ) : null}
                                          {params.InputProps.endAdornment}
                                        </React.Fragment>
                                      )
                                    }}

                                  />
                                );
                              }
                              }
                            />
                            <div className="invalid-feedback">{get(formData, 'pincode.error')}</div>
                          </div>

                        </div>
                        <div className="col-md-3">
                          <div className="form-group">
                            <label htmlFor="">State<span className="asterisk">*</span></label>
                            <input
                              type="text"
                              className="form-control"
                              placeholder="state"
                              name="state"
                              value={get(formData, 'state.value')}
                              disabled
                            />
                            <div className="invalid-feedback">{get(formData, 'state.error')}</div>
                          </div>
                        </div>
                        <div className="col-md-3">
                          <div className="form-group">
                            <label htmlFor="">District<span className="asterisk">*</span></label>
                            <input
                              type="text"
                              className="form-control"
                              placeholder="district"
                              name="district"
                              value={get(formData, 'district.value')}
                              disabled
                            />
                            <div className="invalid-feedback">{get(formData, 'district.error')}</div>
                          </div>
                        </div>
                        <div className="col-md-3">
                          <div className="form-group">
                            <label htmlFor="">City<span className="asterisk">*</span></label>
                            <input
                              type="text"
                              className="form-control"
                              placeholder="city"
                              name="city"
                              value={get(formData, 'city.value')}
                              disabled
                            />
                            <div className="invalid-feedback">{get(formData, 'city.error')}</div>
                          </div>
                        </div>
                        <div className="col-md-3">
                          <div className="form-group">
                            <label htmlFor="">Area/Village<span className="asterisk">*</span></label>
                            <input
                              type="text"
                              className="form-control"
                              placeholder="area"
                              name="area"
                              value={get(formData, 'area.value')}
                              disabled
                            />
                            <div className="invalid-feedback">{get(formData, 'area.error')}</div>
                          </div>
                        </div>
                        <div className="col-md-3">
                          <div className="form-group">
                            <label htmlFor="">Line 1</label>
                            <input
                              type="text"
                              className="form-control"
                              placeholder=""
                              name="addressLine1"
                              value={get(formData, 'addressLine1.value')}
                              onChange={(e) => setFormValue('addressLine1', e.target.value)}
                            />
                          </div>
                        </div>
                        <div className="col-md-3">
                          <div className="form-group">
                            <label htmlFor="">Line 2</label>
                            <input
                              type="text"
                              className="form-control"
                              placeholder=""
                              name="addressLine2"
                              value={get(formData, 'addressLine2.value')}
                              onChange={(e) => setFormValue('addressLine2', e.target.value)}
                            />
                          </div>
                        </div>

                      </div>
                    </div>


                    <div className="panel panel-default">
                      <div className="panel-heading">
                        <h3 className="panel-title">Bank Details </h3>
                      </div>
                      <div className="panel-body">

                        <div className="col-md-4">
                          <div className="form-group">
                            <label htmlFor="">Ac Holder Name<span className="asterisk">*</span></label>
                            <input type="text"
                              className="form-control"
                              value={get(formData, 'accountHolderName.value')}
                              onChange={(e) => setFormValue('accountHolderName', e.target.value)} />
                            <div className="invalid-feedback">{get(formData, 'accountHolderName.error')}</div>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-group">
                            <label htmlFor="">Ac Number<span className="asterisk">*</span></label>
                            <input type="text"
                              className="form-control"
                              value={get(formData, 'accountNumber.value')}
                              onChange={(e) => setFormValue('accountNumber', e.target.value)} />
                            <div className="invalid-feedback">{get(formData, 'accountNumber.error')}</div>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-group">
                            <label htmlFor="">Salary<span className="asterisk">*</span></label>
                            <input type="text"
                              className="form-control"
                              value={get(formData, 'salary.value')}
                              onChange={(e) => setFormValue('salary', e.target.value)} />
                            <div className="invalid-feedback">{get(formData, 'salary.error')}</div>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-group">
                            <label htmlFor="">IFSC Code<span className="asterisk">*</span></label>
                            <input type="text"
                              className="form-control"
                              value={get(formData, 'ifscCode.value')}
                              onChange={(e) => onIfscChange(e.target.value)}
                              style={{ textTransform: 'uppercase' }}
                            />
                            <div className="invalid-feedback">{get(formData, 'ifscCode.error')}</div>
                            {!get(formData, 'ifscCode.error') && ((get(formData, 'ifscCode.value').length < 11 && get(formData, 'ifscCode.value').length > 0) && <div className="invalid-feedback">IFSC code should be 11 digits</div>)}

                          </div>
                        </div>
                        <div className="col-md-4">
                          <div className="form-group">
                            <label htmlFor="">Bank Name<span className="asterisk">*</span></label>
                            <input type="text"
                              className="form-control"
                              value={get(formData, 'bankName.value')}
                              disabled
                            />
                            <div className="invalid-feedback">{get(formData, 'bankName.error')}</div>
                          </div>
                        </div>

                        <div className="col-md-4">
                          <div className="form-group">
                            <label htmlFor="">Bank Branch<span className="asterisk">*</span></label>
                            <input type="text"
                              className="form-control"
                              value={get(formData, 'bankBranch.value')}
                              disabled
                            />
                            <div className="invalid-feedback">{get(formData, 'bankBranch.error')}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>



                  <div className="panel-footer text-center">
                    <a href="#" className="btn btn-primary fire" onClick={saveForm}>
                      <i className="zmdi zmdi-check"></i> {t("common.save")}
                    </a>
                    <a href="#" className="btn btn-default" onClick={resetForm}>
                      <i className="zmdi zmdi-close"></i> {t("common.clear")}
                    </a>
                  </div>
                </div>
              </form>
            </div>

            <div className="panel panel-default">
              <div className="panel-heading">
                <h3 className="panel-title">{t("adminMenus.employees")} {t("common.list")}</h3>
                <div className="panel-options">
                  <p>
                    {selected.length > 0 ?
                      <a href="#" className="btn btn-info btn-sm" onClick={() => deleteAction(selected)} >
                        <i className="zmdi zmdi-delete"></i> {t("common.delete")}
                      </a> :
                      null}
                  </p>
                </div>
              </div>
              <div className="panel-body">
                <div className="ag-grid-wrapper">
                  <Grid colDefs={employeeColumns} rowData={employeesData} selectedRows={setSelected} actions={{ isRequired: true, editAction: editAction, deleteAction: deleteAction }} flex={true} />
                </div>
              </div>
            </div>
          </div>
        </LoadingOverlay>
      </div>
    </>
  );
};

export default withTranslation()(Employees);
