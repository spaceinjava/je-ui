import React, { Component } from 'react'
import { connect } from 'react-redux'
import { validate, login, resetPassword } from '../../redux/actions/authActions'
import "./Login.css";
import logopng from '../../assets/images/logo.png';
import { map } from 'lodash';
import LoadingOverlay from 'react-loading-overlay';

class Login extends Component {
    constructor(props) {
        super();
        this.state = {
            authFields: { hashcode: '', secretcode: '' },
            loginFields: { username: '', password: '' },
            resetPwdFields: { currentPwd: '', newPwd: '', confirmNewPwd: '' },
            fieldErrors: {}
        }
    }


    formValidation = (formObj) => {
        let obj = this.state[formObj];
        let errors = {};
        let isFormValid = true;
        map(obj, function (value, key) {
            if (!value) {
                isFormValid = false;
                errors[key] = `Please enter ${key} field`;
            }
        })
        this.setState({ fieldErrors: errors });
        return isFormValid;
    }

    componentDidMount = () => {
        console.log(document.cookie)
        if (this.getCookie("authCode") === '') {
            if (this.getCookie("hash") !== '' && this.getCookie("secret") !== '') {
                let data = {
                    response_type: "code",
                    hash: this.getCookie("hash"),
                    secret: this.getCookie("secret")
                }
                this.props.validate(data)
            }
        }
    }

    fieldOnChange = (event, fieldObj) => {
        let fields = this.state[fieldObj];
        fields[event.target.name] = event.target.value;
        this.setState({ [fieldObj]: fields });
    }

    invokeAuthorize = (event) => {
        event.preventDefault();
        if (this.formValidation("authFields")) {
            const { hashcode, secretcode } = this.state.authFields;
            let data = {
                response_type: "code",
                hash: hashcode,
                secret: secretcode
            }
            this.props.validate(data)
        }
    }

    getCookie = (cname) => {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    loginFormSubmit = (event) => {
        event.preventDefault();
        if (this.formValidation("loginFields")) {
            const { username, password } = this.state.loginFields;


            if (this.getCookie("hash") !== '' && this.getCookie("secret") !== '') {
                let data = {
                    response_type: "code",
                    hash: this.getCookie("hash"),
                    secret: this.getCookie("secret")
                }
                this.props.validate(data).then(() => {
                    let data = {
                        is_tenant: true,
                        response_type: "token",
                        hash: this.getCookie("hash"),
                        secret: this.getCookie("secret"),
                        code: this.getCookie("authCode"),
                        username: username,
                        password: password
                    }
                    this.props.login(data).then(res => {
                        const { loginFields } = this.state
                        loginFields.username = ""
                        loginFields.password = ""
                        this.setState({
                            loginFields: loginFields
                        })
                    })
                })
            }


        }
    }

    confirmPwdFieldOnChange = (event, fieldObj) => {
        let fields = this.state[fieldObj];
        fields[event.target.name] = event.target.value;
        this.setState({ [fieldObj]: fields });
        let errors = {};
        if (!this.newPasswordCheck(fields.newPwd, event.target.value)) {
            errors[event.target.name] = `Confirm Password doesn't matches with New Password`;
            this.setState({ fieldErrors: errors });
        } else {
            errors[event.target.name] = ``;
            this.setState({ fieldErrors: errors });
        }
    }


    newPasswordCheck = (newPwd, confirmNewPwd) => {
        if (newPwd && confirmNewPwd)
            return newPwd === confirmNewPwd
    }

    resetPassword = (event) => {
        event.preventDefault();
        const { currentPwd, newPwd, confirmNewPwd } = this.state.resetPwdFields;
        if (this.formValidation("resetPwdFields") && this.newPasswordCheck(newPwd, confirmNewPwd)) {
            let data = {
                is_tenant: true,
                token: this.getCookie("authToken"),
                user_code: this.getCookie("loginId"),
                password: currentPwd,
                new_password: newPwd
            }
            this.props.resetPassword(data).then(res => {
                const { resetPwdFields } = this.state
                resetPwdFields.currentPwd = ""
                resetPwdFields.newPwd = ""
                resetPwdFields.confirmNewPwd = ""
                this.setState({
                    resetPwdFields: resetPwdFields
                })
            })
        }
    }

    render() {
        const { hashcode, secretcode } = this.state.authFields;
        const { username, password } = this.state.loginFields;
        const { currentPwd, newPwd, confirmNewPwd } = this.state.resetPwdFields;
        return (
            <LoadingOverlay
                active={this.props.loading}
                spinner
                text='Loading...'
            >
                <div id="login">
                    <div className="row1">
                        <div className="col-1">
                            <div className="left-bg">
                                <img src={logopng} alt="" />
                                <h4 className="text-white">Capture the true value of the cloud for your business</h4>
                            </div>
                        </div>
                        <div className="col-2 text-left">
                            {(this.getCookie("authCode") && this.getCookie("authCode").length > 0) || (this.getCookie("hash") && this.getCookie("hash").length > 0) ?
                                <>
                                    {this.getCookie("loginStatus") === "NEW" ?
                                        <div className="login-panel2" id="recoverform">
                                            <div className="panel2">
                                                <div className="login-icon"> <i className="zmdi zmdi-account"></i></div>
                                                <h3 className="panel-title">Reset Password </h3>
                                                <form onSubmit={this.resetPassword}>
                                                    <div className="form-group">
                                                        <label htmlFor="">Current Password</label>
                                                        <input type="password" className="form-control" placeholder="" name="currentPwd" value={currentPwd} onChange={(e) => this.fieldOnChange(e, "resetPwdFields")} />
                                                        <div className="invalid-feedback">{this.state.fieldErrors["currentPwd"]}</div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="">New Password</label>
                                                        <input type="password" className="form-control" placeholder="" name="newPwd" value={newPwd} onChange={(e) => this.fieldOnChange(e, "resetPwdFields")} />
                                                        <div className="invalid-feedback">{this.state.fieldErrors["newPwd"]}</div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="">Confirm Password</label>
                                                        <input type="password" className="form-control" placeholder="" name="confirmNewPwd" value={confirmNewPwd} onChange={(e) => this.confirmPwdFieldOnChange(e, "resetPwdFields")} />
                                                        <div className="invalid-feedback">{this.state.fieldErrors["confirmNewPwd"]}</div>
                                                    </div>
                                                    <button type="submit" className="btn btn-primary">Save <i className="zmdi zmdi-arrow-right"></i></button>
                                                </form>
                                            </div>
                                        </div> :
                                        <div className="login-panel2" id="recoverform">
                                            <div className="panel2">
                                                <div className="login-icon"> <i className="zmdi zmdi-account"></i></div>
                                                <h3 className="panel-title">Login </h3>
                                                <form onSubmit={this.loginFormSubmit}>
                                                    <div className="form-group">
                                                        <label htmlFor="">Usercode</label>
                                                        <input type="text" className="form-control" placeholder="" name="username" value={username} onChange={(e) => this.fieldOnChange(e, "loginFields")} />
                                                        <div className="invalid-feedback">{this.state.fieldErrors["username"]}</div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="">Password</label>
                                                        <input type="password" className="form-control" placeholder="" name="password" value={password} onChange={(e) => this.fieldOnChange(e, "loginFields")} />
                                                        <div className="invalid-feedback">{this.state.fieldErrors["password"]}</div>
                                                    </div>
                                                    <button type="submit" className="btn btn-primary">Login <i className="zmdi zmdi-arrow-right"></i></button>
                                                </form>
                                            </div>
                                        </div>
                                    }
                                </>
                                :
                                <div className="login-panel" id="loginform">
                                    <div className="panel2">
                                        <div className="login-icon"> <i className="zmdi zmdi-account"></i></div>
                                        <h3 className="panel-title">Authorize </h3>
                                        <form onSubmit={this.invokeAuthorize}>
                                            <div className="form-group">
                                                <label htmlFor="">Hash</label>
                                                <input type="text" className="form-control" placeholder="" name="hashcode" value={hashcode} onChange={(e) => this.fieldOnChange(e, "authFields")} />
                                                <div className="invalid-feedback">{this.state.fieldErrors["hashcode"]}</div>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="">Secret</label>
                                                <input type="password" className="form-control" placeholder="" name="secretcode" value={secretcode} onChange={(e) => this.fieldOnChange(e, "authFields")} />
                                                <div className="invalid-feedback">{this.state.fieldErrors["secretcode"]}</div>
                                            </div>
                                            <button type="submit" className="btn btn-primary" id="validate">Validate <i className="zmdi zmdi-arrow-right"></i></button>
                                        </form>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </LoadingOverlay>
        );
    }
}

const mapStateToProps = state => ({
    isValidated: state.authReducer.isValidated,
    isAuthenticated: state.authReducer.isAuthenticated,
    authInfo: state.authReducer.authInfo,
    loading: state.authReducer.loading,
    error: state.authReducer.error
})

export default connect(mapStateToProps, { validate, login, resetPassword })(Login)
