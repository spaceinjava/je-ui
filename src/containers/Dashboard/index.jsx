import React, { Component } from "react";
import { withTranslation } from "react-i18next";

function Dashboard(props) {
    const { t } = props;
    return <div> {t("metal")}</div>;
}

export default withTranslation()(Dashboard);