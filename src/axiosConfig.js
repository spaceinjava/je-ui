import axios from 'axios';
import configProps from './configProps';

console.log("configProps : ", configProps);

export default axios.create({
    baseURL: configProps.baseUrl,
    auth: {
        username: configProps.authUser,
        password: configProps.authPass
    }
  });
