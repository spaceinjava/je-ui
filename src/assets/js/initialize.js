/******Sticky Header*****/

$(window).scroll(function() {
    if ($(window).scrollTop() >= 50) {
        $('.mainHeader').addClass('sticky-header');

    } else {
        $('.mainHeader').removeClass('sticky-header');

    }
});

$(document).ready(function() {
    $('.btn-collapse').on('click', function() {
        $('.innerLeftNav').toggleClass('clicked');
        $('.btn-collapse i').toggleClass('zmdi-chevron-right zmdi-chevron-left');
        $('.innerRightCntnt').toggleClass('widthChange');
    });

    $('.select').select2({
        placeholder: "select",
    });

    // $('.').select2({
    //     closeOnSelect: false,
    //     allowHtml: true,
    //     allowClear: true,
    //     tags: true,
    //     placeholder: "Choose multiple elements",
    // });
    // tail.select(".select", {
    //     placeholder: "Select option...",
    //     search: "true"
    // });

    $('#alltransactions').DataTable({
        "scrollX": true,
        "select": true,
        "columnDefs": [{
            "targets": 0,
            "orderable": false
        }]
    });
    $('#alltransactions2').DataTable({
        "scrollX": true,
        "select": true,
        "columnDefs": [{
            "targets": 0,
            "orderable": false
        }]
    });
    $('#modalTable').DataTable({
        "scrollX": true,
        "select": true,
        "columnDefs": [{
            "targets": 0,
            "orderable": false
        }]
    });

    $('[data-toggle="tooltip"]').tooltip();

    // Profile open/close
    $(".collapseBtn").click(function() {
        $(".collapse-panel").slideToggle(500, 'swing');
    });

    $('.collapseBtn').click(function() {
        $(this).find('i').toggleClass('zmdi-caret-up-circle zmdi-caret-down-circle')
    });
    // Date picker
    $('#dateCommon').datepicker({
        todayHighlight: true,
        autoclose: true,
        format: "dd/mm/yyyy",
        clearBtn: true,
        todayBtn: "linked"
    });


    // modal not close
    // $('#addNew').modal({
    //     backdrop: 'static',
    //     keyboard: false
    // })
});