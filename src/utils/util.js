
/**
 * checks given input is a number >= 0
 */
export function validateNum(num) {
  return !isNaN(parseFloat(num)) && isFinite(num) && num >= 0;
}

/**
 * rounds the given number to 3 digits
 */
export function roundToThree(num) {
  return validateNum(num) ? +(Math.round(num + "e+3") + "e-3") : 0;
};

/**
 * Convert to Date Format -> yyyy-mm-dd
 */
 export const formatDate = (date) => {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;
    return `${year}-${month}-${day}`
};
