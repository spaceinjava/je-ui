
// Supplier or Party Type Constants
export const DEALER_PARTY_TYPE = "DEALER";
export const HALLMARK_PARTY_TYPE = "HALLMARK";
export const MELTING_PARTY_TYPE = "MELTING";

// UOM Constants
export const UOM_TYPE = {
    PIECE: "Piece",
    GRAM: "Gram",
    KARAT: "Karat"
}

// 1 Gram = 5 Karats
export const GRAM_TO_KARAT = 5;

// 1 Karat = 0.2 Grams
export const KARAT_TO_GRAM = 0.2;

// common gst is 3%
export const GST_PERCENT = 3;
