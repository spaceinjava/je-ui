import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom'
import Router from './Router';
import { connect } from 'react-redux'
import Home from './containers/Home/Home';
import Login from './containers/Login/Login'
import Layout from './containers/Layout';
import { getCookie } from './redux/actions/authActions'
import './App.css';

class App extends React.Component {
  render() {
    const location = window.location;
    const { isAuthenticated } = this.props;
    let login_status = getCookie("loginStatus");
    return (
      <div className="App">
        {!isAuthenticated || login_status === "NEW" || login_status === "RESET" ? 
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/login" component={Login} />
            <Redirect to="/" />
          </Switch>
          :
          <Layout location={location}>
            <Router location={location} isAuthenticated={isAuthenticated} />
          </Layout>
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.authReducer.isAuthenticated,
  loading: state.authReducer.loading,
  error: state.authReducer.error
})

export default connect(mapStateToProps, { getCookie })(App);