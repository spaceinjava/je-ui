import React from 'react';
import DateRangePicker from '@wojtekmaj/react-daterange-picker';
import { withTranslation } from "react-i18next";

const DatePicker = (props) => {
    const { value, onChange } = props;
    return (
        <div>
            <DateRangePicker
                onChange={onChange}
                value={value}
                monthPlaceholder="mm"
                yearPlaceholder="yy"
                dayPlaceholder="dd"
                rangeDivider=" to "
                format="MM/dd/yy"
            />
        </div>
    );
}

export default withTranslation()(DatePicker);