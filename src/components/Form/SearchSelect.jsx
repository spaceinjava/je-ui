import React, { useState } from "react";
import { withTranslation } from "react-i18next";
import Select from "react-select";

const SearchSelect = (props) => {
    const { t } = props;
    return (
        <Select name={props.name}
            value={props.value}
            onChange={props.onChange}
            options={props.options}
            placeholder={props.placeholder} />
    );
}
export default withTranslation()(SearchSelect);