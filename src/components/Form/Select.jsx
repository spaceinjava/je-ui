import React, { useState } from "react";
import { withTranslation } from "react-i18next";

const Select = (props) => {
    const { t } = props;
    return (
        <select className="form-control select" name={props.name} value={props.value} onChange={props.onChange}>
            <option value={""}>Select</option>
            {props.data ? props.data.map((ele, index) => {
                return <option id={ele.id} key={`opt_${index}`} value={ele.id}>{ele.name}</option>
            }) : []}

        </select>
    );
}
export default withTranslation()(Select);