import React, { useState } from "react";
import { withTranslation } from "react-i18next";
import { UOM_TYPE as UOM } from "../utils/commonConstants";

const UOMElement = (props) => {
    const { t } = props;
    return (
        <select className="form-control select" name={props.name} value={props.value} onChange={props.onChange}>
            <option value={""}>Select</option>
            <option value={UOM.PIECE}>per {UOM.PIECE}</option>
            <option value={UOM.GRAM}>per {UOM.GRAM}</option>
            <option value={UOM.KARAT}>per {UOM.KARAT}</option>
        </select>
    );
}
export default withTranslation()(UOMElement);