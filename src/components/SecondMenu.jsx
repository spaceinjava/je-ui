import React, { Component } from "react";
import { withTranslation } from "react-i18next";

export const SecondMenu = (props) => {
  const { t } = props;
  return (
    <>
      {props.menuOptions.map((eachMenu, index) => {
        return (eachMenu.menuItems) ?
          <li key={`li${index}`} className="hasAccordian"> <a className={eachMenu.active ? "active" : "collapsed"} role="button" data-toggle="collapse" data-parent="#accordion" href={`#${eachMenu.id}`} aria-expanded="true" aria-controls={eachMenu.id}> {eachMenu.name}</a>
            <ul key={`ul_${index}`} className={eachMenu.active ? "collapse in" : "panel-collapse collapse"} id={eachMenu.id}>
              {eachMenu.menuItems.map((subMenu, index) => {
                return (subMenu.menuItems) ?
                  <li key={`li${index}`} className="hasAccordian"> <a className={subMenu.active ? "active" : "collapsed"} role="button" data-toggle="collapse" data-parent="#accordion" href={`#${subMenu.id}`} aria-expanded="true" aria-controls={subMenu.id}> {subMenu.name}</a>
                    <ul key={`ul_${index}`} className={subMenu.active ? "panel-collapse subList collapse in" : "panel-collapse subList collapse"} id={subMenu.id}>
                      {subMenu.menuItems.map((menuItem) => {
                        return (<li key={`${menuItem.href}_${menuItem.name}`}>
                          <a className={menuItem.active ? "active" : ""} href={menuItem.href}>{t(`${menuItem.title}`)}</a>
                        </li>)
                      })}
                    </ul>
                  </li>
                  :
                  <li key={`${subMenu.href}_${subMenu.name}`}>
                    <a className={subMenu.active ? "active" : ""} href={subMenu.href}>{t(`${subMenu.title}`)}</a>
                  </li>
              })}
            </ul>
          </li>
          :
          <li key={`${eachMenu.href}_${eachMenu.name}`}>
            <a className={eachMenu.active ? "active" : ""} href={eachMenu.href}>{t(`${eachMenu.title}`)}</a>
          </li>
          ;
      })}
    </>
  );
};

export default withTranslation()(SecondMenu);
