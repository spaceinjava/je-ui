import {
  Checkbox,
  FormControl,
  Grid,
  InputAdornment,
  MenuItem,
  OutlinedInput,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableSortLabel,
  makeStyles,
} from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
import PropTypes from "prop-types";
import React, { useState, useEffect } from "react";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import SearchIcon from "@material-ui/icons/Search";

import BootstrapInput from "./BootstrapInput";
import "../../assets/css/mat-table.css";

const useStyles = makeStyles((theme) => ({
  editIcon: {
    color: "#b5b3b3",
  },
  deleteIcon: {
    color: "#db5656",
  },
  searchTextField: {
    borderRadius: theme.spacing(3),
    height: theme.spacing(4),
    width: theme.spacing(30),
  },
  searchGrid: {
    textAlign: "right",
    marginBottom: theme.spacing(1.5),
  },
  radioDiv: {
    marginTop: theme.spacing(0.75),
    marginLeft: theme.spacing(0.5),
  },
}));

const CustomGrid = ({
  uniqueId,
  sortColumn,
  isEdit,
  isDelete,
  isCheckbox,
  isRadio,
  columns,
  gridData,
  defaultRowsPerPage,
  fnOnChangeRadioItem,
  onCheckboxChange,
  setEditId,
  onDataEdited,
}) => {
  const classes = useStyles();

  const [order, setOrder] = useState("asc");
  const [page, setPage] = useState(0);
  const [selected, setSelected] = useState([]);
  const [rowsPerPage, setRowsPerPage] = useState(defaultRowsPerPage);
  const [orderBy, setOrderBy] = useState(sortColumn);
  const [rows, setRows] = useState([]);
  const [searchText, setSearchText] = useState("");
  const [filteredData, setFilteredData] = useState([]);
  const [selectedRadioItem, setSelectedRadioItem] = useState("");

  const isSelected = (name) => selected.indexOf(name) !== -1;

  useEffect(() => {
    setRows(gridData);
  }, [gridData]);

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      let newSelecteds = [];
      newSelecteds = rows.map((n) => n[uniqueId]);
      setSelected(newSelecteds);
      onCheckboxChange(newSelecteds);
      return;
    }
    setSelected([]);
    onCheckboxChange([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
    onCheckboxChange(newSelected);
  };

  const descendingComparator = (a, b, orderBy) => {
    if (
      a[orderBy] &&
      b[orderBy] &&
      b[orderBy].toString().toLowerCase() < a[orderBy].toString().toLowerCase()
    ) {
      return -1;
    }
    if (
      a[orderBy] &&
      b[orderBy] &&
      b[orderBy].toString().toLowerCase() > a[orderBy].toString().toLowerCase()
    ) {
      return 1;
    }
    return 0;
  };

  const getComparator = (order, orderBy) => {
    return order === "desc"
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  };

  const stableSort = (array, comparator) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  };
  const onEditClick = (id) => {
    setEditId(id);
  };
  const onDeleteClick = () => { };
  const handleSearch = (event) => {
    setSearchText(event.target.value);
  };

  const onChangeGridTextValue = (id, col, val) => {
    const dataToChange = [...filteredData];
    dataToChange.forEach((item) => {
      if (item[uniqueId] === id) item[col] = val;
    });
    setFilteredData(dataToChange);
    onDataEdited(dataToChange);
  };

  const onChangeRadioItem = (val) => {
    setSelectedRadioItem(val);
    fnOnChangeRadioItem(val);
  }

  useEffect(() => {
    let dataSet = [...rows];
    setPage(0);
    if (searchText !== "") {
      if (dataSet.length > 0)
        dataSet = dataSet.filter((item) => {
          let isMatch = false;
          columns.forEach((val) => {
            if (item[val.id] && item[val.id] !== null && item[val.id].toString().toLowerCase().includes(searchText.toLowerCase()))
              isMatch = true;
          });
          return isMatch;
        });
      setFilteredData(dataSet);
    } else {
      setFilteredData(rows);
    }
  }, [searchText, rows]);

  const EnhancedTableHead = (props) => {
    const { onRequestSort, onSelectAllClick, numSelected, rowCount } = props;
    const createSortHandler = (property) => (event) => {
      onRequestSort(event, property);
    };

    return (
      <TableHead>
        <TableRow>
          {isCheckbox ? (
            <TableCell padding="checkbox">
              <Checkbox
                indeterminate={numSelected > 0 && numSelected < rowCount}
                checked={rowCount > 0 && numSelected === rowCount}
                onChange={onSelectAllClick}
                inputProps={{ "aria-label": "select all desserts" }}
                color="primary"
              />
            </TableCell>
          ) : (
            ``
          )}
          {isRadio ? <TableCell /> : ``}

          {columns.map((headCell) =>
            headCell.isSort ? (
              <TableCell
                key={headCell.id}
                sortDirection={orderBy === headCell.id ? order : false}
              >
                <TableSortLabel
                  active={orderBy === headCell.id}
                  direction={orderBy === headCell.id ? order : "asc"}
                  onClick={createSortHandler(headCell.id)}
                >
                  {headCell.label}
                </TableSortLabel>
              </TableCell>
            ) : (
              <TableCell key={headCell.id}>{headCell.label}</TableCell>
            )
          )}
          {isEdit || isDelete ? <TableCell /> : ``}
        </TableRow>
      </TableHead>
    );
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage - 1);
  };

  EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    rowCount: PropTypes.number.isRequired,
  };

  return (
    <div className="gridTable">
      <Grid container spacing={0} className={classes.searchGrid}>
        <Grid item lg={12}>
          <FormControl variant="outlined">
            <OutlinedInput
              className={classes.searchTextField}
              id="outlined-adornment-search"
              type="text"
              onChange={handleSearch}
              value={searchText}
              placeholder="Enter Keyword"
              endAdornment={
                <InputAdornment position="end">
                  <SearchIcon className={classes.searchIcon} />
                </InputAdornment>
              }
            />
          </FormControl>
        </Grid>
      </Grid>
      <TableContainer>
        <Table
          aria-labelledby="tableTitle"
          size="small"
          aria-label="enhanced table"
        >
          <EnhancedTableHead
            classes={classes}
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
            onSelectAllClick={handleSelectAllClick}
            rowCount={rows.length}
            numSelected={selected.length}
          />
          {rows.length > 0 ? (
            <TableBody>
              {stableSort(filteredData, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row[uniqueId]);
                  const labelId = `enhanced-table-checkbox-${index}`;
                  return (
                    <TableRow
                      tabIndex={-1}
                      key={index}
                      className={isEdit || isDelete ? `hoverIcon` : ``}
                    >
                      {isCheckbox ? (
                        <TableCell padding="checkbox">
                          <Checkbox
                            checked={isItemSelected}
                            color="primary"
                            inputProps={{ "aria-labelledby": labelId }}
                            onClick={(event) =>
                              handleClick(event, row[uniqueId])
                            }
                          />
                        </TableCell>
                      ) : (
                        ``
                      )}
                      {isRadio ? (
                        <div className={classes.radioDiv}>
                          <input
                            type="radio"
                            name={row[uniqueId]}
                            checked={row[uniqueId] === selectedRadioItem && !row.isRadioDisable}
                            disabled={row.isRadioDisable}
                            onChange={(e) =>
                              onChangeRadioItem(row[uniqueId])
                            }
                          />
                        </div>
                      ) : (
                        ``
                      )}
                      {columns.map((column) => {
                        return (
                          <TableCell key={column[uniqueId]} className={row.rowColorStyle && row.rowColorStyle !== "" ? row.rowColorStyle : ``}>
                            {column.isEdit ? (
                              <input
                                type="text"
                                className="form-control"
                                name={row[uniqueId]}
                                value={row[column.id]}
                                onChange={(e) =>
                                  onChangeGridTextValue(
                                    row[uniqueId],
                                    column.id,
                                    e.target.value
                                  )
                                }
                              />
                            ) : (
                              row[column.id]
                            )}
                          </TableCell>
                        );
                      })}
                      {isEdit || isDelete ? (
                        <TableCell className="actionCell">
                          {isEdit ? (
                            <span
                              onClick={() => onEditClick(row[uniqueId])}
                              className="tableEditIcon"
                              name={row[uniqueId]}
                              id={row[uniqueId]}
                            >
                              <EditIcon className={classes.editIcon} />
                            </span>
                          ) : (
                            ``
                          )}
                          {isDelete ? (
                            <span
                              onClick={() => onDeleteClick(row[uniqueId])}
                              className="tableDeleteIcon"
                              name={row[uniqueId]}
                              id={row[uniqueId]}
                            >
                              <DeleteIcon className={classes.deleteIcon} />
                            </span>
                          ) : (
                            ``
                          )}
                        </TableCell>
                      ) : (
                        ``
                      )}
                    </TableRow>
                  );
                })}
            </TableBody>
          ) : (
            <TableBody>
              <TableRow>
                <TableCell colSpan="6" align="center">
                  No records found
                </TableCell>
              </TableRow>
            </TableBody>
          )}
        </Table>
      </TableContainer>

      {rows.length > defaultRowsPerPage ? (
        <Grid container className="paginationSection" spacing={0}>
          <Grid item lg={6}>
            Showing
            <FormControl className="tableRowsDdl">
              <Select
                labelId="demo-customized-select-label"
                id="demo-customized-select"
                value={rowsPerPage}
                onChange={handleChangeRowsPerPage}
                input={<BootstrapInput />}
              >
                <MenuItem value={5}>5</MenuItem>
                <MenuItem value={10}>10</MenuItem>
                <MenuItem value={15}>15</MenuItem>
                <MenuItem value={20}>20</MenuItem>
                <MenuItem value={25}>25</MenuItem>
              </Select>
            </FormControl>
            of {filteredData.length}
          </Grid>
          <Grid item lg={6}>
            <Pagination
              count={Math.ceil(filteredData.length / rowsPerPage)}
              shape="rounded"
              showFirstButton
              showLastButton
              page={page + 1}
              onChange={handleChangePage}
            />
          </Grid>
        </Grid>
      ) : (
        ``
      )}
    </div>
  );
};

export default CustomGrid;
