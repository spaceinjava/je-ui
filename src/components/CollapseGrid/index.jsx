import React, { useEffect, useState } from "react";
import {
  Checkbox,
  makeStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableSortLabel,
} from "@material-ui/core";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import PropTypes from "prop-types";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

import "../../assets/css/collapse-table.css";

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  container: {
    maxHeight: 440,
  },
  expandIcon: {
    cursor: "pointer",
  },
  editIcon: {
    color: "#b5b3b3",
  },
  deleteIcon: {
    color: "#db5656",
  },
});

const CollapseGrid = ({
  sortOrder = "asc",
  sortColumn,
  columns,
  expandColumns,
  isCheckBox = true,
  data,
  uniqueId = "id",
  editData,
  deleteData,
}) => {
  const classes = useStyles();

  const [order, setOrder] = useState(sortOrder);
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState(sortColumn);
  const [rows, setRows] = useState([]);

  const isSelected = (name) => selected.indexOf(name) !== -1;

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      let newSelecteds = [];
      newSelecteds = rows.map((n) => n.id);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const descendingComparator = (a, b, orderBy) => {
    if (
      a[orderBy] &&
      b[orderBy] &&
      b[orderBy].toString().toLowerCase() < a[orderBy].toString().toLowerCase()
    ) {
      return -1;
    }
    if (
      a[orderBy] &&
      b[orderBy] &&
      b[orderBy].toString().toLowerCase() > a[orderBy].toString().toLowerCase()
    ) {
      return 1;
    }
    return 0;
  };

  const getComparator = (order, orderBy) => {
    return order === "desc"
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  };

  const stableSort = (array, comparator) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const EnhancedTableHead = (props) => {
    const { onRequestSort, onSelectAllClick, numSelected, rowCount } = props;
    const createSortHandler = (property) => (event) => {
      onRequestSort(event, property);
    };

    return (
      <TableHead>
        <TableRow>
          {isCheckBox ? (
            <TableCell padding="checkbox">
              <Checkbox
                indeterminate={numSelected > 0 && numSelected < rowCount}
                checked={rowCount > 0 && numSelected === rowCount}
                onChange={onSelectAllClick}
                inputProps={{ "aria-label": "select all" }}
                color="primary"
              />
            </TableCell>
          ) : (
            ``
          )}
          {columns.map((headCell, i) =>
            i === 0 ? (
              <TableCell
                key={headCell.id}
                sortDirection={orderBy === headCell.id ? order : false}
                style={{ minWidth: headCell.minWidth }}
              >
                <TableSortLabel
                  active={orderBy === headCell.id}
                  direction={orderBy === headCell.id ? order : "asc"}
                  onClick={createSortHandler(headCell.id)}
                >
                  {headCell.label}
                </TableSortLabel>
              </TableCell>
            ) : (
              <TableCell
                key={headCell.id}
                style={{ minWidth: headCell.minWidth }}
              >
                {headCell.label}
              </TableCell>
            )
          )}
        </TableRow>
      </TableHead>
    );
  };

  EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    rowCount: PropTypes.number.isRequired,
  };

  const expandData = (dataToExpand, id, depth) => {
    const tempData = [...dataToExpand];
    tempData.forEach((item) => {
      let tempDepth = depth;
      if (item.localId === id) item.isVisible = !item.isVisible;
      else {
        if (
          expandColumns[tempDepth] &&
          item[expandColumns[tempDepth]["children"]] &&
          typeof item[expandColumns[tempDepth]["children"]] === "object" &&
          item[expandColumns[tempDepth]["children"]].length > 0
        ) {
          expandData(item[expandColumns[tempDepth]["children"]], id, tempDepth);
          tempDepth += 1;
        }
      }
    });
    return tempData;
  };

  const toggleClick = (id) => {
    let dataToExpand = [...rows];
    dataToExpand = expandData(dataToExpand, id, 0);
    setRows(dataToExpand);
  };

  const onEditClick = (id) => {
    editData(rows.filter((val) => val[uniqueId] === id)[0]);
  };

  const onDeleteClick = (id) => {
    deleteData(rows.filter((val) => val[uniqueId] === id));
  };

  const bindChildData = (data, isShow, depth) => {
    let depthTemp = depth + 1;
    return data.map((item, i) => {
      if (
        expandColumns[depthTemp] &&
        item[expandColumns[depthTemp]["children"]] &&
        item[expandColumns[depthTemp]["children"]].length > 0 &&
        isShow
      ) {
        return (
          <>
            <TableRow
              hover
              role="checkbox"
              tabIndex={-1}
              key={item.localId}
              className="panelRow"
            >
              {isCheckBox ? <TableCell /> : ``}
              <TableCell
                style={{ paddingLeft: 16 * depthTemp + 16 }}
                key={i}
                colSpan={columns.length}
              >
                <span
                  className={classes.expandIcon}
                  onClick={() => toggleClick(item.localId)}
                >
                  {!item.isVisible ? <ChevronRightIcon /> : <ExpandMoreIcon />}
                </span>
                {item[expandColumns[depthTemp].id]}
              </TableCell>
            </TableRow>
            <>
              {bindChildData(
                item[expandColumns[depthTemp]["children"]],
                item.isVisible,
                depthTemp
              )}
            </>
          </>
        );
      } else if (isShow) {
        return (
          <TableRow hover role="checkbox" tabIndex={-1} key={item.localId}>
            {isCheckBox ? <TableCell /> : ``}
            {columns.map((column, i) => {
              return (
                <TableCell
                  style={{ paddingLeft: i === 0 ? 16 * depthTemp + 16 : 8 }}
                  key={column.id}
                >
                  {item[column.id]}
                </TableCell>
              );
            })}
          </TableRow>
        );
      }
    });
  };

  useEffect(() => {
    let tempId = 1;

    const addExpandFlag = (data, depth) => {
      let modData = [...data];
      modData.forEach((item) => {
        item.isVisible = false;
        item.localId = tempId;
        tempId += 1;
        let tempDepth = depth;
        if (
          expandColumns[tempDepth] &&
          item[expandColumns[tempDepth]["children"]] &&
          typeof item[expandColumns[tempDepth]["children"]] === "object" &&
          item[expandColumns[tempDepth]["children"]].length > 0
        ) {
          addExpandFlag(
            item[expandColumns[tempDepth]["children"]],
            tempDepth,
            tempId
          );
          tempDepth += 1;
        }
      });
      return modData;
    };

    setRows(addExpandFlag(data, 0, 1));
  }, [data]);

  return (
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader className="collapseTable" aria-label="sticky table">
          <EnhancedTableHead
            classes={classes}
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
            onSelectAllClick={handleSelectAllClick}
            rowCount={rows.length}
            numSelected={selected.length}
          />
          <TableBody>
            {stableSort(rows, getComparator(order, orderBy)).map((row, i) => {
              const isItemSelected = isSelected(row[uniqueId]);
              const labelId = `enhanced-table-checkbox-${i}`;
              return (
                <>
                  <TableRow
                    hover
                    role="checkbox"
                    tabIndex={-1}
                    key={row.localId}
                    className={
                      expandColumns[0].isEdit || expandColumns[0].isDelete
                        ? "panelRow hoverIcon"
                        : "panelRow"
                    }
                  >
                    {isCheckBox ? (
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          color="primary"
                          inputProps={{ "aria-labelledby": labelId }}
                          onClick={(event) => handleClick(event, row[uniqueId])}
                        />
                      </TableCell>
                    ) : (
                      ``
                    )}
                    <TableCell key={i} colSpan={columns.length}>
                      <span
                        className={classes.expandIcon}
                        onClick={() => toggleClick(row.localId)}
                      >
                        {!row.isVisible ? (
                          <ChevronRightIcon />
                        ) : (
                          <ExpandMoreIcon />
                        )}
                      </span>
                      {row[expandColumns[0].id]}
                      {expandColumns[0].isEdit ? (
                        <span
                          onClick={() => onEditClick(row[uniqueId])}
                          className="tableEditIcon"
                          name={row[uniqueId]}
                          id={row[uniqueId]}
                        >
                          <EditIcon className={classes.editIcon} />
                        </span>
                      ) : (
                        ``
                      )}
                      {expandColumns[0].isDelete ? (
                        <span
                          onClick={() => onDeleteClick(row[uniqueId])}
                          className="tableDeleteIcon"
                          name={row[uniqueId]}
                          id={row[uniqueId]}
                        >
                          <DeleteIcon className={classes.deleteIcon} />
                        </span>
                      ) : (
                        ``
                      )}
                    </TableCell>
                  </TableRow>
                  {row.isVisible &&
                  row[expandColumns[0]["children"]] &&
                  row[expandColumns[0]["children"]].length > 0
                    ? bindChildData(
                        row[expandColumns[0]["children"]],
                        row.isVisible,
                        0
                      )
                    : ``}
                </>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
};

export default CollapseGrid;
