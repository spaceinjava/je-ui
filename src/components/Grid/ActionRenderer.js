import React, { Component } from 'react';

class ActionRenderer extends Component {
    render() {
        const props = this.props;
        return (
            <>
                {props.context.actions.editAction ? <a href="#" className="zmdi zmdi-edit m-r-10" onClick={() => props.context.invokeEditAction(props.node)} /> : <></>}
                {props.context.actions.deleteAction ? <a href="#" className="zmdi zmdi-delete" onClick={() => props.context.invokeDeleteAction(props.node)} /> : <></>}
            </>
        );
    }
}

export default ActionRenderer;