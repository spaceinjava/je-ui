import React, { useState } from "react";
import { withTranslation } from "react-i18next";
import { AgGridReact, AgGridColumn } from "ag-grid-react";
import ActionRenderer from './ActionRenderer'
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-alpine.css";

const Grid = (props) => {
  console.log({ props });
  const { t, actions } = props;
  const [gridApi, setGridApi] = useState(null);
  const [gridColumnApi, setGridColumnApi] = useState(null);
  const [rowData, setRowData] = useState(null);
  const onSelectionChanged = () => {
    var selectedRows = gridApi.getSelectedRows();
    console.log({ selectedRows })
    props.selectedRows(selectedRows);
  };
  const onGridReady = (params) => {
    setGridApi(params.api);
    setGridColumnApi(params.columnApi);
  };

  const getGridColumns = (props) => {
    const { colDefs } = props;
    return colDefs.map((eachColumns, index) => {
      return <AgGridColumn key={`grid_col_${index}`} {...eachColumns} headerName={t(`${eachColumns.headerName}`)} />;
    });
  };

  return (
    <div className="grid-container">
      <div
        id="myGrid"
        style={{
          height: "100%",
          width: "100%",
        }}
        className="ag-theme-alpine"
      >
        <AgGridReact
          defaultColDef={{
            flex: props.flex ? 0 : 1,
            width: 150,
            filter: "agTextColumnFilter",
            sortable: true,
            // floatingFilter: true,
            resizable: true,
            headerCheckboxSelection: isFirstColumn,
            checkboxSelection: isFirstColumn,
          }}
          suppressRowClickSelection={true}
          rowSelection={'multiple'}
          pagination={true}
          paginationAutoPageSize={true}

          defaultColGroupDef={{ marryChildren: true }}
          columnTypes={{
            numberColumn: {
              width: 130,
              filter: "agNumberColumnFilter",
            },
            nonEditableColumn: { editable: false },
            dateColumn: {
              filter: "agDateColumnFilter",
              filterParams: {
                comparator: function (filterLocalDateAtMidnight, cellValue) {
                  var dateParts = cellValue.split("/");
                  var day = Number(dateParts[0]);
                  var month = Number(dateParts[1]) - 1;
                  var year = Number(dateParts[2]);
                  var cellDate = new Date(year, month, day);
                  if (cellDate < filterLocalDateAtMidnight) {
                    return -1;
                  } else if (cellDate > filterLocalDateAtMidnight) {
                    return 1;
                  } else {
                    return 0;
                  }
                },
              },
            },
          }}
          rowData={props.rowData}
          onGridReady={onGridReady}
          onSelectionChanged={onSelectionChanged}
          frameworkComponents={{
            'actionRenderer': ActionRenderer
          }}
          context={{
            actions,
            invokeDeleteAction(selectedNode) {
              actions.deleteAction(selectedNode.data);
            },
            invokeEditAction(selectedNode) {
              actions.editAction(selectedNode.data);
            }
          }}
        >
          {getGridColumns(props)}
          {actions ? <AgGridColumn field="Actions" cellRenderer='actionRenderer'/> : <AgGridColumn hide/>}
        </AgGridReact>
      </div>
    </div>
  );
};
function isFirstColumn(params) {
  var displayedColumns = params.columnApi.getAllDisplayedColumns();
  var thisIsFirstColumn = displayedColumns[0] === params.column;
  return thisIsFirstColumn;
}
export default withTranslation()(Grid);
