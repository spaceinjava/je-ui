import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Dashboard from "./containers/Dashboard";
import Admin from "./containers/Admin";
import Creations from "./containers/Creations";
import Transactions from "./containers/Transactions";

const adminPaths = [
  // '/_admin/companies',
  "/_admin/branches",
  "/_admin/department",
  "/_admin/designation",
  "/_admin/employees",
  "/_admin/suppliers",
];

const creationsPaths = [
  "/creations/mainGroup",
  "/creations/product/name",
  "/creations/product/sub",
  "/creations/product/size",
  "/creations/product/sale-wastage",
  "/creations/product/party-wastage",
  "/creations/brand/creation",
  "/creations/brand/product",
  "/creations/brand/sub-product",
  "/creations/stone/group",
  "/creations/stone/name",
  "/creations/stone/size",
  "/creations/stone/color",
  "/creations/stone/clarity",
  "/creations/stone/polish",
  "/creations/stone/party-rate",
  "/creations/stone/sale-rate",
  "/creations/floor",
  "/creations/counter",
  "/creations/purity",
  "/creations/rate",
  "/creations/gst",
  "/creations/hsn",
];

const transactionsPaths = [
  "/transactions/mainGroup",
  "/transactions/product/name",
  "/transactions/product/sub",
  "/transactions/product/size",
  "/transactions/product/sale-wastage",
  "/transactions/product/party-wastage",
  "/transactions/inventory/metal-purchase",
  "/transactions/inventory/branded-purchase",
  "/transactions/inventory/gemstone-purchase",
  "/transactions/barcode/metal-lot",
  "/transactions/barcode/branded-lot",
  "/transactions/barcode/gemstone-lot",
  "/transactions/barcode/branded-barcode",
  "/transactions/barcode/gemstone-barcode",
  "/transactions/barcode/metal-barcode",
  "/transactions/barcode/delete",
  "/transactions/barcode/reprint",
];

const Main = (props) => (
  <Switch>
    <Route exact path="/" component={Dashboard} {...props} />
    <Route exact path="/dashboard" component={Dashboard} {...props} />
    <Route exact path={adminPaths} component={Admin} {...props} />
    <Route exact path={creationsPaths} component={Creations} {...props} />
    <Route exact path={transactionsPaths} component={Transactions} {...props} />
    <Redirect to="/" />
  </Switch>
);

export default Main;
