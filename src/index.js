/* import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
 */

import "bootstrap/dist/css/bootstrap.min.css"
import "./assets/css/custom-radiocheck.css";
import "./assets/css/datatables.css";
import "./assets/css/dropzone.css";
import "./assets/css/font-awesome.min.css";
import "./assets/css/sweetalert.css";
import "./assets/css/tail.select-light.css";
import "./assets/css/material-design-iconic-font.min.css";

import "jquery-ui/themes/base/all.css"
import "./assets/css/style.css";

import "jquery/src/jquery";
import "jquery-ui/ui/widgets/autocomplete"
import "bootstrap/dist/js/bootstrap.min.js";

import React, { Component } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux';
import { I18nextProvider } from "react-i18next";
import i18n from "./utils/i18n";
import App from './App';
import store from './redux/store/index'
// wrap the app in I18next Provider with the configuration loaded from i18n.js
ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <I18nextProvider i18n={i18n}>
        <App />
      </I18nextProvider>
    </Provider>
  </BrowserRouter>,
  document.getElementById("root")
);